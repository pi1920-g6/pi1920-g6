
#include <Wire.h>
#include <stdint.h>
void setup()
{

  pinMode(13, INPUT);
  Wire.begin();		  /* start i2c (as master) */
  Wire.setClock(100000L);
  Serial.begin(115200); /* start serial for output */
}

void loop()
{
  int val = LOW;  
  int flag=0;
  val = digitalRead(13);   // read the input pin
  if(val == HIGH)
  {    
    Wire.requestFrom(0x37, 97); // request 32 bytes from 0x37, do not release the SCL
    while (Wire.available())
    {
      uint8_t c = Wire.read(); // receive a byte as character
      Serial.println(c);        // print the character
    }
    flag=1;
  }
}
