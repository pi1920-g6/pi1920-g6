
#include <Wire.h>
#include <stdint.h>
void setup()
{

  pinMode(13, INPUT);
  Wire.begin();		  /* start i2c (as master) */
  Wire.setClock(100000L);
  Serial.begin(115200); /* start serial for output */
        /* send data to the device with 0x37 */
    Wire.beginTransmission(0x37);
    /* first four byte are the header */
    Wire.write(0x04); // type + program id
    Wire.write(0x10); // number of parameters to be sent
    Wire.write(0x83); // fletcher16's lsb
    Wire.write(0xAC); // fletcher16's msb
    /* sample data to be sent*/
    Wire.write(0x00); // id
    Wire.write(0x03); // data's lsb
    Wire.write(0x00); // data's  msb
  
    Wire.write(0x01); // id
    Wire.write(0x02); // data's lsb
    Wire.write(0x00); // data's  msb

    Wire.write(0x05); // id
    Wire.write(0x00); // data's lsb
    Wire.write(0x00); // data's  msb

    Wire.write(0x06); // id
    Wire.write(0x00); // data's lsb
    Wire.write(0x00); // data's  msb

    Wire.write(0x09); // id
    Wire.write(0x2F); // data's lsb
    Wire.write(0x00); // data's  msb

    Wire.write(0x0A); // id
    Wire.write(0x5A); // data's lsb
    Wire.write(0x00); // data's  msb

    Wire.write(0x0B); // id
    Wire.write(0x21); // data's lsb
    Wire.write(0x00); // data's  msb

    Wire.write(0x0C); // id
    Wire.write(0x4F); // data's lsb
    Wire.write(0x00); // data's  msb

    
    Wire.write(0x0D); // id
    Wire.write(0x2C); // data's lsb
    Wire.write(0x00); // data's  msb


    Wire.write(0x0E); // id
    Wire.write(0x2C); // data's lsb
    Wire.write(0x00); // data's  msb


    Wire.write(0x0F); // id
    Wire.write(0x63); // data's lsb
    Wire.write(0x00); // data's  msb

   
    Wire.write(0x10); // id
    Wire.write(0x24); // data's lsb
    Wire.write(0x00); // data's  msb
    
    Wire.write(0x11); // id
    Wire.write(0x58); // data's lsb
    Wire.write(0x00); // data's  msb

    
    Wire.write(0x12); // id
    Wire.write(0x75); // data's lsb
    Wire.write(0x00); // data's  msb

    
    Wire.write(0x13); // id
    Wire.write(0x00); // data's lsb
    Wire.write(0x00); // data's  msb

  
    Wire.write(0x14); // id
    Wire.write(0x1C); // data's lsb
    Wire.write(0x00); // data's  msb
    
    Wire.endTransmission();
}

void loop()
{

    


}
