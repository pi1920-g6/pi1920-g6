
#include <Wire.h>
#include <stdint.h>
void setup()
{

  pinMode(13, INPUT);
  Wire.begin();		  /* start i2c (as master) */
  Wire.setClock(100000L);
  Serial.begin(115200); /* start serial for output */

          /* send data to the device with 0x37 */
    Wire.beginTransmission(0x37);
    /* first four byte are the header */
    Wire.write(0x04); // type + program id
    Wire.write(0x0B); // number of parameters to be sent
    Wire.write(0xFF); // fletcher16's lsb
    Wire.write(0xBF); // fletcher16's msb
    /* sample data to be sent*/
    Wire.write(0x00); // id
    Wire.write(0x05); // data's lsb
    Wire.write(0x00); // data's  msb
  
    Wire.write(0x01); // id
    Wire.write(0x00); // data's lsb
    Wire.write(0x00); // data's  msb

    Wire.write(0x05); // id
    Wire.write(0x00); // data's lsb
    Wire.write(0x00); // data's  msb

    Wire.write(0x06); // id
    Wire.write(0x00); // data's lsb
    Wire.write(0x00); // data's  msb

    Wire.write(0x15); // id
    Wire.write(0x1E); // data's lsb
    Wire.write(0x00); // data's  msb

    Wire.write(0x16); // id
    Wire.write(0x0A); // data's lsb
    Wire.write(0x00); // data's  msb
    
    Wire.write(0x17); // id
    Wire.write(0x5A); // data's lsb
    Wire.write(0x00); // data's  msb

    
    Wire.write(0x18); // id
    Wire.write(0x14); // data's lsb
    Wire.write(0x00); // data's  msb

    
    Wire.write(0x19); // id
    Wire.write(0x46); // data's lsb
    Wire.write(0x00); // data's  msb

    
    Wire.write(0x1A); // id
    Wire.write(0x37); // data's lsb
    Wire.write(0x00); // data's  msb

        
    Wire.write(0x1B); // id
    Wire.write(0x0A); // data's lsb
    Wire.write(0x28); // data's  msb
  
    Wire.endTransmission();
    Serial.println("Sent parameters");        // print the character
}

void loop()
{

    


}
