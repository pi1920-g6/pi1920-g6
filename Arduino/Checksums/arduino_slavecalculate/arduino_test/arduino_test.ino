
#include <Wire.h>
#include <stdint.h>
void setup()
{

  pinMode(13, INPUT);
  Wire.begin();		  /* start i2c (as master) */
  Wire.setClock(100000L);
  Serial.begin(115200); /* start serial for output */



}

void loop()
{

          /* send data to the device with 0x37 */
    Wire.beginTransmission(0x37);
    /* first four byte are the header */
    Wire.write(0x08); // type + program id
    Wire.write(0x01); // number of parameters to be sent
    Wire.write(0xFF); // fletcher16's lsb
    Wire.write(0x11); // fletcher16's msb
    /* sample data to be sent*/
    Wire.write(0x05); // id
    Wire.write(0x01); // data's lsb
    Wire.write(0x00); // data's  msb
  
    Wire.endTransmission();
    Serial.println("Sent parameters");  

    delay(100);

     int val = LOW;  
  val = digitalRead(13);   // read the input pin
  if(val == HIGH)
  {    
    Wire.requestFrom(0x37, 97); // request 32 bytes from 0x37, do not release the SCL
    while (Wire.available())
    {
      uint8_t c = Wire.read(); // receive a byte as character
      Serial.println(c);        // print the character
    }
  }  

  delay(10000);
}
