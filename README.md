# Projecto Industrial (2019/20)
Grupo 6


Este projeto consiste no desenvolvimento de uma interface HMI com um ecrã TFT para integração em máquinas de soldadura Electrex.

A comunicação entre o circuito de controlo é realizada através do protocolo de comunicação I2C, onde o circuito de controlo da máquina é o Master e o nosso microcontrolador o Slave.
A comunicação entre o nosso microcontrolador e o display TFT Nextion é realizada através do protocolo de comunicação UART.

Principais funcionalidades:

- Permitir ao utilizador alterar o processo de soldadura.
- Permitir ao utilizador alterar o modo de soldadura.
- Permitir ao utilizador configurar os parâmetros de cada processo de soldadura.
- Sinalizar ao utilizador alertas do sistema (sobreaquecimento, VRD, soldadura ativa, etc).
- Gravar ou carregar um determinado programa, que armazena os parâmetros, o modo e o processo de soldadura (até 20 programas).
- Ao iniciar a máquina, iniciar com o seu estado anterior.
- Botão Home (Home Page) e Back(Página anterior).
- Sempre que a soldadura está ativa, o menu apresentado é o Home com valores instantâneos de corrente e tensão.
- Menus de ajuda carregando no encoder.
- Preview de um programa ao carregar nele.
- Factory reset da máquina.
- Reset para valores de fábrica de um programa.


Firmware para o microcontrolador [PIC32MX575F512H](https://www.microchip.com/wwwproducts/en/PIC32MX575F512H).

Ligações úteis:
*  [Datasheet](http://ww1.microchip.com/downloads/en/DeviceDoc/PIC32MX5XX6XX7XX_Family\)Datasheet_DS60001156K.pdf)
*  [Errata](http://ww1.microchip.com/downloads/en/DeviceDoc/PIC32MX575675695775795_Family_Silicon_Errat_DS80000480Q.pdf)