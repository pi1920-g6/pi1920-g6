# ############################################################# #
#             													#
# MAKEFILE FOR CREATING AND RUNNING UNIT TESTS, USING CPPUTEST  #
#                                                               #
# ############################################################# #
# This Makefile is made to follow the following project 		#
# structure:													#
# 																#
#	* XC32_mock													#
#	* Project.X/												#
#		+ include/												#
#			- .h files											#
#		+ source/												#
#			- .c/.cpp files										#
#		+ test/													#
#			- Makefile <-										#
#			- mainTester.cpp									#
#			- .cpp test files									#
#																#
# ############################################################# #

# Adapt the following lines to your own environment
PROJ_LOCATION = ..
CPPUTEST_HOME = /usr
CPPUTEST_LIBS = ${CPPUTEST_HOME}/lib
MOCK_LOCATION=../../XC32_mock
XC32_HOME="/mnt/c/Program Files/Microchip/xc32/v2.40"
DEVICE=32MX795F512L
# Compiler(s) related configurations
CC =gcc 
CFLAGS += -I$(PROJ_LOCATION)/include
CFLAGS += -g
CFLAGS += -Wall
CFLAGS += -Werror
CFLAGS += -D__${DEVICE}__ -D__LANGUAGE_C__ -D_STDLIB_H_ -D__SYS_L1CACHE_H -D__UNITTESTING__
CPPFLAGS += -I$(PROG_LOCATION) -I$(MOCK_LOCATION)

CXX=g++
CXXFLAGS += -I$(CPPUTEST_HOME)/include
CXXFLAGS += -I$(PROJ_LOCATION)/include
CXXFLAGS += -D__UNITTESTING__
CXXFLAGS += -g
CXXFLAGS += -Wall
CXXFLAGS += -Werror

# Names of the modules to be tested.
MODULE_NAMES +=queue
MODULE_NAMES +=controller
MODULE_NAMES +=i2c1
MODULE_NAMES +=utils
MODULE_NAMES +=homeCurrentSM
MODULE_NAMES +=HelpSM
MODULE_NAMES +=paramMMA
# CppUTest files
CPPUTEST_NAMES +=mainTester
CPPUTEST_NAMES +=queueRunner
CPPUTEST_NAMES +=controllerRunner
CPPUTEST_NAMES +=i2cRunner
CPPUTEST_NAMES +=utilsRunner
CPPUTEST_NAMES +=homeCurrentSMRunner
CPPUTEST_NAMES +=HelpSMRunner
CPPUTEST_NAMES +=paramMMARunner
# Module objects files
MODULE_OBJS = $(addprefix $(PROJ_LOCATION)/source/, $(patsubst %,%.o, $(MODULE_NAMES)))
# CppUTest object files
CPPUTEST_OBJS = $(patsubst %,%.o, $(CPPUTEST_NAMES))

.PHONY: run clean cleanall

tests: $(CPPUTEST_OBJS) $(MOCK_LOCATION)/mem_map.o $(MODULE_OBJS) 
	$(CXX) $(CXXFLAGS) -I$(PROJ_LOCATION) -I$(MOCK_LOCATION) $(LD_LIBRARIES) -L$(CPPUTEST_LIBS) $^ -lCppUTest $(CXXOPTIONS) -o tests
run: tests
	- ./tests

clean:
	- rm -f *.o
	- rm -f $(PROJ_LOCATION)/source/*.o
	- rm -f $(MOCK_LOCATION)/*.o
cleanall: clean
	- rm -f tests
