#include <CppUTest/TestHarness.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern "C"
{
#include "controller.h"
}

/**
 * \brief Test group object for the controller.
 * 
 */
TEST_GROUP(Controller)
{
	uint8_t entryID = 10;
	uint8_t entryDataLSB = 0;
	uint8_t entryDataMSB = 9;

	uint16_t entryData = ((uint16_t)entryDataMSB << 8) | entryDataLSB;

	void setup()
	{
		controller_init();
		/* Add to header... */
		controller_buffer_insert(0x02);
		controller_buffer_insert(0x01);
		controller_buffer_insert(0x16);
		controller_buffer_insert(0x35);
		/* Add to body... */
		controller_buffer_insert(entryID);
		controller_buffer_insert(entryDataLSB);
		controller_buffer_insert(entryDataMSB);
	}

	void teardown()
	{
	}
};

/**
 * \brief Tests if the data is moved from the buffer to the cache.
 * 
 */
TEST(Controller, MoveBufferToCache)
{
	controller_error_t status = controller_buffer_save();
	param_id_t paramID=TIG_ISTART;

	uint16_t dataReadFromCache;

	controller_cache_read(paramID, &dataReadFromCache);

	CHECK_EQUAL(entryData, dataReadFromCache);

	CHECK_EQUAL(CONTROLLER_SUCESS, status);
}

TEST(Controller, GetHeaderFromCache)
{
	controller_buffer_save();

	/* Initializes */
	uint8_t cacheHeader[4] = {0};

	controller_cache_get_header(cacheHeader);
	/*
	 * All parameters, so:
	 * + type = 1 (2 bits)
	 * + program_id = 1 (6 bits)
	 * -> 1st byte = 0000 0101
	 * + n_params = 32
	 * -> 2nd byte = 0002 0000
	 * + only one param is saved, therefore the cache
	 * has only everything = 0, except cache[10] = 0x0090
	 * -> 3rd & 4th byte = 96fa
	 */
	CHECK_EQUAL(5, cacheHeader[0]);
	CHECK_EQUAL(32, cacheHeader[1]);
	CHECK_EQUAL(0xfa, cacheHeader[2]);
	CHECK_EQUAL(0x96, cacheHeader[3]);
}