/**
 * \file    i2cRunner.cpp
 * 
 * \brief   This file contains tests for static functions that help
 * 			the configuration of 
 * 
 */
#include <CppUTest/TestHarness.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <algorithm>
#include <vector>
#include <iterator>

extern "C"
{
#include "i2c1.h"
}
TEST_GROUP(I2C)
{
	/* List of reserved I2C Bus Address */
	const uint8_t rsd_addr[13] =
		{
			0b00000000U, 0b00000001U, 0b00000010U, 0b00000011U,
			0b00000100U, 0b00000101U, 0b00000110U, 0b00000111U,
			0b01111000U, 0b01111100U, 0b01111101U, 0b01111110U,
			0b01111111U};

	void setup() {}

	void teardown() {}
};

/**
 * \brief Test for i2c1_is_valid_7_bit_addr(uint8_t address, bool strict)
 */
TEST(I2C, allPossible7BitAddresses)
{

	std::vector<uint8_t> invalidAddr(std::begin(rsd_addr), std::end(rsd_addr));

	for (uint8_t addr = 0; addr < UINT8_MAX; addr++)
	{
		/* if the address is invalid*/
		if ((std::find(invalidAddr.begin(), invalidAddr.end(), addr)) != invalidAddr.end())
		{
			CHECK_FALSE(i2c1_is_valid_7_bit_addr(addr));
			//CHECK_TRUE(i2c1_is_valid_7_bit_addr(addr));
		}
		else
		{
			/* else if the address is less than 7-bits */
			if (addr < 128)
			{
				CHECK_TRUE(i2c1_is_valid_7_bit_addr(addr));
			}
			else
			{
				/* else it is expected to fail if the first 7 bits are equal */
				if ((std::find(invalidAddr.begin(), invalidAddr.end(), addr & 0x7f)) != invalidAddr.end())
				{
					CHECK_FALSE(i2c1_is_valid_7_bit_addr(addr));
				}
				else
				{
					CHECK_TRUE(i2c1_is_valid_7_bit_addr(addr));
				}
			}
		}
	}
}