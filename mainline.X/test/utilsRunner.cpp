#include <CppUTest/TestHarness.h>
#include <stdint.h>
#include <cstdio>
#include <iostream>
#include <vector>
#include <numeric>

extern "C"
{
#include "utils.h"
}

TEST_GROUP(FletcherGroup){
    void setup(){}

    void teardown(){}};

/**
 * \brief A simple test against some known values.
 * 
 */
TEST(FletcherGroup, GenericValues)
{
    const uint8_t str1[6] = "abcde";
    const uint8_t str2[7] = "abcdef";
    const uint8_t str3[8] = "abcdefg";
    const uint8_t str4[9] = "abcdefgh";
    /* removing \n char */
    CHECK_EQUAL(0xC8F0, utils_fletcher16(str1, 5));
    CHECK_EQUAL(0x2057, utils_fletcher16(str2, 6));
    CHECK_EQUAL(0xDEBE, utils_fletcher16(str3, 7));
    CHECK_EQUAL(0x0627, utils_fletcher16(str4, 8));
}

/**
 * \brief A simple test against some known values.
 * 
 */
TEST(FletcherGroup, FletcherGroupBigValues)
{
    const uint8_t str1[41] = "uXGUNfKTiOfrJ5WzpKvs"
                             "00RBkK87IX7AyWNGrG1j";

    const uint8_t str2[61] = "zsN9XmNSrjK1IOfIJdj2"
                             "YyLxfb0aCsjQUOq020TOT"
                             "zGeIQmfNpEPyewfdROs";

    const uint8_t str3[201] = "adi2ZmJg6gN2Txi57juY"
                              "gF3Pi1dtEKvIHjU3BIvT"
                              "cwfzm2qj8LTkTffz0IxH"
                              "8lnAyU14QuNKc2Q9qj2p"
                              "WkvxoZtaS1eLS2YKtBJR"
                              "fVugnrnZo2eDaiH1638p"
                              "iSKvwiMBXENDNvRbMVuG"
                              "hEXxYOkqHgGdmYUXAPgj"
                              "JMAM6TAdyv2OgA6TLbzT"
                              "b8veK9Xyy7OCnNu88svT";

    const uint8_t str4[301] = "tfwAZCjB6Yh9iBqFcV1p"
                              "Sjz4j43msLqvBG6tYoI4"
                              "LU4OQOCnWnZDYpasX9mq"
                              "mnwTzuMpepYQgKqIjEQQ"
                              "Ye31jjZJzRqIkiyBVXmS"
                              "Bfby6DSE7HdmVq4UDk0u"
                              "U1UCU1HDXpwhHEYpczER"
                              "j3sNdXCKte9AUtqTKb82"
                              "efsnKImTKxNorxcbQCY4"
                              "kWhqw9YyFROr1xaYhhEH"
                              "kHLf1d4sbDWzUY2ZsASQ"
                              "hscKYeAvv4aIEP3wgnnN"
                              "1c5oXOFP814elPso9Hiv"
                              "wR51SUl0byxn9TVdu1C3"
                              "kaWdi3xu66rKIllBAkB6";
    const uint8_t str5[2001] = "ykaOUcMZ4IpIUgnlfYpk"
                               "UKtN2bt3eJSUPLWVTJh7"
                               "drMVMgINiBTsiZ2PkXt6"
                               "69Zib7m2MG7hTuyw83Vt"
                               "EoiL2bqsDPIfVdtRNZBV"
                               "7F6UuJ9HDnxhdNCJbikB"
                               "eENbfAg2jSZ4KpFhaBdN"
                               "6sMaujAq4kdQduqA4uMA"
                               "MMKInaXhNoi9y6f9LrN4"
                               "ExNtoMF23GhRjfqyYrfV"
                               "0sZ3UJLUFiyKRZ4Ja1u6"
                               "RcZzH9LUTlcRSIgkfbzf"
                               "QSn0BMUGg1Kk2peQe6bh"
                               "cPAXJePFtgse3N9pJ6OP"
                               "yIgjmU5qvjyGk952l2H9"
                               "7N96VTfoqHdsAJU78AyX"
                               "U2wblyXmCzhLpoVUcNZA"
                               "iNzjX2D0mF55KdUTP7Be"
                               "OvRx7r5YFPBZugWftdER"
                               "qpxxw2gMB8DYD0zVfJcm"
                               "gjXXQa0VxN08ZnqoJP1k"
                               "QopaNdOwohc78WmyjzNv"
                               "uQoQhjF8wPjhHhXS3RNw"
                               "TrbZIr6ZEwvTmYYLZF3I"
                               "ChgiFS4F9159Kbw7Zgwe"
                               "Z4kWIRYNn2Hy5ZTvR7eu"
                               "Ow2wyqrZoqHv1YtYwarw"
                               "bGThu3x8vcYiSR06kzhS"
                               "4ZTJG49Q42GDqwBnNhA3"
                               "Z0tcUbVMuNnfjGoGyRM4"
                               "fB3pzax6kc7Ns42Y4Sft"
                               "0Yhpnc6M2whChMhzAYtM"
                               "RCNptwfpsVttAkH1QRoz"
                               "eK2zJzXk6uwoqwFCblHq"
                               "DqtcWqo4DfNbiwuc8K3q"
                               "QPBKA73jmKfPefK3XEwG"
                               "gvnI0d6tejlLbhGCq61T"
                               "3O0rpPAVHMQYjQFGCH3p"
                               "H3yVA1EXLqnZGiGszCPY"
                               "dSCRjBcTBXdLddhC3pKB"
                               "bhep0LWoAxRKYcoNrKHZ"
                               "JP6Ym0IZGzU25SssbU6F"
                               "Vfw5pbW56yncFztRbBv5"
                               "YpFRwH9InUgPMwnTCW29"
                               "QUOExrGu3uY6u8wnACbZ"
                               "r80MDpFeuriXSdZ5lzwt"
                               "RyTNc77GD53b7CONqJDM"
                               "GMbBIdFyMYhen3DMRzvD"
                               "hxqllzhlsc22wFyLa9i4"
                               "jWJxXmhDQVFcEpM95Rsk"
                               "lylwhnocdMDsHb56hoIM"
                               "Q3HNlXt5kH3tAbqREdkp"
                               "1byXB4zQrSPVLPaKChmX"
                               "yS42DBa4EC2BQZ5GvRRa"
                               "CxXFkeT3eviv82EYcv4j"
                               "1ZXEcYZ4m3vakBlBjvAe"
                               "07iuyVRnM8wNyJrpWwJG"
                               "2e7IcQlKj1T1W5xjMYCp"
                               "m0d0YfBPYG8Piy3f0z2U"
                               "CYwpSE1dGOodP7kRDR5C"
                               "SY7YmB96Rk1zF7yu6dNT"
                               "xc3k9obgyooFESgOKWQ3"
                               "ToVq3YIyHg1DHlz4SsfX"
                               "Ct00nIcXfdq2mrPGOw3Z"
                               "iHTZuCbqOPj4tZZlvzpH"
                               "A8FGZec6cXqiv5d2NbcU"
                               "m6GgdibVs9Ur8hmO6VpX"
                               "UTdczMtQSsUoM6KqUY2I"
                               "GR6EMSYrqkh38Hz3PDLj"
                               "9dH0bo6Wm7c6snye5619"
                               "sM1oZ60QiZR0a8n2DFVN"
                               "hGxK25vM2DSHgVzmAcEo"
                               "jganDNNAbFlUsaM4XCx5"
                               "vn3cDk9IzqIp4tSUevNR"
                               "v8Fb6j9zIH2y4trdi5ZK"
                               "3EGNZA5zKd8Q3N11ocPC"
                               "1u11NJJ3UPztG76RqCLD"
                               "7NzhBw1mKzA2xwHE3usT"
                               "7Kk6HTTCb3mo3LyffpSc"
                               "g248m1Sejs43GeGuL7V5"
                               "gFpbhPjRN8VhnhBui2Q8"
                               "xVv3lscV9sD0koBKsPZW"
                               "3Sqe6PWsJhQEmmssEVJJ"
                               "NwCY2A8GfCoH2ZxcIC6o"
                               "910KyRq7QL7vQASe761A"
                               "ZNfCDsOCaR4nLwHCFlIs"
                               "mXhkqW4GeAvyo5ZSVEQT"
                               "j0i7EfSRgfXb3QUQE12m"
                               "okMIW4NDe5KHztGgosOE"
                               "5aJqcqGQer8hbJ6jOrMH"
                               "8rvuVGpBgxhHOjVUdaQo"
                               "GZLHgR1HjBNMMOODDrIe"
                               "HBt4Bhf1CzuqOt1irSsO"
                               "CSkEzcA8L9F9MVmGvbiq"
                               "XHQ8PPUOSqhzFUhJl2Ga"
                               "yZKFf8gjxt1d6RnQM0T6"
                               "WEUaBsUcyHJ9Wisx1udh"
                               "drNkeHTSiXbLyiDibAza"
                               "CwdsNYI7eHTrJhrj0Ubj"
                               "bSlWlfIcx8t5rtPQ5wej";
    /* removing \n char */
    CHECK_EQUAL(0xED38, utils_fletcher16(str1,   40));
    CHECK_EQUAL(0x6EEE, utils_fletcher16(str2,   60));
    CHECK_EQUAL(0x8DB7, utils_fletcher16(str3,  200));
    CHECK_EQUAL(0x646D, utils_fletcher16(str4,  300));
    CHECK_EQUAL(0xD62D, utils_fletcher16(str5, 2000));
}


TEST_GROUP(IntegerToStringGroup)
{
    void setup()
    {
    }

    void teardown(){}};

/**
 * \brief Testing from 0 to 999.
 * 
 */
TEST(IntegerToStringGroup, testExpectedValues)
{
    /* vector with 999 ints */
    std::vector<uint16_t> intsVector(1000); 
    /* fill from 0 to 999 */
    std::iota (std::begin(intsVector), std::end(intsVector), 0);

    int numberOfDigits = 3;

    for(std::vector<int>::size_type i = 0; i != intsVector.size(); i++)
    {
        char actualString[numberOfDigits];
        char expectedString[numberOfDigits];

        /* convert to string using function */
        utils_int_to_str(actualString, intsVector[i], numberOfDigits);
        /* convert using stdio */
        std::sprintf(expectedString, "%03d", intsVector[i]);

        STRCMP_EQUAL(expectedString, actualString);
    }

}

/**
 * \brief Testing from 0 to 999.
 * 
 */
TEST(IntegerToStringGroup, testUInt16Values)
{
    /* vector with UINT16_MAX ints */
    std::vector<uint16_t> intsVector(UINT16_MAX); 
    /* fill from 0 to UINT16_MAX */
    std::iota (std::begin(intsVector), std::end(intsVector), 0);

    int numberOfDigits = 5;

    for(std::vector<int>::size_type i = 0; i != intsVector.size(); i++)
    {
        char actualString[numberOfDigits];
        char expectedString[numberOfDigits];

        /* convert to string using function */
        utils_int_to_str(actualString, intsVector[i], numberOfDigits);
        /* convert using stdio */
        std::sprintf(expectedString, "%05d", intsVector[i]);

        STRCMP_EQUAL(expectedString, actualString);
    }

}