#include <CppUTest/TestHarness.h>
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>

extern "C"
{
#include "SM_structures.h"
#include "HelpSM.h"
}

/**
 * \brief Test related to the HelpSM functions
 */
TEST_GROUP(HelpSM)
{
	sm_t state;

	void setup() 
	{
		state.c_state=HELP;
		state.p_state=SELECTION_MENU;
	}

	void teardown()
	{
	}
};

/**
 * \brief Help Menu - Test Home button
 */
TEST(HelpSM, Home_HelpSM)
{
	var_interrupt = HOME;
	state = help_menu(state);
	CHECK_EQUAL(state.c_state, HOME_CHANGE_CURRENT);
}


/**
 * \brief Help Menu - Test Previous button
 */
TEST(HelpSM, Back_HelpSM)
{
	var_interrupt = BACK;
	state = help_menu(state);
	CHECK_EQUAL(state.c_state, SELECTION_MENU);
	CHECK_EQUAL(FirstLoop, true);
}


/**
 * \brief Help Menu - Test Encoder button
 */
TEST(HelpSM, EncBut_HelpSM)
{
	var_interrupt = ENC_BUTTON;
	state = help_menu(state);
	CHECK_EQUAL(state.c_state, SELECTION_MENU);
	CHECK_EQUAL(FirstLoop, true);
}