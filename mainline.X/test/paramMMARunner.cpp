#include <CppUTest/TestHarness.h>
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>

extern "C"
{
#include "defines.h"
#include "SM_structures.h"
#include "paramMMA.h"
#include "controller.h"
}

/**
 * \brief Test related to the homeCurrentSM functions
 */
TEST_GROUP(paramMMA)
{
	sm_t state;
	param_id_t parameter;
	

	void setup() 
	{
		state.c_state=MMA_PG1;
		state.p_state=HOME_CHANGE_CURRENT;
		MMA_buttons=HOTSTART_BUTTON;
	}

	void teardown()
	{
	}
};

/**
 * 				MMA 
 */
/**
 * \brief MMA Parameters - Test Home button
 */
TEST(paramMMA, Home_MMA)
{
	state.c_state=MMA_PG1;
	state.p_state=HOME_CHANGE_CURRENT;
	var_interrupt = HOME;
	state = MMA_parameters_page(state);
	CHECK_EQUAL(state.c_state, HOME_CHANGE_CURRENT);
}

/**
 * \brief MMA Parameters - Test Previous button
 */
TEST(paramMMA, Previous_MMA)
{
	state.c_state=MMA_PG1;
	state.p_state=HOME_CHANGE_CURRENT;
	var_interrupt = BACK;
	MMA_buttons=INOMINAL_BUTTON;
	state = MMA_parameters_page(state);
	CHECK_EQUAL(state.c_state, HOME_CHANGE_CURRENT);
}

/**
 * \brief MMA Parameters - Test Previous button
 */
TEST(paramMMA, Previous_MMA_Value)
{
	uint16_t Inom;
	parameter=MMA_INOMINAL;
    controller_cache_write((int)parameter,20);
	
	MMAINominal=30;
	state.c_state=MMA_PG1;
	state.p_state=HOME_CHANGE_CURRENT;
	var_interrupt = BACK;
	MMA_buttons=INOMINALVALUE_BUTTON;
	state = MMA_parameters_page(state);
	CHECK_EQUAL(state.c_state, MMA_PG1);
	
	parameter=MMA_INOMINAL;
    controller_cache_read(parameter,&Inom);
	CHECK_EQUAL(MMAINominal, 20);
}

/**
 * \brief MMA Parameters - Test Encoder Right
 */
TEST(paramMMA, Increment_MMA)
{
	MMAINominal=30;
	state.c_state=MMA_PG1;
	state.p_state=HOME_CHANGE_CURRENT;
	var_interrupt = ENC_RIGHT;
	MMA_buttons=HOTSTARTTIME_BUTTON;
	state = MMA_parameters_page(state);
	CHECK_EQUAL(MMA_buttons, INOMINAL_BUTTON);
	
	
	var_interrupt = ENC_RIGHT;
	MMA_buttons=INOMINALVALUE_BUTTON;
	state = MMA_parameters_page(state);
	CHECK_EQUAL(MMAINominal, 31);
	
	var_interrupt = ENC_RIGHT;
	MMA_buttons=INOMINALVALUE_BUTTON;
	state = MMA_parameters_page(state);
	CHECK_EQUAL(MMAINominal, 32);
	
	var_interrupt = ENC_RIGHT;
	MMAINominal=220;
	MMA_buttons=INOMINALVALUE_BUTTON;
	state = MMA_parameters_page(state);
	CHECK_EQUAL(MMAINominal, 220);
}



/**
 * \brief MMA Parameters - Test Encoder Left
 */
TEST(paramMMA, Decrement_MMA)
{
	MMAHotStartTime=15;
	state.c_state=MMA_PG1;
	state.p_state=HOME_CHANGE_CURRENT;
	var_interrupt = ENC_LEFT;
	MMA_buttons=ARCFORCE_BUTTON;
	state = MMA_parameters_page(state);
	CHECK_EQUAL(MMA_buttons, INOMINAL_BUTTON);
	
	
	var_interrupt = ENC_LEFT;
	MMA_buttons=HOTSTARTTIMEVALUE_BUTTON;
	state = MMA_parameters_page(state);
	CHECK_EQUAL(MMAHotStartTime, 14);
	
	var_interrupt = ENC_LEFT;
	MMA_buttons=HOTSTARTTIMEVALUE_BUTTON;
	state = MMA_parameters_page(state);
	CHECK_EQUAL(MMAHotStartTime, 13);
	
	var_interrupt = ENC_LEFT;
	MMAHotStartTime=1;
	MMA_buttons=HOTSTARTTIMEVALUE_BUTTON;
	state = MMA_parameters_page(state);
	CHECK_EQUAL(MMAHotStartTime, 1);
}


/**
 * \brief MMA Parameters - Test MMA HOT START TIME
 */
TEST(paramMMA, HotStartTime_MMA)
{
	MMAHotStartTime=15;
	var_interrupt = ENC_LEFT;
	MMA_buttons=HOTSTARTTIMEVALUE_BUTTON;
	state = MMA_parameters_page(state);
	CHECK_EQUAL(MMAHotStartTime, 14);
	
	MMAHotStartTime=15;
	var_interrupt = ENC_RIGHT;
	MMA_buttons=HOTSTARTTIMEVALUE_BUTTON;
	state = MMA_parameters_page(state);
	CHECK_EQUAL(MMAHotStartTime, 16);
	

	MMAHotStartTime=1;
	var_interrupt = ENC_LEFT;
	MMA_buttons=HOTSTARTTIMEVALUE_BUTTON;
	state = MMA_parameters_page(state);
	CHECK_EQUAL(MMAHotStartTime, 1);
	
	MMAHotStartTime=20;
	var_interrupt = ENC_RIGHT;
	MMA_buttons=HOTSTARTTIMEVALUE_BUTTON;
	state = MMA_parameters_page(state);
	CHECK_EQUAL(MMAHotStartTime, 20);
}



/**
 * \brief MMA Parameters - Test MMA HOT START
 */
TEST(paramMMA, HotStart_MMA)
{
	MMAHotStart=20;
	var_interrupt = ENC_LEFT;
	MMA_buttons=HOTSTARTVALUE_BUTTON;
	state = MMA_parameters_page(state);
	CHECK_EQUAL(MMAHotStart, 19);
	
	MMAHotStart=20;
	var_interrupt = ENC_RIGHT;
	MMA_buttons=HOTSTARTVALUE_BUTTON;
	state = MMA_parameters_page(state);
	CHECK_EQUAL(MMAHotStart, 21);
	
	MMAHotStart=100;
	var_interrupt = ENC_RIGHT;
	MMA_buttons=HOTSTARTVALUE_BUTTON;
	state = MMA_parameters_page(state);
	CHECK_EQUAL(MMAHotStart, 100);
	
	MMAHotStart=10;
	var_interrupt = ENC_LEFT;
	MMA_buttons=HOTSTARTVALUE_BUTTON;
	state = MMA_parameters_page(state);
	CHECK_EQUAL(MMAHotStart, 10);
	
}


/**
 * \brief MMA Parameters - Test NOMINAL CURRENT
 */
TEST(paramMMA, INO_MMA)
{
	MMAINominal=20;
	var_interrupt = ENC_LEFT;
	MMA_buttons=INOMINALVALUE_BUTTON;
	state = MMA_parameters_page(state);
	CHECK_EQUAL(MMAINominal, 19);
	
	MMAINominal=20;
	var_interrupt = ENC_RIGHT;
	MMA_buttons=INOMINALVALUE_BUTTON;
	state = MMA_parameters_page(state);
	CHECK_EQUAL(MMAINominal, 21);
	
	MMAINominal=220;
	var_interrupt = ENC_RIGHT;
	MMA_buttons=INOMINALVALUE_BUTTON;
	state = MMA_parameters_page(state);
	CHECK_EQUAL(MMAINominal, 220);
	
	MMAINominal=10;
	var_interrupt = ENC_LEFT;
	MMA_buttons=INOMINALVALUE_BUTTON;
	state = MMA_parameters_page(state);
	CHECK_EQUAL(MMAINominal, 10);
	
}


/**
 * \brief MMA Parameters - Test Arc Force
 */
TEST(paramMMA, ArcForce_MMA)
{
	MMAArcForce=25;
	var_interrupt = ENC_LEFT;
	MMA_buttons=ARCFORCEVALUE_BUTTON;
	state = MMA_parameters_page(state);
	CHECK_EQUAL(MMAArcForce, 24);
	
	MMAArcForce=25;
	var_interrupt = ENC_RIGHT;
	MMA_buttons=ARCFORCEVALUE_BUTTON;
	state = MMA_parameters_page(state);
	CHECK_EQUAL(MMAArcForce, 26);
	
	MMAArcForce=50;
	var_interrupt = ENC_RIGHT;
	MMA_buttons=ARCFORCEVALUE_BUTTON;
	state = MMA_parameters_page(state);
	CHECK_EQUAL(MMAArcForce, 50);
	
	MMAArcForce=0;
	var_interrupt = ENC_LEFT;
	MMA_buttons=ARCFORCEVALUE_BUTTON;
	state = MMA_parameters_page(state);
	CHECK_EQUAL(MMAArcForce, 0);
	
}



/**
 * \brief MMA Parameters - Test NoneEvent
 */
TEST(paramMMA, None_MMA)
{
	var_interrupt = NONE;
	state = MMA_parameters_page(state);
	CHECK_EQUAL(FirstLoop, false);
	CHECK_EQUAL(MMA_buttons, HOTSTART_BUTTON);
	CHECK_EQUAL(var_interrupt, NONE);

	
}





/**
 * 				MMA PULSADO - PG1
 */
/**
 * \brief MMA PULSE Parameters - Test Home button
 */
TEST(paramMMA, Home_MMA_Pulse_P1)
{
	state.c_state=MMA_PULSE_PG1;
	state.p_state=HOME_CHANGE_CURRENT;
	var_interrupt = HOME;
	state = MMA_PULSE_parameters_page1(state);
	CHECK_EQUAL(state.c_state, HOME_CHANGE_CURRENT);
}

/**
 * \brief MMA PULSE Parameters - Test Previous button
 */
TEST(paramMMA, Previous_MMA_Pulse_P1)
{
	state.c_state=MMA_PULSE_PG1;
	state.p_state=HOME_CHANGE_CURRENT;
	var_interrupt = BACK;
	MMA_Pulse_Buttons1=PULSE_INOMINAL_BUTTON;
	state = MMA_PULSE_parameters_page1(state);
	CHECK_EQUAL(state.c_state, HOME_CHANGE_CURRENT);
}

/**
 * \brief MMA PULSE Parameters - Test Previous button
 */
TEST(paramMMA, Previous_MMA_Value_Pulse_P1)
{
	uint16_t Inom;
	parameter=MMA_INOMINAL;
    controller_cache_write((int)parameter,20);
	
	MMAINominal=30;
	state.c_state=MMA_PULSE_PG1;
	state.p_state=HOME_CHANGE_CURRENT;
	var_interrupt = BACK;
	MMA_Pulse_Buttons1=PULSE_INOMINALVALUE_BUTTON;
	state = MMA_PULSE_parameters_page1(state);
	CHECK_EQUAL(state.c_state, MMA_PULSE_PG1);
	
	parameter=MMA_INOMINAL;
    controller_cache_read(parameter,&Inom);
	CHECK_EQUAL(MMAINominal, 20);
}

/**
 * \brief MMA PULSE Parameters - Test Encoder Right
 */
TEST(paramMMA, Increment_MMA_Pulse_P1)
{
	MMAINominal=30;
	
	
	state.c_state=MMA_PULSE_PG1;
	state.p_state=HOME_CHANGE_CURRENT;
	
	
	var_interrupt = ENC_RIGHT;
	MMA_Pulse_Buttons1=PULSE_HOTSTARTTIME_BUTTON;
	state = MMA_PULSE_parameters_page1(state);
	CHECK_EQUAL(MMA_Pulse_Buttons1, PULSE_INOMINAL_BUTTON);
	
	var_interrupt = ENC_RIGHT;
	MMA_Pulse_Buttons1=PULSE_INOMINALVALUE_BUTTON;
	state = MMA_PULSE_parameters_page1(state);
	CHECK_EQUAL(MMAINominal, 31);
	
	var_interrupt = ENC_RIGHT;
	MMA_Pulse_Buttons1=PULSE_INOMINALVALUE_BUTTON;
	state = MMA_PULSE_parameters_page1(state);
	CHECK_EQUAL(MMAINominal, 32);
	
	var_interrupt = ENC_RIGHT;
	MMAINominal=220;
	MMA_Pulse_Buttons1=PULSE_INOMINALVALUE_BUTTON;
	state = MMA_PULSE_parameters_page1(state);
	CHECK_EQUAL(MMAINominal, 220);
}



/**
 * \brief MMA PULSE Parameters - Test Encoder Left
 */
TEST(paramMMA, Decrement_MMA_Pulse_P1)
{
	MMAHotStartTime=15;
	state.c_state=MMA_PULSE_PG1;
	state.p_state=HOME_CHANGE_CURRENT;
	
	var_interrupt = ENC_LEFT;
	MMA_Pulse_Buttons1=PULSE_INOMINAL_BUTTON;
	state = MMA_PULSE_parameters_page1(state);
	CHECK_EQUAL(MMA_Pulse_Buttons1, HOTSTARTTIME_BUTTON);
	
	
	var_interrupt = ENC_LEFT;
	MMA_Pulse_Buttons1=PULSE_HOTSTARTTIMEVALUE_BUTTON;
	state = MMA_PULSE_parameters_page1(state);
	CHECK_EQUAL(MMAHotStartTime, 14);
	
	var_interrupt = ENC_LEFT;
	MMA_Pulse_Buttons1=PULSE_HOTSTARTTIMEVALUE_BUTTON;
	state = MMA_PULSE_parameters_page1(state);
	CHECK_EQUAL(MMAHotStartTime, 13);
	
	var_interrupt = ENC_LEFT;
	MMAHotStartTime=1;
	MMA_Pulse_Buttons1=PULSE_HOTSTARTTIMEVALUE_BUTTON;
	state = MMA_PULSE_parameters_page1(state);
	CHECK_EQUAL(MMAHotStartTime, 1);
}

/**
 * \brief MMA PULSE Parameters - Test MMA HOT START TIME
 */
TEST(paramMMA, HotStartTime_MMA_Pulse_P1)
{
	MMAHotStartTime=15;
	var_interrupt = ENC_LEFT;
	MMA_Pulse_Buttons1=PULSE_HOTSTARTTIMEVALUE_BUTTON;
	state = MMA_PULSE_parameters_page1(state);
	CHECK_EQUAL(MMAHotStartTime, 14);
	
	MMAHotStartTime=15;
	var_interrupt = ENC_RIGHT;
	MMA_Pulse_Buttons1=PULSE_HOTSTARTTIMEVALUE_BUTTON;
	state = MMA_PULSE_parameters_page1(state);
	CHECK_EQUAL(MMAHotStartTime, 16);
	

	MMAHotStartTime=1;
	var_interrupt = ENC_LEFT;
	MMA_Pulse_Buttons1=PULSE_HOTSTARTTIMEVALUE_BUTTON;
	state = MMA_PULSE_parameters_page1(state);
	CHECK_EQUAL(MMAHotStartTime, 1);
	
	MMAHotStartTime=20;
	var_interrupt = ENC_RIGHT;
	MMA_Pulse_Buttons1=PULSE_HOTSTARTTIMEVALUE_BUTTON;
	state = MMA_PULSE_parameters_page1(state);
	CHECK_EQUAL(MMAHotStartTime, 20);
}


/**
 * \brief MMA PULSE Parameters - Test MMA HOT START
 */
TEST(paramMMA, HotStart_MMA_Pulse_P1)
{
	MMAHotStart=20;
	var_interrupt = ENC_LEFT;
	MMA_Pulse_Buttons1=PULSE_HOTSTARTVALUE_BUTTON;
	state = MMA_PULSE_parameters_page1(state);
	CHECK_EQUAL(MMAHotStart, 19);
	
	MMAHotStart=20;
	var_interrupt = ENC_RIGHT;
	MMA_Pulse_Buttons1=PULSE_HOTSTARTVALUE_BUTTON;
	state = MMA_PULSE_parameters_page1(state);
	CHECK_EQUAL(MMAHotStart, 21);
	
	MMAHotStart=100;
	var_interrupt = ENC_RIGHT;
	MMA_Pulse_Buttons1=PULSE_HOTSTARTVALUE_BUTTON;
	state = MMA_PULSE_parameters_page1(state);
	CHECK_EQUAL(MMAHotStart, 100);
	
	MMAHotStart=10;
	var_interrupt = ENC_LEFT;
	MMA_Pulse_Buttons1=PULSE_HOTSTARTVALUE_BUTTON;
	state = MMA_PULSE_parameters_page1(state);
	CHECK_EQUAL(MMAHotStart, 10);
	
}

/**
 * \brief MMA PULSE Parameters - Test NOMINAL CURRENT
 */
TEST(paramMMA, INO_MMA_Pulse_P1)
{
	MMAINominal=20;
	var_interrupt = ENC_LEFT;
	MMA_Pulse_Buttons1=PULSE_INOMINALVALUE_BUTTON;
	state = MMA_PULSE_parameters_page1(state);
	CHECK_EQUAL(MMAINominal, 19);
	
	MMAINominal=20;
	var_interrupt = ENC_RIGHT;
	MMA_Pulse_Buttons1=PULSE_INOMINALVALUE_BUTTON;
	state = MMA_PULSE_parameters_page1(state);
	CHECK_EQUAL(MMAINominal, 21);
	
	MMAINominal=220;
	var_interrupt = ENC_RIGHT;
	MMA_Pulse_Buttons1=PULSE_INOMINALVALUE_BUTTON;
	state = MMA_PULSE_parameters_page1(state);
	CHECK_EQUAL(MMAINominal, 220);
	
	MMAINominal=10;
	var_interrupt = ENC_LEFT;
	MMA_Pulse_Buttons1=PULSE_INOMINALVALUE_BUTTON;
	state = MMA_PULSE_parameters_page1(state);
	CHECK_EQUAL(MMAINominal, 10);
	
}

/**
 * \brief MMA Pulse Parameters - Test Base Current
 */
TEST(paramMMA, BaseCurrent_MMA_Pulse_P1)
{
	MMAIBase=75;
	var_interrupt = ENC_LEFT;
	MMA_Pulse_Buttons1=PULSE_BASECURRENTVALUE_BUTTON;
	state = MMA_PULSE_parameters_page1(state);
	CHECK_EQUAL(MMAIBase, 74);
	
	MMAIBase=75;
	var_interrupt = ENC_RIGHT;
	MMA_Pulse_Buttons1=PULSE_BASECURRENTVALUE_BUTTON;
	state = MMA_PULSE_parameters_page1(state);
	CHECK_EQUAL(MMAIBase, 76);
	
	MMAIBase=95;
	var_interrupt = ENC_RIGHT;
	MMA_Pulse_Buttons1=PULSE_BASECURRENTVALUE_BUTTON;
	state = MMA_PULSE_parameters_page1(state);
	CHECK_EQUAL(MMAIBase, 95);
	
	MMAIBase=50;
	var_interrupt = ENC_LEFT;
	MMA_Pulse_Buttons1=PULSE_BASECURRENTVALUE_BUTTON;
	state = MMA_PULSE_parameters_page1(state);
	CHECK_EQUAL(MMAIBase, 50);
	
}



/**
 * \brief MMA Pulse Parameters - Test NoneEvent
 */
TEST(paramMMA, None_MMA_Pulse_P1)
{
	var_interrupt = NONE;
	state = MMA_PULSE_parameters_page1(state);
	CHECK_EQUAL(FirstLoop, false);
	CHECK_EQUAL(MMA_Pulse_Buttons1, PULSE_HOTSTART_BUTTON);
	CHECK_EQUAL(var_interrupt, NONE);
}




/**
 * 				MMA PULSADO - PG2
 */
/**
 * \brief MMA PULSE Parameters - Test Home button
 */
TEST(paramMMA, Home_MMA_Pulse_P2)
{
	state.c_state=MMA_PULSE_PG2;
	state.p_state=HOME_CHANGE_CURRENT;
	var_interrupt = HOME;
	state = MMA_PULSE_parameters_page2(state);
	CHECK_EQUAL(state.c_state, HOME_CHANGE_CURRENT);
}

/**
 * \brief MMA PULSE Parameters - Test Previous button
 */
TEST(paramMMA, Previous_MMA_Pulse_P2)
{
	state.c_state=MMA_PULSE_PG2;
	state.p_state=HOME_CHANGE_CURRENT;
	var_interrupt = BACK;
	MMA_Pulse_Buttons2=PULSE_WIDTH_BUTTON;
	state = MMA_PULSE_parameters_page2(state);
	CHECK_EQUAL(state.c_state, MMA_PULSE_PG1);
}


/**
 * \brief MMA PULSE Parameters - Test Previous button
 */
TEST(paramMMA, Previous_MMA_Value_Pulse_P2)
{
	uint16_t PulseW;
	parameter=MMA_PULSE_WIDTH;
    controller_cache_write((int)parameter,20);
	
	PulseW=30;
	state.c_state=MMA_PULSE_PG2;
	state.p_state=HOME_CHANGE_CURRENT;
	var_interrupt = BACK;
	MMA_Pulse_Buttons2=PULSE_WIDTHVALUE_BUTTON;
	state = MMA_PULSE_parameters_page2(state);
	CHECK_EQUAL(state.c_state, MMA_PULSE_PG2);
	
	parameter=MMA_PULSE_WIDTH;
    controller_cache_read(parameter,&PulseW);
	CHECK_EQUAL(PulseW, 20);
}

/**
 * \brief MMA PULSE Parameters - Test Encoder Right
 */
TEST(paramMMA, Increment_MMA_Pulse_P2)
{
	MMAWidth=30;
	
	
	state.c_state=MMA_PULSE_PG2;
	state.p_state=HOME_CHANGE_CURRENT;
	
	
	var_interrupt = ENC_RIGHT;
	MMA_Pulse_Buttons2=PULSE_WIDTH_BUTTON;
	state = MMA_PULSE_parameters_page2(state);
	CHECK_EQUAL(MMA_Pulse_Buttons2, PULSE_FREQ_BUTTON);
	
	var_interrupt = ENC_RIGHT;
	MMA_Pulse_Buttons2=PULSE_WIDTHVALUE_BUTTON;
	state = MMA_PULSE_parameters_page2(state);
	CHECK_EQUAL(MMAWidth, 31);
	
	var_interrupt = ENC_RIGHT;
	MMA_Pulse_Buttons2=PULSE_WIDTHVALUE_BUTTON;
	state = MMA_PULSE_parameters_page2(state);
	CHECK_EQUAL(MMAWidth, 32);
	
	MMAWidth=90;
	var_interrupt = ENC_RIGHT;
	MMA_Pulse_Buttons2=PULSE_WIDTHVALUE_BUTTON;
	state = MMA_PULSE_parameters_page2(state);
	CHECK_EQUAL(MMAWidth, 90);

}


/**
 * \brief MMA PULSE Parameters - Test Encoder Left
 */
TEST(paramMMA, Decrement_MMA_Pulse_P2)
{
	MMAWidth=30;
	
	
	state.c_state=MMA_PULSE_PG2;
	state.p_state=HOME_CHANGE_CURRENT;
	
	
	var_interrupt = ENC_LEFT;
	MMA_Pulse_Buttons2=PULSE_FREQ_BUTTON;
	state = MMA_PULSE_parameters_page2(state);
	CHECK_EQUAL(MMA_Pulse_Buttons2, PULSE_WIDTH_BUTTON);
	
	var_interrupt = ENC_LEFT;
	MMA_Pulse_Buttons2=PULSE_WIDTHVALUE_BUTTON;
	state = MMA_PULSE_parameters_page2(state);
	CHECK_EQUAL(MMAWidth, 29);
	
	var_interrupt = ENC_LEFT;
	MMA_Pulse_Buttons2=PULSE_WIDTHVALUE_BUTTON;
	state = MMA_PULSE_parameters_page2(state);
	CHECK_EQUAL(MMAWidth, 28);
	
	MMAWidth=10;
	var_interrupt = ENC_LEFT;
	MMA_Pulse_Buttons2=PULSE_WIDTHVALUE_BUTTON;
	state = MMA_PULSE_parameters_page2(state);
	CHECK_EQUAL(MMAWidth, 10);
	

}

/**
 * \brief MMA PULSE Parameters - Test MMA Width
 */
TEST(paramMMA, Width_MMA_Pulse_P2)
{
	MMAWidth=45;
	var_interrupt = ENC_LEFT;
	MMA_Pulse_Buttons2=PULSE_WIDTHVALUE_BUTTON;
	state = MMA_PULSE_parameters_page2(state);
	CHECK_EQUAL(MMAWidth, 44);
	
	MMAWidth=45;
	var_interrupt = ENC_RIGHT;
	MMA_Pulse_Buttons2=PULSE_WIDTHVALUE_BUTTON;
	state = MMA_PULSE_parameters_page2(state);
	CHECK_EQUAL(MMAWidth, 46);
	

	MMAWidth=10;
	var_interrupt = ENC_LEFT;
	MMA_Pulse_Buttons2=PULSE_WIDTHVALUE_BUTTON;
	state = MMA_PULSE_parameters_page2(state);
	CHECK_EQUAL(MMAWidth, 10);
	
	MMAWidth=90;
	var_interrupt = ENC_RIGHT;
	MMA_Pulse_Buttons2=PULSE_WIDTHVALUE_BUTTON;
	state = MMA_PULSE_parameters_page2(state);
	CHECK_EQUAL(MMAWidth, 90);
}
/**
 * \brief MMA PULSE Parameters - Test MMA Pulse Freq
 */
TEST(paramMMA, PulseFreq_MMA_Pulse_P2)
{
	MMAPFreq=50;
	var_interrupt = ENC_RIGHT;
	MMA_Pulse_Buttons2=PULSE_FREQVALUE_BUTTON;
	state = MMA_PULSE_parameters_page2(state);
	CHECK_EQUAL(MMAPFreq, 51);
	
	MMAPFreq=50;
	var_interrupt = ENC_LEFT;
	MMA_Pulse_Buttons2=PULSE_FREQVALUE_BUTTON;
	state = MMA_PULSE_parameters_page2(state);
	CHECK_EQUAL(MMAPFreq, 49);
	
	MMAPFreq=5;
	var_interrupt = ENC_LEFT;
	MMA_Pulse_Buttons2=PULSE_FREQVALUE_BUTTON;
	state = MMA_PULSE_parameters_page2(state);
	CHECK_EQUAL(MMAPFreq, 5);
	
	MMAPFreq=100;
	var_interrupt = ENC_RIGHT;
	MMA_Pulse_Buttons2=PULSE_FREQVALUE_BUTTON;
	state = MMA_PULSE_parameters_page2(state);
	CHECK_EQUAL(MMAPFreq, 100);
	
}
/**
 * \brief MMA Pulse Parameters - Test NoneEvent
 */
TEST(paramMMA, None_MMA_Pulse_P2)
{
	var_interrupt = NONE;
	state = MMA_PULSE_parameters_page2(state);
	CHECK_EQUAL(FirstLoop, false);
	CHECK_EQUAL(MMA_Pulse_Buttons2, PULSE_WIDTH_BUTTON);
	CHECK_EQUAL(var_interrupt, NONE);
}