# Unit tests using CppUTest.
http://cpputest.github.io/


## Installation guide
1. Enable Windows Subsystem for Linux by opening PowerShell as Administrator and running:
```console
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux
```
2. Restart your computer when prompted.

3. Install Ubuntu:
https://www.microsoft.com/store/productId/9NBLGGH4MSV6

4. Install required development tools:
```console
$ sudo apt update
$ sudo apt install build-essential doxygen cpputest
```