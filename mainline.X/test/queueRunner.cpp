#include <CppUTest/TestHarness.h>
#include <stdint.h>
#include <stdio.h>

extern "C"
{
#include "queue.h"
}

/**
 * \brief Test related to the queue
 */
TEST_GROUP(QueueGroup)
{
	queue_t queue;

	void setup() 
	{
		queue_init(&queue); // initializes the queue
	}

	void teardown()
	{
	}
};

TEST(QueueGroup, TestQueueInit)
{
	CHECK_EQUAL(256, QUEUE_MAX_SIZE);

	CHECK_EQUAL(0, queue.size);
	CHECK_EQUAL(0, queue.head);
	CHECK_EQUAL((QUEUE_MAX_SIZE - 1), queue.tail);

	for (int i = 0; i < QUEUE_MAX_SIZE; i++)
	{
		CHECK_EQUAL(0, queue.storage[i]);
	}
}

TEST(QueueGroup, EnqueueTest)
{
	uint8_t tmp[QUEUE_MAX_SIZE];
	int i;
	for (i = 0; i < QUEUE_MAX_SIZE - 1; i++)
	{
		tmp[i] = (rand() & UINT8_MAX);
		queue_enqueue(&queue, tmp[i]);
	}

	for (i = 0; i < QUEUE_MAX_SIZE - 1; i++)
	{
		CHECK_EQUAL(tmp[i], queue.storage[i]);
	}
}

TEST(QueueGroup, DequeueTest)
{
	uint8_t tmp[QUEUE_MAX_SIZE];
	int i;
	for (i = 0; i < QUEUE_MAX_SIZE - 1; i++)
	{
		tmp[i] = (rand() & UINT8_MAX);
		queue_enqueue(&queue, tmp[i]);
	}
	for (i = 0; i < QUEUE_MAX_SIZE - 1; i++)
	{
		uint8_t value;
		queue_dequeue(&queue, &value);
		CHECK_EQUAL(tmp[i], value);
	}
}

TEST(QueueGroup, QueueSizeTest)
{
	int i;
	for (i = 0; i < QUEUE_MAX_SIZE - 1; i++)
	{
		CHECK_EQUAL(i, queue_size(&queue));
		queue_enqueue(&queue, (rand() & UINT8_MAX));
	}

	CHECK_EQUAL(i, queue_size(&queue));
}