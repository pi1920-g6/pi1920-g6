#include <CppUTest/TestHarness.h>
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>

extern "C"
{
#include "defines.h"
#include "SM_structures.h"
#include "homeCurrentSM.h"
#include "controller.h"
}

/**
 * \brief Test related to the homeCurrentSM functions
 */
TEST_GROUP(homeCurrentSM)
{
	sm_t state;
	param_id_t parameter;

	void setup() 
	{
		state.c_state=HOME_CHANGE_CURRENT;
		state.p_state=HOME_CHANGE_CURRENT;
	}

	void teardown()
	{
	}
};

/**
 * 				HOME CURRENT
 */
/**
 * \brief Home Current - Test Encoder Hold
 */
TEST(homeCurrentSM, Hold_HCC)
{
	state.c_state=HOME_CHANGE_CURRENT;
	state.p_state=HOME_CHANGE_CURRENT;
	var_interrupt = ENC_HOLD;
	state = home_change_current(state);
	CHECK_EQUAL(state.c_state, HELP);
}

/**
 * \brief Home Current - Test First Loop
 */
TEST(homeCurrentSM, FirstLoopTest_HCC)
{
	
	var_interrupt = NONE;
	state = home_change_current(state);
	CHECK_EQUAL(FirstLoop, false);
}

/**
 * \brief Home Current - Test Encoder button
 */
TEST(homeCurrentSM, EncBut_HCC)
{
	
	var_interrupt = ENC_BUTTON;
	state = home_change_current(state);
	CHECK_EQUAL(state.c_state, SELECTION_MENU);
	
}

/**
 * \brief Home Current  - Test First Loop true
 */
TEST(homeCurrentSM, FirstLoopTest_MS)
{
	var_interrupt = NONE;
	state = home_change_current(state);
	CHECK_EQUAL(FirstLoop, false);
	
	var_interrupt = ENC_BUTTON;
	state = home_change_current(state);
	CHECK_EQUAL(FirstLoop, true);
}

/**
 * \brief Home Current - Test Current value increment
 */
TEST(homeCurrentSM, CurrentValueTestIncrement)
{
	CurrentValue=1;
	var_interrupt = ENC_RIGHT;
	state = home_change_current(state);
	CHECK_EQUAL(CurrentValue, 2);
}

/**
 * \brief Home Current - Test Current Value Decrement
 */
TEST(homeCurrentSM, CurrentValueTestDecrement)
{
	CurrentValue=1;
	var_interrupt = ENC_LEFT;
	state = home_change_current(state);
	CHECK_EQUAL(CurrentValue, 0);
}

/**
 * \brief Home Current - Test Current value decrement below minimun value
 */
TEST(homeCurrentSM, DecrementFail)
{
	CurrentValue=0;
	var_interrupt = ENC_LEFT;
	state = home_change_current(state);
	CHECK_EQUAL(CurrentValue, 0);
}


/**
 * \brief Home Current - Test Current value increment above maximum value
 */
TEST(homeCurrentSM, IncrementFail)
{
	CurrentValue=999;
	var_interrupt = ENC_RIGHT;
	state = home_change_current(state);
	CHECK_EQUAL(CurrentValue, 999);
}


/**
 * 				SELECTION_MENU
 */

/**
 * \brief Selection Menu - Test Home button
 */
TEST(homeCurrentSM, butHome_MS)
{
	
	var_interrupt = HOME;
	state.c_state=SELECTION_MENU;
	state = selection_menu(state);
	CHECK_EQUAL(state.c_state, HOME_CHANGE_CURRENT);
}

/**
 * \brief Selection Menu - Test Hold button
 */
TEST(homeCurrentSM, EncHold_MS)
{
	
	var_interrupt = ENC_HOLD;
	state.c_state=SELECTION_MENU;
	state = selection_menu(state);
	CHECK_EQUAL(state.c_state, HELP);
}

/**
 * \brief Selection Menu - Test previous button
 */
TEST(homeCurrentSM, butBack_MS)
{
	
	var_interrupt = BACK;
	state.c_state=SELECTION_MENU;
	state = selection_menu(state);
	CHECK_EQUAL(state.c_state, HOME_CHANGE_CURRENT);
}

/**
 * \brief Selection Menu - Test Encoder button with WelProc read simulation (WeldProc=3)
 */
TEST(homeCurrentSM, EncBut_MS_WelProc3)
{
	
	var_interrupt = ENC_BUTTON;
	parameter=WELD_PROCESS;
    controller_cache_write((int)parameter,3);
	state.c_state=SELECTION_MENU;
	WeldProc=3;
	buttons=PARAMS_BUTTON;
	state = selection_menu(state);
	CHECK_EQUAL(state.c_state, TIG_PULSE_PG1);
}

/**
 * \brief Selection Menu - Test Encoder button with WelProc read simulation (WeldProc=5)
 */
TEST(homeCurrentSM, EncBut_MS_WelProc5)
{
	parameter=WELD_PROCESS;
    controller_cache_write((int)parameter,5);
	
	var_interrupt = ENC_BUTTON;
	state.c_state=SELECTION_MENU;
	WeldProc=5;
	buttons=PARAMS_BUTTON;
	state = selection_menu(state);
	CHECK_EQUAL(state.c_state, MMA_PULSE_PG1);
}

/**
 * \brief Selection Menu - Test Encoder Right with buttons=LOADSAVE_BUTTON
 */
TEST(homeCurrentSM, EncRight_MS_Buttons)
{
	var_interrupt = ENC_RIGHT;
	state.c_state=SELECTION_MENU;
	buttons=LOADSAVE_BUTTON;
	state = selection_menu(state);
	CHECK_EQUAL(buttons, SETTINGS_BUTTON);
}


/**
 * \brief Selection Menu - Test Encoder Left with buttons=PARAMS_BUTTON
 */
TEST(homeCurrentSM, EncLeft_MS_Buttons)
{
	var_interrupt = ENC_LEFT;
	state.c_state=SELECTION_MENU;
	buttons=PARAMS_BUTTON;
	state = selection_menu(state);
	CHECK_EQUAL(buttons, MODES_BUTTON);
}