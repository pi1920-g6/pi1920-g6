/**
 * \file    utils.c
 */
/**
 * \addtogroup  misc Misc.
 * \ref utils.c
 */
#ifndef __UNITTESTING__

#include <xc.h>

#else

#include "../../XC32_mock/mem_map.h"

#endif

#include <stdint.h>
#include <string.h>
#include "defines.h"
#include "utils.h"

#define MS_SCALE (SYSCLK / 1E3) // millisecond scale
#define US_SCALE (SYSCLK / 1E6) // microsecond scale

#ifndef __UNITTESTING__

static volatile uint32_t readCoreTimer(void)
{
	volatile uint32_t tick;
	/* get the clock tick */
	asm volatile("mfc0 %0, $9" : "=r"(tick));
	return tick;
}

void delay_ms(uint16_t value)
{
	/* current clock tick */
	volatile uint32_t initTick = readCoreTimer();
	/* how many clock ticks it takes for a millisecond */
	volatile register uint32_t tickerPerUnit = value * MS_SCALE;
	/* wait for the ticks_per_ms ticks */
	while (readCoreTimer() < initTick + tickerPerUnit);
}

void delay_us(uint16_t value)
{
	/* current clock tick */
	volatile uint32_t initTick = readCoreTimer();
	/* how many clock ticks it takes for a microsecond */
	volatile register uint32_t tickersPerUnit = value * US_SCALE;
	/* wait for the ticks_per_us ticks */
	while (readCoreTimer() < initTick + tickersPerUnit);
}

#endif

uint16_t utils_fletcher16(const uint8_t *data, uint16_t bytes)
{
	uint16_t sum1 = 0xff, sum2 = 0xff;

	while (bytes)
	{
		uint8_t tlen = bytes > 20 ? 20 : (uint8_t) bytes;
		bytes -= tlen;
		do
		{
			sum2 += sum1 += *data++;
		} while (--tlen);
		sum1 = (sum1 & 0xff) + (sum1 >> 8);
		sum2 = (sum2 & 0xff) + (sum2 >> 8);
	}
	/* Second reduction step to reduce sums to 8 bits */
	sum1 = (sum1 & 0xff) + (sum1 >> 8);
	sum2 = (sum2 & 0xff) + (sum2 >> 8);
	return sum2 << 8 | sum1;
}

void utils_int_to_str(char *str, uint16_t val, uint8_t digits)
{
	uint8_t i = 1;
	for (; i <= digits; i++)
	{
		str[digits - i] = (char)((val % 10) + '0');
		val /= 10;
	}

	str[i-1] = '\0';
}