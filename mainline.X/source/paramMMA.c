/**
 * \file    paramMMA.c
 */
/**
 * \addtogroup  smPMMA paramMMA
 * \ingroup sm
 * @{
 *  \ref paramMMA.c
 * @}
 */
#include <stdbool.h>

#include "SM_structures.h"
#include "paramMMA.h"
#include "defines.h"
#include "nextion.h"
#include "utils.h"
#include "controller.h"

#ifndef __UNITTESTING__
    #include <xc.h>
    #include <sys/attribs.h>
#else
    #include "../../XC32_mock/mem_map.h"
#endif

/*
 * VARIABLES
 */
/** \brief   Maximum Hot Start value. */
#define MaxHotStart 100
/** \brief   Minimum Hot Start value. */
#define MinHotStart 10
/** \brief   Maximum Hot Start time. */
#define MaxHotTime 20
/** \brief   Minimum Hot Start time. */
#define MinHotTime 1
/** \brief   Maximum INominal value. */
#define MaxINominal 220
/** \brief   Minimum INominal value. */
#define MinINominal 10
/** \brief   Maximum ArcForce value. */
#define MaxArcForce 50
/** \brief   Minimum ArcForce value. */
#define MinArcForce 0
/** \brief   Maximum Base Current value. */
#define MaxBaseCurrent 95
/** \brief   Minimum Base Current value. */
#define MinBaseCurrent 50
/** \brief   Maximum Width value. */
#define MaxWidth 90
/** \brief   Minimum Width value. */
#define MinWidth 10
/** \brief   Maximum Pulse Frequency value. */
#define MaxPulseFreq 100
/** \brief   Minimum Pulse Frequency value. */
#define MinPulseFreq 5


/** \brief   Variable to store the Hot Start value. */
uint16_t MMAHotStart; 
/** \brief   Variable to store the Hot Start time value. */
uint16_t MMAHotStartTime;
/** \brief   Variable to store the Nominal Current value. */
uint16_t MMAINominal;
/** \brief   Variable to store the Arc Force value. */
uint16_t MMAArcForce;
/** \brief   Variable to store the Base current value. */
uint16_t MMAIBase;
/** \brief   Variable to store the Width of the peak current value. */
uint16_t MMAWidthval;
/** \brief   Variable to store the Frequency of the pulse. */
uint16_t MMAPFreq;
/** \brief   Variable to store welding state. */
uint16_t weldstate;

/** \brief   Variable used to send to nextion MMA HotStart. */
char NextionMMA_HotStart[3];
/** \brief   Variable used to send to nextion MMA HotStart Time. */
char NextionMMA_HotStartTime[3];
/** \brief   Variable used to send to nextion MMA Nominal Current. */
char NextionMMA_INominal[3];
/** \brief   Variable used to send to nextion MMA Arc Force. */
char NextionMMA_AcrForce[3];
/** \brief   Variable used to send to nextion MMA Base Current. */
char NextionMMA_BaseCurrent[3];
/** \brief   Variable used to send to nextion MMA Width. */
char NextionMMA_Width[2];
/** \brief   Variable used to send to nextion MMA Pulse Frequency. */
char NextionMMA_PulseFreq[2];

/** \brief   Variable used to identify the HotStart object. */
char obj_buttonHotStart[3]= "i0";
/** \brief   Variable used to identify the HotStart value object. */
char obj_HotStartValue[3]="n0";
/** \brief   Variable used to identify the HotStartTime object. */
char obj_buttonHotStartTime[3]= "i1";
/** \brief   Variable used to identify the HotStartTime value object. */
char obj_HotStartTimeValue[3]="x1";
/** \brief   Variable used to identify the Nominal Current object. */
char obj_buttonINominal[3]= "i2";
/** \brief   Variable used to identify the Nominal Current value object. */
char obj_INominalValue[3]="n2";
/** \brief   Variable used to identify the Arc Force object (MMA). */
char obj_buttonArcForce[3]= "i3";
/** \brief   Variable used to identify the Arcforce value object (MMA). */
char obj_ArcForceValue[3]="n3";
/** \brief   Variable used to identify the Arc Force object (MMA PULSE). */
char obj_buttonBaseCurrent[3]= "i3";
/** \brief   Variable used to identify the Arcforce value object (MMA PULSE). */
char obj_BaseCurrentValue[3]="n3";
/** \brief   Variable used to identify the Width object (MMA PULSE). */
char obj_buttonWidth[3]= "i0";
/** \brief   Variable used to identify the Width value object (MMA PULSE). */
char obj_WidthValue[3]="n0";
/** \brief   Variable used to identify the Pulse Frequency object (MMA PULSE). */
char obj_buttonPulseFreq[3]= "i1";
/** \brief   Variable used to identify the Pulse Frequency value object (MMA PULSE). */
char obj_PulseFreqValue[3]="x1";
/** \brief   Variable used to identify the first page marker. (MMA PULSE). */
char obj_buttonMarker1[3]="c0";
/** \brief   Variable used to identify the second page marker. (MMA PULSE). */
char obj_buttonMarker2[3]="c1";




/** \brief   Variable used to identify the MMA Parameters page. */
char MenuPage_MMA[3]= "11";
/** \brief   Variable used to identify the first MMA Parameters page. */
char MenuPage_MMAPulse1[3]= "12";
/** \brief   Variable used to identify the second MMA Parameters page. */
char MenuPage_MMAPulse2[3]= "13";
/** \brief   Variable used to identify the Help page of MMA parameter HotStart. */
char HelpPage_MMAHotStart[3]="46";
/** \brief   Variable used to identify the Help page of MMA parameter HotStartTime. */
char HelpPage_MMAHotStartTime[3]="47";
/** \brief   Variable used to identify the Help page of MMA parameter ArcForce. */
char HelpPage_MMAArcForce[3]="48";
/** \brief   Variable used to identify the Help page of MMA parameter INominal. */
char HelpPage_MMAInominal[3]="37";
/** \brief   Variable used to identify the Help page of MMA Base current. */
char HelpPage_MMAIBase[3]="49";
/** \brief   Variable used to identify the Help page of MMA parameter Pulse Width. */
char HelpPage_MMAWidth[3]="44";
/** \brief   Variable used to identify the Help page of MMA parameter Pulse Frequency. */
char HelpPage_MMAFreq[3]="50";

/** \brief   Buttons on MMA menu. */
typedef enum MMAParam_buttons_t
{
    HOTSTART_BUTTON,
    HOTSTARTTIME_BUTTON,
    INOMINAL_BUTTON,
    ARCFORCE_BUTTON,
    HOTSTARTVALUE_BUTTON,
    HOTSTARTTIMEVALUE_BUTTON,   
    INOMINALVALUE_BUTTON,
    ARCFORCEVALUE_BUTTON
} MMAParam_buttons_t;

/** \brief   Buttons on MMA Pulse menu - page 1. */
typedef enum MMAPulse_Page1_Param_buttons_t
{
    PULSE_HOTSTART_BUTTON,
    PULSE_HOTSTARTTIME_BUTTON,
    PULSE_INOMINAL_BUTTON,
    PULSE_BASECURRENT_BUTTON,
    PULSE_HOTSTARTVALUE_BUTTON,
    PULSE_HOTSTARTTIMEVALUE_BUTTON,   
    PULSE_INOMINALVALUE_BUTTON,
    PULSE_BASECURRENTVALUE_BUTTON
} MMAPulse1Param_buttons_t;


/** \brief   Buttons on MMA Pulse menu - page 2. */
typedef enum MMAPulse_Page2_Param_buttons_t
{
    PULSE_WIDTH_BUTTON,
    PULSE_FREQ_BUTTON,
    PULSE_WIDTHVALUE_BUTTON,
    PULSE_FREQVALUE_BUTTON
} MMAPulse2Param_buttons_t;

param_id_t param;
MMAParam_buttons_t MMA_buttons;
MMAPulse1Param_buttons_t MMA_Pulse_Buttons1;
MMAPulse2Param_buttons_t MMA_Pulse_Buttons2;


/**
 * \brief Responsible for the MMA (NOT PULSED) parameteres and value changes.
 * 
 * This routine will control the flow between the MMA parameters aswell
 * as allowing the user to change them according to max and min value.
 * 
 * Parameters available on this page:
 * MMA HOTSTART - Min value 10, Max value 100 (%)
 * MMA HOTSTART TIME - Min value 1, Max value 20 (Seconds/10)
 * MMA INOMINAL - Min value 10, Max value 220 (A)
 * MMA ARCFORCE - Min value 0, Max value 50 (%)
 * 
 * Page 1/1 for MMA.
 * 
 *  Events:
 * - Encoder rotation to the right or left will change the button or the value
 * of the parameter.
 * - Encoder click will allow the user to open parameter value so it can be 
 * edited if on the button, if on the value, will validate the change and save it.
 * - Encoder hold will open a Help menu for this page.
 * - Back will take the user back to Selection Menu buttons on Home menu if on 
 * buttons. If on values, will discard changes.
 * - Home will take the user back to Current Value change on Home menu.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t MMA_parameters_page(sm_t stateMachine)
{
    switch(var_interrupt)
    {
        /*
         * HOME EVENT
         * - State machine current state is now Home_Change_Current
         */
        case HOME:
            stateMachine.c_state = HOME_CHANGE_CURRENT;  //update current state to HOME CHANGE CURRENT 
            stateMachine.p_state = MMA_PG1;//update previous state to MMA_PG1
            FirstLoop=true;  //FirsLoop flag set as true for the new state
            var_interrupt=NONE; //Clear event flag 
            break;
        
        /*
         * BACK EVENT
         * - Return to Selection Menu (Home buttons) if on the any parameter button
         * - Return to the button if on the value change option, without validation of 
         * the value changes, reseting the previous stored value.
         */
        case BACK: 
             switch(MMA_buttons)
            {
                case HOTSTART_BUTTON: //Case button HotStart -> Go to previous state
                    stateMachine.c_state = SELECTION_MENU; //update current state to SELECTION MENU
                    stateMachine.p_state = MMA_PG1;//update previous state to MMA_PG1
                    FirstLoop=true;//FirsLoop flag set as true for the new state
                    break;
                    
                case HOTSTARTTIME_BUTTON: //Case button HotStartTime -> Go to previous state
                    stateMachine.c_state = SELECTION_MENU; //update current state to SELECTION MENU
                    stateMachine.p_state = MMA_PG1;//update previous state to MMA_PG1
                    FirstLoop=true; //FirsLoop flag set as true for the new state
                    break;
                    
                case INOMINAL_BUTTON: //Case button I Nominal -> Go to previous state
                    stateMachine.c_state = SELECTION_MENU; //update current state to SELECTION MENU
                    stateMachine.p_state = MMA_PG1;//update previous state to MMA_PG1
                    FirstLoop=true;//FirsLoop flag set as true for the new state
                    break;
                    
                case ARCFORCE_BUTTON: //Case button Arc Force -> Go to previous state
                    stateMachine.c_state = SELECTION_MENU; //update current state to SELECTION MENU
                    stateMachine.p_state = MMA_PG1;//update previous state to MMA_PG1
                    FirstLoop=true;//FirsLoop flag set as true for the new state
                    break;
                    
                case HOTSTARTVALUE_BUTTON: //Case HotStart value option -> Go to Button HotStart
                    nextion_unselect(obj_HotStartValue); //remove highlight from HotStart value
                    nextion_select(obj_buttonHotStart);//highlight HotStart button
                    param=MMA_HOTSTART; //memory parameter -> MMA Hotstart
                    controller_cache_read(param,&MMAHotStart); //read MMA HotStart from memory
                    utils_int_to_str(NextionMMA_HotStart,MMAHotStart,3); //Convert to string
                    nextion_change_val(obj_HotStartValue,NextionMMA_HotStart); //Send value to Nextion HotStart value object
                    MMA_buttons=HOTSTART_BUTTON; //Update MMA buttons to HotStart (button)
                    break;
                   
                case HOTSTARTTIMEVALUE_BUTTON: //Case HotStartTime value option -> Go to Button HotStartTime
                    nextion_unselect(obj_HotStartTimeValue);//remove highlight from HotStartTime value
                    nextion_select(obj_buttonHotStartTime);//highlight HotStart button
                    param=MMA_HOTSTART_TIME; //memory parameter -> MMA Hotstart Time
                    controller_cache_read(param,&MMAHotStartTime); //read MMA HotStartTime from memory
                    utils_int_to_str(NextionMMA_HotStartTime,MMAHotStartTime,2);//Convert to string
                    nextion_change_val(obj_HotStartTimeValue,NextionMMA_HotStartTime); //Send value to Nextion HotStartTime value object
                    MMA_buttons=HOTSTARTTIME_BUTTON; //Update MMA buttons to HotStartTime (button)
                    break;
                   
                case INOMINALVALUE_BUTTON: //Case INominal value option -> Go to Button INominal
                    nextion_unselect(obj_INominalValue); //remove highlight from INominal value
                    nextion_select(obj_buttonINominal); //highlight INominal button
                    param=MMA_INOMINAL; //memory parameter -> MMA INominal
                    controller_cache_read(param,&MMAINominal); //read MMA INominal from memory
                    utils_int_to_str(NextionMMA_INominal,MMAINominal,3); //Convert to string
                    nextion_change_val(obj_INominalValue,NextionMMA_INominal);//Send value to Nextion INominal value object
                    MMA_buttons=INOMINAL_BUTTON; //Update MMA buttons to INominal (button)
                    break;
                   
                case ARCFORCEVALUE_BUTTON: //Case ArcForce value option -> Go to Button ArcForce
                    nextion_unselect(obj_ArcForceValue); //remove highlight from ArcForce value
                    nextion_select(obj_buttonArcForce); //highlight ArcForce button
                    param=MMA_ARCFORCE; //memory parameter -> MMA Arc Force
                    controller_cache_read(param,&MMAArcForce); //read MMA ArcForce from memory
                    utils_int_to_str(NextionMMA_AcrForce,MMAArcForce,2); //Convert to string
                    nextion_change_val(obj_ArcForceValue,NextionMMA_AcrForce);//Send value to Nextion ArcForce value object
                    MMA_buttons=ARCFORCE_BUTTON;//Update MMA buttons to ArcForce (button) 
                    break;
                   
                default:
                    break;
            }
            var_interrupt=NONE; //clear event flag
            break;
        
        /*
         * ENCODER BUTTON EVENT
         * - Access value change if on button
         * - Validate changes and go to button if is on value
         */    
        case ENC_BUTTON:
           switch(MMA_buttons)
            {
                case HOTSTART_BUTTON://Case button HotStart -> Go to value HotStart
                    nextion_unselect(obj_buttonHotStart);  //remove highlight from HotStart button
                    nextion_select(obj_HotStartValue); //highlight HotStart value
                    MMA_buttons=HOTSTARTVALUE_BUTTON; //Update MMA buttons to HotStart (value)
                    break;
                    
                case HOTSTARTTIME_BUTTON://Case button HotStartTime -> Go to value HotStartTime
                    nextion_unselect(obj_buttonHotStartTime); //remove highlight from HotStartTime button
                    nextion_select(obj_HotStartTimeValue); //highlight HotStartTime value
                    MMA_buttons=HOTSTARTTIMEVALUE_BUTTON; //Update MMA buttons to HotStartTime (value)
                    break;
                    
                case INOMINAL_BUTTON://Case button INominal -> Go to value INominal
                    nextion_unselect(obj_buttonINominal); //remove highlight from INominal button
                    nextion_select(obj_INominalValue); //highlight INominal value
                    MMA_buttons=INOMINALVALUE_BUTTON; //Update MMA buttons to INominal (value)
                    break;
                    
                case ARCFORCE_BUTTON: //Case button ArcForce -> Go to value ArcForce
                    nextion_unselect(obj_buttonArcForce); //remove highlight from ArcForce button
                    nextion_select(obj_ArcForceValue); //highlight ArcForce value
                    MMA_buttons=ARCFORCEVALUE_BUTTON; //Update MMA buttons to ArcForce (value)
                    break;
                    
                case HOTSTARTVALUE_BUTTON: //Case value HotStart -> Go to button HotStart
                    nextion_unselect(obj_HotStartValue); //remove highlight from HotStart Value
                    nextion_select(obj_buttonHotStart); //highlight ArcForce button
                    param=MMA_HOTSTART;//memory parameter -> MMA HotStart
                    controller_cache_write((int)param,MMAHotStart); //Write the new value on memory
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    MMA_buttons=HOTSTART_BUTTON; //Update MMA buttons to HotStart (button)
                    break;
                   
                case HOTSTARTTIMEVALUE_BUTTON: //Case value HotStartTime -> Go to button HotStartTime
                    nextion_unselect(obj_HotStartTimeValue);  //remove highlight from HotStartTime Value
                    nextion_select(obj_buttonHotStartTime); //highlight HotStartTime button
                    param=MMA_HOTSTART_TIME; //memory parameter -> MMA HotStartTime
                    controller_cache_write((int)param,MMAHotStartTime); //Write the new value on memory
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    MMA_buttons=HOTSTARTTIME_BUTTON; //Update MMA buttons to HotStartTime (button)
                    break;
                   
                case INOMINALVALUE_BUTTON: //Case value INominal -> Go to button INominal
                    nextion_unselect(obj_INominalValue);  //remove highlight from INominal Value
                    nextion_select(obj_buttonINominal); //highlight INominal button
                    param=MMA_INOMINAL; //memory parameter -> MMA INominal
                    controller_cache_write((int)param,MMAINominal); //Write the new value on memory
                    MMA_buttons=INOMINAL_BUTTON; //Update MMA buttons to INominal (button)
                    break;
                   
                case ARCFORCEVALUE_BUTTON: //Case value ArcForce -> Go to button ArcForce
                    nextion_unselect(obj_ArcForceValue);  //remove highlight from ArcForce Value
                    nextion_select(obj_buttonArcForce); //highlight ArcForce button
                    param=MMA_ARCFORCE;  //memory parameter -> MMA Arc Force
                    controller_cache_write((int)param,MMAArcForce); //Write the new value on memory
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase                    
                    MMA_buttons=ARCFORCE_BUTTON; //Update MMA buttons to ArcForce (button)
                    break;
                   
                default:
                    break;
            }
            var_interrupt=NONE; //Clear event flag
            break;
        
            
        /*
         * ENCODER HOLD EVENT
         * - Change current state and previous state
         * - Open HELP page depending on the parameter currently selected
         */        
        case ENC_HOLD:
            switch(MMA_buttons)
            {
                case HOTSTART_BUTTON: //Case button HotStart 
                    nextion_page(HelpPage_MMAHotStart); 
                    break;
                    
                case HOTSTARTTIME_BUTTON: //Case button HotStartTime 
                    nextion_page(HelpPage_MMAHotStartTime); 
                    break;
                    
                case INOMINAL_BUTTON: //Case button I Nominal 
                    nextion_page(HelpPage_MMAInominal); 
                    break;
                    
                case ARCFORCE_BUTTON: //Case button Arc Force 
                    nextion_page(HelpPage_MMAArcForce); 
                    break;
                    
                case HOTSTARTVALUE_BUTTON: //Case HotStart value option
                    nextion_page(HelpPage_MMAHotStart); 
                    break;
                   
                case HOTSTARTTIMEVALUE_BUTTON: //Case HotStartTime
                    nextion_page(HelpPage_MMAHotStartTime); 
                    break;
                   
                case INOMINALVALUE_BUTTON: //Case INominal
                    nextion_page(HelpPage_MMAInominal); 
                    break;
                   
                case ARCFORCEVALUE_BUTTON: //Case ArcForce
                    nextion_page(HelpPage_MMAArcForce); 
                    break;
                   
                default:
                    break;
            }
            stateMachine.c_state = HELP;
            stateMachine.p_state = MMA_PG1;
            FirstLoop=true;
            var_interrupt=NONE;
            break;
        
            
        /*
         * ENCODER RIGHT EVENT
         * - If on button, move to the right button
         * - If on value, increment value its less than MaxValue
         * - Send to nextion new value
         */        
        case ENC_RIGHT:
           switch(MMA_buttons)
            {
                case HOTSTART_BUTTON: //Case button HotStart -> Go to button HotStartTime
                    nextion_unselect(obj_buttonHotStart); //remove highlight from HotStart button
                    nextion_select(obj_buttonHotStartTime); // highlight  HotStartTime button
                    MMA_buttons=HOTSTARTTIME_BUTTON; //Update MMA buttons to HotStartTime (button)
                    break;
                    
                case HOTSTARTTIME_BUTTON: //Case button HotStartTime -> Go to button INominal
                    nextion_unselect(obj_buttonHotStartTime); //remove highlight from HotStartTime button
                    nextion_select(obj_buttonINominal); // highlight  INominal button
                    MMA_buttons=INOMINAL_BUTTON; //Update MMA buttons to INominal (button)
                    break;
                    
                case INOMINAL_BUTTON: //Case button INominal -> Go to button ArcForce
                    nextion_unselect(obj_buttonINominal); //remove highlight from INominal button
                    nextion_select(obj_buttonArcForce); // highlight  ArcForce button
                    MMA_buttons=ARCFORCE_BUTTON; //Update MMA buttons to ArcForce (button)
                    break;
                    
                case ARCFORCE_BUTTON: //Case button ArcForce -> no more buttons to the right
                    break;
                    
                case HOTSTARTVALUE_BUTTON: //Case HotStart value -> Increment value (if below max)
                    if(MMAHotStart<MaxHotStart) //Only increment value if below MaxValue
                    {
                        MMAHotStart++; //Increment HotStart value
                        utils_int_to_str(NextionMMA_HotStart,MMAHotStart,3); //convert to string
                        nextion_change_val(obj_HotStartValue,NextionMMA_HotStart); //Send value to Nextion
                    }
                   break;
                   
                case HOTSTARTTIMEVALUE_BUTTON: //Case HotStartTime value -> Increment value (if below max)
                    if(MMAHotStartTime<MaxHotTime) //Only increment value if below MaxValue
                    {
                        MMAHotStartTime++; //Increment HotStartTime value
                        utils_int_to_str(NextionMMA_HotStartTime,MMAHotStartTime,2); //convert to string
                        nextion_change_val(obj_HotStartTimeValue,NextionMMA_HotStartTime); //Send value to Nextion
                    }
                   break;
                   
                case INOMINALVALUE_BUTTON: //Case INominal value -> Increment value (if below max)
                    if(MMAINominal<MaxINominal) //Only increment value if below MaxValue
                    {
                        MMAINominal++; //Increment INominal value
                        utils_int_to_str(NextionMMA_INominal,MMAINominal,3); //convert to string
                        nextion_change_val(obj_INominalValue,NextionMMA_INominal); //Send value to Nextion
                    }
                   break;
                   
                case ARCFORCEVALUE_BUTTON: //Case ArcForce value -> Increment value (if below max)
                    if(MMAArcForce<MaxArcForce) //Only increment value if below MaxValue
                    {
                        MMAArcForce++; //Increment Arc Force value
                        utils_int_to_str(NextionMMA_AcrForce,MMAArcForce,2); //convert to string
                        nextion_change_val(obj_ArcForceValue,NextionMMA_AcrForce); //Send value to Nextion
                    }
                   break;
                   
                default:
                    break;
            }
            var_interrupt=NONE; //Clear event flag
            break;    
        
        /*
         * ENCODER LEFT EVENT
         * - If on button, move to the left button
         * - If on value, decrement value its above MinValue
         * - Send to nextion new value
         */ 
        case ENC_LEFT:
            switch(MMA_buttons)
            {
                case HOTSTART_BUTTON: //Case button HotStart -> no more buttons to the left
                    break;
                    
                case HOTSTARTTIME_BUTTON: //Case button HotStartTime -> go to button HotStart
                    nextion_unselect(obj_buttonHotStartTime); //remove highlight from HotStartTime button
                    nextion_select(obj_buttonHotStart); //highlight HotStart button
                    MMA_buttons=HOTSTART_BUTTON; //Update MMA buttons to HotStart (button)
                    break;
                    
                case INOMINAL_BUTTON:  //Case button Inominal -> go to button HotStartTime
                    nextion_unselect(obj_buttonINominal); //remove highlight from INominal button
                    nextion_select(obj_buttonHotStartTime); //highlight HotStartTime button
                    MMA_buttons=HOTSTARTTIME_BUTTON; //Update MMA buttons to HotStartTime (button)
                    break;
                    
                case ARCFORCE_BUTTON:  //Case button ArcForce -> go to button Inominal
                    nextion_unselect(obj_buttonArcForce); //remove highlight from ArcForce button
                    nextion_select(obj_buttonINominal); //highlight INominal button
                    MMA_buttons=INOMINAL_BUTTON;  //Update MMA buttons to INominal (button)
                    break;
                    
               case HOTSTARTVALUE_BUTTON:  //Case HotStart value -> Decrement value (if above min)
                    if(MMAHotStart>MinHotStart) //Only decrement value if bove MinValue
                    {
                        MMAHotStart--; //Decrement HotStart value
                        utils_int_to_str(NextionMMA_HotStart,MMAHotStart,3); //Convert to string
                        nextion_change_val(obj_HotStartValue,NextionMMA_HotStart); //Send value to Nextion
               
                    }
                   break;
                   
                case HOTSTARTTIMEVALUE_BUTTON:  //Case HotStartTime value -> Decrement value (if above min)
                    if(MMAHotStartTime>MinHotTime) //Only decrement value if bove MinValue
                    {
                        MMAHotStartTime--; //Decrement HotStartTime value
                        utils_int_to_str(NextionMMA_HotStartTime,MMAHotStartTime,2); //Convert to string
                        nextion_change_val(obj_HotStartTimeValue,NextionMMA_HotStartTime); //Send value to Nextion
               
                    }
                   break;
                   
                case INOMINALVALUE_BUTTON:  //Case INominal value -> Decrement value (if above min)
                    if(MMAINominal>MinINominal) //Only decrement value if bove MinValue
                    {
                        MMAINominal--; //Decrement INominal value
                        utils_int_to_str(NextionMMA_INominal,MMAINominal,3); //Convert to string
                        nextion_change_val(obj_INominalValue,NextionMMA_INominal); //Send value to Nextion
                    }
                   break;
                   
                case ARCFORCEVALUE_BUTTON:  //Case ArcForce value -> Decrement value (if above min)
                    if(MMAArcForce>MinArcForce) //Only decrement value if bove MinValue
                    {
                        MMAArcForce--; //Decrement ArcForce value
                        utils_int_to_str(NextionMMA_AcrForce,MMAArcForce,2);//Convert to string
                        nextion_change_val(obj_ArcForceValue,NextionMMA_AcrForce); //Send value to Nextion
                    }
                   break;
                   
                default:
                    break;
            }
           var_interrupt=NONE; //Clear event flag
            break;   
        
        /*
         * NONE
         * - Update values on Nextion
         * - Set nextion page
         * - Highlight First Object
         */ 
        case NONE:
            if(FirstLoop==true) //first time executing when on this state 
            {
               nextion_page(MenuPage_MMA);//Change nextion page to MMA PAGE 1
                
               MMA_buttons=HOTSTART_BUTTON; //Init MMA button on HotStart button
               
               /*
                Read from memory and load values to Nextion
                */
               param=MMA_HOTSTART; //memory parameter -> MMA HOTSTART
               controller_cache_read(param,&MMAHotStart); //read HotStart value from memory
               utils_int_to_str(NextionMMA_HotStart,MMAHotStart,3); //Convert to string
               nextion_change_val(obj_HotStartValue,NextionMMA_HotStart); //Send value to Nextion
               
               param=MMA_HOTSTART_TIME; //memory parameter -> MMA HOTSTARTTIME
               controller_cache_read(param,&MMAHotStartTime); //read HotStartTime value from memory
               utils_int_to_str(NextionMMA_HotStartTime,MMAHotStartTime,2); //Convert to string
               nextion_change_val(obj_HotStartTimeValue,NextionMMA_HotStartTime); //Send value to Nextion
               
               param=MMA_INOMINAL; //memory parameter -> MMA INOMINAL
               controller_cache_read(param,&MMAINominal); //read INominal value from memory
               utils_int_to_str(NextionMMA_INominal,MMAINominal,3); //Convert to string
               nextion_change_val(obj_INominalValue,NextionMMA_INominal); //Send value to Nextion
               
               
               param=MMA_ARCFORCE; //memory parameter -> MMA ARCFORCE
               controller_cache_read(param,&MMAArcForce); //read ArcForce value from memory
               utils_int_to_str(NextionMMA_AcrForce,MMAArcForce,2); //Convert to string
               nextion_change_val(obj_ArcForceValue,NextionMMA_AcrForce); //Send value to Nextion
               
               /*
                Remove all highlights when page is loaded
                */
               nextion_unselect(obj_buttonHotStart); //remove highlight from HotStart button
               nextion_unselect(obj_HotStartValue); //remove highlight from HotStart value
               nextion_unselect(obj_buttonHotStartTime); //remove highlight from HotStartTime button
               nextion_unselect(obj_HotStartTimeValue); //remove highlight from HotStartTime value
               nextion_unselect(obj_buttonINominal); //remove highlight from INominal button
               nextion_unselect(obj_INominalValue); //remove highlight from INominal value
               nextion_unselect(obj_buttonArcForce); //remove highlight from ArcForce button
               nextion_unselect(obj_ArcForceValue); //remove highlight from ArcForce value
               
                /*
                Highlights first button
                */
               nextion_select(obj_buttonHotStart); //Highligh HotStart button
               
               FirstLoop=false; //Clear value update flag
            }
            
            if(val_update==true) //Val_update variable from timer for value updates 
            {
                
                param = WELD_STATE; //memory parameter -> Welding State
                controller_cache_read(param, &weldstate); //read Welding State from memory

                if(weldstate == 1) //If welding active, go to Home page
                {
                    stateMachine.c_state = HOME_CHANGE_CURRENT;//update current state to HOME CHANGE CURRENT
                    stateMachine.p_state = MMA_PG1; //update previous state to MMA PAGE 1
                    FirstLoop=true;  //FirsLoop flag set as true for the new state  
                    var_interrupt=NONE; //clear event flag
                }
                val_update=false; //Clear value update flag
            }
            var_interrupt=NONE;//clear event flag
            break;   
            
        default:
            var_interrupt=NONE;//clear event flag
            break;
    }
    return stateMachine;
}





/**
 * \brief Responsible for the MMA PULSED parameteres and value changes.
 * 
 * This routine will control the flow between the MMA Pulsed parameters aswell
 * as allowing the user to change them according to max and min value.
 * 
 * Parameters available on this page:
 * MMA HOTSTART - Min value 10, Max value 100 (%)
 * MMA HOTSTART TIME - Min value 1, Max value 20 (Seconds/10)
 * MMA INOMINAL - Min value 10, Max value 220 (A)
 * MMA IBase - Min value 50, Max value 95 (%)
 * 
 * Page 1/2 for MMA Pulsed.
 * 
 *  Events:
 * - Encoder rotation to the right or left will change the button or the value
 * of the parameter.
 * - Encoder click will allow the user to open parameter value so it can be 
 * edited if on the button, if on the value, will validate the change and save it.
 * - Encoder hold will open a Help menu for this page.
 * - Back will take the user back to Selection Menu buttons on Home menu if on 
 * buttons. If on values, will discard changes.
 * - Home will take the user back to Current Value change on Home menu.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t MMA_PULSE_parameters_page1(sm_t stateMachine)
{
    switch(var_interrupt)
    {
        /*
         * HOME EVENT
         * - State machine current state is now Home_Change_Current
         */
        case HOME:
            stateMachine.c_state = HOME_CHANGE_CURRENT; //update current state to HOME CHANGE CURRENT
            stateMachine.p_state = MMA_PULSE_PG1; //update previous state to MMA_PULSE_PG1
            FirstLoop=true;  //FirsLoop flag set as true for the new state
            var_interrupt=NONE; //Clear event flag 
            break;
        
        /*
         * BACK EVENT
         * - Return to Selection Menu (Home buttons) if on the any parameter button
         * - Return to the button if on the value change option, without validation of 
         * the value changes, reseting the previous stored value.
         */
        case BACK: 
             switch(MMA_Pulse_Buttons1)
            {
                case PULSE_HOTSTART_BUTTON: //Case button HotStart -> Go to previous state
                    stateMachine.c_state = SELECTION_MENU; //update current state to SELECTION MENU
                    stateMachine.p_state = MMA_PULSE_PG1; //update previous state to MMA_PULSE_PG1
                    FirstLoop=true; //FirsLoop flag set as true for the new state
                    break;
                    
                case PULSE_HOTSTARTTIME_BUTTON: //Case button HotStartTime -> Go to previous state
                    stateMachine.c_state = SELECTION_MENU; //update current state to SELECTION MENU
                    stateMachine.p_state = MMA_PULSE_PG1;  //update previous state to MMA_PULSE_PG1
                    FirstLoop=true; //FirsLoop flag set as true for the new state
                    break;
                    
                case PULSE_INOMINAL_BUTTON: //Case button INominal -> Go to previous state
                    stateMachine.c_state = SELECTION_MENU; //update current state to SELECTION MENU
                    stateMachine.p_state = MMA_PULSE_PG1; //update previous state to MMA_PULSE_PG1
                    FirstLoop=true; //FirsLoop flag set as true for the new state
                    break;
                     
                case PULSE_BASECURRENT_BUTTON: //Case button BaseCurrent -> Go to previous state
                    stateMachine.c_state = SELECTION_MENU; //update current state to SELECTION MENU
                    stateMachine.p_state = MMA_PULSE_PG1; //update previous state to MMA_PULSE_PG1
                    FirstLoop=true; //FirsLoop flag set as true for the new state
                    break;
                    
                case PULSE_HOTSTARTVALUE_BUTTON: //Case HotStart value option -> Go to Button HotStart
                    nextion_unselect(obj_HotStartValue); //remove highlight from HotStart value
                    nextion_select(obj_buttonHotStart); //highlight HotStart button
                    param=MMA_HOTSTART;  //memory parameter -> MMA Hotstart
                    controller_cache_read(param,&MMAHotStart); //read MMA HotStart from memory
                    utils_int_to_str(NextionMMA_HotStart,MMAHotStart,3); //Convert to string
                    nextion_change_val(obj_HotStartValue,NextionMMA_HotStart); //Send value to Nextion HotStart value object
                    MMA_Pulse_Buttons1=PULSE_HOTSTART_BUTTON; //Update MMA Pulse buttons to HotStart (button)
                    break;
                   
                case PULSE_HOTSTARTTIMEVALUE_BUTTON: //Case HotStartTime value option -> Go to Button HotStartTime
                    nextion_unselect(obj_HotStartTimeValue); //remove highlight from HotStartTime value
                    nextion_select(obj_buttonHotStartTime); //highlight HotStart button
                    param=MMA_HOTSTART_TIME; //memory parameter -> MMA Hotstart Time
                    controller_cache_read(param,&MMAHotStartTime); //read MMA HotStartTime from memory
                    utils_int_to_str(NextionMMA_HotStartTime,MMAHotStartTime,2); //Convert to string
                    nextion_change_val(obj_HotStartTimeValue,NextionMMA_HotStartTime); //Send value to Nextion HotStartTime value object
                    MMA_Pulse_Buttons1=PULSE_HOTSTARTTIME_BUTTON; //Update MMA Pulse buttons to HotStartTime (button)
                    break;
                   
                case PULSE_INOMINALVALUE_BUTTON: //Case INominal value option -> Go to Button INominal
                    nextion_unselect(obj_INominalValue); //remove highlight from INominal value
                    nextion_select(obj_buttonINominal); //highlight INominal button
                    param=MMA_INOMINAL; //memory parameter -> MMA INominal
                    controller_cache_read(param,&MMAINominal); //read MMA INominal from memory
                    utils_int_to_str(NextionMMA_INominal,MMAINominal,3); //Convert to string
                    nextion_change_val(obj_INominalValue,NextionMMA_INominal); //Send value to Nextion INominal value object
                    MMA_Pulse_Buttons1=PULSE_INOMINAL_BUTTON; //Update MMA Pulse buttons to INominal (button)
                    break;
                   
                case PULSE_BASECURRENTVALUE_BUTTON: //Case BaseCurrent value option -> Go to Button BaseCurrent
                    nextion_unselect(obj_BaseCurrentValue); //remove highlight from BaseCurrent value
                    nextion_select(obj_buttonBaseCurrent); //highlight BaseCurrent button
                    param=MMA_IBASE; //memory parameter -> MMA BaseCurrent
                    controller_cache_read(param,&MMAIBase); //read MMA BaseCurrent from memory
                    utils_int_to_str(NextionMMA_BaseCurrent,MMAIBase,2); //Convert to string
                    nextion_change_val(obj_BaseCurrentValue,NextionMMA_BaseCurrent); //Send value to Nextion BaseCurrent value object
                    MMA_Pulse_Buttons1=PULSE_BASECURRENT_BUTTON; //Update MMA Pulse buttons to BaseCurrent (button) 
                    break;
                   
                default:
                    break;
            }
            var_interrupt=NONE; //clear event flag
            break;
        
	    /*
         * ENCODER BUTTON EVENT
         * - Access value change if on button
         * - Validate changes and go to button if is on value
         */     
        case ENC_BUTTON:
           switch(MMA_Pulse_Buttons1)
            {
                case PULSE_HOTSTART_BUTTON: //Case button HotStart -> Go to value HotStart
                    nextion_unselect(obj_buttonHotStart); //remove highlight from HotStart button
                    nextion_select(obj_HotStartValue); //highlight HotStart value
                    MMA_Pulse_Buttons1=PULSE_HOTSTARTVALUE_BUTTON; //Update MMA Pulse buttons to HotStart (value)
                    break;
                    
                case PULSE_HOTSTARTTIME_BUTTON: //Case button HotStartTime -> Go to value HotStartTime
                    nextion_unselect(obj_buttonHotStartTime); //remove highlight from HotStartTime button
                    nextion_select(obj_HotStartTimeValue); //highlight HotStartTime value
                    MMA_Pulse_Buttons1=PULSE_HOTSTARTTIMEVALUE_BUTTON;//Update MMA Pulse buttons to HotStartTime (value) 
                    break;
                    
                case PULSE_INOMINAL_BUTTON: //Case button INominal -> Go to value INominal
                    nextion_unselect(obj_buttonINominal); //remove highlight from INominal button
                    nextion_select(obj_INominalValue); //highlight INominal value
                    MMA_Pulse_Buttons1=PULSE_INOMINALVALUE_BUTTON; //Update MMA Pulse buttons to INominal (value)
                    break;
                    
                case PULSE_BASECURRENT_BUTTON: //Case button BaseCurrent -> Go to value BaseCurrent
                    nextion_unselect(obj_buttonBaseCurrent);//remove highlight from BaseCurrent button
                    nextion_select(obj_BaseCurrentValue); //highlight BaseCurrent value
                    MMA_Pulse_Buttons1=PULSE_BASECURRENTVALUE_BUTTON; //Update MMA Pulse buttons to BaseCurrent (value)
                    break;
                    
                case PULSE_HOTSTARTVALUE_BUTTON: //Case value HotStart -> Go to button HotStart
                    nextion_unselect(obj_HotStartValue); //remove highlight from HotStart Value
                    nextion_select(obj_buttonHotStart);//highlight ArcForce button
                    param=MMA_HOTSTART; //memory parameter -> MMA HotStart
                    controller_cache_write((int)param,MMAHotStart); //Write the new value on memory
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    MMA_Pulse_Buttons1=PULSE_HOTSTART_BUTTON; //Update MMA Pulse buttons to HotStart (button)
                    break;
                   
                case PULSE_HOTSTARTTIMEVALUE_BUTTON: //Case value HotStartTime -> Go to button HotStartTime
                    nextion_unselect(obj_HotStartTimeValue); //remove highlight from HotStartTime Value
                    nextion_select(obj_buttonHotStartTime);//highlight HotStartTime button
                    param=MMA_HOTSTART_TIME; //memory parameter -> MMA HotStartTime
                    controller_cache_write((int)param,MMAHotStartTime); //Write the new value on memory
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    MMA_Pulse_Buttons1=PULSE_HOTSTARTTIME_BUTTON; //Update MMA Pulse buttons to HotStartTime (button)
                    break;
                   
                case PULSE_INOMINALVALUE_BUTTON: //Case value INominal -> Go to button INominal
                    nextion_unselect(obj_INominalValue);  //remove highlight from INominal Value
                    nextion_select(obj_buttonINominal); //highlight INominal button
                    param=MMA_INOMINAL; //memory parameter -> MMA INominal
                    controller_cache_write((int)param,MMAINominal); //Write the new value on memory
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    MMA_Pulse_Buttons1=PULSE_INOMINAL_BUTTON; //Update MMA Pulse buttons to INominal (button)
                    break;
                   
                case PULSE_BASECURRENTVALUE_BUTTON: //Case value BaseCurrent -> Go to button BaseCurrent
                    nextion_unselect(obj_BaseCurrentValue); //remove highlight from BaseCurrent Value
                    nextion_select(obj_buttonBaseCurrent); //highlight BaseCurrent button
                    param=MMA_IBASE; //memory parameter -> MMA BaseCurrent
                    controller_cache_write((int)param,MMAIBase); //Write the new value on memory
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    MMA_Pulse_Buttons1=PULSE_BASECURRENT_BUTTON; //Update MMA Pulse buttons to BaseCurrent (button)
                    break;
                   
                default:
                    break;
            }
            var_interrupt=NONE; //Clear event flag
            break;
        
            

        /*
         * ENCODER HOLD EVENT
         * - Change current state and previous state
         * - Open HELP page depending on the parameter currently selected
         */        
        case ENC_HOLD:
            switch(MMA_Pulse_Buttons1)
            {
                case PULSE_HOTSTART_BUTTON: //Case button HotStart
                    nextion_page(HelpPage_MMAHotStart); 
                    break;
                    
                case PULSE_HOTSTARTTIME_BUTTON: //Case button HotStartTime
                    nextion_page(HelpPage_MMAHotStartTime); 
                    break;
                    
                case PULSE_INOMINAL_BUTTON: //Case button INominal 
                    nextion_page(HelpPage_MMAInominal); 
                    break;
                     
                case PULSE_BASECURRENT_BUTTON: //Case button BaseCurrent 
                    nextion_page(HelpPage_MMAIBase); 
                    break;
                    
                case PULSE_HOTSTARTVALUE_BUTTON: //Case HotStart value option 
                    nextion_page(HelpPage_MMAHotStart); 
                    break;
                   
                case PULSE_HOTSTARTTIMEVALUE_BUTTON: //Case HotStartTime value option 
                    nextion_page(HelpPage_MMAHotStartTime); 
                    break;
                   
                case PULSE_INOMINALVALUE_BUTTON: //Case INominal value option 
                    nextion_page(HelpPage_MMAInominal); 
                    break;
                   
                case PULSE_BASECURRENTVALUE_BUTTON: //Case BaseCurrent value option 
                    nextion_page(HelpPage_MMAIBase); 
                    break;
                   
                default:
                    break;
            }
            stateMachine.c_state = HELP;
            stateMachine.p_state = MMA_PULSE_PG1;
            FirstLoop=true;
            var_interrupt=NONE;
            break;
        

        /*
         * ENCODER RIGHT EVENT
         * - If on button, move to the right button
         * - If on value, increment value its less than MaxValue
         * - Send to nextion new value
         */        
        case ENC_RIGHT:
           switch(MMA_Pulse_Buttons1)
            {
                case PULSE_HOTSTART_BUTTON: //Case button HotStart -> Go to button HotStartTime
                    nextion_unselect(obj_buttonHotStart); //remove highlight from HotStart button
                    nextion_select(obj_buttonHotStartTime); // highlight  HotStartTime button
                    MMA_Pulse_Buttons1=PULSE_HOTSTARTTIME_BUTTON; //Update MMA Pulse buttons to HotStartTime (button)
                    break;
                    
                case PULSE_HOTSTARTTIME_BUTTON: //Case button HotStartTime -> Go to button INominal
                    nextion_unselect(obj_buttonHotStartTime); //remove highlight from HotStartTime button
                    nextion_select(obj_buttonINominal); //highlight  INominal button
                    MMA_Pulse_Buttons1=PULSE_INOMINAL_BUTTON; //Update MMA Pulse buttons to INominal (button)
                    break;
                    
                case PULSE_INOMINAL_BUTTON: //Case button INominal -> Go to button ArcForce
                    nextion_unselect(obj_buttonINominal); //remove highlight from INominal button
                    nextion_select(obj_buttonBaseCurrent); //highlight  ArcForce button
                    MMA_Pulse_Buttons1=PULSE_BASECURRENT_BUTTON; //Update MMA Pulse buttons to ArcForce (button)
                    break;
                    
                case PULSE_BASECURRENT_BUTTON: //Case button BaseCurrent -> Next page 
                    stateMachine.c_state = MMA_PULSE_PG2; //update current state to MMA PULSE PAGE 2
                    stateMachine.p_state = MMA_PULSE_PG1; //update previous state to MMA PULSE PAGE 1
                    FirstLoop=true; //FirsLoop flag set as true for the new state
                    var_interrupt=NONE; //Clear event flag
                    break;
                    
                case PULSE_HOTSTARTVALUE_BUTTON: //Case HotStart value -> Increment value (if below max)
                    if(MMAHotStart<MaxHotStart) //Only increment value if below MaxValue
                    {
                        MMAHotStart++; //Increment HotStart value
                        utils_int_to_str(NextionMMA_HotStart,MMAHotStart,3); //convert to string
                        nextion_change_val(obj_HotStartValue,NextionMMA_HotStart); //Send value to Nextion
                    }
                   break;
                   
                case PULSE_HOTSTARTTIMEVALUE_BUTTON: //Case HotStartTime value -> Increment value (if below max)
                    if(MMAHotStartTime<MaxHotTime) //Only increment value if below MaxValue
                    {
                        MMAHotStartTime++; //Increment HotStartTime value
                        utils_int_to_str(NextionMMA_HotStartTime,MMAHotStartTime,2); //convert to string
                        nextion_change_val(obj_HotStartTimeValue,NextionMMA_HotStartTime); //Send value to Nextion
                    }
                   break;
                   
                case PULSE_INOMINALVALUE_BUTTON: //Case INominal value -> Increment value (if below max)
                    if(MMAINominal<MaxINominal) //Only increment value if below MaxValue
                    {
                        MMAINominal++; //Increment INominal value
                        utils_int_to_str(NextionMMA_INominal,MMAINominal,3); //convert to string
                        nextion_change_val(obj_INominalValue,NextionMMA_INominal); //Send value to Nextion
                    }
                   break;
                   
                case PULSE_BASECURRENTVALUE_BUTTON:  //Case BaseCurrent value -> Increment value (if below max)
                    if(MMAIBase<MaxBaseCurrent) //Only increment value if below MaxValue
                    {
                        MMAIBase++; //Increment BaseCurrent value
                        utils_int_to_str(NextionMMA_BaseCurrent,MMAIBase,2); //convert to string
                        nextion_change_val(obj_BaseCurrentValue,NextionMMA_BaseCurrent); //Send value to Nextion
                    }
                   break;
                   
                default:
                    break;
            }
            var_interrupt=NONE;  //Clear event flag
            break;    
        
        /*
         * ENCODER LEFT EVENT
         * - If on button, move to the left button
         * - If on value, decrement value its above MinValue
         * - Send to nextion new value
         */ 
        case ENC_LEFT:
            switch(MMA_Pulse_Buttons1)
            {
                case PULSE_HOTSTART_BUTTON: //Case button HotStart -> no more buttons to the left
                    break;
                    
                case PULSE_HOTSTARTTIME_BUTTON: //Case button HotStartTime -> go to button HotStart
                    nextion_unselect(obj_buttonHotStartTime); //remove highlight from HotStartTime button
                    nextion_select(obj_buttonHotStart); //highlight HotStart button
                    MMA_Pulse_Buttons1=HOTSTART_BUTTON; //Update MMA Pulse buttons to HotStart (button)
                    break;
                    
                case PULSE_INOMINAL_BUTTON: //Case button Inominal -> go to button HotStartTime
                    nextion_unselect(obj_buttonINominal); //remove highlight from INominal button
                    nextion_select(obj_buttonHotStartTime); //highlight HotStartTime button
                    MMA_Pulse_Buttons1=HOTSTARTTIME_BUTTON; //Update MMA Pulse buttons to HotStartTime (button)
                    break;
                    
                case PULSE_BASECURRENT_BUTTON: //Case button BaseCurrent -> go to button Inominal
                    nextion_unselect(obj_buttonBaseCurrent); //remove highlight from BaseCurrent button
                    nextion_select(obj_buttonINominal); //highlight INominal button
                    MMA_Pulse_Buttons1=INOMINAL_BUTTON; //Update MMA Pulse buttons to INominal (button)
                    break;
                    
               case PULSE_HOTSTARTVALUE_BUTTON: //Case HotStart value -> Decrement value (if above min)
                    if(MMAHotStart>MinHotStart) //Only decrement value if bove MinValue
                    {
                        MMAHotStart--; //Decrement HotStart value
                        utils_int_to_str(NextionMMA_HotStart,MMAHotStart,3); //Convert to string
                        nextion_change_val(obj_HotStartValue,NextionMMA_HotStart); //Send value to Nextion
               
                    }
                   break;
                   
                case PULSE_HOTSTARTTIMEVALUE_BUTTON:  //Case HotStartTime value -> Decrement value (if above min)
                    if(MMAHotStartTime>MinHotTime)  //Only decrement value if bove MinValue
                    {
                        MMAHotStartTime--; //Decrement HotStartTime value
                        utils_int_to_str(NextionMMA_HotStartTime,MMAHotStartTime,2); //Convert to string
                        nextion_change_val(obj_HotStartTimeValue,NextionMMA_HotStartTime); //Send value to Nextion
               
                    }
                   break;
                   
                case PULSE_INOMINALVALUE_BUTTON: //Case INominal value -> Decrement value (if above min)
                    if(MMAINominal>MinINominal) //Only decrement value if bove MinValue
                    {
                        MMAINominal--; //Decrement INominal value
                        utils_int_to_str(NextionMMA_INominal,MMAINominal,3); //Convert to string 
                        nextion_change_val(obj_INominalValue,NextionMMA_INominal);//Send value to Nextion
                    }
                   break;
                   
                case PULSE_BASECURRENTVALUE_BUTTON: //Case ArcForce value -> Decrement value (if above min)
                    if(MMAIBase>MinBaseCurrent) //Only decrement value if bove MinValue
                    {
                        MMAIBase--; //Decrement ArcForce value
                        utils_int_to_str(NextionMMA_BaseCurrent,MMAIBase,2); //Convert to string
                        nextion_change_val(obj_BaseCurrentValue,NextionMMA_BaseCurrent); //Send value to Nextion
                    }
                   break;
                   
                default:
                    break;
            }
           var_interrupt=NONE; //Clear event flag
            break;   
        
        /*
         * NONE
         * - Update values on Nextion
         * - Set nextion page
         * - Highlight First Object
         */ 
        case NONE:
            if(FirstLoop==true) //first time executing when on this state 
            {
               nextion_page(MenuPage_MMAPulse1); //Change nextion page to MMA PAGE 1
                
               param=MMA_HOTSTART;  //memory parameter -> MMA HOTSTART
               controller_cache_read(param,&MMAHotStart);  //read HotStart value from memory
               utils_int_to_str(NextionMMA_HotStart,MMAHotStart,3); //Convert to string
               nextion_change_val(obj_HotStartValue,NextionMMA_HotStart); //Send value to Nextion
               
               param=MMA_HOTSTART_TIME; //memory parameter -> MMA HOTSTART TIME
               controller_cache_read(param,&MMAHotStartTime); //read HotStartTime value from memory
               utils_int_to_str(NextionMMA_HotStartTime,MMAHotStartTime,2); //Convert to string
               nextion_change_val(obj_HotStartTimeValue,NextionMMA_HotStartTime); //Send value to Nextion
               
               param=MMA_INOMINAL; //memory parameter -> MMA INominal
               controller_cache_read(param,&MMAINominal); //read INominal value from memory
               utils_int_to_str(NextionMMA_INominal,MMAINominal,3); //Convert to string
               nextion_change_val(obj_INominalValue,NextionMMA_INominal); //Send value to Nextion
               
               
               param=MMA_IBASE;//memory parameter -> MMA Base current
               controller_cache_read(param,&MMAIBase); //read BaseCurrent value from memory
               utils_int_to_str(NextionMMA_BaseCurrent,MMAIBase,2); //Convert to string
               nextion_change_val(obj_BaseCurrentValue,NextionMMA_BaseCurrent); //Send value to Nextion
               
               /*
                Remove all highlights when page is loaded
                */
               nextion_unselect(obj_buttonHotStart); //remove highlight from HotStart button
               nextion_unselect(obj_HotStartValue);  //remove highlight from HotStart value
               nextion_unselect(obj_buttonHotStartTime); //remove highlight from HotStartTime button
               nextion_unselect(obj_HotStartTimeValue); //remove highlight from HotStartTime value
               nextion_unselect(obj_buttonINominal); //remove highlight from INominal button
               nextion_unselect(obj_INominalValue); //remove highlight from INominal value
               nextion_unselect(obj_buttonBaseCurrent);  //remove highlight from BaseCurrent button
               nextion_unselect(obj_BaseCurrentValue); //remove highlight from BaseCurrent value
               nextion_checkbox_off(obj_buttonMarker2); //remove highlight from Page2 marker
               
                /*
                Highlights Page marker
                */
               nextion_checkbox_on(obj_buttonMarker1); //highlight first page marker
               
               /*
                * Select the button depending of previous page
                */ 
               if(stateMachine.p_state==MMA_PULSE_PG2) //If previous page was page 2
               {
                    nextion_select(obj_buttonBaseCurrent); //select bottom right object
                    MMA_Pulse_Buttons1=PULSE_BASECURRENT_BUTTON;  //Update MMA Pulse buttons to BaseCurrent (button)
               }else{
                    nextion_select(obj_buttonHotStart); //select top left object
                    MMA_Pulse_Buttons1=PULSE_HOTSTART_BUTTON; //Update MMA Pulse buttons to HotStart (button)
               }
             
               
               FirstLoop=false; //Clear value update flag
            }
            if(val_update==true) //Val_update variable from timer for value updates 
            { 
                
                param = WELD_STATE; //memory parameter -> Welding State
                controller_cache_read(param, &weldstate); //read Welding State from memory

                if(weldstate == 1) //If welding active, go to Home page
                {
                    stateMachine.c_state = HOME_CHANGE_CURRENT; //update current state to HOME CHANGE CURRENT
                    stateMachine.p_state = MMA_PULSE_PG1; //update previous state to MMA PULSE PAGE 1
                    FirstLoop=true; //FirsLoop flag set as true for the new state  
                    var_interrupt=NONE; //clear event flag
                }
                val_update=false; //Clear value update flag
            }
            var_interrupt=NONE; //clear event flag
            break;   
            
        default:
            var_interrupt=NONE; //clear event flag
            break;
    }
    return stateMachine;
}





/**
 * \brief Responsible for the MMA PULSED parameteres and value changes (2nd page).
 * 
 * This routine will control the flow between the MMA Pulsed parameters aswell
 * as allowing the user to change them according to max and min value.
 * 
 * Parameters available on this page:
 * MMA PULSE WIDTH - Min value 10, Max value 90 (%)
 * MMA PULSE FREQUENCY - Min value 5, Max value 100 (Hz/10)
 * 
 * Page 2/2 for MMA Pulsed.
 * 
 *  Events:
 * - Encoder rotation to the right or left will change the button or the value
 * of the parameter.
 * - Encoder click will allow the user to open parameter value so it can be 
 * edited if on the button, if on the value, will validate the change and save it.
 * - Encoder hold will open a Help menu for this page.
 * - Back will take the user back to Selection Menu buttons on Home menu if on 
 * buttons. If on values, will discard changes.
 * - Home will take the user back to Current Value change on Home menu.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t MMA_PULSE_parameters_page2(sm_t stateMachine)
{
    switch(var_interrupt)
    {
        /*
         * HOME EVENT
         * - State machine current state is now Home_Change_Current
         */
        case HOME:
            stateMachine.c_state = HOME_CHANGE_CURRENT; //update current state to HOME CHANGE CURRENT
            stateMachine.p_state = MMA_PULSE_PG2; //update previous state to MMA_PULSE_PG2
            FirstLoop=true; //FirsLoop flag set as true for the new state
            var_interrupt=NONE;//Clear event flag 
            break;
        
        /*
         * BACK EVENT
         * - Return to MMA PULSE PAGE 1 if on the any parameter button
         * - Return to the button if on the value change option, without validation of 
         * the value changes, reseting the previous stored value.
         */
        case BACK: 
            switch(MMA_Pulse_Buttons2)
            {
                case PULSE_WIDTH_BUTTON: //Case button PulseWidth -> Go to previous state
                    stateMachine.c_state = MMA_PULSE_PG1; //update current state to MMA_PULSE_PG1
                    stateMachine.p_state = MMA_PULSE_PG2; //update previous state to MMA_PULSE_PG2
                    FirstLoop=true; //FirsLoop flag set as true for the new state
                    break;
                    
                case PULSE_FREQ_BUTTON: //Case button PulseFeq -> Go to previous state
                    stateMachine.c_state = MMA_PULSE_PG1; //update current state to MMA_PULSE_PG1
                    stateMachine.p_state = MMA_PULSE_PG2; //update previous state to MMA_PULSE_PG2
                    FirstLoop=true; //FirsLoop flag set as true for the new state
                    break; 
                    
                case PULSE_WIDTHVALUE_BUTTON: //Case PulseWidth value option -> Go to Button PulseWidth
                    nextion_unselect(obj_WidthValue); //remove highlight from PulseWidth value
                    nextion_select(obj_buttonWidth);//highlight PulseWidth button
                    param=MMA_PULSE_WIDTH; //memory parameter -> MMA Pulse Width
                    controller_cache_read(param,&MMAWidthval); //read MMA Pulse Width from memory
                    utils_int_to_str(NextionMMA_Width,MMAWidthval,2); //Convert to string
                    nextion_change_val(obj_WidthValue,NextionMMA_Width); //Send value to Nextion
                    MMA_Pulse_Buttons2=PULSE_WIDTH_BUTTON;  //Update MMA Pulse buttons to Pulse Width (button)
                    break;
                   
                case PULSE_FREQVALUE_BUTTON: //Case PulseFreq value option -> Go to Button PulseFreq
                    nextion_unselect(obj_PulseFreqValue); //remove highlight from PulseFreq value
                    nextion_select(obj_buttonPulseFreq);//highlight PulseFreq button
                    param=MMA_PULSE_FREQ; //memory parameter -> MMA Pulse Freq
                    controller_cache_read(param,&MMAPFreq); //read MMA Pulse Freq from memory
                    utils_int_to_str(NextionMMA_PulseFreq,MMAPFreq,3); //Convert to string
                    nextion_change_val(obj_PulseFreqValue,NextionMMA_PulseFreq); //Send value to Nextion
                    MMA_Pulse_Buttons2=PULSE_FREQ_BUTTON;  //Update MMA Pulse buttons to Pulse Freq (button)
                    break;
                   
                default:
                    break;
            }
            var_interrupt=NONE; //clear event flag
            break;
        
	    /*
         * ENCODER BUTTON EVENT
         * - Access value change if on button
         * - Validate changes and go to button if is on value
         */   
        case ENC_BUTTON:
           switch(MMA_Pulse_Buttons2)
            {
                case PULSE_WIDTH_BUTTON: //Case button PulseWidth -> Go to value PulseWidth
                    nextion_unselect(obj_buttonWidth); //remove highlight from PulseWidth button
                    nextion_select(obj_WidthValue); //highlight PulseWidth value
                    MMA_Pulse_Buttons2=PULSE_WIDTHVALUE_BUTTON; //Update MMA Pulse buttons to PulseWidth (value)
                    break;
                    
                case PULSE_FREQ_BUTTON: //Case button PulseFreq -> Go to value PulseFreq
                    nextion_unselect(obj_buttonPulseFreq); //remove highlight from PulseFreq button
                    nextion_select(obj_PulseFreqValue); //highlight PulseFreq value
                    MMA_Pulse_Buttons2=PULSE_FREQVALUE_BUTTON; //Update MMA Pulse buttons to PulseFreq (value)
                    break;
                    
                case PULSE_WIDTHVALUE_BUTTON: //Case value PulseWidth -> Go to button PulseWidth
                    nextion_unselect(obj_WidthValue); //remove highlight from PulseWidth Value
                    nextion_select(obj_buttonWidth); //highlight PulseWidth button
                    param=MMA_PULSE_WIDTH; //memory parameter -> MMA PulseWidth
                    controller_cache_write((int)param,MMAWidthval);  //Write the new value on memory
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    MMA_Pulse_Buttons2=PULSE_WIDTH_BUTTON; //Update MMA Pulse buttons to PulseWidth (button)
                    break;
                   
                case PULSE_FREQVALUE_BUTTON: //Case value PulseFreq -> Go to button PulseFreq
                    nextion_unselect(obj_PulseFreqValue); //remove highlight from PulseFreq Value
                    nextion_select(obj_buttonPulseFreq); //highlight PulseFreq button
                    param=MMA_PULSE_FREQ; //memory parameter -> MMA PulseFreq
                    controller_cache_write((int)param,MMAPFreq);  //Write the new value on memory
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    MMA_Pulse_Buttons2=PULSE_FREQ_BUTTON; //Update MMA Pulse buttons to PulseFreq (button)
                    break;
                   
                default:
                    break;
            }
            var_interrupt=NONE; //Clear event flag
            break;
        
            
         /*
         * ENCODER HOLD EVENT
         * - Change current state and previous state
         * - Open HELP page depending on the parameter currently selected
         */       
        case ENC_HOLD:
            switch(MMA_Pulse_Buttons2)
            {
                case PULSE_WIDTH_BUTTON: //Case button PulseWidth -> Go to previous state
                    nextion_page(HelpPage_MMAWidth);
                    break;
                    
                case PULSE_FREQ_BUTTON: //Case button PulseFeq -> Go to previous state
                    nextion_page(HelpPage_MMAFreq);
                    break; 
                    
                case PULSE_WIDTHVALUE_BUTTON: //Case PulseWidth value option -> Go to Button PulseWidth
                    nextion_page(HelpPage_MMAWidth);
                    break;
                   
                case PULSE_FREQVALUE_BUTTON: //Case PulseFreq value option -> Go to Button PulseFreq
                    nextion_page(HelpPage_MMAFreq);
                    break;
                   
                default:
                    break;
            }
            stateMachine.c_state = HELP;
            stateMachine.p_state = MMA_PULSE_PG2;
            FirstLoop=true;
            var_interrupt=NONE;
            break;
        
            
         /*
         * ENCODER RIGHT EVENT
         * - If on button, move to the right button
         * - If on value, increment value its less than MaxValue
         * - Send to nextion new value
         */           
        case ENC_RIGHT:
            switch(MMA_Pulse_Buttons2)
            {
                case PULSE_WIDTH_BUTTON: //Case button PulseWidth -> Go to button PulseFreq
                    nextion_unselect(obj_buttonWidth); //remove highlight from PulseWidth button
                    nextion_select(obj_buttonPulseFreq); // highlight  PulseFreq button
                    MMA_Pulse_Buttons2=PULSE_FREQ_BUTTON; //Update MMA Pulse buttons to PulseWidth (button)
                    break;
                    
                case PULSE_FREQ_BUTTON: //Case button BaseCurrent -> no button to the right 
                    break;
                    
                case PULSE_WIDTHVALUE_BUTTON: //Case PulseWidth value -> Increment value (if below max)
                    if(MMAWidthval<MaxWidth) //Only increment value if below MaxValue
                    {
                        MMAWidthval++; //Increment PulseWidth value
                        utils_int_to_str(NextionMMA_Width,MMAWidthval,2); //convert to string
                        nextion_change_val(obj_WidthValue,NextionMMA_Width);  //Send value to Nextion         
                    }
                   break;
                   
                case PULSE_FREQVALUE_BUTTON: //Case PulseFreq value -> Increment value (if below max)
                    if(MMAPFreq<MaxPulseFreq) //Only increment value if below MaxValue
                    {
                        MMAPFreq++; //Increment PulseFreq value
                        utils_int_to_str(NextionMMA_PulseFreq,MMAPFreq,3); //convert to string
                        nextion_change_val(obj_PulseFreqValue,NextionMMA_PulseFreq);   //Send value to Nextion              
                    }
                   break;
                   
                default:
                    break;
            }
            var_interrupt=NONE; //Clear event flag
            break;    
        

        /*
         * ENCODER LEFT EVENT
         * - If on button, move to the left button
         * - If on value, decrement value its above MinValue
         * - Send to nextion new value
         */ 
        case ENC_LEFT:
            switch(MMA_Pulse_Buttons2)
            {
                case PULSE_WIDTH_BUTTON: //Case button PulseWidth -> Go to previous page
                    stateMachine.c_state = MMA_PULSE_PG1; //update current state to MMA PULSE PAGE 1
                    stateMachine.p_state = MMA_PULSE_PG2; //update current state to MMA PULSE PAGE 2
                    FirstLoop=true;  //FirsLoop flag set as true for the new state
                    var_interrupt=NONE; //Clear event flag
                    break;
                    
                case PULSE_FREQ_BUTTON:  //Case button PulseFreq -> go to button PulseWidth
                    nextion_unselect(obj_buttonPulseFreq); //remove highlight from PulseFreq button
                    nextion_select(obj_buttonWidth);  //highlight PulseWidth button
                    MMA_Pulse_Buttons2=PULSE_WIDTH_BUTTON; //Update MMA Pulse buttons to PulseWidth (button)
                    break;
                    
                    
               case PULSE_WIDTHVALUE_BUTTON: //Case PulseWidth value -> Decrement value (if above min)
                    if(MMAWidthval>MinWidth) //Only decrement value if bove MinValue
                    {
                        MMAWidthval--; //Decrement PulseWidth value
                        utils_int_to_str(NextionMMA_Width,MMAWidthval,2); //Convert to string
                        nextion_change_val(obj_WidthValue,NextionMMA_Width); //Send value to Nextion
                    }
                   break;
                   
                case PULSE_FREQVALUE_BUTTON: //Case PulseFreq value -> Decrement value (if above min)
                     if(MMAPFreq>MinPulseFreq) //Only decrement value if bove MinValue
                    {
                        MMAPFreq--; //Decrement PulseFreq value
                        utils_int_to_str(NextionMMA_PulseFreq,MMAPFreq,3); //Convert to string
                        nextion_change_val(obj_PulseFreqValue,NextionMMA_PulseFreq); //Send value to Nextion
                    }
                   break;
                   
                default:
                    break;
            }
           var_interrupt=NONE; //clear event flag
            break;   
        
        /*
         * NONE
         * - Update values on Nextion
         * - Set nextion page
         * - Highlight First Object
         */ 
        case NONE:
            if(FirstLoop==true) //first time executing when on this state 
            {
               nextion_page(MenuPage_MMAPulse2); //Change nextion page to MMA PAGE 2
                
               MMA_Pulse_Buttons2=PULSE_WIDTH_BUTTON;  //Update MMA Pulse buttons to Pulse Width (button)
               
               param=MMA_PULSE_WIDTH;  //memory parameter -> MMA Pulse Width
               controller_cache_read(param,&MMAWidthval); //read PulseWidth value from memory
               utils_int_to_str(NextionMMA_Width,MMAWidthval,2); //Convert to string
               nextion_change_val(obj_WidthValue,NextionMMA_Width); //Send value to Nextion
               
               param=MMA_PULSE_FREQ; //memory parameter -> MMA Pulse Freq
               controller_cache_read(param,&MMAPFreq); //read PulseFreq value from memory
               utils_int_to_str(NextionMMA_PulseFreq,MMAPFreq,3); //Convert to string
               nextion_change_val(obj_PulseFreqValue,NextionMMA_PulseFreq); //Send value to Nextion
               
               /*
                Remove all highlights when page is loaded
                */
               nextion_unselect(obj_buttonWidth); //remove highlight from PulseWidth button
               nextion_unselect(obj_WidthValue); //remove highlight from PulseWidth value
               nextion_unselect(obj_buttonPulseFreq); //remove highlight from PulseFreq button
               nextion_unselect(obj_PulseFreqValue); //remove highlight from PulseFreq value
               nextion_checkbox_off(obj_buttonMarker1); //remove highlight first page marker
               
                /*
                Highlights Page marker
                */
               nextion_checkbox_on(obj_buttonMarker2);//highlight second page maker
               
               /*
                * Select the top left button
                */ 
               nextion_select(obj_buttonWidth); //highlight first button
               
               FirstLoop=false; //Clear value update flag
            }
            if(val_update==true) //Val_update variable from timer for value updates 
            {
                
                param = WELD_STATE; //memory parameter -> Welding State
                controller_cache_read(param, &weldstate); //read Welding State from memory

                if(weldstate == 1) //If welding active, go to Home page
                {
                    stateMachine.c_state = HOME_CHANGE_CURRENT; //update current state to HOME CHANGE CURRENT
                    stateMachine.p_state = MMA_PULSE_PG2; //update previous state to MMA Pulse page 2
                    FirstLoop=true; //FirsLoop flag set as true for the new state  
                    var_interrupt=NONE; //clear event flag
                }
                val_update=false; //Clear value update flag
            }
            var_interrupt=NONE; //clear event flag
            break;   
            
        default:
            var_interrupt=NONE; //clear event flag
            break;
    }
    return stateMachine;
}
