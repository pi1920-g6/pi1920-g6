/**
 * \file    HelpSM.c
 */
/**
 * \addtogroup  smHp HelpSM
 * \ingroup sm
 * @{
 *  \ref HelpSM.c
 * @}
 */
#include <stdbool.h>

#include "SM_structures.h"
#include "HelpSM.h"
#include "defines.h"
#include "nextion.h"
#include "utils.h"
#include "controller.h"

#ifndef __UNITTESTING__
    #include <xc.h>
    #include <sys/attribs.h>
#else
    #include "../../XC32_mock/mem_map.h"
#endif


/**
 * \brief Responsible for the Help Menu.
 * 
 * This routine will control de Help menu, showing user information
 * about the previous menu.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return State Machine current-state and previous-state.
 */
sm_t help_menu(sm_t stateMachine)
{
    switch(var_interrupt)
    {
        /*
         * HOME EVENT
         * - Go to home menu
         */
        case HOME:
            stateMachine.c_state = HOME_CHANGE_CURRENT;
            FirstLoop=true;
            var_interrupt=NONE;
            break;
        
        /*
         * BACK EVENT
         * - Go to previous menu
         */
        case BACK: 
            stateMachine.c_state =  stateMachine.p_state;
            FirstLoop=true;
            var_interrupt=NONE;
            break;
        
        /*
         * ENCODER BUTTON EVENT
         * - do nothing
         */
        case ENC_BUTTON:
            stateMachine.c_state =  stateMachine.p_state;
            FirstLoop=true;
            var_interrupt=NONE;
            break;
        
            
        /*
         * ENCODER HOLD EVENT
         * - do nothing
         */        
        case ENC_HOLD:
            var_interrupt=NONE;
            break;
        
            
        /*
         * ENCODER RIGHT EVENT
         * - Do nothing
         */        
        case ENC_RIGHT:
            var_interrupt=NONE; 
            break;    
        
        /*
         * ENCODER LEFT EVENT
         * - Do nothing
         */ 
        case ENC_LEFT:
           var_interrupt=NONE;
            break;   
        
        /*
         * NONE
         * - Do nothing
         */ 
        case NONE:
            var_interrupt=NONE;
            break;   
            
        default:
            var_interrupt=NONE;
            break;
    }
    return stateMachine;
}