/**
 * \file    uart1.c
 * \brief   UART device-driver.
 */
/**
 * \defgroup dd Device drivers
 */
/**
 * \addtogroup  uart1 UART1
 * \ingroup dd
 * @{
 *  \ref uart1.c
 * @}
 */
#include <xc.h>
#include <stdint.h>
#include <sys/attribs.h>
#include <stdbool.h>

#include "uart1.h"

void uart1_init(void)
{
	U1MODE = 0x0000;
	U1STA	= 0x0000;
	
    U1ABRG=21; 
	U1MODEbits.BRGH = 0;
	
    U1MODEbits.SIDL = 0;	// Continue operation in idle mode 
	U1MODEbits.IREN = 0;	// Disable Irda
	U1MODEbits.UEN = 0;	// Only use TX and RX pints. No HW flow control 
	U1MODEbits.WAKE = 0;	// Wake -up on start bit disabled
	U1MODEbits.LPBACK = 0; // No loopback  
	U1MODEbits.ABAUD = 0;	// No autobaud
	U1MODEbits.RXINV = 0;	// Idle logic value is 1
	U1MODEbits.PDSEL = 0;	// 8 bit data, no parity
	U1MODEbits.STSEL = 0;	// 1 stop bit
	U1STAbits.ADM_EN = 0;	// No automatic address detection
	U1STAbits.UTXISEL = 0;	// Interrupt when TX buffer has at least 1 empty position
	U1STAbits.UTXINV = 0;	// Idle logic value is 1
	
	U1MODEbits.ON = 1;
	U1STAbits.UTXEN = 1;
	U1STAbits.URXEN = 1;
}

void uart1_write(uint8_t byte)
{
    U1TXREG = byte;                        // Copy char c to U4TXREG register
    while (U1STAbits.UTXBF == 1);       // Wait while transmit buffer is full 
}

bool uart1_read(uint8_t *byte)
{
	if (U1STAbits.OERR || U1STAbits.FERR || U1STAbits.PERR)
	{
		U1RXREG;
		U1STAbits.OERR = 0;
		return false;
	}

	if (U1STAbits.URXDA)
	{
		*byte = U1ARXREG; // get data from UART RX FIFO
		
		return true;
	}
	return false;
}

void uart1_read_string(uint8_t *string){

    uint8_t i = 0;
    while(U1STAbits.URXDA){
        uint8_t c;
        if(uart1_read(&c)){
            *(string+i) = c;
            i++;
        }
    }
}