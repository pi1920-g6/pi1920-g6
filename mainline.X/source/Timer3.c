/**
 * \file    Timer3.c
 * \brief   Timer3 device-driver.
 */
/**
 * \defgroup dd Device drivers
 */
/**
 * \addtogroup  tim3 Timer3
 * \ingroup dd
 * @{
 *  \ref Timer3.c
 * @}
 */

#include <xc.h>
#include <sys/attribs.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include "Timer3.h"
#include "GeneralConfigs.h"
#include "SM_structures.h"
#include "nextion.h"

#define TIMER_CON_ENABLE        T3CONbits.ON
#define TIMER_CON_PRESCALER     T3CONbits.TCKPS
#define TIMER_CON_32BIT         T3CONbits.T32
#define TIMER_INT_ENABLE        IEC0bits.T3IE
#define TIMER_INT_FLAG          IFS0bits.T3IF
#define TIMER_INT_PRIORITY      IPC3bits.T3IP

void Timer3Init(void){
    T3CONbits.ON = 0; // Stop timer
    T3CONbits.SIDL = 0; // Continue operation when in IDLE mode
    T3CONbits.TGATE = 0; // Gate time accumulation is disabled
    T3CONbits.TCKPS = 7; // 256  prescaler
    // interrupts
    IEC0bits.T3IE = 1; // Disable timer 3 interrupts
    IFS0bits.T3IF = 0; // Reset timer 3 interrupt flag
    IPC3bits.T3IP = 4; // Interrupt priority
    IPC3bits.IC3IS = 1; // Interrupt subpriorities
    PR3=0x3D09; // Period set to 0,2s
    TMR3=0;
    T3CONbits.ON=1; // Start the timer
}

/*
 *  INTERRUPT TIMER 
 *  toggles on the nextion update variable
 */
void __ISR (_TIMER_3_VECTOR, IPL4SOFT) T3Interrupt(void)
{
    val_update=true; //update values flag
    TIMER_INT_FLAG  = 0; 
}