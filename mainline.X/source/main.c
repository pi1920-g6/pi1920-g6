#include "config_bits.h"

#include <xc.h>
#include <sys/attribs.h>
#include <math.h>
#include <ieee754.h>
#include <stdio.h>
#include <stdbool.h>

#include "SM_structures.h"
#include "GeneralConfigs.h"
#include "Timer2.h"
#include "Timer3.h"
#include "ChangeNotification.h"
#include "stateMachine.h"
#include "uart1.h"
#include "nextion.h"
#include "controller.h"
#include "i2c4.h"

//apagar
#include "defines.h"

#ifdef __UNITTESTING__
#warning    A compilar para unit tests
#endif
/*
 * 
 */

volatile event var_interrupt;
volatile bool FirstLoop;
volatile bool val_update;

/** \brief   Variable used to identify the Loading screen page. */
static char LoadingScreen[3] = "16";

int main(int argc, char **argv)
{



    Pinconfig();
    Timer2Init();
    Timer3Init();
    CNInterruptinit();
	controller_init();
	i2c4_init(I2C1_SLAVE_ADDRESS, I2C1_SLAVE_NO_MASK);
    uart1_init();
    nextion_page(LoadingScreen);
    
    
    /*
     * Init state Machine
     */
    sm_t stateMachine;
    stateMachine.c_state=HOME_CHANGE_CURRENT;
    stateMachine.p_state=HOME_CHANGE_CURRENT;
    
    /*
     * Extern variables init
     */
    var_interrupt=NONE;
    FirstLoop=true;
    val_update=false;
    programID_memory=0;
    
    //Set interrupt cable as 0 on init
    LATDbits.LATD8=0;
       
    
    while (1)
    {
		// detect stop condition
		if (i2c4_stop_condition()) {
			controller_inbound_buffer_save();
		}
		
       stateMachine=HomeStateMachine(stateMachine);
    }

    return (EXIT_SUCCESS);
}
