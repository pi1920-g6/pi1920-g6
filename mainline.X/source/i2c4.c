/**
 * \file    i2c4.c
 * \brief   I2C4 device-driver.
 */
/**
 * \defgroup dd Device drivers
 */
/**
 * \addtogroup  i2c I2C
 * \ingroup dd
 * @{
 *  \ref i2c4.c
 * @}
 */

#ifndef __UNITTESTING__
#include <xc.h>
#include <sys/attribs.h>
#else
#include "../../XC32_mock/mem_map.h"
#endif

#include <stdint.h>
#include <stdbool.h>

#include "i2c4.h"
#include "controller.h"

/* 
 * Macros related to the I2C peripheral registers.
 */

/** @brief Transmit register     */
#define I2C_TRANSMIT_REGISTER	I2C4TRN
/** @brief Receive register      */
#define I2C_RECEIVE_REGISTER	I2C4RCV 
/** @brief Address register      */
#define I2C_ADDRESS_REGISTER	I2C4ADD 
/** @brief Address mask register */
#define I2C_MASK_REGISTER		I2C4MSK
/** @brief Control register      */
#define I2C_CONTROL_REGISTER	I2C4CON
/* 
 * Control register bits
 */
/** @brief Enable bit. */
#define I2C_CONTROL_ENABLE_BIT          I2C4CONbits.ON    
/** @brief SCLx Release Control bit. */
#define I2C_CONTROL_SCL_RELEASE_BIT     I2C4CONbits.SCLREL 
/** @brief Slew Rate Control Disable bit. */
#define I2C_CONTROL_SLEW_RATE_BIT       I2C4CONbits.DISSLW 
/** @brief Reserved Address Rule Enable bit. */
#define I2C_CONTROL_STRIC_ADDRESS_BIT   I2C4CONbits.STRICT 

/* 
 * Status register bits
 */
/** @brief Read/Write Information bit. */
#define I2C_STATUS_READ_NOT_WRITE_BIT   I2C4STATbits.R_W   
/** @brief Data/Address bit.  */
#define I2C_STATUS_DATA_NOT_ADDRESS_BIT I2C4STATbits.D_A   
/** @brief General Call Status bit. */
#define I2C_STATUS_GENERAL_CALL_BIT     I2C4STATbits.GCSTAT 
/** @brief Acknowledge Status bit. */
#define I2C_STATUS_ACKNOWLEDGE_BIT      I2C4STATbits.ACKSTAT
/** @brief Start bit. */
#define I2C_STATUS_START_BIT            I2C4STATbits.S     
/** @brief Stop bit. */
#define I2C_STATUS_STOP_BIT             I2C4STATbits.P    
/** @brief Receive buffer full status bit. */
#define I2C_STATUS_RECEIVE_BUFFER_BIT   I2C4STATbits.RBF  
/** @brief Transmit buffer full status bit. */
#define I2C_STATUS_TRANSMIT_BUFFER_BIT  I2C4STATbits.TBF    

/* 
 * Interrupts
 */
/** @brief Interrupt enable. */
#define I2C_INT_ENABLE                  IEC1bits.I2C4SIE     
/** @brief Interrupt priority. */
#define I2C_INT_PRIORITY                IPC7bits.I2C4IP    
/** @brief Interrupt flag. */
#define I2C_INT_FLAG                    IFS1bits.I2C4SIF   

/*
 * General macros
 */
#define I2C_NUM_RSR_BUS_ADDR 13 /* Number of reserved bus addresses */

/*
 * Variables
 */
/** \brief   Flag that signals if a new I2C1 slave event happened. */
volatile static bool i2c4_slave_event_flag = false;
/*
 * Routines
 */

/**
 * \brief   Checks if a 7-bit address if valid.
 * 
 * This function checks if a- 7-bit-address is valid by comparing it
 * against the reserved bus addresses. If the address itself or any
 * address resulting from the given mask is equal to at least one of
 * the reserved bus list it will be deemed invalid.
 * 
 * \param   address     Address to be checked.
 * 
 * \return  True if the address is valid, False otherwise.
 */
#ifndef __UNITTESTING__
static bool i2c4_is_valid_7_bit_addr(uint8_t address)
#else

bool i2c1_is_valid_7_bit_addr(uint8_t address)
#endif
{
    /* List of reserved I2C Bus Address */
    const uint8_t reserved_addr[I2C_NUM_RSR_BUS_ADDR] = {
        0b00000000U, 0b00000001U, 0b00000010U, 0b00000011U,
        0b00000100U, 0b00000101U, 0b00000110U, 0b00000111U,
        0b01111000U, 0b01111100U, 0b01111101U, 0b01111110U,
        0b01111111U
    };

    /* Iterate every reserved i2c bus address*/
    uint8_t i;
    for (i = 0; i < I2C_NUM_RSR_BUS_ADDR; i++)
    {
        /* Compare the given address to every address of reserved 
         * bus addresses. The address is 7-bit, so the 8th bit is 
         * not relevant and therefore ignored.
         */
        if ((address & 0x7f) == reserved_addr[i])
        {
            return false;
        }
    }
    return true;
}

/**
 * \brief Initialises the I2C1 peripheral.
 * 
 * This routine initialises the I2C1 peripheral with an address \c address 
 * masked by a \c mask. If the given address is a reserved I2C address the
 * initialisation fails.
 * 
 * \param address	I2C slave address.
 * \param mask		I2C slave address mask.
 * 
 * \return \c I2C_SUCCESS if is initialised, \c I2C_INVALID_ADDRESS otherwise.
 */
i2c_error_t i2c4_init(uint8_t address, uint8_t mask)
{
    /* Disable module and clear configuration */
    I2C_CONTROL_REGISTER = 0;
    /* Configure the module */
    I2C_CONTROL_SLEW_RATE_BIT = 1; /* Enable slew rate */
    I2C_CONTROL_STRIC_ADDRESS_BIT = 1; /* Enable strict address rule */

    /* If the strict address rule is enabled, check if given address 
     * is reserved */
    if (I2C_CONTROL_STRIC_ADDRESS_BIT == 1)
    {
        if (i2c4_is_valid_7_bit_addr(address) == true)
        {
            I2C_ADDRESS_REGISTER = address;
            I2C_MASK_REGISTER = mask;
        }
        else
        {
            return I2C_INVALID_ADDRESS;
        }
    }
    /*
     * Configure interrupt with priority 6.
     */
    I2C_INT_ENABLE = 1;
    I2C_INT_PRIORITY = 6;
    I2C_INT_FLAG = 0;
    /* Enable the I2C peripheral */
    I2C_CONTROL_ENABLE_BIT = 1;

    return I2C_SUCCESS;
}

/**
 * \brief   Detects if a Stop condition has occurred.
 * 
 * After the detection of a Stop event, the slave module sets the Stop bit and 
 * clears the Start bit. Those bits will remain unchanged until a Start or
 * Repeated Start event occurred, so an additional flag is used and cleared when
 * this function is called.  The flag is set at every slave interrupt.
 * 
 * 
 * \return \c true if a new stop condition has been detected, \c false otherwise.
 */
bool i2c4_stop_condition(void)
{
    if (I2C_STATUS_STOP_BIT == 1 && I2C_STATUS_START_BIT == 0)
    {
        if (i2c4_slave_event_flag == true)
        {
            i2c4_slave_event_flag = false;
            return true;
        }
    }
    i2c4_slave_event_flag = false;
    return false;
}

#ifndef __UNITTESTING__

/**
 * \brief   This interrupt handles read and write requests for the I2C1 peripheral.
 * 
 * This interrupt is generated on the falling edge of the ninth SCLx clock.
 * Whenever a interrupt occurred, initially it is checked if the last byte was an
 * address or an acknowledge and then if it was a read or write request.
 * 
 * If it was an address and read request, some data is sent to master, else 
 * nothing is done. In both cases, the receive buffer is cleared. 
 * Otherwise, if it is received, that data is added to a buffer, else, if is an 
 * acknowledge a data from a buffer is sent.
 * 
 * At the end of every interrupt, a flag is set signalling that a new event happened.
 */
void __ISR(_I2C_4_VECTOR, IPL6SOFT) _i2c4_handler(void)
{
    if (I2C_STATUS_DATA_NOT_ADDRESS_BIT)
    {
        if (I2C_STATUS_READ_NOT_WRITE_BIT)
        {
            uint8_t tx_buf;
            controller_outbound_buffer_dequeue(&tx_buf);
            I2C_TRANSMIT_REGISTER = (volatile uint8_t) tx_buf;
            i2c4_read_write_flag = true;

            I2C_CONTROL_SCL_RELEASE_BIT = 1;
        }
        else
        {
            controller_inbound_buffer_enqueue(I2C_RECEIVE_REGISTER);
            i2c4_read_write_flag = false;
        }
    }
    else
    {
        I2C_RECEIVE_REGISTER; /* clear buffer */
        if (I2C_STATUS_READ_NOT_WRITE_BIT)
        {
            uint8_t tx_buf;
            controller_outbound_buffer_dequeue(&tx_buf);
            I2C_TRANSMIT_REGISTER = (volatile uint8_t) tx_buf;
            i2c4_read_write_flag = true;
            I2C_CONTROL_SCL_RELEASE_BIT = 1;
        }
    }
    i2c4_slave_event_flag = true;
    I2C_INT_FLAG = 0;
}
#endif
