#include <xc.h>
#include <sys/attribs.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "ChangeNotification.h"
#include "GeneralConfigs.h"
#include "SM_structures.h"

//FALLING EDGE SET TO 0
#define FallingEdge 0
#define ToleranceEncoder 1

//DEFINES
#define CN_CON_ENABLE           CNCONbits.ON
#define CNEN_CON_CN16           CNENbits.CNEN16
#define CN_INT_ENABLE           IEC1bits.CNIE 
#define CN_INT_FLAG             IFS1bits.CNIF 
#define CN_INT_PRIORITY         IPC6bits.CNIP
#define CN_INT_SUBPRIORITY      IPC6bits.CNIS


//FALLING EDGE SET TO 0
#define FallingEdge 0

/*
* FUNCTION: CNInterruptinit()
* INPUTS:   -
* OUTPUTS:  -
* SUMMARY:  This function will configure the Change Notice (CN) interrupt.
* LIST:     Activate change notice system and define CN used. 
 *          Define priority and enable the interrupts. 
*        
*           
*/


void CNInterruptinit(void){

    CN_CON_ENABLE = 1; // Activate change notice system
    CNEN_CON_CN16 =1; //CN16
    CN_INT_PRIORITY = 7; // Interrupt priority of 2
    CN_INT_SUBPRIORITY = 2; // Set sub-priority of 2
    CN_INT_FLAG = 0; // reset interrupt flag
    CN_INT_ENABLE = 1; // Enable interrupt
    
    
    encrotation=0;
    
}

/*
* INTERRUPT: CHANGE NOTICE
* SUMMARY:   Detects a turn on the encoder.
* LIST:      Detect when the encoder turns right or turns left.

*/

void __ISR(_CHANGE_NOTICE_VECTOR,IPL7SOFT) isrEncoder(void) 
{
    if(encrotation==ToleranceEncoder)
    {
        if(PORTDbits.RD7 == FallingEdge)
        {
                    if(PORTEbits.RE3 == FallingEdge)
                    {
                        var_interrupt=ENC_RIGHT;
                    }
                    else
                    {
                        var_interrupt=ENC_LEFT;
                    }
        }
        encrotation=0;
    }else{
        encrotation++;
    }  
     CN_INT_FLAG = 0; // reset interrupt flag
}