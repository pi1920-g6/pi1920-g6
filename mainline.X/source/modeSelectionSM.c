/**
 * \file    modeSelectionSM.c
 */
/**
 * \addtogroup  smMS modeSelectionSM
 * \ingroup sm
 * @{
 *  \ref modeSelectionSM.c
 * @}
 */
#include <xc.h>
#include <sys/attribs.h>
#include <stdbool.h>

#include "SM_structures.h"
#include "homeCurrentSM.h"
#include "defines.h"
#include "nextion.h"
#include "utils.h"
#include "controller.h"

#include "modeSelectionSM.h"

/** \brief Variable to select which sub-menu to go to. */
uint8_t sel_menu_index = 0;
/** \brief Variable to select which process to write into the cache. */
uint8_t sel_processes_index;
/** \brief Variable to select whether or not to toggle Pulse on or off. */
uint8_t sel_pulse_index;
/** \brief Variable to select which opmode to write into the cache. */
uint8_t sel_modes_index;
/** \brief Temporary variable used to store various values before writing them into the cache. */
uint8_t assignement_var;
/** \brief Variable used to identify whether or not the BACK button was pressed in any submenu. */
uint8_t ms_back_flag = 0;
/** \brief Variable used to cap how far the main menu can move. Used to prevent OPMODE from being selected when the MMA process is active. */
uint8_t menu_max = 2;
/** \brief Variable to store the weld process value read from the cache. */
uint16_t wptemp;
/** \brief Variable to store the opmode value read from the cache. */
uint16_t opmtemp;
/** \brief Variable to store the weld state value read from the cache. */
uint16_t weldstate;
/** \brief Variable used to control the transition betweenMMA and non-MMA processes. */
bool old_mma;

/** \brief Variable used to identify Processes option selection. */
char obj_buttonProcesses[3]="t0";
/** \brief Variable used to identify Pulse option selection. */
char obj_buttonPulse[3]="t1";
/** \brief Variable used to identify Modes option selection. */
char obj_buttonMode[3]="t2";
/** \brief Variable used to identify TIGHF button. */
char obj_optionProcessTIGHF[3]="i0";
/** \brief Variable used to identify LIFTIGbutton. */
char obj_optionProcessLIFTIG[3]="i1";
/** \brief Variable used to identify MMA button. */
char obj_optionProcessMMA[3]="i2";
/** \brief Variable used to identify Pulse ON button. */
char obj_optionPulseON[3]="b0";
/** \brief Variable used to identify Pulse OFF button. */
char obj_optionPulseOFF[3]="b1";
/** \brief Variable used to identify 2T Mode button. */
char obj_optionMode2T[3]="i3";
/** \brief Variable used to identify 4T Mode button. */
char obj_optionMode4T[3]="i4";
/** \brief Variable used to identify Spot Mode button. */
char obj_optionModeSPOT[3]="i5";

/** \brief   Variable used to identify menu page. */
char MenuPage_MS[3]= "1";
/** \brief   Variable used to identify Processes submenu help page. */
char MenuPage_MSProcesses[3]= "30";
/** \brief   Variable used to identify Pulse submenu help page. */
char MenuPage_MSPulse[3]= "31";
/** \brief   Variable used to identify OpMode submenu help page. */
char MenuPage_MSOpMode[3]= "32";


param_id_t param;
weld_opt w_opt;
opmode_opt op_opt;

/**
 * \brief Responsible for the control of the main mode selection menu.
 * 
 * This routine controls the transitions between the 3 submenus.
 * Encoder rotation alternates between all submenus.
 * Encoder click changes the state machine state to that of the selected submenu.
 * Encoder Hold will open a Help menu.
 * Back will transition back to the selection menu state.
 * Home will transition to the Home Current state.
 * 
 * \param stateMachine 	State Machine current-state and previous-state.
 * 
 * \return  State Machine current-state and previous-state.
 */
sm_t mode_selection(sm_t stateMachine)
{
       
    switch(var_interrupt)
    {
        /*
         * HOME EVENT
         * - Return to HOME state
         */
        case HOME:
            stateMachine.c_state = HOME_CHANGE_CURRENT;
            stateMachine.p_state=MODE_SELECTION;
            FirstLoop = true;
            var_interrupt=NONE;
            break;
        
        /*
         * BACK EVENT
         * - Return to the selection menu
         * - Revert all nextion displays to their previous states.
         */
        case BACK:
            stateMachine.c_state = SELECTION_MENU;
            stateMachine.p_state= MODE_SELECTION;
            FirstLoop = true;
            var_interrupt=NONE;
            break;
        
        /*
         * ENC_BUTTON EVENT
         * - switch to the selected chosen option submenu
         */
        case ENC_BUTTON:
            switch(sel_menu_index)
            {
                case 0:
                    stateMachine.c_state = MS_PROCESSES;
                    nextion_gray_background(obj_buttonProcesses);
                    break;
                case 1:
                    stateMachine.c_state = MS_PULSE;
                    nextion_gray_background(obj_buttonPulse);
                    break;
                case 2:
                    stateMachine.c_state = MS_MODE;
                    nextion_gray_background(obj_buttonMode);
                    break;
                default:
                    break;
            }
            var_interrupt=NONE;
            break;
        
        /*
         * ENCODER HOLD EVENT
         * - Change current state and previous state
         * - Open HELP menu of Home Change Current
         */
        case ENC_HOLD:
            switch(sel_menu_index)
            {
                case 0:
                    nextion_page(MenuPage_MSProcesses);
                    break;
                case 1:
                    nextion_page(MenuPage_MSPulse);
                    break;
                case 2:
                    nextion_page(MenuPage_MSOpMode);
                    break;
            }
            stateMachine.p_state = MODE_SELECTION;
            stateMachine.c_state = HELP;
            FirstLoop = true;
            var_interrupt=NONE;
            break;
            
        /*
         * ENC_RIGHT EVENT
         * - Update the sel_menu_index variable to point to the appropriate  option.
         * - Update the nextion displays.
         */
        case ENC_RIGHT:
            if(sel_menu_index < menu_max) { 
                sel_menu_index = sel_menu_index + 1; 
            }
            switch(sel_menu_index)
            {
                case 0:
                    break;
                case 1:
                    nextion_unselect(obj_buttonProcesses);
                    nextion_select(obj_buttonPulse);
                    break;
                case 2:
                    nextion_unselect(obj_buttonPulse);
                    nextion_select(obj_buttonMode);
                    break;
                default:
                    break;
            }
            var_interrupt=NONE; 
            break;    
            
        /*
         * ENC_LEFT EVENT
         * - Update the sel_menu_index variable to point to the appropriate  option.
         * - Update the nextion displays.
         */
        case ENC_LEFT:
            if(sel_menu_index > 0) {
                sel_menu_index = sel_menu_index - 1;
            }
            switch(sel_menu_index)
            {
                case 2:
                    break;
                case 1:
                    nextion_unselect(obj_buttonMode);
                    nextion_select(obj_buttonPulse);
                    break;
                case 0:
                    nextion_unselect(obj_buttonPulse);
                    nextion_select(obj_buttonProcesses);
                    break;
                default:
                    break;
            }
            var_interrupt=NONE;
            break;
        
        /*
         * NONE
         * - Update values on Nextion (If first time on the page or as part of the BACK-based display reset.)
         * - Set nextion page (if previous menu is not selection menu)
         * - Return to the home menu in case welding begins.
         */ 
        case NONE:
            if(FirstLoop)
            {
                nextion_page(MenuPage_MS);
                controller_cache_read(0,&wptemp);
                controller_cache_read(1,&opmtemp);
                // opmtemp=1; // for testing only
                if(wptemp%2 == 0)
                {
                    nextion_select(obj_optionPulseOFF);
                    nextion_unselect(obj_optionPulseON);
                    sel_pulse_index = 0;
                    switch(wptemp)
                    {
                        case 0:
                            nextion_select(obj_optionProcessTIGHF);
                            nextion_unselect(obj_optionProcessLIFTIG);
                            nextion_unselect(obj_optionProcessMMA);
                            nextion_visible(obj_buttonMode);
                            nextion_visible(obj_optionMode2T);
                            nextion_visible(obj_optionMode4T);
                            nextion_visible(obj_optionModeSPOT);
                            menu_max = 2;
                            sel_processes_index = 0;
                            break;
                        case 2:
                            nextion_unselect(obj_optionProcessTIGHF);
                            nextion_select(obj_optionProcessLIFTIG);
                            nextion_unselect(obj_optionProcessMMA);
                            nextion_visible(obj_buttonMode);
                            nextion_visible(obj_optionMode2T);
                            nextion_visible(obj_optionMode4T);
                            nextion_visible(obj_optionModeSPOT);
                            menu_max = 2;
                            sel_processes_index = 1;
                            break;
                        case 4:
                            nextion_unselect(obj_optionProcessTIGHF);
                            nextion_unselect(obj_optionProcessLIFTIG);
                            nextion_select(obj_optionProcessMMA);
                            nextion_invisible(obj_buttonMode);
                            nextion_invisible(obj_optionMode2T);
                            nextion_invisible(obj_optionMode4T);
                            nextion_invisible(obj_optionModeSPOT);
                            old_mma = true;
                            menu_max = 1;
                            sel_processes_index = 2;
                            break;
                        default:
                            break;
                    }
                } else {
                    nextion_unselect(obj_optionPulseOFF);
                    nextion_select(obj_optionPulseON);
                    sel_pulse_index = 1;
                    switch(wptemp)
                    {
                        case 1:
                            nextion_select(obj_optionProcessTIGHF);
                            nextion_unselect(obj_optionProcessLIFTIG);
                            nextion_unselect(obj_optionProcessMMA);
                            nextion_visible(obj_buttonMode);
                            nextion_visible(obj_optionMode2T);
                            nextion_visible(obj_optionMode4T);
                            nextion_visible(obj_optionModeSPOT);
                            menu_max = 2;
                            sel_processes_index = 0;
                            break;
                        case 3:
                            nextion_unselect(obj_optionProcessTIGHF);
                            nextion_select(obj_optionProcessLIFTIG);
                            nextion_unselect(obj_optionProcessMMA);
                            nextion_visible(obj_buttonMode);
                            nextion_visible(obj_optionMode2T);
                            nextion_visible(obj_optionMode4T);
                            nextion_visible(obj_optionModeSPOT);
                            menu_max = 2;
                            sel_processes_index = 1;
                            break;
                        case 5:
                            nextion_unselect(obj_optionProcessTIGHF);
                            nextion_unselect(obj_optionProcessLIFTIG);
                            nextion_select(obj_optionProcessMMA);
                            nextion_invisible(obj_buttonMode);
                            nextion_invisible(obj_optionMode2T);
                            nextion_invisible(obj_optionMode4T);
                            nextion_invisible(obj_optionModeSPOT);
                            old_mma = true;
                            menu_max = 1;
                            sel_processes_index = 2;
                            break;
                        default:
                            break;
                    }
                }
                switch(opmtemp)
                {
                    case 0:
                        nextion_unselect(obj_optionMode2T);
                        nextion_unselect(obj_optionMode4T);
                        nextion_unselect(obj_optionModeSPOT);
                        sel_modes_index = 3;
                        break;
                    case 1:
                        nextion_select(obj_optionMode2T);
                        nextion_unselect(obj_optionMode4T);
                        nextion_unselect(obj_optionModeSPOT);
                        sel_modes_index = 0;
                        break;
                    case 2:
                        nextion_unselect(obj_optionMode2T);
                        nextion_select(obj_optionMode4T);
                        nextion_unselect(obj_optionModeSPOT);
                        sel_modes_index = 1;
                        break;
                    case 3:
                        nextion_unselect(obj_optionMode2T);
                        nextion_unselect(obj_optionMode4T);
                        nextion_select(obj_optionModeSPOT);
                        sel_modes_index = 2;
                        break;
                }                
                FirstLoop = false;
                switch(ms_back_flag)
                {
                    case 0:
                        break;
                    case 1:
                        nextion_select(obj_buttonProcesses);
                        nextion_unselect(obj_buttonPulse);
                        nextion_unselect(obj_buttonMode);
                        break;
                    case 2:
                        nextion_unselect(obj_buttonProcesses);
                        nextion_select(obj_buttonPulse);
                        nextion_unselect(obj_buttonMode);
                        break;
                    case 3:
                        nextion_unselect(obj_buttonProcesses);
                        nextion_unselect(obj_buttonPulse);
                        nextion_select(obj_buttonMode);
                        break;
                    default:
                        break;
                }
            }
            
            if(val_update==true)
            {
                
                param = WELD_STATE;
                controller_cache_read(param, &weldstate);

                if(weldstate == 1)
                {
                    stateMachine.c_state = HOME_CHANGE_CURRENT;
                    stateMachine.p_state = MODE_SELECTION;
                    FirstLoop=true;
                    var_interrupt=NONE;
                }
                val_update=false;
            }
            
            var_interrupt=NONE;
            break;
            
        default:
            var_interrupt=NONE;
            break;
    }
    return stateMachine;
}

/**
 * \brief Responsible for the control of the Process selection submenu.
 * 
 * This routine controls which of TIGHF, LIFTIG and MMA process is active.
 * Encoder rotation alternates between all three options.
 * Encoder click commits the selected option by writing it in the cache.
 * Encoder Hold will open a Help menu.
 * Back will transition back to the submenu selection menu.
 * Home will transition to the Home Current state.
 * 
 * \param stateMachine 	State Machine current-state and previous-state.
 * 
 * \return  State Machine current-state and previous-state.
 */
sm_t ms_Processes(sm_t stateMachine)
{

    switch(var_interrupt)
    {
        /*
         * HOME EVENT
         * - Return to HOME state
         */
        case HOME:
            stateMachine.c_state = HOME_CHANGE_CURRENT;
            stateMachine.p_state=MS_PROCESSES;
            FirstLoop = true;
            var_interrupt=NONE;
            break;
        
        /*
         * BACK EVENT
         * - Return to the modes menu
         */
        case BACK:
            stateMachine.c_state = MODE_SELECTION;
            stateMachine.p_state=MS_PROCESSES;
            FirstLoop = true;
            ms_back_flag = 1;
            var_interrupt=NONE;
            break;
        
        /*
         * ENC_BUTTON EVENT
         * - switch to the selected chosen option menu
         */
        case ENC_BUTTON:
            param = WELD_PROCESS;
            assignement_var = (int)w_opt + sel_pulse_index;
            controller_cache_write((int)param, assignement_var );
            if(w_opt == ms_MMA)
            {
                param = OP_MODE;
                controller_cache_write((int)param, 0);
                menu_max = 1;
                nextion_unselect(obj_optionMode2T);
                nextion_unselect(obj_optionMode4T);
                nextion_unselect(obj_optionModeSPOT);
                nextion_invisible(obj_buttonMode);
                nextion_invisible(obj_optionMode2T);
                nextion_invisible(obj_optionMode4T);
                nextion_invisible(obj_optionModeSPOT);
                old_mma = true;
            } else if(old_mma) {  
                param = OP_MODE;
                op_opt = ms_T2;
                assignement_var = (int)op_opt;
                controller_cache_write((int)param,assignement_var);
                menu_max = 2;
                nextion_select(obj_optionMode2T);
                nextion_unselect(obj_optionMode4T);
                nextion_unselect(obj_optionModeSPOT);
                nextion_unselect(obj_buttonMode);
                nextion_visible(obj_buttonMode);
                nextion_visible(obj_optionMode2T);
                nextion_visible(obj_optionMode4T);
                nextion_visible(obj_optionModeSPOT);
                sel_modes_index = 0;
                old_mma = false;
            }
            controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
            controller_waitwrite(); //wait for NACK
            programID_memory=0; //Set programID to previous saved just for showcase
            nextion_select(obj_buttonProcesses);
            stateMachine.c_state = MODE_SELECTION;
            var_interrupt=NONE;
            break;
        
        /*
         * ENCODER HOLD EVENT
         * - Change current state and previous state
         * - Open HELP menu of Home Change Current
         */
        case ENC_HOLD:
            nextion_page(MenuPage_MSProcesses);
            stateMachine.p_state = MODE_SELECTION;
            stateMachine.c_state = HELP;
            var_interrupt=NONE;
            FirstLoop=true;
            break;
            
        /*
         * ENC_RIGHT EVENT
         * - Update the sel_processes_index variable to point to the appropriate  option.
         * - Update the nextion displays.
         */
        case ENC_RIGHT:
            if(sel_processes_index < 2) { 
                sel_processes_index = sel_processes_index + 1;
            }
            switch(sel_processes_index)
            {
                case 1:
                    nextion_unselect(obj_optionProcessTIGHF);
                    nextion_select(obj_optionProcessLIFTIG);
                    w_opt = ms_LIFTIG;
                    break;
                case 2:
                    nextion_unselect(obj_optionProcessLIFTIG);
                    nextion_select(obj_optionProcessMMA);
                    w_opt = ms_MMA;
                    break;
                default:
                    break;                    
            }
            var_interrupt=NONE; 
            break;    
            
        /*
         * ENC_LEFT EVENT
         * - Update the sel_processes_index variable to point to the appropriate  option.
         * - Update the nextion displays.
         */
        case ENC_LEFT:
            if(sel_processes_index > 0) {
                sel_processes_index = sel_processes_index - 1;
            }
            switch(sel_processes_index)
            {
                case 1:
                    nextion_unselect(obj_optionProcessMMA);
                    nextion_select(obj_optionProcessLIFTIG);
                    w_opt = ms_LIFTIG;
                    break;
                case 0:
                    nextion_unselect(obj_optionProcessLIFTIG);
                    nextion_select(obj_optionProcessTIGHF);
                    w_opt = ms_TIGHF;
                    break;
                default:
                    break;                    
            }
            var_interrupt=NONE;
            break;   
        
        /*
         * NONE
         * - Do nothing, except to return to the home menu in case welding begins.
         */ 
        case NONE:
            if(val_update==true)
            {
                
                param = WELD_STATE;
                controller_cache_read(param, &weldstate);

                if(weldstate == 1)
                {
                    stateMachine.c_state = HOME_CHANGE_CURRENT;
                    stateMachine.p_state = MS_PROCESSES;
                    FirstLoop=true;
                    var_interrupt=NONE;
                }
                val_update=false;
            }
            
            var_interrupt=NONE;
            break;
            
        default:
            var_interrupt=NONE;
            break;
    }
    return stateMachine;
}

/**
 * \brief Responsible for the control of the Pulse toggle submenu.
 * 
 * This routine controls the transition between the ON and OFF buttons in the submenu.
 * Encoder rotation alternates between the two options.
 * Encoder click commits the selected option by writing it in the cache.
 * Encoder Hold will open a Help menu.
 * Back will transition back to the selection menu state.
 * Home will transition to the Home Current state.
 * 
 * \param stateMachine 	State Machine current-state and previous-state.
 * 
 * \return  State Machine current-state and previous-state.
 */
sm_t ms_Pulse(sm_t stateMachine)
{
    switch(var_interrupt)
    {
        /*
         * HOME EVENT
         * - Return to HOME state
         */
        case HOME:
            stateMachine.c_state = HOME_CHANGE_CURRENT;
            stateMachine.p_state=MS_PROCESSES;
            FirstLoop = true;
            var_interrupt=NONE;
            break;
        
        /*
         * BACK EVENT
         * - Return to the modes menu
         */
        case BACK:
            stateMachine.c_state = MODE_SELECTION;
            stateMachine.p_state=MS_PROCESSES;
            FirstLoop = true;
            ms_back_flag = 2;
            var_interrupt=NONE;
            break;
        
        /*
         * ENC_BUTTON EVENT
         * - switch to the selected chosen option menu
         */
        case ENC_BUTTON:
            param = WELD_PROCESS;
            if(sel_pulse_index == 1)
            {
                switch(w_opt)
                {
                    case ms_TIGHF:
                        w_opt = ms_TIGHF_P;
                        break;
                    case ms_LIFTIG:
                        w_opt = ms_LIFTIG_P;
                        break;
                    case ms_MMA:
                        w_opt = ms_MMA_P;
                        break;
                    default:
                        break;
                }
            }
            controller_cache_write((int)param,(int)w_opt);
            controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
            controller_waitwrite(); //wait for NACK
            programID_memory=0; //Set programID to previous saved just for showcase
            nextion_select(obj_buttonPulse);
            stateMachine.c_state = MODE_SELECTION;
            var_interrupt=NONE;
            break;
        
        /*
         * ENCODER HOLD EVENT
         * - Change current state and previous state
         * - Open HELP menu of Home Change Current
         */
        case ENC_HOLD:
            nextion_page(MenuPage_MSPulse);
            stateMachine.p_state = MODE_SELECTION;
            stateMachine.c_state = HELP;
            var_interrupt=NONE;
            FirstLoop=true;
            break;
            
        /*
         * ENC_RIGHT EVENT
         * - Update the sel_pulse_index variable to point to the appropriate  option.
         * - Update the nextion displays.
         */
        case ENC_RIGHT:
            if(sel_pulse_index > 0) {
                sel_pulse_index = sel_pulse_index - 1;
            }
            nextion_unselect(obj_optionPulseON);
            nextion_select(obj_optionPulseOFF);
            var_interrupt=NONE; 
            break;    
            
        /*
         * ENC_LEFT EVENT
         * - Update the sel_pulse_index variable to point to the appropriate  option.
         * - Update the nextion displays.
         */
        case ENC_LEFT:
            if(sel_pulse_index < 1) {
                sel_pulse_index = sel_pulse_index + 1;
            }
            nextion_unselect(obj_optionPulseOFF);
            nextion_select(obj_optionPulseON);
            var_interrupt=NONE;
            break;   
        
        /*
         * NONE
         * - Do nothing, except to return to the home menu in case welding begins.
         */ 
        case NONE:
            if(val_update==true)
            {
                
                param = WELD_STATE;
                controller_cache_read(param, &weldstate);

                if(weldstate == 1)
                {
                    stateMachine.c_state = HOME_CHANGE_CURRENT;
                    stateMachine.p_state = MS_PULSE;
                    FirstLoop=true;
                    var_interrupt=NONE;
                }
                val_update=false;
            }
            
            var_interrupt=NONE;
            break;
            
        default:
            var_interrupt=NONE;
            break;
    }
    return stateMachine;
}

/**
 * \brief Responsible for the control of the OpMode selection submenu.
 * 
 * This routine controls which of T2, T4 or SPOT OpMode is active.
 * Encoder rotation alternates between all three options.
 * Encoder click commits the selected option by writing it in the cache.
 * Encoder Hold will open a Help menu.
 * Back will transition back to the submenu selection menu.
 * Home will transition to the Home Current state.
 * 
 * \param stateMachine 	State Machine current-state and previous-state.
 * 
 * \return  State Machine current-state and previous-state.
 */
sm_t ms_Mode(sm_t stateMachine)
{
    switch(var_interrupt)
    {
        /*
         * HOME EVENT
         * - Return to HOME state
         */
        case HOME:
            stateMachine.c_state = HOME_CHANGE_CURRENT;
            stateMachine.p_state=MS_MODE;
            FirstLoop = true;
            var_interrupt=NONE;
            break;
        
        /*
         * BACK EVENT
         * - Return to the modes menu
         */
        case BACK:
            stateMachine.c_state = MODE_SELECTION;
            stateMachine.p_state=MS_PROCESSES;
            FirstLoop = true;
            ms_back_flag = 3;
            var_interrupt=NONE;
            break;
        
        /*
         * ENC_BUTTON EVENT
         * - switch to the selected chosen option menu
         */
        case ENC_BUTTON:
            param = OP_MODE;
            assignement_var = (int)op_opt;
            controller_cache_write((int)param, assignement_var );
            controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
            controller_waitwrite(); //wait for NACK
            programID_memory=0; //Set programID to previous saved just for showcase
            nextion_select(obj_buttonMode);
            stateMachine.c_state = MODE_SELECTION;
            var_interrupt=NONE;
            break;
        
        /*
         * ENCODER HOLD EVENT
         * - Change current state and previous state
         * - Open HELP menu of Home Change Current
         */
        case ENC_HOLD:
            nextion_page(MenuPage_MSOpMode);
            stateMachine.p_state = MODE_SELECTION;
            stateMachine.c_state = HELP;
            var_interrupt=NONE;
            FirstLoop=true;
            break;
            
        /*
         * ENC_RIGHT EVENT
         * - Update the sel_modes_index variable to point to the appropriate  option.
         * - Update the nextion displays.
         */
        case ENC_RIGHT:
            if(sel_modes_index < 2) { 
                sel_modes_index = sel_modes_index + 1;
            }
            switch(sel_modes_index)
            {
                case 0:
                    break;
                case 1:
                    nextion_unselect(obj_optionMode2T);
                    nextion_select(obj_optionMode4T);
                    op_opt = ms_T4;
                    break;
                case 2:
                    nextion_unselect(obj_optionMode4T);
                    nextion_select(obj_optionModeSPOT);
                    op_opt = ms_SPOT;
                    break;
                case 3:
                    break;
                default:
                    break;                    
            }
            var_interrupt=NONE; 
            break;    
            
        /*
         * ENC_LEFT EVENT
         * - Update the sel_modes_index variable to point to the appropriate  option.
         * - Update the nextion displays.
         */
        case ENC_LEFT:
            if(sel_modes_index > 0) {
                sel_modes_index = sel_modes_index - 1;
            }
            switch(sel_modes_index)
            {
                case 3:
                    break;
                case 2:
                    break;
                case 1:
                    nextion_unselect(obj_optionModeSPOT);
                    nextion_select(obj_optionMode4T);
                    op_opt = ms_T4;
                    break;
                case 0:
                    nextion_unselect(obj_optionMode4T);
                    nextion_select(obj_optionMode2T);
                    op_opt = ms_T2;
                    break;
                default:
                    break;                    
            }
            var_interrupt=NONE;
            break;   
        
        /*
         * NONE
         * - Do nothing, except to return to the home menu in case welding begins.
         */ 
        case NONE:
            if(val_update==true)
            {
                
                param = WELD_STATE;
                controller_cache_read(param, &weldstate);

                if(weldstate == 1)
                {
                    stateMachine.c_state = HOME_CHANGE_CURRENT;
                    stateMachine.p_state = MS_MODE;
                    FirstLoop=true;
                    var_interrupt=NONE;
                }
                val_update=false;
            }
            
            var_interrupt=NONE;
            break;
            
        default:
            var_interrupt=NONE;
            break;
    }
    return stateMachine;
}
