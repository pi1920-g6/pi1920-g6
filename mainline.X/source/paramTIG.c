/**
 * \file    paramTIG.c
 */
/**
 * \addtogroup  smTIG paramTIG
 * \ingroup sm
 * @{
 *  \ref paramTIG.c
 * @}
 */
#include <xc.h>
#include <sys/attribs.h>
#include <stdbool.h>

#include "SM_structures.h"
#include "paramTIG.h"
#include "defines.h"
#include "nextion.h"
#include "utils.h"
#include "controller.h"
#include "homeCurrentSM.h"

/*
 * VARIABLES
 */

/** \brief   Maximum Pre Gas value. */
#define MaxPreGas 100
/** \brief   Minimum Pre Gas value. */
#define MinPreGas 1
/** \brief   Maximum Start Current value. */
#define MaxIStart 220
/** \brief   Minimum Start Current value. */
#define MinIStart 10
/** \brief   Maximum Up Slope time. */
#define MaxUpSlope 100
/** \brief   Minimum Up Slope time. */
#define MinUpSlope 0
/** \brief   Maximum INominal value. */
#define MaxINominal 220
/** \brief   Minimum INominal value. */
#define MinINominal 10
/** \brief   Maximum IBase value. */
#define MaxIBase 95
/** \brief   Minimum IBase value. */
#define MinIBase 1
/** \brief   Maximum Pulse width value. */
#define MaxPulseWidth 90
/** \brief   Minimum Pulse width value. */
#define MinPulseWidth 10
/** \brief   Maximum Pulse frequency value. */
#define MaxPulseFreq 200
/** \brief   Minimum Pulse frequrncy value. */
#define MinPulseFreq 1
/** \brief   Maximum Down Slope time. */
#define MaxDownSlope 100
/** \brief   Minimum Down Slope time. */
#define MinDownSlope 0
/** \brief   Maximum Final Current value. */
#define MaxIEnd 220
/** \brief   Minimum Final Current value. */
#define MinIEnd 10
/** \brief   Maximum Post Gás value. */
#define MaxPostGas 100
/** \brief   Minimum Post Gás value. */
#define MinPostGas 1
/** \brief   Dynamics On. */
#define DynamicsOn 1
/** \brief   Dynamics Off. */
#define DynamicsOff 0
/** \brief   Maximum SpotTime time. */
#define MaxSpotTime 100
/** \brief   Minimum SpotTime time. */
#define MinSpotTime 0


/** \brief   Variable to store the Pre gas value. */
uint16_t TIGPreGas; 
/** \brief   Variable to store the Start current value. */
uint16_t TIGIStart;
/** \brief   Variable to store the Up Slope time. */
uint16_t TIGUpSlope;
/** \brief   Variable to store the Nominal Current value. */
uint16_t TIGINominal;
/** \brief   Variable to store the Base Current value. */
uint16_t TIGIBase;
/** \brief   Variable to store the Pulse Width value. */
uint16_t TIGPulseWidth;
/** \brief   Variable to store the Pulse Frequency value. */
uint16_t TIGPulseFreq;
/** \brief   Variable to store the Down Slope time. */
uint16_t TIGDownSlope;
/** \brief   Variable to store the Final current value. */
uint16_t TIGIEnd;
/** \brief   Variable to store the Post Gas value. */
uint16_t TIGPostGas;
/** \brief   Variable to store the Dynamics value. */
uint16_t TIGDynamics;
/** \brief   Variable to store the Spot Time value. */
uint16_t TIGSpotTime;
/** \brief   Variable used to identify the welding process. */
uint16_t wprocess;
/** \brief   Variable used to identify the welding state. */
uint16_t wstate;
/** \brief   Variable used to identify the operating mode. */
uint16_t omode;

/** \brief   Variable used to send to nextion TIG Pre Gas. */
char NextionTIG_PreGas[3];
/** \brief   Variable used to send to nextion TIG Start Current. */
char NextionTIG_IStart[3];
/** \brief   Variable used to send to nextion TIG Up Slope. */
char NextionTIG_UpSlope[3];
/** \brief   Variable used to send to nextion TIG Nominal Current. */
char NextionTIG_INominal[3];
/** \brief   Variable used to send to nextion TIG base Current. */
char NextionTIG_IBase[3];
/** \brief   Variable used to send to nextion TIG Pulse Width. */
char NextionTIG_PulseWidth[3];
/** \brief   Variable used to send to nextion TIG Pulse Freq. */
char NextionTIG_PulseFreq[3];
/** \brief   Variable used to send to nextion TIG Down Slope. */
char NextionTIG_DownSlope[3];
/** \brief   Variable used to send to nextion TIG Final Current. */
char NextionTIG_IEnd[3];
/** \brief   Variable used to send to nextion TIG Post Gas. */
char NextionTIG_PostGas[3];
/** \brief   Variable used to send to nextion TIG Dynamics value. */
char NextionTIG_Dynamics[1];
/** \brief   Variable used to send to nextion TIG Spot Time value. */
char NextionTIG_SpotTime[3];

/** \brief   Variable used to identify the Pre gas object. */
char obj_buttonPreGas[3] = "i0";
/** \brief   Variable used to identify the Pre gas value object. */
char obj_PreGasValue[3] = "x0";
/** \brief   Variable used to identify the Start current object. */
char obj_buttonIStart[3] = "i1";
/** \brief   Variable used to identify the Start current value object. */
char obj_IStartValue[3] = "n1";
/** \brief   Variable used to identify the Up slope object. */
char obj_buttonUpSlope[3] = "i2";
/** \brief   Variable used to identify the Up slope value object. */
char obj_UpSlopeValue[3] = "x2";
/** \brief   Variable used to identify the Nominal Current object. */
char obj_buttonTIGINominal[3] = "i3";
/** \brief   Variable used to identify the Nominal Current value object. */
char obj_TIGINominalValue[3] = "n3";
/** \brief   Variable used to identify the Down Slope object. */
char obj_buttonDownSlope[3] = "i0";
/** \brief   Variable used to identify the Down Slope value object. */
char obj_DownSlopeValue[3] = "x0";
/** \brief   Variable used to identify the Final current object. */
char obj_buttonIEnd[3]= "i1";
/** \brief   Variable used to identify the Final current value object. */
char obj_IEndValue[3]="n1";
/** \brief   Variable used to identify the Post Gas object. */
char obj_buttonPostGas[3]= "i2";
/** \brief   Variable used to identify the Post Gas value object. */
char obj_PostGasValue[3]="x2";
/** \brief   Variable used to identify the Dynamics object. */
char obj_buttonDynamics[3]= "i3";
/** \brief   Variable used to identify the Dynamics value object. */
char obj_DynamicsValue[3]="n3";
/** \brief   Variable used to identify the Dynamics value object. */
char obj_buttonSpotTime[3] = "i0";
/** \brief   Variable used to identify the Dynamics value object. */
char obj_SpotTimeValue[3] = "x0";
/** \brief   Variable used to identify the Base Current object. */
char obj_buttonIBase[3] = "i0";
/** \brief   Variable used to identify the Base Current value object. */
char obj_IBaseValue[3] = "n0";
/** \brief   Variable used to identify the Base Current object. */
char obj_buttonPulseWidth[3] = "i1";
/** \brief   Variable used to identify the Base Current value object. */
char obj_PulseWidthValue[3] = "n1";
/** \brief   Variable used to identify the Base Current object. */
char obj_buttonTIGPulseFreq[3] = "i2";
/** \brief   Variable used to identify the Base Current value object. */
char obj_TIGPulseFreqValue[3] = "n2";
/** \brief   Variable used to identify the Down Slope object. */
char obj_buttonPulseDownSlope[3] = "i3";
/** \brief   Variable used to identify the Down Slope value object. */
char obj_PulseDownSlopeValue[3] = "x3";
/** \brief   Variable used to identify the Final current object. */
char obj_buttonPulseIEnd[3]= "i0";
/** \brief   Variable used to identify the Final current value object. */
char obj_PulseIEndValue[3]="n0";
/** \brief   Variable used to identify the Post Gas object. */
char obj_buttonPulsePostGas[3]= "i1";
/** \brief   Variable used to identify the Post Gas value object. */
char obj_PulsePostGasValue[3]="x1";
/** \brief   Variable used to identify the Dynamics object. */
char obj_buttonPulseDynamics[3]= "i2";
/** \brief   Variable used to identify the Dynamics value object. */
char obj_PulseDynamicsValue[3]="n2";
/** \brief   Variable used to identify the Dynamics value object. */
char obj_buttonPulseSpotTime[3] = "i3";
/** \brief   Variable used to identify the Dynamics value object. */
char obj_PulseSpotTimeValue[3] = "x3";

/** \brief   Variable used to identify the TIG Parameters 1 page. */
char MenuPage_TIG_Parm1[3] = "02";
/** \brief   Variable used to identify the TIG Parameters 2 page. */
char MenuPage_TIG_Parm2[3] = "03";
/** \brief   Variable used to identify the TIG Spot Parameter page 3. */
char MenuPage_TIG_Spot_Parm1[3] = "04";
/** \brief   Variable used to identify the TIG Spot Parameter page 3. */
char MenuPage_TIG_Spot_Parm2[3] = "05";
/** \brief   Variable used to identify the TIG Spot Parameter page 3. */
char MenuPage_TIG_Spot_Parm3[3] = "06";
/** \brief   Variable used to identify the Pulse TIG Parameters 1 page. */
char MenuPage_TIG_Pulse_Parm1[3] = "07";
/** \brief   Variable used to identify the Pulse TIG Parameters 2 page. */
char MenuPage_TIG_Pulse_Parm2[3] = "08";
/** \brief   Variable used to identify the Pulse TIG Parameters 3 page. */
char MenuPage_TIG_Pulse_Parm3[3] = "09";
/** \brief   Variable used to identify the Pulse TIG Spot Parameters page. */
char MenuPage_TIG_Pulse_Spot_Parm[3] = "10";
/** \brief   Variable used to identify the Pre Gas Help page. */
char MenuPage_Help_PreGas[3] = "34";
/** \brief   Variable used to identify the Start Current Help page. */
char MenuPage_Help_IStart[3] = "35";
/** \brief   Variable used to identify the Up Slope Help page. */
char MenuPage_Help_UpSlope[3] = "36";
/** \brief   Variable used to identify the Peak Current Help page. */
char MenuPage_Help_INominal[3] = "37";
/** \brief   Variable used to identify the Down Slope Help page. */
char MenuPage_Help_DownSlope[3] = "38";
/** \brief   Variable used to identify the Final Current Help page. */
char MenuPage_Help_IFinal[3] = "39";
/** \brief   Variable used to identify the Post Gas Help page. */
char MenuPage_Help_PostGas[3] = "40";
/** \brief   Variable used to identify the Dynamics Help page. */
char MenuPage_Help_Dynamics[3] = "41";
/** \brief   Variable used to identify the Spot Time Help page. */
char MenuPage_Help_SpotTime[3] = "42";
/** \brief   Variable used to identify the Base Current Help page. */
char MenuPage_Help_IBase[3] = "43";
/** \brief   Variable used to identify the Pulse Width Help page. */
char MenuPage_Help_PulseWidth[3] = "44";
/** \brief   Variable used to identify the Pulse Frequency Help page. */
char MenuPage_Help_PulseFreq[3] = "45";

/** \brief   Variable used to identify the Weld Process in use object. */
char obj_weld_process[5] = "t00";

/** \brief   Variable used to identify the parameter to read from the cache. */
param_id_t param;

/** \brief   Buttons on TIG parameters page 1 and Pulse TIG parameters page 1. */
typedef enum TIG_PAG1_Param_buttons_t
{
    PREGAS_BUTTON,
    ISTART_BUTTON,
    UPSLOPE_BUTTON,
    INOMINAL_BUTTON,   
    PREGASVALUE_BUTTON,
    ISTARTVALUE_BUTTON,
    UPSLOPEVALUE_BUTTON,
    INOMINALVALUE_BUTTON
    
} TIG_PAG1_Param_buttons_t;

/** \brief   Buttons on TIG parameters page 2. */
typedef enum TIG_PAG2_Param_buttons_t
{
    DOWNSLOPE_BUTTON,
    IEND_BUTTON,   
    POSTGAS_BUTTON,
    DYNAMICS_BUTTON,      
    DOWNSLOPEVALUE_BUTTON,
    IENDVALUE_BUTTON,   
    POSTGASVALUE_BUTTON,
    DYNAMICSVALUE_BUTTON
    
} TIG_PAG2_Param_buttons_t;

/** \brief   Buttons on TIG parameters page 3 (If spot is active). */
typedef enum TIG_SPOT_PAG_Param_buttons_t
{
    SPOTTIME_BUTTON,
    SPOTTIMEVALUE_BUTTON
} TIG_SPOT_PAG_Param_buttons_t;

/** \brief   Buttons on Pulse TIG parameters page 2. */
typedef enum TIG_Pulse_PAG2_Param_buttons_t
{
    IBASE_BUTTON,
    PULSEWIDTH_BUTTON,
    PULSEFREQ_BUTTON ,       
    PULSEDOWNSLOPE_BUTTON,
    IBASEVALUE_BUTTON,
    PULSEWIDTHVALUE_BUTTON,
    PULSEFREQVALUE_BUTTON,        
    PULSEDOWNSLOPEVALUE_BUTTON

} TIG_Pulse_PAG2_Param_buttons_t;

/** \brief   Buttons on Pulse TIG parameters page 3 (Whit and whitout spot active). */
typedef enum TIG_Pulse_PAG3_Param_buttons_t
{
 
    PULSEIEND_BUTTON,   
    PULSEPOSTGAS_BUTTON,
    PULSEDYNAMICS_BUTTON,
    PULSESPOTTIME_BUTTON,
    PULSEIENDVALUE_BUTTON,   
    PULSEPOSTGASVALUE_BUTTON,
    PULSEDYNAMICSVALUE_BUTTON,
    PULSESPOTTIMEVALUE_BUTTON
} TIG_Pulse_PAG3_Param_buttons_t;

TIG_PAG1_Param_buttons_t TIG_Buttons1;
TIG_PAG2_Param_buttons_t TIG_Buttons2;
TIG_SPOT_PAG_Param_buttons_t TIG_Spot_Buttons;
TIG_Pulse_PAG2_Param_buttons_t Pulse_TIG_Buttons2;
TIG_Pulse_PAG3_Param_buttons_t Pulse_TIG_Buttons3;

/**
 * \brief Responsible for the TIG (NOT PULSED) parameteres and value changes.
 * 
 * This routine will control the flow between the TIG parameters aswell
 * as allowing the user to change them according to max and min value.
 * 
 * Parameters available on this page:
 * TIG Pre Gas  - Min value 1, Max value 100  (Seconds/10)
 * TIG IStart   - Min value 1, Max value 220  (A)
 * TIG UpSlope  - Min value 0, Max value 100  (Seconds/10)
 * TIG INominal - Min value 10, Max value 220 (%)
 * 
 * Page 1/2 for TIG.
 * 
 *  Events:
 * - Encoder rotation to the right or left will change the button or the value
 * of the parameter.
 * - Encoder click will allow the user to open parameter value so it can be 
 * edited if on the button, if on the value, will validate the change and save it.
 * - Encoder hold will open a Help menu for this page.
 * - Back will take the user back to Selection Menu buttons on Home menu if on 
 * buttons. If on values, will discard changes.
 * - Home will take the user back to Current Value change on Home menu.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t TIG_parameters_page1(sm_t stateMachine)
{
    switch(var_interrupt)
    {
        /*
         * HOME EVENT
         * - State machine current state is now Home_Change_Current
         */
        case HOME:
            stateMachine.c_state = HOME_CHANGE_CURRENT;
            stateMachine.p_state = TIG_PG1;
            FirstLoop=true;
            var_interrupt=NONE;
            break;
        
        /*
         * BACK EVENT
         * - Return to Selection Menu (Home buttons) if on the any parameter button
         * - Return to the parameter button if on the value button change option, without validation of 
         * the value changes, reseting the previous stored value.
         */    
        case BACK:
            switch(TIG_Buttons1)
            {
                case PREGAS_BUTTON:
                        stateMachine.c_state = SELECTION_MENU;
                        stateMachine.p_state = TIG_PG1;
                        FirstLoop=true;
                        break;
                case ISTART_BUTTON:
                        stateMachine.c_state = SELECTION_MENU;
                        stateMachine.p_state = TIG_PG1;
                        FirstLoop=true;
                        break;
                case UPSLOPE_BUTTON:
                        stateMachine.c_state = SELECTION_MENU;
                        stateMachine.p_state = TIG_PG1;
                        FirstLoop=true;
                        break;
                case INOMINAL_BUTTON:
                        stateMachine.c_state = SELECTION_MENU;
                        stateMachine.p_state = TIG_PG1;
                        FirstLoop=true;
                        break;
                        
                case PREGASVALUE_BUTTON:
                    nextion_unselect(obj_PreGasValue);
                    nextion_select(obj_buttonPreGas);
                    param=TIG_PRE_GAS;
                    controller_cache_read(param,&TIGPreGas);
                    utils_int_to_str(NextionTIG_PreGas,TIGPreGas,3);
                    nextion_change_val(obj_PreGasValue,NextionTIG_PreGas);
                    TIG_Buttons1=PREGAS_BUTTON;
                    break;  
                    
                case ISTARTVALUE_BUTTON:
                    nextion_unselect(obj_IStartValue);
                    nextion_select(obj_buttonIStart);
                    param=TIG_ISTART;
                    controller_cache_read(param,&TIGIStart);
                    utils_int_to_str(NextionTIG_IStart,TIGIStart,3);
                    nextion_change_val(obj_IStartValue,NextionTIG_IStart);
                    TIG_Buttons1=ISTART_BUTTON;
                    break;
                    
                case UPSLOPEVALUE_BUTTON:
                    nextion_unselect(obj_UpSlopeValue);
                    nextion_select(obj_buttonUpSlope);
                    param=TIG_UPSLOPE;
                    controller_cache_read(param,&TIGUpSlope);
                    utils_int_to_str(NextionTIG_UpSlope,TIGUpSlope,3);
                    nextion_change_val(obj_UpSlopeValue,NextionTIG_UpSlope);
                    TIG_Buttons1=UPSLOPE_BUTTON;
                    break;
                    
                case INOMINALVALUE_BUTTON:
                    nextion_unselect(obj_TIGINominalValue);
                    nextion_select(obj_buttonTIGINominal);
                    param=TIG_INOMINAL;
                    controller_cache_read(param,&TIGINominal);
                    utils_int_to_str(NextionTIG_INominal,TIGINominal,3);
                    nextion_change_val(obj_TIGINominalValue,NextionTIG_IStart);
                    TIG_Buttons1=INOMINAL_BUTTON;
                    break;
                
                default:
                    break;
            }
            var_interrupt = NONE;
            break;
        
        /*
         * ENCODER BUTTON EVENT
         * - Access value change if on parameter button
         * - Validate changes and go to parameter button if is on value button
         */  
        case ENC_BUTTON:
           switch(TIG_Buttons1)
           {
               case PREGAS_BUTTON:
                    nextion_unselect(obj_buttonPreGas);
                    nextion_select(obj_PreGasValue);
                    TIG_Buttons1=PREGASVALUE_BUTTON;
                    break;
                    
                case ISTART_BUTTON:
                    nextion_unselect(obj_buttonIStart);
                    nextion_select(obj_IStartValue);
                    TIG_Buttons1=ISTARTVALUE_BUTTON;
                    break;
                    
                case UPSLOPE_BUTTON:
                    nextion_unselect(obj_buttonUpSlope);
                    nextion_select(obj_UpSlopeValue);
                    TIG_Buttons1=UPSLOPEVALUE_BUTTON;
                    break;
                    
                case INOMINAL_BUTTON:
                    nextion_unselect(obj_buttonTIGINominal);
                    nextion_select(obj_TIGINominalValue);
                    TIG_Buttons1=INOMINALVALUE_BUTTON;
                    break;
                    
                case PREGASVALUE_BUTTON:
                    nextion_unselect(obj_PreGasValue);
                    nextion_select(obj_buttonPreGas);
                    param=TIG_PRE_GAS;
                    controller_cache_write((int)param,TIGPreGas);
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    TIG_Buttons1=PREGAS_BUTTON;
                    break;
                    
                case ISTARTVALUE_BUTTON:
                    nextion_unselect(obj_IStartValue);
                    nextion_select(obj_buttonIStart);
                    param=TIG_ISTART;
                    controller_cache_write((int)param,TIGIStart);
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    TIG_Buttons1=ISTART_BUTTON;
                    break;
                    
                case UPSLOPEVALUE_BUTTON:
                    nextion_unselect(obj_UpSlopeValue);
                    nextion_select(obj_buttonUpSlope);
                    param=TIG_UPSLOPE;
                    controller_cache_write((int)param,TIGUpSlope);
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    TIG_Buttons1=UPSLOPE_BUTTON;
                    break;
                    
                case INOMINALVALUE_BUTTON:
                    nextion_unselect(obj_TIGINominalValue);
                    nextion_select(obj_buttonTIGINominal);
                    param=TIG_INOMINAL;
                    controller_cache_write((int)param,TIGINominal);
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    TIG_Buttons1=INOMINAL_BUTTON;
                    break;
                 
                default:
                    break;
            }
            var_interrupt=NONE;
            break;
 
        /*
         * ENCODER HOLD EVENT
         * - Open HELP page for the selected parameter or parameter value 
         */        
        case ENC_HOLD:
            switch(TIG_Buttons1)
           {
               case PREGAS_BUTTON:
                    nextion_page(MenuPage_Help_PreGas); 
                    break;
                    
                case ISTART_BUTTON:
                    nextion_page(MenuPage_Help_IStart); 
                    break;
                    
                case UPSLOPE_BUTTON:
                    nextion_page(MenuPage_Help_UpSlope); 
                    break;
                    
                case INOMINAL_BUTTON:
                    nextion_page(MenuPage_Help_INominal); 
                    break;
                    
                case PREGASVALUE_BUTTON:
                    nextion_page(MenuPage_Help_PreGas); 
                    break;
                    
                case ISTARTVALUE_BUTTON:
                    nextion_page(MenuPage_Help_IStart); 
                    break;
                    
                case UPSLOPEVALUE_BUTTON:
                    nextion_page(MenuPage_Help_UpSlope); 
                    break;
                    
                case INOMINALVALUE_BUTTON:
                    nextion_page(MenuPage_Help_INominal); 
                    break;
                    
                default:
                    break;
            }
            
            stateMachine.c_state = HELP;
            stateMachine.p_state = TIG_PG1;
            FirstLoop=true;
            var_interrupt=NONE;
            break;
            
        /*
         * ENCODER RIGHT EVENT
         * - If on button, move to the right button
         * - If on value, increment value its less than MaxValue
         * - Send to nextion new value
         */          
        case ENC_RIGHT:
           switch(TIG_Buttons1)
            {
               case PREGAS_BUTTON:
                    nextion_unselect(obj_buttonPreGas);
                    nextion_select(obj_buttonIStart);
                    TIG_Buttons1=ISTART_BUTTON;
                    break;
                    
                case ISTART_BUTTON:
                    nextion_unselect(obj_buttonIStart);
                    nextion_select(obj_buttonUpSlope);
                    TIG_Buttons1=UPSLOPE_BUTTON;
                    break;
                    
                case UPSLOPE_BUTTON:
                    nextion_unselect(obj_buttonUpSlope);
                    nextion_select(obj_buttonTIGINominal);
                    TIG_Buttons1=INOMINAL_BUTTON;
                    break;
                    
                case INOMINAL_BUTTON:
                    stateMachine.c_state = TIG_PG2;
                    stateMachine.p_state = TIG_PG1;
                    FirstLoop = true;
                    var_interrupt=NONE;
                    break;
                    
                case PREGASVALUE_BUTTON:
                    if(TIGPreGas < MaxPreGas)
                    {
                        TIGPreGas++;
                        utils_int_to_str(NextionTIG_PreGas,TIGPreGas,3);
                        nextion_change_val(obj_PreGasValue,NextionTIG_PreGas);
                    }
                    break;
                   
                case ISTARTVALUE_BUTTON:
                    if(TIGIStart < MaxIStart)
                    {
                        TIGIStart++;
                        utils_int_to_str(NextionTIG_IStart,TIGIStart,3);
                        nextion_change_val(obj_IStartValue,NextionTIG_IStart);
               
                    }
                    break;
                   
                case UPSLOPEVALUE_BUTTON:
                    if(TIGUpSlope<MaxUpSlope)
                    {
                        TIGUpSlope++;
                        utils_int_to_str(NextionTIG_UpSlope,TIGUpSlope,3);
                        nextion_change_val(obj_UpSlopeValue,NextionTIG_UpSlope);
                    }
                    break;
                    
                case INOMINALVALUE_BUTTON:
                    if(TIGINominal<MaxINominal)
                    {
                        TIGINominal++;
                        utils_int_to_str(NextionTIG_INominal,TIGINominal,3);
                        nextion_change_val(obj_TIGINominalValue,NextionTIG_INominal);
                    }
                    break;
                   
                default:
                    break;
            }
            var_interrupt=NONE; 
            break;    
        
        /*
         * ENCODER LEFT EVENT
         * - If on button, move to the left button
         * - If on value, decrement value if its above MinValue
         * - Send to nextion new value
         */ 
        case ENC_LEFT:
               switch(TIG_Buttons1)
            {
               case PREGAS_BUTTON:
                   
                    break;
                    
                case ISTART_BUTTON:
                    nextion_unselect(obj_buttonIStart);
                    nextion_select(obj_buttonPreGas);
                    TIG_Buttons1=PREGAS_BUTTON;
                    break;
                    
                case UPSLOPE_BUTTON:
                    nextion_unselect(obj_buttonUpSlope);
                    nextion_select(obj_buttonIStart);
                    TIG_Buttons1=ISTART_BUTTON;
                    break;
                    
                case INOMINAL_BUTTON:
                    nextion_unselect(obj_buttonTIGINominal);
                    nextion_select(obj_buttonUpSlope);
                    TIG_Buttons1=UPSLOPE_BUTTON;
                    break;
                    
                case PREGASVALUE_BUTTON:
                    if(TIGPreGas > MinPreGas)
                    {
                        TIGPreGas--;
                        utils_int_to_str(NextionTIG_PreGas,TIGPreGas,3);
                        nextion_change_val(obj_PreGasValue,NextionTIG_PreGas);
                    }
                    break;
                   
                case ISTARTVALUE_BUTTON:
                    if(TIGIStart > MinIStart)
                    {
                        TIGIStart--;
                        utils_int_to_str(NextionTIG_IStart,TIGIStart,3);
                        nextion_change_val(obj_IStartValue,NextionTIG_IStart);
               
                    }
                    break;
                   
                case UPSLOPEVALUE_BUTTON:
                    if(TIGUpSlope > MinUpSlope)
                    {
                        TIGUpSlope--;
                        utils_int_to_str(NextionTIG_UpSlope,TIGUpSlope,3);
                        nextion_change_val(obj_UpSlopeValue,NextionTIG_UpSlope);
                    }
                    break;
                    
                case INOMINALVALUE_BUTTON:
                    if(TIGINominal > MinINominal)
                    {
                        TIGINominal--;
                        utils_int_to_str(NextionTIG_INominal,TIGINominal,3);
                        nextion_change_val(obj_TIGINominalValue,NextionTIG_INominal);
                    }
                    break;
                    
                default:
                    break;
            }
           var_interrupt=NONE;
            break;   
        
        /*
         * NONE
         * - Update values on Nextion
         * - Set nextion page
         * - Highlight First Object
         */ 
        case NONE:
            if(FirstLoop==true)
            {
               // Change Nextion Page to TIG_PG1
               nextion_page(MenuPage_TIG_Parm1);
               
               //Read from memory and load values to Nextion
               param=TIG_PRE_GAS;
               controller_cache_read(param,&TIGPreGas);
               utils_int_to_str(NextionTIG_PreGas,TIGPreGas,3);
               nextion_change_val(obj_PreGasValue,NextionTIG_PreGas);;
               
               param=TIG_ISTART;
               controller_cache_read(param,&TIGIStart);
               utils_int_to_str(NextionTIG_IStart,TIGIStart,3);
               nextion_change_val(obj_IStartValue,NextionTIG_IStart);
               
               param=TIG_UPSLOPE;
               controller_cache_read(param,&TIGUpSlope);
               utils_int_to_str(NextionTIG_UpSlope,TIGUpSlope,3);
               nextion_change_val(obj_UpSlopeValue,NextionTIG_UpSlope);
               
               
               param=TIG_INOMINAL;
               controller_cache_read(param,&TIGINominal);
               utils_int_to_str(NextionTIG_INominal,TIGINominal,3);
               nextion_change_val(obj_TIGINominalValue,NextionTIG_INominal);
               
               // Change nexion textbox depending on the welding process
               param=WELD_PROCESS;
               controller_cache_read(param,&wprocess);
               if (wprocess == 0)
               {
                   nextion_change_txt(obj_weld_process,"TIG HF PARAMETERS");
               }
               if (wprocess == 2)
               {
                   nextion_change_txt(obj_weld_process,"LIF TIG PARAMETERS");
               }
               
                
               // Remove all highlights when page is loaded
               nextion_unselect(obj_buttonPreGas);
               nextion_unselect(obj_PreGasValue);
               nextion_unselect(obj_buttonIStart);
               nextion_unselect(obj_IStartValue);
               nextion_unselect(obj_buttonUpSlope);
               nextion_unselect(obj_UpSlopeValue);
               nextion_unselect(obj_buttonTIGINominal);
               nextion_unselect(obj_TIGINominalValue);
               
               /* Depending if the Previus state was TIG_PG2 or anything else 
                * highlight the last or the first parameter on the page.
               */ 
               if(stateMachine.p_state==TIG_PG2){
                    TIG_Buttons1 = INOMINAL_BUTTON;
                    nextion_select(obj_buttonTIGINominal);
               }
               else{
                    TIG_Buttons1 = PREGAS_BUTTON;
                    nextion_select(obj_buttonPreGas);
               }
               
               FirstLoop=false;
            }
            
            //Val_update variable from timer for value updates 
            if(val_update==true)
            {
                
                param = WELD_STATE;
                controller_cache_read(param, &wstate);

                if(wstate == 1)
                {
                    stateMachine.c_state = HOME_CHANGE_CURRENT;
                    stateMachine.p_state = TIG_PG1;
                    FirstLoop=true;
                    var_interrupt=NONE;
                }
                val_update=false;
            }  
            
            var_interrupt=NONE;
            break;   
            
        default:
            var_interrupt=NONE;
            break;
    }
    return stateMachine;
}

/**
 * \brief Responsible for the TIG (NOT PULSED) parameteres and value changes.
 * 
 * This routine will control the flow between the TIG parameters aswell
 * as allowing the user to change them according to max and min value.
 * 
 * Parameters available on this page:
 * TIG DownSlope - Min value 0, Max value 100  (Seconds/10)
 * TIG IEnd      - Min value 10, Max value 220 (A)
 * TIG PostGas   - Min value 1, Max value 200  (Seconds/10)
 * TIG Dynamicis - On/Off
 * 
 * Page 2/2 for TIG.
 * 
 *  Events:
 * - Encoder rotation to the right or left will change the button or the value
 * of the parameter.
 * - Encoder click will allow the user to open parameter value so it can be 
 * edited if on the button, if on the value, will validate the change and save it.
 * - Encoder hold will open a Help menu for this page.
 * - Back will take the user back to Selection Menu buttons on Home menu if on 
 * buttons. If on values, will discard changes.
 * - Home will take the user back to Current Value change on Home menu.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t TIG_parameters_page2(sm_t stateMachine)
{
    switch(var_interrupt)
    {
        /*
         * HOME EVENT
         * - State machine current state is now Home_Change_Current
         */
        case HOME:
            stateMachine.c_state = HOME_CHANGE_CURRENT;
            stateMachine.p_state = TIG_PG2; //TODO: confirm state
            FirstLoop=true;
            var_interrupt=NONE;
            break;
        
        /*
         * BACK EVENT
         * - Return to Selection Menu (Home buttons) if on the any parameter button
         * - Return to the parameter button if on the value button change option, without validation of 
         * the value changes, reseting the previous stored value.
         */
        case BACK: 
            switch(TIG_Buttons2)
            {
    
                case DOWNSLOPE_BUTTON:
                        stateMachine.c_state = TIG_PG1;
                        stateMachine.p_state = TIG_PG2;
                        FirstLoop=true;
                        break;
                case  IEND_BUTTON:
                        stateMachine.c_state = TIG_PG1;
                        stateMachine.p_state = TIG_PG2;
                        FirstLoop=true;
                        break;
                case POSTGAS_BUTTON:
                        stateMachine.c_state = TIG_PG1;
                        stateMachine.p_state = TIG_PG2;
                        FirstLoop=true;
                        break;
                case DYNAMICS_BUTTON:
                        stateMachine.c_state = TIG_PG1;
                        stateMachine.p_state = TIG_PG2;
                        FirstLoop=true;
                        break;
                        
                case DOWNSLOPEVALUE_BUTTON:
                    nextion_unselect(obj_DownSlopeValue);
                    nextion_select(obj_buttonDownSlope);
                    param=TIG_DOWNSLOPE_;
                    controller_cache_read(param,&TIGDownSlope);
                    utils_int_to_str(NextionTIG_DownSlope,TIGDownSlope,3);
                    nextion_change_val(obj_DownSlopeValue,NextionTIG_DownSlope);
                    TIG_Buttons2=DOWNSLOPE_BUTTON;
                    break;  
                    
                case IENDVALUE_BUTTON:
                    nextion_unselect(obj_IEndValue);
                    nextion_select(obj_buttonIEnd);
                    param=TIG_IEND;
                    controller_cache_read(param,&TIGIEnd);
                    utils_int_to_str(NextionTIG_IEnd,TIGIEnd,3);
                    nextion_change_val(obj_IEndValue,NextionTIG_IEnd);
                    TIG_Buttons2=IEND_BUTTON;
                    break;
                    
                case POSTGASVALUE_BUTTON:
                    nextion_unselect(obj_PostGasValue);
                    nextion_select(obj_buttonPostGas);
                    param=TIG_POSTGAS;
                    controller_cache_read(param,&TIGPostGas);
                    utils_int_to_str(NextionTIG_PostGas,TIGPostGas,3);
                    nextion_change_val(obj_PostGasValue,NextionTIG_PostGas);
                    TIG_Buttons2=POSTGAS_BUTTON;
                    break;
                    
                case DYNAMICSVALUE_BUTTON:
                    nextion_unselect(obj_DynamicsValue);
                    nextion_select(obj_buttonDynamics);
                    param=TIG_DYNAMICS;
                    controller_cache_read(param,&TIGDynamics);
                    utils_int_to_str(NextionTIG_Dynamics,TIGDynamics,3);
                    nextion_change_val(obj_DynamicsValue,NextionTIG_Dynamics);
                    TIG_Buttons2=DYNAMICS_BUTTON;
                    break;
                
                default:
                    break;
                  
            }
            
            var_interrupt = NONE;
            break;
        
        /*
         * ENCODER BUTTON EVENT
         * - Access value change if on parameter button
         * - Validate changes and go to parameter button if is on value button
         */  
        case ENC_BUTTON:
           switch(TIG_Buttons2)
            {
               case DOWNSLOPE_BUTTON:
                    nextion_unselect(obj_buttonDownSlope);
                    nextion_select(obj_DownSlopeValue);
                    TIG_Buttons2=DOWNSLOPEVALUE_BUTTON;
                    break;
                    
                case IEND_BUTTON:
                    nextion_unselect(obj_buttonIEnd);
                    nextion_select(obj_IEndValue);
                    TIG_Buttons2=IENDVALUE_BUTTON;
                    break;
                    
                case POSTGAS_BUTTON:
                    nextion_unselect(obj_buttonPostGas);
                    nextion_select(obj_PostGasValue);
                    TIG_Buttons2=POSTGASVALUE_BUTTON;
                    break;
                    
                case DYNAMICS_BUTTON:
                    nextion_unselect(obj_buttonDynamics);
                    nextion_select(obj_DynamicsValue);
                    TIG_Buttons2=DYNAMICSVALUE_BUTTON;
                    break;
                    
                case DOWNSLOPEVALUE_BUTTON:
                    nextion_unselect(obj_DownSlopeValue);
                    nextion_select(obj_buttonDownSlope);
                    param=TIG_DOWNSLOPE_;
                    controller_cache_write((int)param,TIGDownSlope);
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    TIG_Buttons2=DOWNSLOPE_BUTTON;
                    break;
                    
                case IENDVALUE_BUTTON:
                    nextion_unselect(obj_IEndValue);
                    nextion_select(obj_buttonIEnd);
                    param=TIG_IEND;
                    controller_cache_write((int)param,TIGIEnd);
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    TIG_Buttons2=IEND_BUTTON;
                    break;
                 
                case POSTGASVALUE_BUTTON:
                    nextion_unselect(obj_PostGasValue);
                    nextion_select(obj_buttonPostGas);
                    param=TIG_POSTGAS;
                    controller_cache_write((int)param,TIGPostGas);
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    TIG_Buttons2=POSTGAS_BUTTON;
                    break;

                case DYNAMICSVALUE_BUTTON:
                    nextion_unselect(obj_DynamicsValue);
                    nextion_select(obj_buttonDynamics);
                    param=TIG_DYNAMICS;
                    controller_cache_write((int)param,TIGDynamics);
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    TIG_Buttons2=DYNAMICS_BUTTON;
                    break;
                    
                default:
                    break;
            }
            var_interrupt=NONE;
            break;
            
        /*
         * ENCODER HOLD EVENT
         * - Open HELP page for the selected parameter or parameter value 
         */          
        case ENC_HOLD:
            switch(TIG_Buttons2)
            {
               case DOWNSLOPE_BUTTON:
                    nextion_page(MenuPage_Help_DownSlope);
                    break;
                    
                case IEND_BUTTON:
                    nextion_page(MenuPage_Help_IFinal);
                    break;
                    
                case POSTGAS_BUTTON:
                    nextion_page(MenuPage_Help_PostGas);
                    break;
                    
                case DYNAMICS_BUTTON:
                    nextion_page(MenuPage_Help_Dynamics);
                    break;
                    
                case DOWNSLOPEVALUE_BUTTON:
                    nextion_page(MenuPage_Help_DownSlope);
                    break;
                    
                case IENDVALUE_BUTTON:
                    nextion_page(MenuPage_Help_IFinal);
                    break;
                 
                case POSTGASVALUE_BUTTON:
                    nextion_page(MenuPage_Help_PostGas);
                    break;

                case DYNAMICSVALUE_BUTTON:
                    nextion_page(MenuPage_Help_Dynamics);
                    break;
                    
                default:
                    break;
            }
            stateMachine.c_state = HELP;
            stateMachine.p_state = TIG_PG2;
            FirstLoop=true;
            var_interrupt=NONE;
            break;
            
        /*
         * ENCODER RIGHT EVENT
         * - If on button, move to the right button
         * - If on value, increment value its less than MaxValue
         * - Send to nextion new value
         */      
        case ENC_RIGHT:
           switch(TIG_Buttons2)
            {
               case DOWNSLOPE_BUTTON:
                    nextion_unselect(obj_buttonDownSlope);
                    nextion_select(obj_buttonIEnd);
                    TIG_Buttons2=IEND_BUTTON;
                    break;
                    
                case IEND_BUTTON:
                    nextion_unselect(obj_buttonIEnd);
                    nextion_select(obj_buttonPostGas);
                    TIG_Buttons2=POSTGAS_BUTTON;
                    break;
                    
                case POSTGAS_BUTTON:
                    nextion_unselect(obj_buttonPostGas);
                    nextion_select(obj_buttonDynamics);
                    TIG_Buttons2=DYNAMICS_BUTTON;
                    break;
                    
                case DYNAMICS_BUTTON:

                    break;
                    
                case DOWNSLOPEVALUE_BUTTON:
                    if(TIGDownSlope < MaxDownSlope)
                    {
                        TIGDownSlope++;
                        utils_int_to_str(NextionTIG_DownSlope,TIGDownSlope,3);
                        nextion_change_val(obj_DownSlopeValue,NextionTIG_DownSlope);
                    }
                    break;
                   
                case IENDVALUE_BUTTON:
                    if(TIGIEnd < MaxIEnd)
                    {
                        TIGIEnd++;
                        utils_int_to_str(NextionTIG_IEnd,TIGIEnd,3);
                        nextion_change_val(obj_IEndValue,NextionTIG_IEnd);               
                    }
                    break;
                   
                case POSTGASVALUE_BUTTON:
                    if(TIGPostGas<MaxPostGas)
                    {
                        TIGPostGas++;
                        utils_int_to_str(NextionTIG_PostGas,TIGPostGas,3);
                        nextion_change_val(obj_PostGasValue,NextionTIG_PostGas);
                    }
                    break;
                    
                case DYNAMICSVALUE_BUTTON:
         
                    if(TIGDynamics==DynamicsOff)
                    {
                        TIGDynamics = DynamicsOn;
                        utils_int_to_str(NextionTIG_Dynamics,TIGDynamics,3);
                        nextion_change_val(obj_DynamicsValue,NextionTIG_Dynamics);
                    }
                    break;
                   
                default:
                    break;
            }
            var_interrupt=NONE; 
            break;    
        
        /*
         * ENCODER LEFT EVENT
         * - If on button, move to the left button
         * - If on value, decrement value if its above MinValue
         * - Send to nextion new value
         */ 
        case ENC_LEFT:
            switch(TIG_Buttons2)
            {
                case DOWNSLOPE_BUTTON:
                    stateMachine.c_state = TIG_PG1;
                    stateMachine.p_state = TIG_PG2;
                    FirstLoop = true;
                    break;
                    
                case IEND_BUTTON:
                    nextion_unselect(obj_buttonIEnd);
                    nextion_select(obj_buttonDownSlope);
                    TIG_Buttons2=DOWNSLOPE_BUTTON;
                    break;
                    
                case POSTGAS_BUTTON:
                    nextion_unselect(obj_buttonPostGas);
                    nextion_select(obj_buttonIEnd);
                    TIG_Buttons2=IEND_BUTTON;
                    break;
                    
                case DYNAMICS_BUTTON:
                    nextion_unselect(obj_buttonDynamics);
                    nextion_select(obj_buttonPostGas);
                    TIG_Buttons2=POSTGAS_BUTTON;
                    break;
                    
                case DOWNSLOPEVALUE_BUTTON:
                    if(TIGDownSlope > MinDownSlope)
                    {
                        TIGDownSlope--;
                        utils_int_to_str(NextionTIG_DownSlope,TIGDownSlope,3);
                        nextion_change_val(obj_DownSlopeValue,NextionTIG_DownSlope);
                    }
                    break;
                   
                case IENDVALUE_BUTTON:
                    if(TIGIEnd > MinIEnd)
                    {
                        TIGIEnd--;
                        utils_int_to_str(NextionTIG_IEnd,TIGIEnd,3);
                        nextion_change_val(obj_IEndValue,NextionTIG_IEnd);
               
                    }
                    break;
                   
                case POSTGASVALUE_BUTTON:
                    if(TIGPostGas>MinPostGas)
                    {
                        TIGPostGas--;
                        utils_int_to_str(NextionTIG_PostGas,TIGPostGas,3);
                        nextion_change_val(obj_PostGasValue,NextionTIG_PostGas);
                    }
                    break;
                    
                case DYNAMICSVALUE_BUTTON:
                    if(TIGDynamics==DynamicsOn)
                    {
                        TIGDynamics = DynamicsOff;
                        utils_int_to_str(NextionTIG_Dynamics,TIGDynamics,3);
                        nextion_change_val(obj_DynamicsValue,NextionTIG_Dynamics);
                    }            
                    break;
                   
                default:
                    break;
            }
            var_interrupt=NONE; 
            break;
            
        /*
         * NONE
         * - Update values on Nextion
         * - Set nextion page
         * - Highlight First Object
         */ 
        case NONE:
            if(FirstLoop==true)
            {
               // Change Nextion Page to TIG_PG2
               nextion_page(MenuPage_TIG_Parm2);
               
               //Update TIG buttons 2 to DownSlope (button)
               TIG_Buttons2 = DOWNSLOPE_BUTTON;
               
               //Read from memory and load values to Nextion
               param=TIG_DOWNSLOPE_;
               controller_cache_read(param,&TIGDownSlope);
               utils_int_to_str(NextionTIG_DownSlope,TIGDownSlope,3);
               nextion_change_val(obj_DownSlopeValue,NextionTIG_DownSlope);
               
               param=TIG_IEND;
               controller_cache_read(param,&TIGIEnd);
               utils_int_to_str(NextionTIG_IEnd,TIGIEnd,3);
               nextion_change_val(obj_IEndValue,NextionTIG_IEnd);
               
               param=TIG_POSTGAS;
               controller_cache_read(param,&TIGPostGas);
               utils_int_to_str(NextionTIG_PostGas,TIGPostGas,3);
               nextion_change_val(obj_PostGasValue,NextionTIG_PostGas);
               
               
               param=TIG_DYNAMICS;
               controller_cache_read(param,&TIGDynamics);
               utils_int_to_str(NextionTIG_Dynamics,TIGDynamics,3);
               nextion_change_val(obj_DynamicsValue,NextionTIG_Dynamics);
               
               // Change nexion textbox depending on the welding process
               param=WELD_PROCESS;
               controller_cache_read(param,&wprocess);
               if (wprocess == 0)
               {
                   nextion_change_txt(obj_weld_process,"TIG HF PARAMETERS");
               }
               if (wprocess == 2)
               {
                   nextion_change_txt(obj_weld_process,"LIF TIG PARAMETERS");
               }
               
               // Remove all highlights when page is loaded
               nextion_unselect(obj_buttonDownSlope);
               nextion_unselect(obj_DownSlopeValue);
               nextion_unselect(obj_buttonIEnd);
               nextion_unselect(obj_IEndValue);
               nextion_unselect(obj_buttonPostGas);
               nextion_unselect(obj_PostGasValue);
               nextion_unselect(obj_buttonDynamics);
               nextion_unselect(obj_DynamicsValue);
                
               TIG_Buttons2 = PREGAS_BUTTON;
               nextion_select(obj_buttonDownSlope);             

               FirstLoop=false;
            }
            
            //Val_update variable from timer for value updates
            if(val_update==true)
            {
                
                param = WELD_STATE;
                controller_cache_read(param, &wstate);

                if(wstate == 1)
                {
                    stateMachine.c_state = HOME_CHANGE_CURRENT;
                    stateMachine.p_state = TIG_PG2;
                    FirstLoop=true;
                    var_interrupt=NONE;
                }
                val_update=false;
            }
             
            var_interrupt=NONE;
            break;   
            
        default:
            var_interrupt=NONE;
            break;
    }
    return stateMachine;
}

/**
 * \brief Responsible for the TIG (NOT PULSED) parameteres in Spot mode and value changes.
 * 
 * This routine will control the flow between the TIG parameters in Spot mode 
 * aswell as allowing the user to change them according to max and min value.
 * 
 * Parameters available on this page:
 * TIG Pre Gas  - Min value 1, Max value 100  (Seconds/10)
 * TIG IStart   - Min value 1, Max value 220  (A)
 * TIG UpSlope  - Min value 0, Max value 100  (Seconds/10)
 * TIG INominal - Min value 10, Max value 220 (%)
 * 
 * Page 1/3 for TIG Spot.
 * 
 *  Events:
 * - Encoder rotation to the right or left will change the button or the value
 * of the parameter.
 * - Encoder click will allow the user to open parameter value so it can be 
 * edited if on the button, if on the value, will validate the change and save it.
 * - Encoder hold will open a Help menu for this page.
 * - Back will take the user back to Selection Menu buttons on Home menu if on 
 * buttons. If on values, will discard changes.
 * - Home will take the user back to Current Value change on Home menu.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t TIG_Spot_parameters_page1(sm_t stateMachine)
{
    switch(var_interrupt)
    {
        /*
         * HOME EVENT
         * - State machine current state is now Home_Change_Current
         */
        case HOME:
            stateMachine.c_state = HOME_CHANGE_CURRENT;
            stateMachine.p_state = TIG_SPOT_PG1;
            FirstLoop=true;
            var_interrupt=NONE;
            break;
        
       /*
         * BACK EVENT
         * - Return to Selection Menu (Home buttons) if on the any parameter button
         * - Return to the parameter button if on the value button change option, without validation of 
         * the value changes, reseting the previous stored value.
         */
            
        case BACK:
            switch(TIG_Buttons1)
            {
                case PREGAS_BUTTON:
                        stateMachine.c_state = SELECTION_MENU;
                        stateMachine.p_state = TIG_SPOT_PG1;
                        FirstLoop=true;
                        break;
                case ISTART_BUTTON:
                        stateMachine.c_state = SELECTION_MENU;
                        stateMachine.p_state = TIG_SPOT_PG1;
                        FirstLoop=true;
                        break;
                case UPSLOPE_BUTTON:
                        stateMachine.c_state = SELECTION_MENU;
                        stateMachine.p_state = TIG_SPOT_PG1;
                        FirstLoop=true;
                        break;
                case INOMINAL_BUTTON:
                        stateMachine.c_state = SELECTION_MENU;
                        stateMachine.p_state = TIG_SPOT_PG1;
                        FirstLoop=true;
                        break;
                        
                case PREGASVALUE_BUTTON:
                    nextion_unselect(obj_PreGasValue);
                    nextion_select(obj_buttonPreGas);
                    param=TIG_PRE_GAS;
                    controller_cache_read(param,&TIGPreGas);
                    utils_int_to_str(NextionTIG_PreGas,TIGPreGas,3);
                    nextion_change_val(obj_PreGasValue,NextionTIG_PreGas);
                    TIG_Buttons1=PREGAS_BUTTON;
                    break;  
                    
                case ISTARTVALUE_BUTTON:
                    nextion_unselect(obj_IStartValue);
                    nextion_select(obj_buttonIStart);
                    param=TIG_ISTART;
                    controller_cache_read(param,&TIGIStart);
                    utils_int_to_str(NextionTIG_IStart,TIGIStart,3);
                    nextion_change_val(obj_IStartValue,NextionTIG_IStart);
                    TIG_Buttons1=ISTART_BUTTON;
                    break;
                    
                case UPSLOPEVALUE_BUTTON:
                    nextion_unselect(obj_UpSlopeValue);
                    nextion_select(obj_buttonUpSlope);
                    param=TIG_UPSLOPE;
                    controller_cache_read(param,&TIGUpSlope);
                    utils_int_to_str(NextionTIG_UpSlope,TIGUpSlope,3);
                    nextion_change_val(obj_UpSlopeValue,NextionTIG_UpSlope);
                    TIG_Buttons1=UPSLOPE_BUTTON;
                    break;
                    
                case INOMINALVALUE_BUTTON:
                    nextion_unselect(obj_TIGINominalValue);
                    nextion_select(obj_buttonTIGINominal);
                    param=TIG_INOMINAL;
                    controller_cache_read(param,&TIGINominal);
                    utils_int_to_str(NextionTIG_INominal,TIGINominal,3);
                    nextion_change_val(obj_TIGINominalValue,NextionTIG_IStart);
                    TIG_Buttons1=INOMINAL_BUTTON;
                    break;
                
                default:
                    break;
            }
            var_interrupt = NONE;
            break;
        
        /*
         * ENCODER BUTTON EVENT
         * - Access value change if on parameter button
         * - Validate changes and go to parameter button if is on value button
         */  
        case ENC_BUTTON:
           switch(TIG_Buttons1)
           {
               case PREGAS_BUTTON:
                    nextion_unselect(obj_buttonPreGas);
                    nextion_select(obj_PreGasValue);
                    TIG_Buttons1=PREGASVALUE_BUTTON;
                    break;
                    
                case ISTART_BUTTON:
                    nextion_unselect(obj_buttonIStart);
                    nextion_select(obj_IStartValue);
                    TIG_Buttons1=ISTARTVALUE_BUTTON;
                    break;
                    
                case UPSLOPE_BUTTON:
                    nextion_unselect(obj_buttonUpSlope);
                    nextion_select(obj_UpSlopeValue);
                    TIG_Buttons1=UPSLOPEVALUE_BUTTON;
                    break;
                    
                case INOMINAL_BUTTON:
                    nextion_unselect(obj_buttonTIGINominal);
                    nextion_select(obj_TIGINominalValue);
                    TIG_Buttons1=INOMINALVALUE_BUTTON;
                    break;
                    
                case PREGASVALUE_BUTTON:
                    nextion_unselect(obj_PreGasValue);
                    nextion_select(obj_buttonPreGas);
                    param=TIG_PRE_GAS;
                    controller_cache_write((int)param,TIGPreGas);
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    TIG_Buttons1=PREGAS_BUTTON;
                    break;
                    
                case ISTARTVALUE_BUTTON:
                    nextion_unselect(obj_IStartValue);
                    nextion_select(obj_buttonIStart);
                    param=TIG_ISTART;
                    controller_cache_write((int)param,TIGIStart);
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    TIG_Buttons1=ISTART_BUTTON;
                    break;
                    
                case UPSLOPEVALUE_BUTTON:
                    nextion_unselect(obj_UpSlopeValue);
                    nextion_select(obj_buttonUpSlope);
                    param=TIG_UPSLOPE;
                    controller_cache_write((int)param,TIGUpSlope);
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    TIG_Buttons1=UPSLOPE_BUTTON;
                    break;
                    
                case INOMINALVALUE_BUTTON:
                    nextion_unselect(obj_TIGINominalValue);
                    nextion_select(obj_buttonTIGINominal);
                    param=TIG_INOMINAL;
                    controller_cache_write((int)param,TIGINominal);
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    TIG_Buttons1=INOMINAL_BUTTON;
                    break;
                 
                default:
                    break;
            }
            var_interrupt=NONE;
            break;
         
         /*
         * ENCODER HOLD EVENT
         * - Open HELP page for the selected parameter or parameter value 
         */         
        case ENC_HOLD:
            
            switch(TIG_Buttons1)
           {
               case PREGAS_BUTTON:
                    nextion_page(MenuPage_Help_PreGas); 
                    break;
                    
                case ISTART_BUTTON:
                    nextion_page(MenuPage_Help_IStart); 
                    break;
                    
                case UPSLOPE_BUTTON:
                    nextion_page(MenuPage_Help_UpSlope); 
                    break;
                    
                case INOMINAL_BUTTON:
                    nextion_page(MenuPage_Help_INominal); 
                    break;
                    
                case PREGASVALUE_BUTTON:
                    nextion_page(MenuPage_Help_PreGas); 
                    break;
                    
                case ISTARTVALUE_BUTTON:
                    nextion_page(MenuPage_Help_IStart); 
                    break;
                    
                case UPSLOPEVALUE_BUTTON:
                    nextion_page(MenuPage_Help_UpSlope); 
                    break;
                    
                case INOMINALVALUE_BUTTON:
                    nextion_page(MenuPage_Help_INominal); 
                    break;
                    
                default:
                    break;
            }
            
            stateMachine.c_state = HELP;
            stateMachine.p_state = TIG_SPOT_PG1;
            FirstLoop=true;
            var_interrupt=NONE;
            break;
        
        /*
         * ENCODER RIGHT EVENT
         * - If on button, move to the right button
         * - If on value, increment value its less than MaxValue
         * - Send to nextion new value
         */           
        case ENC_RIGHT:
           switch(TIG_Buttons1)
            {
               case PREGAS_BUTTON:
                    nextion_unselect(obj_buttonPreGas);
                    nextion_select(obj_buttonIStart);
                    TIG_Buttons1=ISTART_BUTTON;
                    break;
                    
                case ISTART_BUTTON:
                    nextion_unselect(obj_buttonIStart);
                    nextion_select(obj_buttonUpSlope);
                    TIG_Buttons1=UPSLOPE_BUTTON;
                    break;
                    
                case UPSLOPE_BUTTON:
                    nextion_unselect(obj_buttonUpSlope);
                    nextion_select(obj_buttonTIGINominal);
                    TIG_Buttons1=INOMINAL_BUTTON;
                    break;
                    
                case INOMINAL_BUTTON:
                    stateMachine.c_state = TIG_SPOT_PG2;
                    stateMachine.p_state = TIG_SPOT_PG1;
                    FirstLoop = true;
                    var_interrupt=NONE;
                    break;
                    
                case PREGASVALUE_BUTTON:
                    if(TIGPreGas < MaxPreGas)
                    {
                        TIGPreGas++;
                        utils_int_to_str(NextionTIG_PreGas,TIGPreGas,3);
                        nextion_change_val(obj_PreGasValue,NextionTIG_PreGas);
                    }
                    break;
                   
                case ISTARTVALUE_BUTTON:
                    if(TIGIStart < MaxIStart)
                    {
                        TIGIStart++;
                        utils_int_to_str(NextionTIG_IStart,TIGIStart,3);
                        nextion_change_val(obj_IStartValue,NextionTIG_IStart);
               
                    }
                    break;
                   
                case UPSLOPEVALUE_BUTTON:
                    if(TIGUpSlope<MaxUpSlope)
                    {
                        TIGUpSlope++;
                        utils_int_to_str(NextionTIG_UpSlope,TIGUpSlope,3);
                        nextion_change_val(obj_UpSlopeValue,NextionTIG_UpSlope);
                    }
                    break;
                    
                case INOMINALVALUE_BUTTON:
                    if(TIGINominal<MaxINominal)
                    {
                        TIGINominal++;
                        utils_int_to_str(NextionTIG_INominal,TIGINominal,3);
                        nextion_change_val(obj_TIGINominalValue,NextionTIG_INominal);
                    }
                    break;
                   
                default:
                    break;
            }
            var_interrupt=NONE; 
            break;    
        
        /*
         * ENCODER LEFT EVENT
         * - If on button, move to the left button
         * - If on value, decrement value if its above MinValue
         * - Send to nextion new value
         */ 
        case ENC_LEFT:
               switch(TIG_Buttons1)
            {
               case PREGAS_BUTTON:
                   
                    break;
                    
                case ISTART_BUTTON:
                    nextion_unselect(obj_buttonIStart);
                    nextion_select(obj_buttonPreGas);
                    TIG_Buttons1=PREGAS_BUTTON;
                    break;
                    
                case UPSLOPE_BUTTON:
                    nextion_unselect(obj_buttonUpSlope);
                    nextion_select(obj_buttonIStart);
                    TIG_Buttons1=ISTART_BUTTON;
                    break;
                    
                case INOMINAL_BUTTON:
                    nextion_unselect(obj_buttonTIGINominal);
                    nextion_select(obj_buttonUpSlope);
                    TIG_Buttons1=UPSLOPE_BUTTON;
                    break;
                    
                case PREGASVALUE_BUTTON:
                    if(TIGPreGas > MinPreGas)
                    {
                        TIGPreGas--;
                        utils_int_to_str(NextionTIG_PreGas,TIGPreGas,3);
                        nextion_change_val(obj_PreGasValue,NextionTIG_PreGas);
                    }
                    break;
                   
                case ISTARTVALUE_BUTTON:
                    if(TIGIStart > MinIStart)
                    {
                        TIGIStart--;
                        utils_int_to_str(NextionTIG_IStart,TIGIStart,3);
                        nextion_change_val(obj_IStartValue,NextionTIG_IStart);
               
                    }
                    break;
                   
                case UPSLOPEVALUE_BUTTON:
                    if(TIGUpSlope > MinUpSlope)
                    {
                        TIGUpSlope--;
                        utils_int_to_str(NextionTIG_UpSlope,TIGUpSlope,3);
                        nextion_change_val(obj_UpSlopeValue,NextionTIG_UpSlope);
                    }
                    break;
                    
                case INOMINALVALUE_BUTTON:
                    if(TIGINominal > MinINominal)
                    {
                        TIGINominal--;
                        utils_int_to_str(NextionTIG_INominal,TIGINominal,3);
                        nextion_change_val(obj_TIGINominalValue,NextionTIG_INominal);
                    }
                    break;
                    
                default:
                    break;
            }
           var_interrupt=NONE;
            break;   
        
        /*
         * NONE
         * - Update values on Nextion
         * - Set nextion page
         * - Highlight First Object
         */ 
        case NONE:
            if(FirstLoop==true)
            {
               // Change Nextion Page to TIG_SPOT_PG1
               nextion_page(MenuPage_TIG_Spot_Parm1);
               
               //Read from memory and load values to Nextion
               param=TIG_PRE_GAS;
               controller_cache_read(param,&TIGPreGas);
               utils_int_to_str(NextionTIG_PreGas,TIGPreGas,3);
               nextion_change_val(obj_PreGasValue,NextionTIG_PreGas);;
               
               param=TIG_ISTART;
               controller_cache_read(param,&TIGIStart);
               utils_int_to_str(NextionTIG_IStart,TIGIStart,3);
               nextion_change_val(obj_IStartValue,NextionTIG_IStart);
               
               param=TIG_UPSLOPE;
               controller_cache_read(param,&TIGUpSlope);
               utils_int_to_str(NextionTIG_UpSlope,TIGUpSlope,3);
               nextion_change_val(obj_UpSlopeValue,NextionTIG_UpSlope);
               
               param=TIG_INOMINAL;
               controller_cache_read(param,&TIGINominal);
               utils_int_to_str(NextionTIG_INominal,TIGINominal,3);
               nextion_change_val(obj_TIGINominalValue,NextionTIG_INominal);
               
               // Change nexion textbox depending on the welding process
               param=WELD_PROCESS;
               controller_cache_read(param,&wprocess);
               if (wprocess == 0)
               {
                   nextion_change_txt(obj_weld_process,"TIG HF PARAMETERS");
               }
               if (wprocess == 2)
               {
                   nextion_change_txt(obj_weld_process,"LIF TIG PARAMETERS");
               }
               
                // Remove all highlights when page is loaded
                nextion_unselect(obj_buttonPreGas);
                nextion_unselect(obj_PreGasValue);
                nextion_unselect(obj_buttonIStart);
                nextion_unselect(obj_IStartValue);
                nextion_unselect(obj_buttonUpSlope);
                nextion_unselect(obj_UpSlopeValue);
                nextion_unselect(obj_buttonTIGINominal);
                nextion_unselect(obj_TIGINominalValue);
                
                /* Depending if the Previus state was TIG_SPOT_PG2 or anything else 
                * highlight the last or the first parameter on the page.
                */ 
                if(stateMachine.p_state==TIG_SPOT_PG2){
                    TIG_Buttons1 = INOMINAL_BUTTON;
                    nextion_select(obj_buttonTIGINominal);
                }
                else{
                    TIG_Buttons1 = PREGAS_BUTTON;
                    nextion_select(obj_buttonPreGas);
                }
                
                FirstLoop=false;
            }
            
            //Val_update variable from timer for value updates
            if(val_update==true)
            {
                
                param = WELD_STATE;
                controller_cache_read(param, &wstate);

                if(wstate == 1)
                {
                    stateMachine.c_state = HOME_CHANGE_CURRENT;
                    stateMachine.p_state = TIG_SPOT_PG1;
                    FirstLoop=true;
                    var_interrupt=NONE;
                }
                val_update=false;
            }
            
            var_interrupt=NONE;
            break;   
            
        default:
            var_interrupt=NONE;
            break;
    }
    return stateMachine;
}

/**
 * \brief Responsible for the TIG (NOT PULSED) parameteres in Spot mode and value changes.
 * 
 * This routine will control the flow between the TIG parameters in Spot mode 
 * aswell as allowing the user to change them according to max and min value.
 * 
 * Parameters available on this page:
 * TIG DownSlope - Min value 0, Max value 100  (Seconds/10)
 * TIG IEnd      - Min value 10, Max value 220 (A)
 * TIG PostGas   - Min value 1, Max value 200  (Seconds/10)
 * TIG Dynamicis - On/Off
 * 
 * Page 2/3 for TIG Spot.
 * 
 *  Events:
 * - Encoder rotation to the right or left will change the button or the value
 * of the parameter.
 * - Encoder click will allow the user to open parameter value so it can be 
 * edited if on the button, if on the value, will validate the change and save it.
 * - Encoder hold will open a Help menu for this page.
 * - Back will take the user back to Selection Menu buttons on Home menu if on 
 * buttons. If on values, will discard changes.
 * - Home will take the user back to Current Value change on Home menu.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t TIG_Spot_parameters_page2(sm_t stateMachine)
{
    switch(var_interrupt)
    {
        /*
         * HOME EVENT
         * - State machine current state is now Home_Change_Current
         */
        case HOME:
            stateMachine.c_state = HOME_CHANGE_CURRENT;
            stateMachine.p_state = TIG_SPOT_PG2; //TODO: confirm state
            FirstLoop=true;
            var_interrupt=NONE;
            break;
        
       /*
         * BACK EVENT
         * - Return to Selection Menu (Home buttons) if on the any parameter button
         * - Return to the parameter button if on the value button change option, without validation of 
         * the value changes, reseting the previous stored value.
         */
        case BACK: 
            switch(TIG_Buttons2)
            {
    
                case DOWNSLOPE_BUTTON:
                        stateMachine.c_state = TIG_SPOT_PG1;
                        stateMachine.p_state = TIG_SPOT_PG2;
                        FirstLoop=true;
                        break;
                case  IEND_BUTTON:
                        stateMachine.c_state = TIG_SPOT_PG1;
                        stateMachine.p_state = TIG_SPOT_PG2;
                        FirstLoop=true;
                        break;
                case POSTGAS_BUTTON:
                        stateMachine.c_state = TIG_SPOT_PG1;
                        stateMachine.p_state = TIG_SPOT_PG2;
                        FirstLoop=true;
                        break;
                case DYNAMICS_BUTTON:
                        stateMachine.c_state = TIG_SPOT_PG1;
                        stateMachine.p_state = TIG_SPOT_PG2;
                        FirstLoop=true;
                        break;
                        
                case DOWNSLOPEVALUE_BUTTON:
                    nextion_unselect(obj_DownSlopeValue);
                    nextion_select(obj_buttonDownSlope);
                    param=TIG_DOWNSLOPE_;
                    controller_cache_read(param,&TIGDownSlope);
                    utils_int_to_str(NextionTIG_DownSlope,TIGDownSlope,3);
                    nextion_change_val(obj_DownSlopeValue,NextionTIG_DownSlope);
                    TIG_Buttons2=DOWNSLOPE_BUTTON;
                    break;  
                    
                case IENDVALUE_BUTTON:
                    nextion_unselect(obj_IEndValue);
                    nextion_select(obj_buttonIEnd);
                    param=TIG_IEND;
                    controller_cache_read(param,&TIGIEnd);
                    utils_int_to_str(NextionTIG_IEnd,TIGIEnd,3);
                    nextion_change_val(obj_IEndValue,NextionTIG_IEnd);
                    TIG_Buttons2=IEND_BUTTON;
                    break;
                    
                case POSTGASVALUE_BUTTON:
                    nextion_unselect(obj_PostGasValue);
                    nextion_select(obj_buttonPostGas);
                    param=TIG_POSTGAS;
                    controller_cache_read(param,&TIGPostGas);
                    utils_int_to_str(NextionTIG_PostGas,TIGPostGas,3);
                    nextion_change_val(obj_PostGasValue,NextionTIG_PostGas);
                    TIG_Buttons2=POSTGAS_BUTTON;
                    break;
                    
                case DYNAMICSVALUE_BUTTON:
                    nextion_unselect(obj_DynamicsValue);
                    nextion_select(obj_buttonDynamics);
                    param=TIG_DYNAMICS;
                    controller_cache_read(param,&TIGDynamics);
                    utils_int_to_str(NextionTIG_Dynamics,TIGDynamics,3);
                    nextion_change_val(obj_DynamicsValue,NextionTIG_Dynamics);
                    TIG_Buttons2=DYNAMICS_BUTTON;
                    break;
                
                default:
                    break;
                  
            }
            
            var_interrupt = NONE;
            break;
        
        /*
         * ENCODER BUTTON EVENT
         * - Access value change if on parameter button
         * - Validate changes and go to parameter button if is on value button
         */    
        case ENC_BUTTON:
           switch(TIG_Buttons2)
            {
               case DOWNSLOPE_BUTTON:
                    nextion_unselect(obj_buttonDownSlope);
                    nextion_select(obj_DownSlopeValue);
                    TIG_Buttons2=DOWNSLOPEVALUE_BUTTON;
                    break;
                    
                case IEND_BUTTON:
                    nextion_unselect(obj_buttonIEnd);
                    nextion_select(obj_IEndValue);
                    TIG_Buttons2=IENDVALUE_BUTTON;
                    break;
                    
                case POSTGAS_BUTTON:
                    nextion_unselect(obj_buttonPostGas);
                    nextion_select(obj_PostGasValue);
                    TIG_Buttons2=POSTGASVALUE_BUTTON;
                    break;
                    
                case DYNAMICS_BUTTON:
                    nextion_unselect(obj_buttonDynamics);
                    nextion_select(obj_DynamicsValue);
                    TIG_Buttons2=DYNAMICSVALUE_BUTTON;
                    break;
                    
                case DOWNSLOPEVALUE_BUTTON:
                    nextion_unselect(obj_DownSlopeValue);
                    nextion_select(obj_buttonDownSlope);
                    param=TIG_DOWNSLOPE_;
                    controller_cache_write((int)param,TIGDownSlope);
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    TIG_Buttons2=DOWNSLOPE_BUTTON;
                    break;
                    
                case IENDVALUE_BUTTON:
                    nextion_unselect(obj_IEndValue);
                    nextion_select(obj_buttonIEnd);
                    param=TIG_IEND;
                    controller_cache_write((int)param,TIGIEnd);
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    TIG_Buttons2=IEND_BUTTON;
                    break;
                 
                case POSTGASVALUE_BUTTON:
                    nextion_unselect(obj_PostGasValue);
                    nextion_select(obj_buttonPostGas);
                    param=TIG_POSTGAS;
                    controller_cache_write((int)param,TIGPostGas);
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    TIG_Buttons2=POSTGAS_BUTTON;
                    break;

                case DYNAMICSVALUE_BUTTON:
                    nextion_unselect(obj_DynamicsValue);
                    nextion_select(obj_buttonDynamics);
                    param=TIG_DYNAMICS;
                    controller_cache_write((int)param,TIGDynamics);
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    TIG_Buttons2=DYNAMICS_BUTTON;
                    break;
                    
                default:
                    break;
            }
            var_interrupt=NONE;
            break;
    
         /*
         * ENCODER HOLD EVENT
         * - Open HELP page for the selected parameter or parameter value 
         */        
        case ENC_HOLD:
            switch(TIG_Buttons2)
            {
               case DOWNSLOPE_BUTTON:
                    nextion_page(MenuPage_Help_DownSlope);
                    break;
                    
                case IEND_BUTTON:
                    nextion_page(MenuPage_Help_IFinal);
                    break;
                    
                case POSTGAS_BUTTON:
                    nextion_page(MenuPage_Help_PostGas);
                    break;
                    
                case DYNAMICS_BUTTON:
                    nextion_page(MenuPage_Help_Dynamics);
                    break;
                    
                case DOWNSLOPEVALUE_BUTTON:
                    nextion_page(MenuPage_Help_DownSlope);
                    break;
                    
                case IENDVALUE_BUTTON:
                    nextion_page(MenuPage_Help_IFinal);
                    break;
                 
                case POSTGASVALUE_BUTTON:
                    nextion_page(MenuPage_Help_PostGas);
                    break;

                case DYNAMICSVALUE_BUTTON:
                    nextion_page(MenuPage_Help_Dynamics);
                    break;
                    
                default:
                    break;
            }
            stateMachine.c_state = HELP;
            stateMachine.p_state = TIG_SPOT_PG2;
            FirstLoop=true;
            var_interrupt=NONE;
            break;
        
        /*
         * ENCODER RIGHT EVENT
         * - If on button, move to the right button
         * - If on value, increment value its less than MaxValue
         * - Send to nextion new value
         */         
        case ENC_RIGHT:
           switch(TIG_Buttons2)
            {
               case DOWNSLOPE_BUTTON:
                    nextion_unselect(obj_buttonDownSlope);
                    nextion_select(obj_buttonIEnd);
                    TIG_Buttons2=IEND_BUTTON;
                    break;
                    
                case IEND_BUTTON:
                    nextion_unselect(obj_buttonIEnd);
                    nextion_select(obj_buttonPostGas);
                    TIG_Buttons2=POSTGAS_BUTTON;
                    break;
                    
                case POSTGAS_BUTTON:
                    nextion_unselect(obj_buttonPostGas);
                    nextion_select(obj_buttonDynamics);
                    TIG_Buttons2=DYNAMICS_BUTTON;
                    break;
                    
                case DYNAMICS_BUTTON:

                        stateMachine.c_state = TIG_SPOT_PG3;
                        stateMachine.p_state = TIG_SPOT_PG2;
                        FirstLoop = true;

                    break;
                    
                case DOWNSLOPEVALUE_BUTTON:
                    if(TIGDownSlope < MaxDownSlope)
                    {
                        TIGDownSlope++;
                        utils_int_to_str(NextionTIG_DownSlope,TIGDownSlope,3);
                        nextion_change_val(obj_DownSlopeValue,NextionTIG_DownSlope);
                    }
                    break;
                   
                case IENDVALUE_BUTTON:
                    if(TIGIEnd < MaxIEnd)
                    {
                        TIGIEnd++;
                        utils_int_to_str(NextionTIG_IEnd,TIGIEnd,3);
                        nextion_change_val(obj_IEndValue,NextionTIG_IEnd);               
                    }
                    break;
                   
                case POSTGASVALUE_BUTTON:
                    if(TIGPostGas<MaxPostGas)
                    {
                        TIGPostGas++;
                        utils_int_to_str(NextionTIG_PostGas,TIGPostGas,3);
                        nextion_change_val(obj_PostGasValue,NextionTIG_PostGas);
                    }
                    break;
                    
                case DYNAMICSVALUE_BUTTON:
         
                    if(TIGDynamics==DynamicsOff)
                    {
                        TIGDynamics = DynamicsOn;
                        utils_int_to_str(NextionTIG_Dynamics,TIGDynamics,3);
                        nextion_change_val(obj_DynamicsValue,NextionTIG_Dynamics);
                    }
                    break;
                   
                default:
                    break;
            }
            var_interrupt=NONE; 
            break;    
        
        /*
         * ENCODER LEFT EVENT
         * - If on button, move to the left button
         * - If on value, decrement value if its above MinValue
         * - Send to nextion new value
         */ 
        case ENC_LEFT:
            switch(TIG_Buttons2)
            {
                case DOWNSLOPE_BUTTON:
                    stateMachine.c_state = TIG_SPOT_PG1;
                    stateMachine.p_state = TIG_SPOT_PG2;
                    FirstLoop = true;
                    break;
                    
                case IEND_BUTTON:
                    nextion_unselect(obj_buttonIEnd);
                    nextion_select(obj_buttonDownSlope);
                    TIG_Buttons2=DOWNSLOPE_BUTTON;
                    break;
                    
                case POSTGAS_BUTTON:
                    nextion_unselect(obj_buttonPostGas);
                    nextion_select(obj_buttonIEnd);
                    TIG_Buttons2=IEND_BUTTON;
                    break;
                    
                case DYNAMICS_BUTTON:
                    nextion_unselect(obj_buttonDynamics);
                    nextion_select(obj_buttonPostGas);
                    TIG_Buttons2=POSTGAS_BUTTON;
                    break;
                    
                case DOWNSLOPEVALUE_BUTTON:
                    if(TIGDownSlope > MinDownSlope)
                    {
                        TIGDownSlope--;
                        utils_int_to_str(NextionTIG_DownSlope,TIGDownSlope,3);
                        nextion_change_val(obj_DownSlopeValue,NextionTIG_DownSlope);
                    }
                    break;
                   
                case IENDVALUE_BUTTON:
                    if(TIGIEnd > MinIEnd)
                    {
                        TIGIEnd--;
                        utils_int_to_str(NextionTIG_IEnd,TIGIEnd,3);
                        nextion_change_val(obj_IEndValue,NextionTIG_IEnd);
               
                    }
                    break;
                   
                case POSTGASVALUE_BUTTON:
                    if(TIGPostGas>MinPostGas)
                    {
                        TIGPostGas--;
                        utils_int_to_str(NextionTIG_PostGas,TIGPostGas,3);
                        nextion_change_val(obj_PostGasValue,NextionTIG_PostGas);
                    }
                    break;
                    
                case DYNAMICSVALUE_BUTTON:
                    if(TIGDynamics==DynamicsOn)
                    {
                        TIGDynamics = DynamicsOff;
                        utils_int_to_str(NextionTIG_Dynamics,TIGDynamics,3);
                        nextion_change_val(obj_DynamicsValue,NextionTIG_Dynamics);
                    }            
                    break;
                   
                default:
                    break;
            }
            var_interrupt=NONE; 
            break;   
        /*
         * NONE
         * - Update values on Nextion
         * - Set nextion page
         * - Highlight First Object
         */ 
        case NONE:
            if(FirstLoop==true)
            {
               // Change Nextion Page to TIG_SPOT_PG2
               nextion_page(MenuPage_TIG_Spot_Parm2);
               
               //Read from memory and load values to Nextion
               param=TIG_DOWNSLOPE_;
               controller_cache_read(param,&TIGDownSlope);
               utils_int_to_str(NextionTIG_DownSlope,TIGDownSlope,3);
               nextion_change_val(obj_DownSlopeValue,NextionTIG_DownSlope);
               
               param=TIG_IEND;
               controller_cache_read(param,&TIGIEnd);
               utils_int_to_str(NextionTIG_IEnd,TIGIEnd,3);
               nextion_change_val(obj_IEndValue,NextionTIG_IEnd);
               
               param=TIG_POSTGAS;
               controller_cache_read(param,&TIGPostGas);
               utils_int_to_str(NextionTIG_PostGas,TIGPostGas,3);
               nextion_change_val(obj_PostGasValue,NextionTIG_PostGas);
               
               param=TIG_DYNAMICS;
               controller_cache_read(param,&TIGDynamics);
               utils_int_to_str(NextionTIG_Dynamics,TIGDynamics,3);
               nextion_change_val(obj_DynamicsValue,NextionTIG_Dynamics);
               
               // Change nexion textbox depending on the welding process
               param=WELD_PROCESS;
               controller_cache_read(param,&wprocess);
               if (wprocess == 0)
               {
                   nextion_change_txt(obj_weld_process,"TIG HF PARAMETERS");
               }
               if (wprocess == 2)
               {
                   nextion_change_txt(obj_weld_process,"LIF TIG PARAMETERS");
               }
               
                // Remove all highlights when page is loaded
                nextion_unselect(obj_buttonDownSlope);
                nextion_unselect(obj_DownSlopeValue);
                nextion_unselect(obj_buttonIEnd);
                nextion_unselect(obj_IEndValue);
                nextion_unselect(obj_buttonPostGas);
                nextion_unselect(obj_PostGasValue);
                nextion_unselect(obj_buttonDynamics);
                nextion_unselect(obj_DynamicsValue);
                
                /* Depending if the Previus state was TIG_SPOT_PG3 or anything else 
                * highlight the last or the first parameter on the page.
                */ 
                if(stateMachine.p_state==TIG_SPOT_PG3){
                    TIG_Buttons2 = DYNAMICS_BUTTON;
                    nextion_select(obj_buttonDynamics);
                }
                else{
                    TIG_Buttons2 = DOWNSLOPE_BUTTON;
                    nextion_select(obj_buttonDownSlope);
                }
               

                FirstLoop=false;
            }
            
            //Val_update variable from timer for value updates
            if(val_update==true)
            {
                
                param = WELD_STATE;
                controller_cache_read(param, &wstate);

                if(wstate == 1)
                {
                    stateMachine.c_state = HOME_CHANGE_CURRENT;
                    stateMachine.p_state = TIG_SPOT_PG2;
                    FirstLoop=true;
                    var_interrupt=NONE;
                }
                val_update=false;
            }
            
            var_interrupt=NONE;
            break;   
            
        default:
            var_interrupt=NONE;
            break;
    }
    return stateMachine;
}

/**
 * \brief Responsible for the TIG (NOT PULSED) parameteres in Spot mode and value changes.
 * 
 * This routine will control the flow between the TIG parameters in Spot mode 
 * aswell as allowing the user to change them according to max and min value.
 * 
 * Parameters available on this page:
 * TIG SpotTime - Min value 1, Max value 100  (Seconds/10)
 * 
 * Page 3/3 for TIG Spot.
 * 
 *  Events:
 * - Encoder rotation to the right or left will change the button or the value
 * of the parameter.
 * - Encoder click will allow the user to open parameter value so it can be 
 * edited if on the button, if on the value, will validate the change and save it.
 * - Encoder hold will open a Help menu for this page.
 * - Back will take the user back to Selection Menu buttons on Home menu if on 
 * buttons. If on values, will discard changes.
 * - Home will take the user back to Current Value change on Home menu.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t TIG_Spot_parameters_page3(sm_t stateMachine)
{
    switch (var_interrupt)
    {
        /*
         * HOME EVENT
         * - State machine current state is now Home_Change_Current
         */
        case HOME:
            stateMachine.c_state = HOME_CHANGE_CURRENT;
            stateMachine.p_state = TIG_SPOT_PG3;
            FirstLoop = true;
            var_interrupt = NONE;
            break;

        /*
         * BACK EVENT
         * - Return to Selection Menu (Home buttons) if on the any parameter button
         * - Return to the parameter button if on the value button change option, without validation of 
         * the value changes, reseting the previous stored value.
         */
        case BACK:
            switch(TIG_Spot_Buttons)
            {
    
                case SPOTTIME_BUTTON:
                    stateMachine.c_state = TIG_SPOT_PG2;
                    stateMachine.p_state = TIG_SPOT_PG3;
                    FirstLoop=true;
                    break;

                case SPOTTIMEVALUE_BUTTON:
                    nextion_unselect(obj_SpotTimeValue);
                    nextion_select(obj_buttonSpotTime);
                    param=TIG_DOWNSLOPE_;
                    controller_cache_read(param,&TIGSpotTime);
                    utils_int_to_str(NextionTIG_SpotTime,TIGSpotTime,3);
                    nextion_change_val(obj_SpotTimeValue,NextionTIG_SpotTime);
                    TIG_Spot_Buttons=SPOTTIME_BUTTON;
                    break;  
                
                default:
                    break;  
            }
            
            var_interrupt = NONE;
            break; 

        /*
         * ENCODER BUTTON EVENT
         * - Access value change if on parameter button
         * - Validate changes and go to parameter button if is on value button
         */  
        case ENC_BUTTON:
            switch(TIG_Spot_Buttons)
            {
                case SPOTTIME_BUTTON:
                    nextion_unselect(obj_buttonSpotTime);
                    nextion_select(obj_SpotTimeValue);
                    TIG_Spot_Buttons = SPOTTIMEVALUE_BUTTON;
                    break;
                    
                case SPOTTIMEVALUE_BUTTON:
                    nextion_unselect(obj_SpotTimeValue);
                    nextion_select(obj_buttonSpotTime);
                    param=TIG_SPOTTIME;
                    controller_cache_write((int)param,TIGSpotTime);
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    TIG_Spot_Buttons = SPOTTIME_BUTTON;
                    break;
            }

        var_interrupt = NONE;
        break;

        /*
         * ENCODER HOLD EVENT
         * - Open HELP page for the selected parameter or parameter value 
         */   
        case ENC_HOLD:
            nextion_page(MenuPage_Help_SpotTime);
            stateMachine.c_state = HELP;
            stateMachine.p_state = TIG_SPOT_PG3;
            FirstLoop=true;
            var_interrupt=NONE;
            break;

        /*
         * ENCODER RIGHT EVENT
         * - If on button, move to the right button
         * - If on value, increment value its less than MaxValue
         * - Send to nextion new value
         */ 
        case ENC_RIGHT:
            switch (TIG_Spot_Buttons)
            {
                case SPOTTIME_BUTTON:
                    break;

                case SPOTTIMEVALUE_BUTTON:
                    if (TIGSpotTime < MaxSpotTime) {
                        TIGSpotTime++;
                    }
                    utils_int_to_str(NextionTIG_SpotTime, TIGSpotTime, 3);
                    nextion_change_val(obj_SpotTimeValue,NextionTIG_SpotTime);
                    break;

            default:
                break;
        }

        var_interrupt = NONE;
        break;

        /*
         * ENCODER LEFT EVENT
         * - If on button, move to the left button
         * - If on value, decrement value if its above MinValue
         * - Send to nextion new value
         */ 
        case ENC_LEFT:
            switch (TIG_Spot_Buttons)
            {
                case SPOTTIME_BUTTON:
                    stateMachine.c_state = TIG_SPOT_PG2;
                    stateMachine.p_state = TIG_SPOT_PG3;
                    FirstLoop = true;
                    break;

                case SPOTTIMEVALUE_BUTTON:
                    if (TIGSpotTime > MinSpotTime) {
                        TIGSpotTime--;
                    }
                    utils_int_to_str(NextionTIG_SpotTime, TIGSpotTime, 3);
                    nextion_change_val(obj_SpotTimeValue,NextionTIG_SpotTime);
                    break;

                default:
                    break;
            }

            var_interrupt = NONE;
            break;
        /*
         * NONE
         * - Update values on Nextion
         * - Set nextion page
         * - Highlight First Object
         */
        case NONE:
            if (FirstLoop == true)
            {
                // Change Nextion Page to TIG_SPOT_PG3
                nextion_page(MenuPage_TIG_Spot_Parm3);
                
                //Update TIG Spot buttons to Spot Time (button)
                TIG_Spot_Buttons = SPOTTIME_BUTTON;

                //Read from memory and load value to Nextion
                param = TIG_SPOTTIME;
                controller_cache_read(param, &TIGSpotTime);
                utils_int_to_str(NextionTIG_SpotTime, TIGSpotTime, 3);
                nextion_change_val(obj_SpotTimeValue,NextionTIG_SpotTime);;

                // Change nexion textbox depending on the welding process
                param=WELD_PROCESS;
                controller_cache_read(param,&wprocess);
                if (wprocess == 0)
                {
                    nextion_change_txt(obj_weld_process,"TIG HF PARAMETERS");
                }
                if (wprocess == 2)
                {
                       nextion_change_txt(obj_weld_process,"LIF TIG PARAMETERS");
                }

                nextion_select(obj_buttonSpotTime);

                FirstLoop = false;
            }
            
            //Val_update variable from timer for value updates
            if(val_update==true)
            {

                param = WELD_STATE;
                controller_cache_read(param, &wstate);

                if(wstate == 1)
                {
                    stateMachine.c_state = HOME_CHANGE_CURRENT;
                    stateMachine.p_state = TIG_SPOT_PG3;
                    FirstLoop=true;
                    var_interrupt=NONE;
                }
                val_update=false;
            }

            var_interrupt = NONE;
            break;

        default:
            var_interrupt = NONE;
            break;
    }
    return stateMachine;
}

/**
 * \brief Responsible for the TIG PULSED parameteres and value changes.
 * 
 * This routine will control the flow between the TIG parameters aswell
 * as allowing the user to change them according to max and min value.
 * 
 * Parameters available on this page:
 * TIG Pre Gas  - Min value 1, Max value 100  (Seconds/10)
 * TIG IStart   - Min value 1, Max value 220  (A)
 * TIG UpSlope  - Min value 0, Max value 100  (Seconds/10)
 * TIG INominal - Min value 10, Max value 220 (%)
 * 
 * Page 1/3 for TIG PULSED.
 * 
 *  Events:
 * - Encoder rotation to the right or left will change the button or the value
 * of the parameter.
 * - Encoder click will allow the user to open parameter value so it can be 
 * edited if on the button, if on the value, will validate the change and save it.
 * - Encoder hold will open a Help menu for this page.
 * - Back will take the user back to Selection Menu buttons on Home menu if on 
 * buttons. If on values, will discard changes.
 * - Home will take the user back to Current Value change on Home menu.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t Pulse_TIG_parameters_page1(sm_t stateMachine)
{
    switch(var_interrupt)
    {
        /*
         * HOME EVENT
         * - State machine current state is now Home_Change_Current
         */
        case HOME:
            stateMachine.c_state = HOME_CHANGE_CURRENT;
            stateMachine.p_state = TIG_PULSE_PG1;
            FirstLoop=true;
            var_interrupt=NONE;
            break;
        
       /*
         * BACK EVENT
         * - Return to Selection Menu (Home buttons) if on the any parameter button
         * - Return to the parameter button if on the value button change option, without validation of 
         * the value changes, reseting the previous stored value.
         */   
        case BACK:
            switch(TIG_Buttons1)
            {
                case PREGAS_BUTTON:
                        stateMachine.c_state = SELECTION_MENU;
                        stateMachine.p_state = TIG_PULSE_PG1;
                        FirstLoop=true;
                        break;
                case ISTART_BUTTON:
                        stateMachine.c_state = SELECTION_MENU;
                        stateMachine.p_state = TIG_PULSE_PG1;
                        FirstLoop=true;
                        break;
                case UPSLOPE_BUTTON:
                        stateMachine.c_state = SELECTION_MENU;
                        stateMachine.p_state = TIG_PULSE_PG1;
                        FirstLoop=true;
                        break;
                case INOMINAL_BUTTON:
                        stateMachine.c_state = SELECTION_MENU;
                        stateMachine.p_state = TIG_PULSE_PG1;
                        FirstLoop=true;
                        break;
                        
                case PREGASVALUE_BUTTON:
                    nextion_unselect(obj_PreGasValue);
                    nextion_select(obj_buttonPreGas);
                    param=TIG_PRE_GAS;
                    controller_cache_read(param,&TIGPreGas);
                    utils_int_to_str(NextionTIG_PreGas,TIGPreGas,3);
                    nextion_change_val(obj_PreGasValue,NextionTIG_PreGas);
                    TIG_Buttons1=PREGAS_BUTTON;
                    break;  
                    
                case ISTARTVALUE_BUTTON:
                    nextion_unselect(obj_IStartValue);
                    nextion_select(obj_buttonIStart);
                    param=TIG_ISTART;
                    controller_cache_read(param,&TIGIStart);
                    utils_int_to_str(NextionTIG_IStart,TIGIStart,3);
                    nextion_change_val(obj_IStartValue,NextionTIG_IStart);
                    TIG_Buttons1=ISTART_BUTTON;
                    break;
                    
                case UPSLOPEVALUE_BUTTON:
                    nextion_unselect(obj_UpSlopeValue);
                    nextion_select(obj_buttonUpSlope);
                    param=TIG_UPSLOPE;
                    controller_cache_read(param,&TIGUpSlope);
                    utils_int_to_str(NextionTIG_UpSlope,TIGUpSlope,3);
                    nextion_change_val(obj_UpSlopeValue,NextionTIG_UpSlope);
                    TIG_Buttons1=UPSLOPE_BUTTON;
                    break;
                    
                case INOMINALVALUE_BUTTON:
                    nextion_unselect(obj_TIGINominalValue);
                    nextion_select(obj_buttonTIGINominal);
                    param=TIG_INOMINAL;
                    controller_cache_read(param,&TIGINominal);
                    utils_int_to_str(NextionTIG_INominal,TIGINominal,3);
                    nextion_change_val(obj_TIGINominalValue,NextionTIG_IStart);
                    TIG_Buttons1=INOMINAL_BUTTON;
                    break;
                
                default:
                    break;
            }
            var_interrupt = NONE;
            break;
        
        /*
         * ENCODER BUTTON EVENT
         * - Access value change if on parameter button
         * - Validate changes and go to parameter button if is on value button
         */
        case ENC_BUTTON:
           switch(TIG_Buttons1)
           {
               case PREGAS_BUTTON:
                    nextion_unselect(obj_buttonPreGas);
                    nextion_select(obj_PreGasValue);
                    TIG_Buttons1=PREGASVALUE_BUTTON;
                    break;
                    
                case ISTART_BUTTON:
                    nextion_unselect(obj_buttonIStart);
                    nextion_select(obj_IStartValue);
                    TIG_Buttons1=ISTARTVALUE_BUTTON;
                    break;
                    
                case UPSLOPE_BUTTON:
                    nextion_unselect(obj_buttonUpSlope);
                    nextion_select(obj_UpSlopeValue);
                    TIG_Buttons1=UPSLOPEVALUE_BUTTON;
                    break;
                    
                case INOMINAL_BUTTON:
                    nextion_unselect(obj_buttonTIGINominal);
                    nextion_select(obj_TIGINominalValue);
                    TIG_Buttons1=INOMINALVALUE_BUTTON;
                    break;
                    
                case PREGASVALUE_BUTTON:
                    nextion_unselect(obj_PreGasValue);
                    nextion_select(obj_buttonPreGas);
                    param=TIG_PRE_GAS;
                    controller_cache_write((int)param,TIGPreGas);
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    TIG_Buttons1=PREGAS_BUTTON;
                    break;
                    
                case ISTARTVALUE_BUTTON:
                    nextion_unselect(obj_IStartValue);
                    nextion_select(obj_buttonIStart);
                    param=TIG_ISTART;
                    controller_cache_write((int)param,TIGIStart);
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    TIG_Buttons1=ISTART_BUTTON;
                    break;
                    
                case UPSLOPEVALUE_BUTTON:
                    nextion_unselect(obj_UpSlopeValue);
                    nextion_select(obj_buttonUpSlope);
                    param=TIG_UPSLOPE;
                    controller_cache_write((int)param,TIGUpSlope);
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    TIG_Buttons1=UPSLOPE_BUTTON;
                    break;
                    
                case INOMINALVALUE_BUTTON:
                    nextion_unselect(obj_TIGINominalValue);
                    nextion_select(obj_buttonTIGINominal);
                    param=TIG_INOMINAL;
                    controller_cache_write((int)param,TIGINominal);
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    TIG_Buttons1=INOMINAL_BUTTON;
                    break;
                 
                default:
                    break;
            }
            var_interrupt=NONE;
            break;
            
        /*
         * ENCODER HOLD EVENT
         * - Open HELP page for the selected parameter or parameter value 
         */         
        case ENC_HOLD:
            switch(TIG_Buttons1)
           {
               case PREGAS_BUTTON:
                    nextion_page(MenuPage_Help_PreGas); 
                    break;
                    
                case ISTART_BUTTON:
                    nextion_page(MenuPage_Help_IStart); 
                    break;
                    
                case UPSLOPE_BUTTON:
                    nextion_page(MenuPage_Help_UpSlope); 
                    break;
                    
                case INOMINAL_BUTTON:
                    nextion_page(MenuPage_Help_INominal); 
                    break;
                    
                case PREGASVALUE_BUTTON:
                    nextion_page(MenuPage_Help_PreGas); 
                    break;
                    
                case ISTARTVALUE_BUTTON:
                    nextion_page(MenuPage_Help_IStart); 
                    break;
                    
                case UPSLOPEVALUE_BUTTON:
                    nextion_page(MenuPage_Help_UpSlope); 
                    break;
                    
                case INOMINALVALUE_BUTTON:
                    nextion_page(MenuPage_Help_INominal); 
                    break;
                    
                default:
                    break;
            }
            
            stateMachine.c_state = HELP;
            stateMachine.p_state = TIG_PULSE_PG1;
            FirstLoop=true;
            var_interrupt=NONE;
            break;
        
        /*
         * ENCODER RIGHT EVENT
         * - If on button, move to the right button
         * - If on value, increment value its less than MaxValue
         * - Send to nextion new value
         */        
        case ENC_RIGHT:
           switch(TIG_Buttons1)
            {
               case PREGAS_BUTTON:
                    nextion_unselect(obj_buttonPreGas);
                    nextion_select(obj_buttonIStart);
                    TIG_Buttons1=ISTART_BUTTON;
                    break;
                    
                case ISTART_BUTTON:
                    nextion_unselect(obj_buttonIStart);
                    nextion_select(obj_buttonUpSlope);
                    TIG_Buttons1=UPSLOPE_BUTTON;
                    break;
                    
                case UPSLOPE_BUTTON:
                    nextion_unselect(obj_buttonUpSlope);
                    nextion_select(obj_buttonTIGINominal);
                    TIG_Buttons1=INOMINAL_BUTTON;
                    break;
                    
                case INOMINAL_BUTTON:
                    stateMachine.c_state = TIG_PULSE_PG2;
                    stateMachine.p_state = TIG_PULSE_PG1;
                    FirstLoop = true;
                    var_interrupt=NONE;
                    break;
                    
                case PREGASVALUE_BUTTON:
                    if(TIGPreGas < MaxPreGas)
                    {
                        TIGPreGas++;
                        utils_int_to_str(NextionTIG_PreGas,TIGPreGas,3);
                        nextion_change_val(obj_PreGasValue,NextionTIG_PreGas);
                    }
                    break;
                   
                case ISTARTVALUE_BUTTON:
                    if(TIGIStart < MaxIStart)
                    {
                        TIGIStart++;
                        utils_int_to_str(NextionTIG_IStart,TIGIStart,3);
                        nextion_change_val(obj_IStartValue,NextionTIG_IStart);
               
                    }
                    break;
                   
                case UPSLOPEVALUE_BUTTON:
                    if(TIGUpSlope<MaxUpSlope)
                    {
                        TIGUpSlope++;
                        utils_int_to_str(NextionTIG_UpSlope,TIGUpSlope,3);
                        nextion_change_val(obj_UpSlopeValue,NextionTIG_UpSlope);
                    }
                    break;
                    
                case INOMINALVALUE_BUTTON:
                    if(TIGINominal<MaxINominal)
                    {
                        TIGINominal++;
                        utils_int_to_str(NextionTIG_INominal,TIGINominal,3);
                        nextion_change_val(obj_TIGINominalValue,NextionTIG_INominal);
                    }
                    break;
                   
                default:
                    break;
            }
            var_interrupt=NONE; 
            break;    
        
        /*
         * ENCODER LEFT EVENT
         * - If on button, move to the left button
         * - If on value, decrement value if its above MinValue
         * - Send to nextion new value
         */
        case ENC_LEFT:
               switch(TIG_Buttons1)
            {
               case PREGAS_BUTTON:
                   
                    break;
                    
                case ISTART_BUTTON:
                    nextion_unselect(obj_buttonIStart);
                    nextion_select(obj_buttonPreGas);
                    TIG_Buttons1=PREGAS_BUTTON;
                    break;
                    
                case UPSLOPE_BUTTON:
                    nextion_unselect(obj_buttonUpSlope);
                    nextion_select(obj_buttonIStart);
                    TIG_Buttons1=ISTART_BUTTON;
                    break;
                    
                case INOMINAL_BUTTON:
                    nextion_unselect(obj_buttonTIGINominal);
                    nextion_select(obj_buttonUpSlope);
                    TIG_Buttons1=UPSLOPE_BUTTON;
                    break;
                    
                case PREGASVALUE_BUTTON:
                    if(TIGPreGas > MinPreGas)
                    {
                        TIGPreGas--;
                        utils_int_to_str(NextionTIG_PreGas,TIGPreGas,3);
                        nextion_change_val(obj_PreGasValue,NextionTIG_PreGas);
                    }
                    break;
                   
                case ISTARTVALUE_BUTTON:
                    if(TIGIStart > MinIStart)
                    {
                        TIGIStart--;
                        utils_int_to_str(NextionTIG_IStart,TIGIStart,3);
                        nextion_change_val(obj_IStartValue,NextionTIG_IStart);
               
                    }
                    break;
                   
                case UPSLOPEVALUE_BUTTON:
                    if(TIGUpSlope > MinUpSlope)
                    {
                        TIGUpSlope--;
                        utils_int_to_str(NextionTIG_UpSlope,TIGUpSlope,3);
                        nextion_change_val(obj_UpSlopeValue,NextionTIG_UpSlope);
                    }
                    break;
                    
                case INOMINALVALUE_BUTTON:
                    if(TIGINominal > MinINominal)
                    {
                        TIGINominal--;
                        utils_int_to_str(NextionTIG_INominal,TIGINominal,3);
                        nextion_change_val(obj_TIGINominalValue,NextionTIG_INominal);
                    }
                    break;
                    
                default:
                    break;
            }
           var_interrupt=NONE;
            break;   
        
        /*
         * NONE
         * - Update values on Nextion
         * - Set nextion page
         * - Highlight First Object
         */ 
        case NONE:
            if(FirstLoop==true)
            {
               // Change Nextion Page to TIG_PULSE_PG1
               nextion_page(MenuPage_TIG_Pulse_Parm1);
               
               //Read from memory and load values to Nextion
               param=TIG_PRE_GAS;
               controller_cache_read(param,&TIGPreGas);
               utils_int_to_str(NextionTIG_PreGas,TIGPreGas,3);
               nextion_change_val(obj_PreGasValue,NextionTIG_PreGas);;
               
               param=TIG_ISTART;
               controller_cache_read(param,&TIGIStart);
               utils_int_to_str(NextionTIG_IStart,TIGIStart,3);
               nextion_change_val(obj_IStartValue,NextionTIG_IStart);
               
               param=TIG_UPSLOPE;
               controller_cache_read(param,&TIGUpSlope);
               utils_int_to_str(NextionTIG_UpSlope,TIGUpSlope,3);
               nextion_change_val(obj_UpSlopeValue,NextionTIG_UpSlope);
               
               
               param=TIG_INOMINAL;
               controller_cache_read(param,&TIGINominal);
               utils_int_to_str(NextionTIG_INominal,TIGINominal,3);
               nextion_change_val(obj_TIGINominalValue,NextionTIG_INominal);
               
               // Change nexion textbox depending on the welding process
               param=WELD_PROCESS;
               controller_cache_read(param,&wprocess);
               if (wprocess == 1)
               {
                   nextion_change_txt(obj_weld_process,"TIG HF PULSE PARAMETERS");
               }
               if (wprocess == 3)
               {
                   nextion_change_txt(obj_weld_process,"LIFTIG PULSE PARAMETERS");
               }
               
                // Remove all highlights when page is loaded
                nextion_unselect(obj_buttonPreGas);
                nextion_unselect(obj_PreGasValue);
                nextion_unselect(obj_buttonIStart);
                nextion_unselect(obj_IStartValue);
                nextion_unselect(obj_buttonUpSlope);
                nextion_unselect(obj_UpSlopeValue);
                nextion_unselect(obj_buttonTIGINominal);
                nextion_unselect(obj_TIGINominalValue);
                
                /* Depending if the Previus state was TIG_PULSE_PG2 or anything else 
                * highlight the last or the first parameter on the page.
                */ 
                if(stateMachine.p_state==TIG_PULSE_PG2){
                    TIG_Buttons1 = INOMINAL_BUTTON;
                    nextion_select(obj_buttonTIGINominal);
                }
                else{
                    TIG_Buttons1 = PREGAS_BUTTON;
                    nextion_select(obj_buttonPreGas);
                }
                
                FirstLoop=false;
            }
            
            //Val_update variable from timer for value updates
            if(val_update==true)
            {
                
                param = WELD_STATE;
                controller_cache_read(param, &wstate);

                if(wstate == 1)
                {
                    stateMachine.c_state = HOME_CHANGE_CURRENT;
                    stateMachine.p_state = TIG_PULSE_PG1;
                    FirstLoop=true;
                    var_interrupt=NONE;
                }
                val_update=false;
            }
            
            var_interrupt=NONE;
            break;   
            
        default:
            var_interrupt=NONE;
            break;
    }
    return stateMachine;
}

/**
 * \brief Responsible for the TIG PULSED parameteres and value changes.
 * 
 * This routine will control the flow between the TIG parameters aswell
 * as allowing the user to change them according to max and min value.
 * 
 * Parameters available on this page:
 * TIG IBase      - Min value 1, Max value 95  (%)
 * TIG PulseWidth - Min value 10, Max value 90 (A)
 * TIG PulseFreq. - Min value 1, Max value 200 (Hz)
 * TIG DownSlope  - Min value 0, Max value 100 (Seconds/10)
 * 
 * Page 2/3 for TIG PULSED.
 * 
 *  Events:
 * - Encoder rotation to the right or left will change the button or the value
 * of the parameter.
 * - Encoder click will allow the user to open parameter value so it can be 
 * edited if on the button, if on the value, will validate the change and save it.
 * - Encoder hold will open a Help menu for this page.
 * - Back will take the user back to Selection Menu buttons on Home menu if on 
 * buttons. If on values, will discard changes.
 * - Home will take the user back to Current Value change on Home menu.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t Pulse_TIG_parameters_page2(sm_t stateMachine)
{
    switch(var_interrupt)
    {
        /*
         * HOME EVENT
         * - State machine current state is now Home_Change_Current
         */
        case HOME:
            stateMachine.c_state = HOME_CHANGE_CURRENT;
            stateMachine.p_state = TIG_PULSE_PG2;
            FirstLoop=true;
            var_interrupt=NONE;
            break;
        
        /*
         * BACK EVENT
         * - Return to Selection Menu (Home buttons) if on the any parameter button
         * - Return to the parameter button if on the value button change option, without validation of 
         * the value changes, reseting the previous stored value.
         */
        case BACK: 
            switch(Pulse_TIG_Buttons2)
            {
                case IBASE_BUTTON:
                        stateMachine.c_state = TIG_PULSE_PG1;
                        stateMachine.p_state = TIG_PULSE_PG2;
                        FirstLoop=true;
                        break;
                case PULSEWIDTH_BUTTON:
                        stateMachine.c_state = TIG_PULSE_PG1;
                        stateMachine.p_state = TIG_PULSE_PG2;
                        FirstLoop=true;
                        break;
                case PULSEFREQ_BUTTON:
                        stateMachine.c_state = TIG_PULSE_PG1;
                        stateMachine.p_state = TIG_PULSE_PG2;
                        FirstLoop=true;
                        break;
                case PULSEDOWNSLOPE_BUTTON:
                        stateMachine.c_state = TIG_PULSE_PG1;
                        stateMachine.p_state = TIG_PULSE_PG2;
                        FirstLoop=true;
                        break;
                    
                case IBASEVALUE_BUTTON:
                    nextion_unselect(obj_IBaseValue);
                    nextion_select(obj_buttonIBase);
                    param=TIG_IBASE_;
                    controller_cache_read(param,&TIGIBase);
                    utils_int_to_str(NextionTIG_IBase,TIGIBase,3);
                    nextion_change_val(obj_IBaseValue,NextionTIG_IBase);
                    Pulse_TIG_Buttons2=IBASE_BUTTON;
                    break;
                    
                case PULSEWIDTHVALUE_BUTTON:
                    nextion_unselect(obj_IStartValue);
                    nextion_select(obj_buttonIStart);
                    param=TIG_PULSE_WITH_;
                    controller_cache_read(param,&TIGIStart);
                    utils_int_to_str(NextionTIG_IStart,TIGIStart,3);
                    nextion_change_val(obj_IStartValue,NextionTIG_IStart);
                    Pulse_TIG_Buttons2=PULSEWIDTH_BUTTON;
                    break;
                    
                case PULSEFREQVALUE_BUTTON:
                    nextion_unselect(obj_TIGPulseFreqValue);
                    nextion_select(obj_buttonTIGPulseFreq);
                    param=TIG_PULSE_FREQUENCY;
                    controller_cache_read(param,&TIGPulseFreq);
                    utils_int_to_str(NextionTIG_PulseFreq,TIGPulseFreq,3);
                    nextion_change_val(obj_TIGPulseFreqValue,NextionTIG_PulseFreq);
                    Pulse_TIG_Buttons2=PULSEFREQ_BUTTON;
                    break;
                    
                case PULSEDOWNSLOPEVALUE_BUTTON:
                    nextion_unselect(obj_PulseDownSlopeValue);
                    nextion_select(obj_buttonPulseDownSlope);
                    param=TIG_DOWNSLOPE_;
                    controller_cache_read(param,&TIGDownSlope);
                    utils_int_to_str(NextionTIG_DownSlope,TIGDownSlope,3);
                    nextion_change_val(obj_PulseDownSlopeValue,NextionTIG_DownSlope);
                    Pulse_TIG_Buttons2=PULSEDOWNSLOPE_BUTTON;
                    break;
                
                default:
                    break;
            }
            var_interrupt = NONE;
            break;
        
        /*
         * ENCODER BUTTON EVENT
         * - Access value change if on parameter button
         * - Validate changes and go to parameter button if is on value button
         */  
        case ENC_BUTTON:
           switch(Pulse_TIG_Buttons2)
           {
               case IBASE_BUTTON:
                    nextion_unselect(obj_buttonIBase);
                    nextion_select(obj_IBaseValue);
                    Pulse_TIG_Buttons2=IBASEVALUE_BUTTON;
                    break;
                    
                case PULSEWIDTH_BUTTON:
                    nextion_unselect(obj_buttonPulseWidth);
                    nextion_select(obj_PulseWidthValue);
                    Pulse_TIG_Buttons2=PULSEWIDTHVALUE_BUTTON;
                    break;
                    
                case PULSEFREQ_BUTTON:
                    nextion_unselect(obj_buttonTIGPulseFreq);
                    nextion_select(obj_TIGPulseFreqValue);
                    Pulse_TIG_Buttons2=PULSEFREQVALUE_BUTTON;
                    break;
                    
                case PULSEDOWNSLOPE_BUTTON:
                    nextion_unselect(obj_buttonPulseDownSlope);
                    nextion_select(obj_PulseDownSlopeValue);
                    Pulse_TIG_Buttons2=PULSEDOWNSLOPEVALUE_BUTTON;
                    break;
                    
                case IBASEVALUE_BUTTON:
                    nextion_unselect(obj_IBaseValue);
                    nextion_select(obj_buttonIBase);
                    param=TIG_IBASE_;
                    controller_cache_write((int)param,TIGIBase);
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    Pulse_TIG_Buttons2=IBASE_BUTTON;
                    break;
                    
                case PULSEWIDTHVALUE_BUTTON:
                    nextion_unselect(obj_PulseWidthValue);
                    nextion_select(obj_buttonPulseWidth);
                    param=TIG_PULSE_WITH_;
                    controller_cache_write((int)param,TIGPulseWidth);
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    Pulse_TIG_Buttons2=PULSEWIDTH_BUTTON;
                    break;
                    
                case PULSEFREQVALUE_BUTTON:
                    nextion_unselect(obj_TIGPulseFreqValue);
                    nextion_select(obj_buttonTIGPulseFreq);
                    param=TIG_PULSE_FREQUENCY;
                    controller_cache_write((int)param,TIGPulseFreq);
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    Pulse_TIG_Buttons2=PULSEFREQ_BUTTON;
                    break;
                    
                case PULSEDOWNSLOPEVALUE_BUTTON:
                    nextion_unselect(obj_PulseDownSlopeValue);
                    nextion_select(obj_buttonPulseDownSlope);
                    param=TIG_DOWNSLOPE_;
                    controller_cache_write((int)param,TIGDownSlope);
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    Pulse_TIG_Buttons2=PULSEDOWNSLOPE_BUTTON;
                    break;
                 
                default:
                    break;
            }
            var_interrupt=NONE;
            break;
        
        /*
         * ENCODER HOLD EVENT
         * - Open HELP page for the selected parameter or parameter value 
         */         
        case ENC_HOLD:
            switch(Pulse_TIG_Buttons2)
            {
               case IBASE_BUTTON:
                    nextion_page(MenuPage_Help_IBase);
                    break;
                    
                case PULSEWIDTH_BUTTON:
                    nextion_page(MenuPage_Help_PulseWidth);
                    break;
                    
                case PULSEFREQ_BUTTON:
                    nextion_page(MenuPage_Help_PulseFreq);
                    break;
                    
                case PULSEDOWNSLOPE_BUTTON:
                    nextion_page(MenuPage_Help_DownSlope);
                    break;
                    
                case IBASEVALUE_BUTTON:
                    nextion_page(MenuPage_Help_IBase);
                    break;
                    
                case PULSEWIDTHVALUE_BUTTON:
                    nextion_page(MenuPage_Help_PulseWidth);
                    break;
                    
                case PULSEFREQVALUE_BUTTON:
                    nextion_page(MenuPage_Help_PulseFreq);
                    break;
                    
                case PULSEDOWNSLOPEVALUE_BUTTON:
                    nextion_page(MenuPage_Help_DownSlope);
                    break;
                 
                default:
                    break;
            }
            
            stateMachine.c_state = HELP;
            stateMachine.p_state = TIG_PULSE_PG2;
            FirstLoop=true;
            var_interrupt=NONE;
            break;
        
        /*
         * ENCODER RIGHT EVENT
         * - If on button, move to the right button
         * - If on value, increment value its less than MaxValue
         * - Send to nextion new value
         */      
        case ENC_RIGHT:
           switch(Pulse_TIG_Buttons2)
            {
               case IBASE_BUTTON:
                    nextion_unselect(obj_buttonIBase);
                    nextion_select(obj_buttonPulseWidth);
                    Pulse_TIG_Buttons2=PULSEWIDTH_BUTTON;
                    break;
                    
                case PULSEWIDTH_BUTTON:
                    nextion_unselect(obj_buttonPulseWidth);
                    nextion_select(obj_buttonTIGPulseFreq);
                    Pulse_TIG_Buttons2=PULSEFREQ_BUTTON;
                    break;
                    
                case PULSEFREQ_BUTTON:
                    nextion_unselect(obj_buttonTIGPulseFreq);
                    nextion_select(obj_buttonPulseDownSlope);
                    Pulse_TIG_Buttons2=PULSEDOWNSLOPE_BUTTON;
                    break;
                    
                case PULSEDOWNSLOPE_BUTTON:
                    param=OP_MODE;
                    controller_cache_read(param,&omode);
                    if(omode==3){
                        stateMachine.c_state = TIG_PULSE_SPOT_PG3;
                        stateMachine.p_state = TIG_PULSE_PG2;
                    }
                    else
                    {
                        stateMachine.c_state = TIG_PULSE_PG3;
                        stateMachine.p_state = TIG_PULSE_PG2;
                    }
                    FirstLoop = true;
                    var_interrupt=NONE;
                    break;
                    
                case IBASEVALUE_BUTTON:
                    if(TIGIBase < MaxIBase)
                    {
                        TIGIBase++;
                        utils_int_to_str(NextionTIG_IBase,TIGIBase,3);
                        nextion_change_val(obj_IBaseValue,NextionTIG_IBase);
                    }
                    break;
                   
                case PULSEWIDTHVALUE_BUTTON:
                    if(TIGPulseWidth < MaxPulseWidth)
                    {
                        TIGPulseWidth++;
                        utils_int_to_str(NextionTIG_PulseWidth,TIGPulseWidth,3);
                        nextion_change_val(obj_PulseWidthValue,NextionTIG_PulseWidth);
                    }
                    break;
                   
                case PULSEFREQVALUE_BUTTON:
                    if(TIGPulseFreq<MaxPulseFreq)
                    {
                        TIGPulseFreq++;
                        utils_int_to_str(NextionTIG_PulseFreq,TIGPulseFreq,3);
                        nextion_change_val(obj_TIGPulseFreqValue,NextionTIG_PulseFreq);
                    }
                    break;
                    
                    case PULSEDOWNSLOPEVALUE_BUTTON:
                        if(TIGDownSlope < MaxDownSlope)
                        {
                            TIGDownSlope++;
                            utils_int_to_str(NextionTIG_DownSlope,TIGDownSlope,3);
                            nextion_change_val(obj_PulseDownSlopeValue,NextionTIG_DownSlope);
                        }
                        break;
                   
                default:
                    break;
            }
            var_interrupt=NONE; 
            break;    
        
        /*
         * ENCODER LEFT EVENT
         * - If on button, move to the left button
         * - If on value, decrement value if its above MinValue
         * - Send to nextion new value
         */ 
        case ENC_LEFT:
            switch(Pulse_TIG_Buttons2)
            {
               case IBASE_BUTTON:
                    stateMachine.c_state = TIG_PULSE_PG1;
                    stateMachine.p_state = TIG_PULSE_PG2;
                    FirstLoop = true;
                    var_interrupt=NONE;
                   break;
   
                    
                case PULSEWIDTH_BUTTON:
                    nextion_unselect(obj_buttonPulseWidth);
                    nextion_select(obj_buttonIBase);
                    Pulse_TIG_Buttons2=IBASE_BUTTON;
                    break;
                    
                case PULSEFREQ_BUTTON:
                    nextion_unselect(obj_buttonTIGPulseFreq);
                    nextion_select(obj_buttonPulseWidth);
                    Pulse_TIG_Buttons2=PULSEWIDTH_BUTTON;
                    break;
                    
                case PULSEDOWNSLOPE_BUTTON:
                    nextion_unselect(obj_buttonPulseDownSlope);
                    nextion_select(obj_buttonTIGPulseFreq);
                    Pulse_TIG_Buttons2=PULSEFREQ_BUTTON;
                    break;
                    
                case IBASEVALUE_BUTTON:
                    if(TIGIBase > MinIBase)
                    {
                        TIGIBase--;
                        utils_int_to_str(NextionTIG_IBase,TIGIBase,3);
                        nextion_change_val(obj_IBaseValue,NextionTIG_IBase);
                    }
                    break;
                   
                case PULSEWIDTHVALUE_BUTTON:
                    if(TIGPulseWidth > MinPulseWidth)
                    {
                        TIGPulseWidth--;
                        utils_int_to_str(NextionTIG_PulseWidth,TIGPulseWidth,3);
                        nextion_change_val(obj_PulseWidthValue,NextionTIG_PulseWidth);
                    }
                    break;
                   
                case PULSEFREQVALUE_BUTTON:
                    if(TIGPulseFreq>MinPulseFreq)
                    {
                        TIGPulseFreq--;
                        utils_int_to_str(NextionTIG_PulseFreq,TIGPulseFreq,3);
                        nextion_change_val(obj_TIGPulseFreqValue,NextionTIG_PulseFreq);
                    }
                    break;
                    
                    case PULSEDOWNSLOPEVALUE_BUTTON:
                        if(TIGDownSlope > MinDownSlope)
                        {
                            TIGDownSlope--;
                            utils_int_to_str(NextionTIG_DownSlope,TIGDownSlope,3);
                            nextion_change_val(obj_PulseDownSlopeValue,NextionTIG_DownSlope);
                        }
                        break;
                   
                default:
                    break;
            }
            var_interrupt=NONE; 
            break;  
        
        /*
         * NONE
         * - Update values on Nextion
         * - Set nextion page
         * - Highlight First Object
         */ 
        case NONE:
            if(FirstLoop==true)
            {
               // Change Nextion Page to TIG_PULSE_PG2
               nextion_page(MenuPage_TIG_Pulse_Parm2);
               
               //Read from memory and load values to Nextion
               param=TIG_IBASE_;
               controller_cache_read(param,&TIGIBase);
               utils_int_to_str(NextionTIG_IBase,TIGIBase,3);
               nextion_change_val(obj_IBaseValue,NextionTIG_IBase);;
               
               param=TIG_PULSE_WITH_;
               controller_cache_read(param,&TIGPulseWidth);
               utils_int_to_str(NextionTIG_PulseWidth,TIGPulseWidth,3);
               nextion_change_val(obj_PulseWidthValue,NextionTIG_PulseWidth);
               
               param=TIG_PULSE_FREQUENCY;
               controller_cache_read(param,&TIGPulseFreq);
               utils_int_to_str(NextionTIG_PulseFreq,TIGPulseFreq,3);
               nextion_change_val(obj_TIGPulseFreqValue,NextionTIG_PulseFreq);
               
               param=TIG_DOWNSLOPE_;
               controller_cache_read(param,&TIGDownSlope);
               utils_int_to_str(NextionTIG_DownSlope,TIGDownSlope,3);
               nextion_change_val(obj_PulseDownSlopeValue,NextionTIG_DownSlope);
               
               // Change nexion textbox depending on the welding process
               param=WELD_PROCESS;
               controller_cache_read(param,&wprocess);
               if (wprocess == 1)
               {
                   nextion_change_txt(obj_weld_process,"TIG HF PULSE PARAMETERS");
               }
               if (wprocess == 3)
               {
                   nextion_change_txt(obj_weld_process,"LIFTIG PULSE PARAMETERS");
               }
               
                // Remove all highlights when page is loaded
                nextion_unselect(obj_buttonIBase);
                nextion_unselect(obj_IBaseValue);
                nextion_unselect(obj_buttonPulseWidth);
                nextion_unselect(obj_PulseWidthValue);
                nextion_unselect(obj_buttonTIGPulseFreq);
                nextion_unselect(obj_TIGPulseFreqValue);
                nextion_unselect(obj_buttonPulseDownSlope);
                nextion_unselect(obj_PulseDownSlopeValue);
               
                /* Depending if the Previus state was TIG_PULSE_PG3,
                *  TIG_PULSE_SPOT_PG3 or anything else highlight the last or 
                *  the first parameter on the page.
                */ 
                if(stateMachine.p_state==TIG_PULSE_PG3 || stateMachine.p_state==TIG_PULSE_SPOT_PG3){
                    Pulse_TIG_Buttons2 = PULSEDOWNSLOPE_BUTTON;
                    nextion_select(obj_buttonPulseDownSlope);
                }
                else{
                    Pulse_TIG_Buttons2 = IBASE_BUTTON;
                    nextion_select(obj_buttonIBase);
                }
                FirstLoop=false;
            }
            
            //Val_update variable from timer for value updates
            if(val_update==true)
            {
                
                param = WELD_STATE;
                controller_cache_read(param, &wstate);

                if(wstate == 1)
                {
                    stateMachine.c_state = HOME_CHANGE_CURRENT;
                    stateMachine.p_state = TIG_PULSE_PG2;
                    FirstLoop=true;
                    var_interrupt=NONE;
                }
                val_update=false;
            }
            
            var_interrupt=NONE;
            break;   
            
        default:
            var_interrupt=NONE;
            break;
    }
    return stateMachine;
}

/**
 * \brief Responsible for the TIG PULSED parameteres and value changes.
 * 
 * This routine will control the flow between the TIG parameters aswell
 * as allowing the user to change them according to max and min value.
 * 
 * Parameters available on this page:
 * TIG IEnd     - Min value 10, Max value 220  (A)
 * TIG PostGas  - Min value 1, Max value 200 (Seconds/10)
 * TIG Dynamics - On/Off
 * 
 * Page 3/3 for TIG PULSED (Whitout Spot).
 * 
 *  Events:
 * - Encoder rotation to the right or left will change the button or the value
 * of the parameter.
 * - Encoder click will allow the user to open parameter value so it can be 
 * edited if on the button, if on the value, will validate the change and save it.
 * - Encoder hold will open a Help menu for this page.
 * - Back will take the user back to Selection Menu buttons on Home menu if on 
 * buttons. If on values, will discard changes.
 * - Home will take the user back to Current Value change on Home menu.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t Pulse_TIG_parameters_page3(sm_t stateMachine)
{
    switch(var_interrupt)
    {
        /*
         * HOME EVENT
         * - State machine current state is now Home_Change_Current
         */
        case HOME:
            stateMachine.c_state = HOME_CHANGE_CURRENT;
            stateMachine.p_state = TIG_PULSE_PG3;
            FirstLoop=true;
            var_interrupt=NONE;
            break;
            
        /*
         * BACK EVENT
         * - Return to Selection Menu (Home buttons) if on the any parameter button
         * - Return to the parameter button if on the value button change option, without validation of 
         * the value changes, reseting the previous stored value.
         */
        case BACK: 
            switch(Pulse_TIG_Buttons3)
            {
                case PULSEIEND_BUTTON:
                        stateMachine.c_state = TIG_PULSE_PG2;
                        stateMachine.p_state = TIG_PULSE_PG3;
                        FirstLoop=true;
                        break;
                case PULSEPOSTGAS_BUTTON:
                        stateMachine.c_state = TIG_PULSE_PG2;
                        stateMachine.p_state = TIG_PULSE_PG3;
                        FirstLoop=true;
                        break;
                case PULSEDYNAMICS_BUTTON:
                        stateMachine.c_state = TIG_PULSE_PG2;
                        stateMachine.p_state = TIG_PULSE_PG3;
                        FirstLoop=true;
                        break;    
                        
                case PULSEIENDVALUE_BUTTON:
                    nextion_unselect(obj_PulseIEndValue);
                    nextion_select(obj_buttonPulseIEnd);
                    param=TIG_IEND;
                    controller_cache_read(param,&TIGIEnd);
                    utils_int_to_str(NextionTIG_IEnd,TIGIEnd,3);
                    nextion_change_val(obj_PulseIEndValue,NextionTIG_IEnd);
                    Pulse_TIG_Buttons3=PULSEIEND_BUTTON;
                    break;  
                    
                case PULSEPOSTGASVALUE_BUTTON:
                    nextion_unselect(obj_PulsePostGasValue);
                    nextion_select(obj_buttonPulsePostGas);
                    param=TIG_POSTGAS;
                    controller_cache_read(param,&TIGPostGas);
                    utils_int_to_str(NextionTIG_PostGas,TIGPostGas,3);
                    nextion_change_val(obj_PulsePostGasValue,NextionTIG_PostGas);
                    Pulse_TIG_Buttons3=PULSEPOSTGAS_BUTTON;
                    break;
                    
                case PULSEDYNAMICSVALUE_BUTTON:
                    nextion_unselect(obj_PulseDynamicsValue);
                    nextion_select(obj_buttonPulseDynamics);
                    param=TIG_DYNAMICS;
                    controller_cache_read(param,&TIGDynamics);
                    utils_int_to_str(NextionTIG_Dynamics,TIGDynamics,3);
                    nextion_change_val(obj_PulseDynamicsValue,NextionTIG_Dynamics);
                    Pulse_TIG_Buttons3=PULSEDYNAMICS_BUTTON;
                    break;                
                default:
                    break;
            }
            var_interrupt = NONE;
            break;
            
         /*
         * ENCODER BUTTON EVENT
         * - Access value change if on parameter button
         * - Validate changes and go to parameter button if is on value button
         */    
        case ENC_BUTTON:
           switch(Pulse_TIG_Buttons3)
            {    
                case PULSEIEND_BUTTON:
                    nextion_unselect(obj_buttonPulseIEnd);
                    nextion_select(obj_PulseIEndValue);
                    Pulse_TIG_Buttons3=PULSEIENDVALUE_BUTTON;
                    break;
                    
                case PULSEPOSTGAS_BUTTON:
                    nextion_unselect(obj_buttonPulsePostGas);
                    nextion_select(obj_PulsePostGasValue);
                    Pulse_TIG_Buttons3=PULSEPOSTGASVALUE_BUTTON;
                    break;  
                    
                case PULSEDYNAMICS_BUTTON:
                    nextion_unselect(obj_buttonPulseDynamics);
                    nextion_select(obj_PulseDynamicsValue);
                    Pulse_TIG_Buttons3=PULSEDYNAMICSVALUE_BUTTON;
                    break;
                    
                case PULSEIENDVALUE_BUTTON:
                    nextion_unselect(obj_PulseIEndValue);
                    nextion_select(obj_buttonPulseIEnd);
                    param=TIG_IEND;
                    controller_cache_write((int)param,TIGIEnd);
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    Pulse_TIG_Buttons3=PULSEIEND_BUTTON;
                    break;
                    
                case PULSEPOSTGASVALUE_BUTTON:
                    nextion_unselect(obj_PulsePostGasValue);
                    nextion_select(obj_buttonPulsePostGas);
                    param=TIG_POSTGAS;
                    controller_cache_write((int)param,TIGPostGas);
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    Pulse_TIG_Buttons3=PULSEPOSTGAS_BUTTON;
                    break;

                case PULSEDYNAMICSVALUE_BUTTON:
                    nextion_unselect(obj_PulseDynamicsValue);
                    nextion_select(obj_buttonPulseDynamics);
                    param=TIG_DYNAMICS;
                    controller_cache_write((int)param,TIGDynamics);
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    Pulse_TIG_Buttons3=PULSEDYNAMICS_BUTTON;
                    break;
                    
                default:
                    break;
            }
            var_interrupt=NONE;
            break;
        
            
        /*
         * ENCODER HOLD EVENT
         * - Open HELP page for the selected parameter or parameter value 
         */        
        case ENC_HOLD:
            switch(Pulse_TIG_Buttons3)
            {    
                case PULSEIEND_BUTTON:
                    nextion_page(MenuPage_Help_IFinal);
                    break;
                    
                case PULSEPOSTGAS_BUTTON:
                    nextion_page(MenuPage_Help_PostGas);
                    break;  
                    
                case PULSEDYNAMICS_BUTTON:
                    nextion_page(MenuPage_Help_Dynamics);
                    break;
                    
                case PULSEIENDVALUE_BUTTON:
                    nextion_page(MenuPage_Help_IFinal);
                    break;
                    
                case PULSEPOSTGASVALUE_BUTTON:
                    nextion_page(MenuPage_Help_IFinal);
                    break;

                case PULSEDYNAMICSVALUE_BUTTON:
                    nextion_page(MenuPage_Help_Dynamics);
                    break;
                    
                default:
                    break;
            }

            stateMachine.c_state = HELP;
            stateMachine.p_state = TIG_PULSE_PG3;
            FirstLoop=true;
            var_interrupt=NONE;
            break;

        /*
         * ENCODER RIGHT EVENT
         * - If on button, move to the right button
         * - If on value, increment value its less than MaxValue
         * - Send to nextion new value
         */       
        case ENC_RIGHT:
           switch(Pulse_TIG_Buttons3)
            {
                case PULSEIEND_BUTTON:
                    nextion_unselect(obj_buttonPulseIEnd);
                    nextion_select(obj_buttonPulsePostGas);
                    Pulse_TIG_Buttons3=PULSEPOSTGAS_BUTTON;
                    break;
                    
                case PULSEPOSTGAS_BUTTON:
                    nextion_unselect(obj_buttonPulsePostGas);
                    nextion_select(obj_buttonPulseDynamics);
                    Pulse_TIG_Buttons3=PULSEDYNAMICS_BUTTON;
                    break;
                    
                case PULSEDYNAMICS_BUTTON:

                    break;
                   
                case PULSEIENDVALUE_BUTTON:
                    if(TIGIEnd < MaxIEnd)
                    {
                        TIGIEnd++;
                        utils_int_to_str(NextionTIG_IEnd,TIGIEnd,3);
                        nextion_change_val(obj_PulseIEndValue,NextionTIG_IEnd);               
                    }
                    break;
                   
                case PULSEPOSTGASVALUE_BUTTON:
                    if(TIGPostGas<MaxPostGas)
                    {
                        TIGPostGas++;
                        utils_int_to_str(NextionTIG_PostGas,TIGPostGas,3);
                        nextion_change_val(obj_PulsePostGasValue,NextionTIG_PostGas);
                    }
                    break;
                    
                case PULSEDYNAMICSVALUE_BUTTON:
        
                    if(TIGDynamics==DynamicsOff)
                    {
                        TIGDynamics = DynamicsOn;
                        utils_int_to_str(NextionTIG_Dynamics,TIGDynamics,3);
                        nextion_change_val(obj_PulseDynamicsValue,NextionTIG_Dynamics);
                    }
                    break;
                   
                default:
                    break;
            }
            var_interrupt=NONE; 
            break;    
        
        /*
         * ENCODER LEFT EVENT
         * - If on button, move to the left button
         * - If on value, decrement value if its above MinValue
         * - Send to nextion new value
         */ 
        case ENC_LEFT:
           switch(Pulse_TIG_Buttons3)
            {
                case PULSEIEND_BUTTON:
                    stateMachine.c_state = TIG_PULSE_PG2;
                    stateMachine.p_state = TIG_PULSE_PG3;
                    FirstLoop = true;
                    var_interrupt=NONE;
                    break;
                    
                case PULSEPOSTGAS_BUTTON:
                    nextion_unselect(obj_buttonPulsePostGas);
                    nextion_select(obj_buttonPulseIEnd);
                    Pulse_TIG_Buttons3=PULSEIEND_BUTTON;
                    break;
                    
                case PULSEDYNAMICS_BUTTON:
                    nextion_unselect(obj_buttonPulseDynamics);
                    nextion_select(obj_buttonPulsePostGas);
                    Pulse_TIG_Buttons3=PULSEPOSTGAS_BUTTON;
                    break;
                   
                case PULSEIENDVALUE_BUTTON:
                    if(TIGIEnd > MinIEnd)
                    {
                        TIGIEnd--;
                        utils_int_to_str(NextionTIG_IEnd,TIGIEnd,3);
                        nextion_change_val(obj_PulseIEndValue,NextionTIG_IEnd);               
                    }
                    break;
                   
                case PULSEPOSTGASVALUE_BUTTON:
                    if(TIGPostGas>MinPostGas)
                    {
                        TIGPostGas--;
                        utils_int_to_str(NextionTIG_PostGas,TIGPostGas,3);
                        nextion_change_val(obj_PulsePostGasValue,NextionTIG_PostGas);
                    }
                    break;
                    
                case PULSEDYNAMICSVALUE_BUTTON:
                    if(TIGDynamics==DynamicsOn)
                    {
                        TIGDynamics = DynamicsOff;
                        utils_int_to_str(NextionTIG_Dynamics,TIGDynamics,3);
                        nextion_change_val(obj_PulseDynamicsValue,NextionTIG_Dynamics);
                    }
                    break;
                   
                default:
                    break;
            }
            var_interrupt=NONE; 
            break;     
        /*
         * NONE
         * - Update values on Nextion
         * - Set nextion page
         * - Highlight First Object
         */ 
        case NONE:
            if(FirstLoop==true)
            {
               // Change Nextion Page to TIG_PULSE_PG3
               nextion_page(MenuPage_TIG_Pulse_Parm3);
                
               //Update TIG Pulse buttons 3 to Pulse IEnd (button)
               Pulse_TIG_Buttons3 = PULSEIEND_BUTTON;
                
               //Read from memory and load values to Nextion
               param=TIG_IEND;
               controller_cache_read(param,&TIGIEnd);
               utils_int_to_str(NextionTIG_IEnd,TIGIEnd,3);
               nextion_change_val(obj_PulseIEndValue,NextionTIG_IEnd);
               
               param=TIG_POSTGAS;
               controller_cache_read(param,&TIGPostGas);
               utils_int_to_str(NextionTIG_PostGas,TIGPostGas,3);
               nextion_change_val(obj_PulsePostGasValue,NextionTIG_PostGas);
               
               
               param=TIG_DYNAMICS;
               controller_cache_read(param,&TIGDynamics);
               utils_int_to_str(NextionTIG_Dynamics,TIGDynamics,3);
               nextion_change_val(obj_PulseDynamicsValue,NextionTIG_Dynamics);
               
               // Change nexion textbox depending on the welding process
               param=WELD_PROCESS;
               controller_cache_read(param,&wprocess);
               if (wprocess == 1)
               {
                   nextion_change_txt(obj_weld_process,"TIG HF PULSE PARAMETERS");
               }
               if (wprocess == 3)
               {
                   nextion_change_txt(obj_weld_process,"LIFTIG PULSE  PARAMETERS");
               }
               
                // Remove all highlights when page is loaded
                nextion_select(obj_buttonPulseIEnd);
                nextion_unselect(obj_PulseIEndValue);
                nextion_unselect(obj_buttonPulsePostGas);
                nextion_unselect(obj_PulsePostGasValue);
                nextion_unselect(obj_buttonPulseDynamics);
                nextion_unselect(obj_PulseDynamicsValue);

                FirstLoop=false;

            }
            
            //Val_update variable from timer for value updates
            if(val_update==true)
            {
                
                param = WELD_STATE;
                controller_cache_read(param, &wstate);

                if(wstate == 1)
                {
                    stateMachine.c_state = HOME_CHANGE_CURRENT;
                    stateMachine.p_state = TIG_PULSE_PG3;
                    FirstLoop=true;
                    var_interrupt=NONE;
                }
                val_update=false;
            }
            var_interrupt=NONE;
            break;   
            
        default:
            var_interrupt=NONE;
            break;
    }
    return stateMachine;
}

/**
 * \brief Responsible for the TIG PULSED parameteres and value changes.
 * 
 * This routine will control the flow between the TIG parameters aswell
 * as allowing the user to change them according to max and min value.
 * 
 * Parameters available on this page:
 * TIG IEnd     - Min value 10, Max value 220  (A)
 * TIG PostGas  - Min value 1, Max value 200 (Seconds/10)
 * TIG Dynamics - On/Off
 * TIG SpotTime - Min value 1, Max value 100 (Seconds/10)
 * 
 * Page 3/3 for TIG PULSED (Whit Spot).
 * 
 *  Events:
 * - Encoder rotation to the right or left will change the button or the value
 * of the parameter.
 * - Encoder click will allow the user to open parameter value so it can be 
 * edited if on the button, if on the value, will validate the change and save it.
 * - Encoder hold will open a Help menu for this page.
 * - Back will take the user back to Selection Menu buttons on Home menu if on 
 * buttons. If on values, will discard changes.
 * - Home will take the user back to Current Value change on Home menu.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t TIG_Pulse_Spot_parameters_page(sm_t stateMachine)
{
    switch(var_interrupt)
    {
        /*
         * HOME EVENT
         * - State machine current state is now Home_Change_Current
         */
        case HOME:
            stateMachine.c_state = HOME_CHANGE_CURRENT;
            stateMachine.p_state = TIG_PULSE_SPOT_PG3; //TODO: confirm state
            FirstLoop=true;
            var_interrupt=NONE;
            break;
        
        /*
         * BACK EVENT
         * - Return to Selection Menu (Home buttons) if on the any parameter button
         * - Return to the parameter button if on the value button change option, without validation of 
         * the value changes, reseting the previous stored value.
         */
        case BACK: 
            switch(Pulse_TIG_Buttons3)
            {

                case PULSEIEND_BUTTON:
                        stateMachine.c_state = TIG_PULSE_PG2;
                        stateMachine.p_state = TIG_PULSE_SPOT_PG3;
                        FirstLoop=true;
                        break;
                case PULSEPOSTGAS_BUTTON:
                        stateMachine.c_state = TIG_PULSE_PG2;
                        stateMachine.p_state = TIG_PULSE_SPOT_PG3;
                        FirstLoop=true;
                        break;
                case PULSEDYNAMICS_BUTTON:
                        stateMachine.c_state = TIG_PULSE_PG2;
                        stateMachine.p_state = TIG_PULSE_SPOT_PG3;
                        FirstLoop=true;
                        break; 
                case PULSESPOTTIME_BUTTON:
                        stateMachine.c_state = TIG_PULSE_PG2;
                        stateMachine.p_state = TIG_PULSE_SPOT_PG3;
                        FirstLoop=true;
                        break; 
                        
                case PULSEIENDVALUE_BUTTON:
                    nextion_unselect(obj_PulseIEndValue);
                    nextion_select(obj_buttonPulseIEnd);
                    param=TIG_IEND;
                    controller_cache_read(param,&TIGIEnd);
                    utils_int_to_str(NextionTIG_IEnd,TIGIEnd,3);
                    nextion_change_val(obj_PulseIEndValue,NextionTIG_IEnd);
                    Pulse_TIG_Buttons3=PULSEIEND_BUTTON;
                    break;  
                    
                case PULSEPOSTGASVALUE_BUTTON:
                    nextion_unselect(obj_PulsePostGasValue);
                    nextion_select(obj_buttonPulsePostGas);
                    param=TIG_POSTGAS;
                    controller_cache_read(param,&TIGPostGas);
                    utils_int_to_str(NextionTIG_PostGas,TIGPostGas,3);
                    nextion_change_val(obj_PulsePostGasValue,NextionTIG_PostGas);
                    Pulse_TIG_Buttons3=PULSEPOSTGAS_BUTTON;
                    break;
                    
                case PULSEDYNAMICSVALUE_BUTTON:
                    nextion_unselect(obj_PulseDynamicsValue);
                    nextion_select(obj_buttonPulseDynamics);
                    param=TIG_DYNAMICS;
                    controller_cache_read(param,&TIGDynamics);
                    utils_int_to_str(NextionTIG_Dynamics,TIGDynamics,3);
                    nextion_change_val(obj_PulseDynamicsValue,NextionTIG_Dynamics);
                    Pulse_TIG_Buttons3=PULSEDYNAMICS_BUTTON;
                    break;
                
                 case PULSESPOTTIMEVALUE_BUTTON:
                    nextion_unselect(obj_PulseSpotTimeValue);
                    nextion_select(obj_buttonPulseSpotTime);
                    param=TIG_DYNAMICS;
                    controller_cache_read(param,&TIGSpotTime);
                    utils_int_to_str(NextionTIG_SpotTime,TIGSpotTime,3);
                    nextion_change_val(obj_PulseSpotTimeValue,NextionTIG_SpotTime);
                    Pulse_TIG_Buttons3=PULSESPOTTIME_BUTTON;
                    break;
                
                default:
                    break;
            }
            var_interrupt = NONE;
            break;
        
        /*
         * ENCODER BUTTON EVENT
         * - Access value change if on parameter button
         * - Validate changes and go to parameter button if is on value button
         */  
        case ENC_BUTTON:
           switch(Pulse_TIG_Buttons3)
            {    
                case PULSEIEND_BUTTON:
                    nextion_unselect(obj_buttonPulseIEnd);
                    nextion_select(obj_PulseIEndValue);
                    Pulse_TIG_Buttons3=PULSEIENDVALUE_BUTTON;
                    break;
                    
                case PULSEPOSTGAS_BUTTON:
                    nextion_unselect(obj_buttonPulsePostGas);
                    nextion_select(obj_PulsePostGasValue);
                    Pulse_TIG_Buttons3=PULSEPOSTGASVALUE_BUTTON;
                    break;
                    
                case PULSEDYNAMICS_BUTTON:
                    nextion_unselect(obj_buttonPulseDynamics);
                    nextion_select(obj_PulseDynamicsValue);
                    Pulse_TIG_Buttons3=PULSEDYNAMICSVALUE_BUTTON;
                    break;
                    
                case PULSESPOTTIME_BUTTON:
                    nextion_unselect(obj_buttonPulseSpotTime);
                    nextion_select(obj_PulseSpotTimeValue);
                    Pulse_TIG_Buttons3=PULSESPOTTIMEVALUE_BUTTON;
                    break;
                    
                case PULSEIENDVALUE_BUTTON:
                    nextion_unselect(obj_PulseIEndValue);
                    nextion_select(obj_buttonPulseIEnd);
                    param=TIG_IEND;
                    controller_cache_write((int)param,TIGIEnd);
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    Pulse_TIG_Buttons3=PULSEIEND_BUTTON;
                    break;
                 
                case PULSEPOSTGASVALUE_BUTTON:
                    nextion_unselect(obj_PulsePostGasValue);
                    nextion_select(obj_buttonPulsePostGas);
                    param=TIG_POSTGAS;
                    controller_cache_write((int)param,TIGPostGas);
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    Pulse_TIG_Buttons3=PULSEPOSTGAS_BUTTON;
                    break;

                case PULSEDYNAMICSVALUE_BUTTON:
                    nextion_unselect(obj_PulseDynamicsValue);
                    nextion_select(obj_buttonPulseDynamics);
                    param=TIG_DYNAMICS;
                    controller_cache_write((int)param,TIGDynamics);
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    Pulse_TIG_Buttons3=PULSEDYNAMICS_BUTTON;
                    break;
                    
                 case PULSESPOTTIMEVALUE_BUTTON:
                    nextion_unselect(obj_PulseSpotTimeValue);
                    nextion_select(obj_buttonPulseSpotTime);
                    param=TIG_SPOTTIME;
                    controller_cache_write((int)param,TIGSpotTime);
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase
                    Pulse_TIG_Buttons3=PULSESPOTTIME_BUTTON;
                    break;
                    
                default:
                    break;
            }
            var_interrupt=NONE;
            break;
        
            
        /*
         * ENCODER HOLD EVENT
         * - Open HELP page for the selected parameter or parameter value 
         */        
        case ENC_HOLD:
            switch(Pulse_TIG_Buttons3)
            {    
                case PULSEIEND_BUTTON:
                    nextion_page(MenuPage_Help_IFinal);
                    break;
                    
                case PULSEPOSTGAS_BUTTON:
                    nextion_page(MenuPage_Help_PostGas);
                    break;  
                    
                case PULSEDYNAMICS_BUTTON:
                    nextion_page(MenuPage_Help_Dynamics);
                    break;
                
                case PULSESPOTTIME_BUTTON:
                    nextion_page(MenuPage_Help_SpotTime);
                    break;
                    
                case PULSEIENDVALUE_BUTTON:
                    nextion_page(MenuPage_Help_IFinal);
                    break;
                    
                case PULSEPOSTGASVALUE_BUTTON:
                    nextion_page(MenuPage_Help_IFinal);
                    break;

                case PULSEDYNAMICSVALUE_BUTTON:
                    nextion_page(MenuPage_Help_Dynamics);
                    break;
                    
                case PULSESPOTTIMEVALUE_BUTTON:
                    nextion_page(MenuPage_Help_SpotTime);
                    break;
                    
                default:
                    break;
            }
            stateMachine.c_state = HELP;
            stateMachine.p_state = TIG_PULSE_SPOT_PG3;
            FirstLoop=true;
            var_interrupt=NONE;
            break;
        
        /*
         * ENCODER RIGHT EVENT
         * - If on button, move to the right button
         * - If on value, increment value its less than MaxValue
         * - Send to nextion new value
         */        
        case ENC_RIGHT:
           switch(Pulse_TIG_Buttons3)
            {
                case PULSEIEND_BUTTON:
                    nextion_unselect(obj_buttonPulseIEnd);
                    nextion_select(obj_buttonPulsePostGas);
                    Pulse_TIG_Buttons3=PULSEPOSTGAS_BUTTON;
                    break;
                    
                case PULSEPOSTGAS_BUTTON:
                    nextion_unselect(obj_buttonPulsePostGas);
                    nextion_select(obj_buttonPulseDynamics);
                    Pulse_TIG_Buttons3=PULSEDYNAMICS_BUTTON;
                    break;
                    
                case PULSEDYNAMICS_BUTTON:
                    nextion_unselect(obj_buttonPulseDynamics);
                    nextion_select(obj_buttonPulseSpotTime);
                    Pulse_TIG_Buttons3=PULSESPOTTIME_BUTTON;
                    break;
                    
                case PULSESPOTTIME_BUTTON:

                    break;
                   
                case PULSEIENDVALUE_BUTTON:
                    if(TIGIEnd < MaxIEnd)
                    {
                        TIGIEnd++;
                        utils_int_to_str(NextionTIG_IEnd,TIGIEnd,3);
                        nextion_change_val(obj_PulseIEndValue,NextionTIG_IEnd);               
                    }
                    break;
                   
                case PULSEPOSTGASVALUE_BUTTON:
                    if(TIGPostGas<MaxPostGas)
                    {
                        TIGPostGas++;
                        utils_int_to_str(NextionTIG_PostGas,TIGPostGas,3);
                        nextion_change_val(obj_PulsePostGasValue,NextionTIG_PostGas);
                    }
                    break;
                    
                case PULSEDYNAMICSVALUE_BUTTON:
    
                    if(TIGDynamics==DynamicsOff)
                    {
                        TIGDynamics = DynamicsOn;
                        utils_int_to_str(NextionTIG_Dynamics,TIGDynamics,3);
                        nextion_change_val(obj_PulseDynamicsValue,NextionTIG_Dynamics);
                    }
                    break;
                    
                case PULSESPOTTIMEVALUE_BUTTON:
                    if(TIGSpotTime<MaxSpotTime)
                    {
                        TIGSpotTime++;
                        utils_int_to_str(NextionTIG_SpotTime,TIGSpotTime,3);
                        nextion_change_val(obj_PulseSpotTimeValue,NextionTIG_SpotTime);
                    }
                    break;
                default:
                    break;
            }
            var_interrupt=NONE; 
            break;    
        
        /*
         * ENCODER LEFT EVENT
         * - If on button, move to the left button
         * - If on value, decrement value if its above MinValue
         * - Send to nextion new value
         */  
        case ENC_LEFT:
           switch(Pulse_TIG_Buttons3)
            {
                case PULSEIEND_BUTTON:
                    stateMachine.c_state = TIG_PULSE_PG2;
                    stateMachine.p_state = TIG_PULSE_SPOT_PG3;
                    FirstLoop = true;
                    var_interrupt=NONE;
                    break;
                    
                case PULSEPOSTGAS_BUTTON:
                    nextion_unselect(obj_buttonPulsePostGas);
                    nextion_select(obj_buttonPulseIEnd);
                    Pulse_TIG_Buttons3=PULSEIEND_BUTTON;
                    break;
                    
                case PULSEDYNAMICS_BUTTON:
                    nextion_unselect(obj_buttonPulseDynamics);
                    nextion_select(obj_buttonPulsePostGas);
                    Pulse_TIG_Buttons3=PULSEPOSTGAS_BUTTON;
                    break;
                
                case PULSESPOTTIME_BUTTON:
                    nextion_unselect(obj_buttonPulseSpotTime);
                    nextion_select(obj_buttonPulseDynamics);
                    Pulse_TIG_Buttons3=PULSEDYNAMICS_BUTTON;
                     
                    break;
                   
                case PULSEIENDVALUE_BUTTON:
                    if(TIGIEnd > MinIEnd)
                    {
                        TIGIEnd--;
                        utils_int_to_str(NextionTIG_IEnd,TIGIEnd,3);
                        nextion_change_val(obj_PulseIEndValue,NextionTIG_IEnd);               
                    }
                    break;
                   
                case PULSEPOSTGASVALUE_BUTTON:
                    if(TIGPostGas>MinPostGas)
                    {
                        TIGPostGas--;
                        utils_int_to_str(NextionTIG_PostGas,TIGPostGas,3);
                        nextion_change_val(obj_PulsePostGasValue,NextionTIG_PostGas);
                    }
                    break;
                    
                case PULSEDYNAMICSVALUE_BUTTON:
                    if(TIGDynamics==DynamicsOn)
                    {
                        TIGDynamics = DynamicsOff;
                        utils_int_to_str(NextionTIG_Dynamics,TIGDynamics,3);
                        nextion_change_val(obj_PulseDynamicsValue,NextionTIG_Dynamics);
                    }           
                    break;
                                   
                case PULSESPOTTIMEVALUE_BUTTON:
                    if(TIGSpotTime>MinSpotTime)
                    {
                        TIGSpotTime--;
                        utils_int_to_str(NextionTIG_SpotTime,TIGSpotTime,3);
                        nextion_change_val(obj_PulseSpotTimeValue,NextionTIG_SpotTime);
                    }
                    break;
                default:
                    break;
            }
            var_interrupt=NONE; 
            break;     
        /*
         * NONE
         * - Update values on Nextion
         * - Set nextion page
         * - Highlight First Object
         */ 
        case NONE:
            if(FirstLoop==true)
            {
               // Change Nextion Page to TIG_PULSE_SPOT
               nextion_page(MenuPage_TIG_Pulse_Spot_Parm);
               
               //Update TIG Pulse buttons 3 to Pulse IEnd (button)
               Pulse_TIG_Buttons3 = PULSEIEND_BUTTON;
               
               //Read from memory and load values to Nextion
               param=TIG_IEND;
               controller_cache_read(param,&TIGIEnd);
               utils_int_to_str(NextionTIG_IEnd,TIGIEnd,3);
               nextion_change_val(obj_PulseIEndValue,NextionTIG_IEnd);
               
               param=TIG_POSTGAS;
               controller_cache_read(param,&TIGPostGas);
               utils_int_to_str(NextionTIG_PostGas,TIGPostGas,3);
               nextion_change_val(obj_PulsePostGasValue,NextionTIG_PostGas);
                
               param=TIG_DYNAMICS;
               controller_cache_read(param,&TIGDynamics);
               utils_int_to_str(NextionTIG_Dynamics,TIGDynamics,3);
               nextion_change_val(obj_PulseDynamicsValue,NextionTIG_Dynamics);
               
               param=TIG_SPOTTIME;
               controller_cache_read(param,&TIGSpotTime);
               utils_int_to_str(NextionTIG_SpotTime,TIGSpotTime,3);
               nextion_change_val(obj_PulseSpotTimeValue,NextionTIG_SpotTime);
               
               // Change nexion textbox depending on the welding process
               param=WELD_PROCESS;
               controller_cache_read(param,&wprocess);
               if (wprocess == 1)
               {
                   nextion_change_txt(obj_weld_process,"TIG HF PULSE PARAMETERS");
               }
               if (wprocess == 3)
               {
                   nextion_change_txt(obj_weld_process,"LIFTIG PULSE PARAMETERS");
               }
               
               // Remove all highlights when page is loaded
               nextion_unselect(obj_buttonPulseIEnd);
               nextion_unselect(obj_PulseIEndValue);
               nextion_unselect(obj_buttonPulsePostGas);
               nextion_unselect(obj_PulsePostGasValue);
               nextion_unselect(obj_buttonPulseDynamics);
               nextion_unselect(obj_PulseDynamicsValue);
               nextion_unselect(obj_buttonPulseSpotTime);
               nextion_unselect(obj_PulseSpotTimeValue);
               nextion_select(obj_buttonPulseIEnd);
               
                FirstLoop=false;
            }
            
            //Val_update variable from timer for value updates
            if(val_update==true)
            {
                
                param = WELD_STATE;
                controller_cache_read(param, &wstate);

                if(wstate == 1)
                {
                    stateMachine.c_state = HOME_CHANGE_CURRENT;
                    stateMachine.p_state = TIG_PULSE_SPOT_PG3;
                    FirstLoop=true;
                    var_interrupt=NONE;
                }
                val_update=false;
            }
            
            var_interrupt=NONE;
            break;   
            
        default:
            var_interrupt=NONE;
            break;
    }
    return stateMachine;
}
