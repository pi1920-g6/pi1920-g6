/**
 * \file    stateMachine.c
 */
/**
 * \defgroup sm State machine
 * \ref stateMachine.c
 */
#include <xc.h>
#include <sys/attribs.h>

#include "SM_structures.h"
#include "homeCurrentSM.h"
#include "stateMachine.h"
#include "paramMMA.h"
#include "modeSelectionSM.h"
#include "HelpSM.h"
#include "settings.h"
#include "programSM.h"
#include "paramTIG.h"
#include "FactoryReset.h"


sm_t HomeStateMachine(sm_t stateMachine) {
    switch (stateMachine.c_state)
    {
        case HOME_CHANGE_CURRENT:
            stateMachine = home_change_current(stateMachine);
            break;

        case SELECTION_MENU:
            stateMachine = selection_menu(stateMachine);
            break;
  
        case MODE_SELECTION:
            stateMachine = mode_selection(stateMachine);
            break;    
            
        case MS_PROCESSES:
            stateMachine = ms_Processes(stateMachine);
            break;  
            
        case MS_PULSE:
            stateMachine = ms_Pulse(stateMachine);
            break;  
            
        case MS_MODE:
            stateMachine = ms_Mode(stateMachine);
            break;    
            
        case SETTINGS:
            stateMachine = settings(stateMachine);
            break;  
         
        case MEMORY_PG1:
            stateMachine=programSM_page1(stateMachine);
            break;  
            
        case MEMORY_PG2:
            stateMachine=programSM_page2(stateMachine);
            break;  
            
        case MEMORY_TIG:
            stateMachine=programSM_memoryTIG(stateMachine);
            break;
            
        case MEMORY_TIG0:
            stateMachine=programSM_memoryTIG0(stateMachine);
            break;
            
        case MEMORY_TIGPULSE:
            stateMachine=programSM_memoryTIGPulse(stateMachine);
            break;
            
        case MEMORY_TIGPULSE0:
            stateMachine=programSM_memoryTIG0Pulse(stateMachine);
            break;
            
        case MEMORY_MMA:
            stateMachine=programSM_memoryMMA(stateMachine);
            break;
            
        case MEMORY_MMA0:
            stateMachine=programSM_memoryMMA0(stateMachine);
            break;
            
        case MEMORY_MMAPULSE:
            stateMachine=programSM_memoryMMAPulse(stateMachine);
            break;
            
        case MEMORY_MMAPULSE0:
            stateMachine=programSM_memoryMMAPulse0(stateMachine);
            break;
            
        case TIG_PG1:
            stateMachine=TIG_parameters_page1(stateMachine);
            break;  
            
        case TIG_PG2:
            stateMachine=TIG_parameters_page2(stateMachine);
            break;  
            
        case TIG_SPOT_PG1:
            stateMachine=TIG_Spot_parameters_page1(stateMachine);
            break;
        
        case TIG_SPOT_PG2:
            stateMachine=TIG_Spot_parameters_page2(stateMachine);
            break;
            
        case TIG_SPOT_PG3:
            stateMachine=TIG_Spot_parameters_page3(stateMachine);
            break;  
        
        case TIG_PULSE_PG1:
            stateMachine=Pulse_TIG_parameters_page1(stateMachine);
            break;  
            
        case TIG_PULSE_PG2:
            stateMachine= Pulse_TIG_parameters_page2(stateMachine);
            break;  
            
        case TIG_PULSE_PG3:
            stateMachine= Pulse_TIG_parameters_page3(stateMachine);
            break;  
            
        case TIG_PULSE_SPOT_PG3:
            stateMachine=TIG_Pulse_Spot_parameters_page(stateMachine);
            break;            
            
        case MMA_PG1:
            stateMachine = MMA_parameters_page(stateMachine);
            break;  
            
        case MMA_PULSE_PG1:
            stateMachine = MMA_PULSE_parameters_page1(stateMachine);
            break;  

        case MMA_PULSE_PG2:
            stateMachine = MMA_PULSE_parameters_page2(stateMachine);
            break;  

        case HELP:
            stateMachine = help_menu(stateMachine);
            break;  
            
        case FACTORY_RESET: 
            stateMachine = confirmFactoryReset(stateMachine);
            break;
            
        default:
            break;      
    }
    
    return stateMachine;
}
