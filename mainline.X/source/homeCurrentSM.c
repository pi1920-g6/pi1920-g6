/**
 * \file    homeCurrentSM.c
 */
/**
 * \addtogroup  smHC homeCurrentSM
 * \ingroup sm
 * @{
 *  \ref homeCurrentSM.c
 * @}
 */
#include <stdbool.h>
#include "SM_structures.h"
#include "homeCurrentSM.h"
#include "defines.h"
#include "nextion.h"
#include "utils.h"
#include "controller.h"

#ifndef __UNITTESTING__
#include <xc.h>
#include <sys/attribs.h>
#else
#include "../../XC32_mock/mem_map.h"
#endif

/*
 * VARIABLES
 */

/** \brief   Maximum Current value. */
#define MaxCurrentValue 220
/** \brief   Minimum Current value. */
#define MinCurrentValue 10

/** \brief   Variable to store the Current value. */
uint16_t CurrentValue;
/** \brief   Variable to store the Voltage value. */
uint16_t VoltageValue;
/** \brief   Variable to store the welding process selected. */
uint16_t WeldProc;
/** \brief   Variable to store the operation mode selected. */
uint16_t opmode;
/** \brief   Variable to store the dynamic mode. */
uint16_t dynmode;
/** \brief   Variable to store errors. */
uint16_t overheat;
/** \brief   Variable to store vrd. */
uint16_t vrd_actv;
/** \brief   Variable to store welding state. */
uint16_t weldstate;
/** \brief   Overheat flag to detect changes . */
uint16_t overheat_flag=0;
/** \brief   VRD flag to detect changes. */
uint16_t vrd_actv_flag=0;
/** \brief   Welding state flag to detect changes . */
uint16_t weldstate_flag=0;
/** \brief   MMA current value . */
uint16_t inominal_mma=0;
/** \brief   TIG current value . */
uint16_t inominal_tig=0;
/** \brief   Flag to signal value change. */
bool currentchange=false;
/** \brief   Flag to signal if is MMA current to read. */
bool MMAcurrent=false;
/** \brief   Flag to signal if is TIG current to read. */
bool TIGcurrent=false;

/** \brief  Variable used as argument on nextion function. */
char NextionProgSend[3];
/** \brief  Variable used as argument on nextion function. */
char NextionCurrentSend[3];
/** \brief  Variable used as argument on nextion function. */
char NextionVoltageSend[3];
/** \brief   Variable used to identify the Current object. */
char obj_DisplayCurrent[3] = "n1";
/** \brief   Variable used to identify the Voltage object. */
char obj_DisplayVoltage[3] = "x0";
/** \brief   Variable used to identify the text box with the welding process. */
char obj_weldobj[3] = "t0";
/** \brief   Variable used to identify the program used. */
char obj_program[3] = "t1";
/** \brief   Variable used to identify the button Modes object. */
char obj_buttonModes[3] = "b0";
/** \brief   Variable used to identify the button Params object. */
char obj_buttonParams[3] = "b1";
/** \brief   Variable used to identify the button Load/Save object. */
char obj_buttonLoadSave[3] = "b2";
/** \brief   Variable used to identify the button Settings object. */
char obj_buttonSettings[3] = "b3";
/** \brief   Variable used to identify the start welding object. */
char obj_startweld[3] = "i0";
/** \brief   Variable used to identify the over heat object. */
char obj_overheat[3] = "i1";
/** \brief   Variable used to identify the vrd object. */
char obj_vrd[3] = "i2";
/** \brief   Variable used to identify the page of this menu. */
char MenuPage_HC[3] = "00";
/** \brief   Variable used to identify the Help page of this menu. */
char HelpPage_HC[3] = "29";



param_id_t param;
hc_buttons_t buttons;

/**
 * \brief Responsible for the Current value changes.
 * 
 * This routine will control the different events while on current change.
 * Encoder rotation to the right or left will change current value and save
 * it.
 * Encoder click will allow the user to move to the buttons.
 * Encoder hold will open a Help menu.
 * Back and Home will do nothing on this menu.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return State Machine current-state and previous-state.
 */
sm_t home_change_current(sm_t stateMachine)
{
    switch (var_interrupt)
    {
    /*
         * HOME EVENT
         * - Do nothing, already on home
         */
    case HOME:
        var_interrupt = NONE; //Clear event flag
        break;

    /*
         * BACK EVENT
         * - Do nothing, no previous menu
         */
    case BACK:
        var_interrupt = NONE; //clear event flag
        break;

    /*
         * ENCODER BUTTON EVENT
         * - Change current state and previous state
         * - Remove Current object highlight
         */
    case ENC_BUTTON:
        nextion_unselect(obj_DisplayCurrent); //unselect the display current object
        stateMachine.c_state = SELECTION_MENU; //update current state to Selection Menu
        stateMachine.p_state = HOME_CHANGE_CURRENT; // update previous state to Home Change Current
        FirstLoop = true; //FirsLoop flag set as true for the new state
        var_interrupt = NONE; //clear event flag
        break;

    /*
         * ENCODER HOLD EVENT
         * - Change current state and previous state
         * - Open HELP menu of Home Change Current
         */
    case ENC_HOLD:
        nextion_page(HelpPage_HC); //Change nextion page to Help page of Home Change Current
        stateMachine.c_state = HELP; //update current state to Help 
        stateMachine.p_state = HOME_CHANGE_CURRENT; //update previous state to Home Change Current
        FirstLoop = true;  //FirsLoop flag set as true for the new state
        var_interrupt = NONE; //clear event flag
        break;

        /*
         * ENCODER RIGHT EVENT
         * - Increment CurrentValue if its less than MaxValue
         * - Send to nextion new value
         * - Save new value
         */
    case ENC_RIGHT:
        if (weldstate == 0) //Only decrement value if above MinValue
        {
            if((MMAcurrent==true) && (inominal_mma < MaxCurrentValue))
            {
                inominal_mma++;//Decrement CurrentValue
                utils_int_to_str(NextionCurrentSend, inominal_mma, 3); //Convert to string
                nextion_change_val(obj_DisplayCurrent,NextionCurrentSend);//Send value to Nextion
                currentchange=true; //current value changed
                
            }else if((TIGcurrent==true) && (inominal_tig < MaxCurrentValue))
            {
                inominal_tig++;//Decrement CurrentValue
                utils_int_to_str(NextionCurrentSend, inominal_tig, 3); //Convert to string
                nextion_change_val(obj_DisplayCurrent,NextionCurrentSend);//Send value to Nextion
                currentchange=true; //current value changed
            }
        }
        var_interrupt = NONE; //clear event flag
        break;

    /*
         * ENCODER LEFT EVENT
         * - Decrement CurrentValue if its more than MinValue
         * - Send to nextion new value
         * - Save new value
         */
    case ENC_LEFT:
        if (weldstate == 0) //Only decrement value if above MinValue
        {
            if((MMAcurrent==true) && (inominal_mma > MinCurrentValue))
            {
                inominal_mma--;//Decrement CurrentValue
                utils_int_to_str(NextionCurrentSend, inominal_mma, 3); //Convert to string
                nextion_change_val(obj_DisplayCurrent,NextionCurrentSend);//Send value to Nextion
                currentchange=true; //current value changed
                
            }else if((TIGcurrent==true) && (inominal_tig > MinCurrentValue))
            {
                inominal_tig--;//Decrement CurrentValue
                utils_int_to_str(NextionCurrentSend, inominal_tig, 3); //Convert to string
                nextion_change_val(obj_DisplayCurrent,NextionCurrentSend);//Send value to Nextion
                currentchange=true; //current value changed
            }
        }
        var_interrupt = NONE; //clear event flag
        break;

    /*
         * NONE
         * - Update values on Nextion (first time on the page)
         * - Set nextion page (if previous menu is not selection menu)
         * - Highlight Current Object
         * - Write Program, Mode and Type text
         * - Set icons on or off      
         */
    case NONE:
        if (FirstLoop == true) //first time executing when on this state
        {
            if(stateMachine.p_state != SELECTION_MENU) 
            {
                nextion_page(MenuPage_HC); //Change nextion page to Home Change Current
            }
            else{
                //if previous menu is Selection menu, it's already on the page
                nextion_unselect(obj_buttonModes);      //remove highlight on modes button
                nextion_unselect(obj_buttonParams);     //remove highlight on params button
                nextion_unselect(obj_buttonLoadSave);   //remove highlight on LoadSave button
                nextion_unselect(obj_buttonSettings);   //remove highlight on Settings button
            }
            
            currentchange=false; //reset currentchange flag
            
            nextion_change_val(obj_startweld,"00"); //WELD icon - OFF
            nextion_change_val(obj_vrd,"00"); //VRD icon - OFF
            nextion_change_val(obj_overheat,"00"); //Overheat icon - OFF
            nextion_select(obj_DisplayCurrent); //Highlight Current Display object
            
            param = VOLTAGE_READ; //memory parameter -> Voltage Read
            controller_cache_read(param, &VoltageValue); //Read Voltage Value from memory
            utils_int_to_str(NextionVoltageSend,VoltageValue, 3); //Convert value to string
            nextion_change_val(obj_DisplayVoltage,NextionVoltageSend); //Send value to Nextion Voltage Object
            
                               
            param = WELD_PROCESS; //memory parameter -> Welding Process
            controller_cache_read(param, &WeldProc); //Read welding process from memory

            param = OP_MODE; //memory parameter -> Operation Mode
            controller_cache_read(param, &opmode);//Read operation mode from memory
            
            //Print on the menu the process and mode currently on use
            switch (WeldProc)
            {
            case 0: //Case 0 means it is TIG HF
                MMAcurrent=false;
                TIGcurrent=true;
                switch (opmode) 
                {
                    case 0: //Not available on TIG HF
                        break;
                    case 1: //Operation mode is 2T
                        nextion_change_txt(obj_weldobj,"TIG HF 2T"); //send to Nextion 
                        //intended object 
                        break;
                    case 2://Operation mode is 4T
                        nextion_change_txt(obj_weldobj,"TIG HF 4T");//send to Nextion 
                        //intended object 
                        break;
                    case 3://Operation mode is Spot
                        nextion_change_txt(obj_weldobj,"TIG HF SPOT");//send to Nextion 
                        //intended object 
                        break;
                    default:    
                        break;
                }
                break;
            case 1: //Case 1 means it is TIG HF PULSE
                MMAcurrent=false;
                TIGcurrent=true;
                switch (opmode)
                {
                    case 0: //Not available on TIG HF PULSE
                        break;
                    case 1: //Operation mode is 2T
                        nextion_change_txt(obj_weldobj,"TIG HF PULSE 2T");//send to Nextion 
                        //intended object 
                        break;
                    case 2://Operation mode is 4T
                        nextion_change_txt(obj_weldobj,"TIG HF PULSE 4T");//send to Nextion 
                        //intended object 
                        break;
                    case 3://Operation mode is Spot
                        nextion_change_txt(obj_weldobj,"TIG HF PULSE SPOT");//send to Nextion 
                        //intended object 
                        break;
                    default:    
                        break;
                }
                break;
            case 2: //Case 2 means it is LIFTIG
                MMAcurrent=false;
                TIGcurrent=true;
                switch (opmode)
                {
                    case 0:  //Not available on LIFTIG
                        break;
                    case 1://Operation mode is 2T
                        nextion_change_txt(obj_weldobj,"LIFTIG 2T");//send to Nextion 
                        //intended object 
                        break;
                    case 2://Operation mode is 4T
                        nextion_change_txt(obj_weldobj,"LIFTIG 4T");//send to Nextion 
                        //intended object 
                        break;
                    case 3://Operation mode is Spot
                        nextion_change_txt(obj_weldobj,"LIFTIG SPOT");//send to Nextion 
                        //intended object 
                        break;
                    default:    
                        break;
                }
                break;
            case 3://Case 3 means it is LIFTIG PULSE
                MMAcurrent=false;
                TIGcurrent=true;
                switch (opmode)
                {
                    case 0:
                        break;
                    case 1://Operation mode is 2T
                        nextion_change_txt(obj_weldobj,"LIFTIG PULSE 2T");//send to Nextion 
                        //intended object 
                        break;
                    case 2://Operation mode is 4T
                        nextion_change_txt(obj_weldobj,"LIFTIG PULSE 4T");//send to Nextion 
                        //intended object 
                        break;
                    case 3://Operation mode is Spot
                        nextion_change_txt(obj_weldobj,"LIFTIG PULSE SPOT");//send to Nextion 
                        //intended object 
                        break;
                    default:    
                        break;
                }
                break;
            case 4: //Case 4 means it is MMA
                MMAcurrent=true;
                TIGcurrent=false;
                nextion_change_txt(obj_weldobj,"MMA");//send to Nextion 
                //intended object 
                break;
            case 5://Case 5 means it is MMA PULSE
                MMAcurrent=true;
                TIGcurrent=false;
                nextion_change_txt(obj_weldobj,"MMA PULSE");//send to Nextion 
                //intended object 
                break;
            default:
                break;
            }
            
            if(MMAcurrent==true){
               param = MMA_INOMINAL;//memory parameter -> MMA current
               controller_cache_read(param, &inominal_mma);//Read Current Value from memory
               utils_int_to_str(NextionCurrentSend,inominal_mma, 3); //Convert value to string
               nextion_change_val(obj_DisplayCurrent,NextionCurrentSend);//Send value to Nextion Current Object   
            }else if (TIGcurrent==true){
                param = TIG_INOMINAL;//memory parameter -> TIG current
                controller_cache_read(param, &inominal_tig);//Read Current Value from memory
                utils_int_to_str(NextionCurrentSend,inominal_tig, 3); //Convert value to string
                nextion_change_val(obj_DisplayCurrent,NextionCurrentSend);//Send value to Nextion Current Object
            }
            
            
            if(programID_memory < 10)
            {
                utils_int_to_str(NextionProgSend,programID_memory,1); //Convert to string
                nextion_change_txt(obj_program,NextionProgSend);
            }else{
                utils_int_to_str(NextionProgSend,programID_memory,2); //Convert to string
                nextion_change_txt(obj_program,NextionProgSend);
            }
            
            param = TIG_DYNAMICS; //memory parameter -> Dynamic mode 
            controller_cache_read(param, &dynmode);//Read dynamic mode from memory
            
            param = ERRORS;//memory parameter -> Errors
            controller_cache_read(param, &overheat);//Read Errors from memory
            
        
        
            if(overheat == 1) //If error read is 1 -> overheat error
            {
                nextion_change_val(obj_overheat,"01"); //Overheat icon - ON
            }else{
                nextion_change_val(obj_overheat,"00"); //Overheat icon - OFF
            }
            overheat_flag=overheat; //Creat a copy of the value
            //to update on nextion only on change

            param = MMA_STATE;//memory parameter -> MMA state
            controller_cache_read(param, &vrd_actv); //Read MMA state from memory


            if(vrd_actv == 8) //If mma state read is 8 -> VRD activation
            {
                nextion_change_val(obj_vrd,"01"); //VRD icon - ON
            }else{
                nextion_change_val(obj_vrd,"00"); //VRD icon - OFF
            }
            vrd_actv_flag=vrd_actv;//Creat a copy of the value
            //to update on nextion only on change
            
            
            param = WELD_STATE; //memory parameter -> Welding state
            controller_cache_read(param, &weldstate);//Read Welding state from memory


            if(weldstate == 1) //If welding state read is 1 -> Welding is active
            {
                nextion_change_val(obj_startweld,"01"); //WELDING icon - ON
            }else{
                nextion_change_val(obj_startweld,"00"); //WELDING icon - OFF
            }
            weldstate_flag=weldstate;//Creat a copy of the value
            //to update on nextion only on change
            
            FirstLoop = false; //FirsLoop flag set as false so it doesn't repeat this setup
        }
                    
        if(val_update==true) //Val_update variable from timer for value updates on nextion
        {
                        
            param = TIG_DYNAMICS; //memory parameter -> Dynamic mode 
            controller_cache_read(param, &dynmode);//Read dynamic mode from memory
            
            param = WELD_STATE; //memory parameter -> Welding state
            controller_cache_read(param, &weldstate);//Read Welding state from memory
            
            if(dynmode == 1 && weldstate == 1) //if it is set on dynamic mode
            {
                param = VOLTAGE_READ; //memory parameter -> Voltage Read
                controller_cache_read(param, &VoltageValue); //Read Voltage Value from memory
                utils_int_to_str(NextionVoltageSend,VoltageValue, 3); //Convert to string
                nextion_change_val(obj_DisplayVoltage,NextionVoltageSend);//Send value to Nextion Voltage Object

                param = CURRENT_READ; //memory parameter -> Current Read
                controller_cache_read(param, &CurrentValue); //Read Current Value from memory
                utils_int_to_str(NextionCurrentSend,CurrentValue, 3);  //Convert to string
                nextion_change_val(obj_DisplayCurrent,NextionCurrentSend); //Send value to Nextion Current Object
            }

            if(currentchange == true) //if current value was updated
            {
                if(MMAcurrent==true)
                {
                    param=MMA_INOMINAL; //memory parameter -> Current Read   
                    controller_cache_write((int)param, inominal_mma); //Write the new value on memory
                }else{
                    param=TIG_INOMINAL; //memory parameter -> Current Read   
                    controller_cache_write((int)param, inominal_tig); //Write the new value on memory
                }
                controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                controller_waitwrite(); //wait for NACK
                
                if(programID_memory!=0) //set to program 0 if different
                {
                    programID_memory=0; //Set programID to previous saved just for showcase
                    utils_int_to_str(NextionProgSend,programID_memory,1); //Convert to string
                    nextion_change_txt(obj_program,NextionProgSend);
                }
                currentchange=false; //reset flag
            }
            
            param = ERRORS; //memory parameter -> Errors
            controller_cache_read(param, &overheat); //Read Errors from memory

            if(overheat!=overheat_flag) //If overheat error changed
            {
                if(overheat == 1) //If it is 1 -> Overheat error on
                {
                   nextion_change_val(obj_overheat,"01"); //OVERHEAT icon - ON
                }else{
                    nextion_change_val(obj_overheat,"00"); //OVERHEAT icon - OFF
                } 
                overheat_flag=overheat; //Update flag value
            }

            param = MMA_STATE; //memory parameter -> MMA state
            controller_cache_read(param, &vrd_actv); //Read MMA STATE from memory

            if(vrd_actv!=vrd_actv_flag) //If VRD changed
            {
                if(vrd_actv == 8) //If it is 8 -> VRD on
                {
                    nextion_change_val(obj_vrd,"01"); //VRD icon - ON
                }else{
                    nextion_change_val(obj_vrd,"00"); //VRD icon - OFF
                }
                vrd_actv_flag=vrd_actv; //Update flag value
            }


            param = WELD_STATE; //memory parameter -> Welding state
            controller_cache_read(param, &weldstate); //Read Welding State from memory

            if(weldstate == 1) //If it is 1 -> Welding Active
            {
                param = VOLTAGE_READ; //memory parameter -> Voltage Read
                controller_cache_read(param, &VoltageValue); //Read Voltage Value from memory
                utils_int_to_str(NextionVoltageSend,VoltageValue, 3); //Convert to string
                nextion_change_val(obj_DisplayVoltage,NextionVoltageSend);//Send value to Nextion Voltage Object

                param = CURRENT_READ; //memory parameter -> Current Read
                controller_cache_read(param, &CurrentValue); //Read Current Value from memory
                utils_int_to_str(NextionCurrentSend,CurrentValue, 3);  //Convert to string
                nextion_change_val(obj_DisplayCurrent,NextionCurrentSend); //Send value to Nextion Current Object
                
                nextion_change_val(obj_startweld,"01"); //WELD icon - ON
            }else{
                param = VOLTAGE_READ; //memory parameter -> Voltage Read
                controller_cache_read(param, &VoltageValue); //Read Voltage Value from memory
                utils_int_to_str(NextionVoltageSend,VoltageValue, 3); //Convert to string
                nextion_change_val(obj_DisplayVoltage,NextionVoltageSend);//Send value to Nextion Voltage Object
                
                if(MMAcurrent==true)
                {
                    param = MMA_INOMINAL;//memory parameter -> MMA current
                    controller_cache_read(param, &inominal_mma);//Read Current Value from memory
                    utils_int_to_str(NextionCurrentSend,inominal_mma, 3); //Convert value to string
                    nextion_change_val(obj_DisplayCurrent,NextionCurrentSend);//Send value to Nextion Current Object   
                }else{
                    param = TIG_INOMINAL;//memory parameter -> TIG current
                    controller_cache_read(param, &inominal_tig);//Read Current Value from memory
                    utils_int_to_str(NextionCurrentSend,inominal_tig, 3); //Convert value to string
                    nextion_change_val(obj_DisplayCurrent,NextionCurrentSend);//Send value to Nextion Current Object
                }
                
                nextion_change_val(obj_startweld,"00"); //WELD icon - OFF
            }
         val_update=false; //Clear value update flag
        }

        var_interrupt = NONE; //Clear event flag
        break;

    default:
        var_interrupt = NONE; //Clear event flag
        break;
    }
    return stateMachine;
}

 /**
 * \brief Responsible for the Selection Menu.
 * 
 * This routine will control the Selection Menu buttons and 
 * redirect to the correct state when pressed.
 * Events:
 * - Encoder rotation to the right or left will change the current button,
 * placing highlight to the one being used and removed from the previous.
 * - Encoder click will allow the user to open the selected buttons' page.
 * - Encoder hold will open a Help menu for this page.
 * - Back and Home will take the user back to Current Value change on this menu.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return State Machine current-state and previous-state.
 */
sm_t selection_menu(sm_t stateMachine)
{
    switch (var_interrupt)
    {
    /*
         * HOME EVENT
         * - State machine current state is now Home_Change_Current
         */
    case HOME:
        stateMachine.c_state = HOME_CHANGE_CURRENT; //update current state to HOME CHANGE CURRENT  
        stateMachine.p_state = SELECTION_MENU;//update previous state to SELECTION MENU
        var_interrupt = NONE; //Clear event flag
        FirstLoop = true; //FirsLoop flag set as true for the new state
        break;

    /*
         * BACK EVENT
         * - State machine current state is now Home_Change_Current
         */
    case BACK:
        stateMachine.c_state = HOME_CHANGE_CURRENT; //update current state to HOME CHANGE CURRENT  
        stateMachine.p_state = SELECTION_MENU;//update previous state to SELECTION MENU
        var_interrupt = NONE; //Clear event flag
        FirstLoop = true; //FirsLoop flag set as true for the new state
        break;

    /*
         * ENCODER BUTTON EVENT
         * - Change state machine state according to the button selected
         * 
         * Modes button: User will be able to choose welding process and mode.
         * 
         * Parameters button: User will be able to edit parameters of the
         * selected welding process and mode.
         * 
         * Load Save button: User will be able to load, save and reset programs.
         * 
         * Settings: User will be able to ajust brightness, language and 
         * factory reset the machine.
         */
    case ENC_BUTTON:
        
        switch (buttons)
        {
        case MODES_BUTTON: //case Modes button pressed
            stateMachine.c_state = MODE_SELECTION; //update current state
            //to MODE SELECTION
            stateMachine.p_state = SELECTION_MENU;//update previous state
            //to SELECTION MENU
            break;
        case PARAMS_BUTTON: //case Parameter button pressed
            //this case should open parameters based on the process selected.
            switch (WeldProc)
            {
                case 0: //Case 0 is TIG HF
                    stateMachine.c_state = TIG_PG1; //Update current state
                    break;
                case 1: //Case 1 is TIG HF PULSE
                    stateMachine.c_state = TIG_PULSE_PG1;//Update current state
                    break;
                case 2: //Case 2 is LIFTIG
                    stateMachine.c_state = TIG_PG1;//Update current state
                    break;
                case 3: //Case 3 is LIFTIG PULSE
                    stateMachine.c_state = TIG_PULSE_PG1;//Update current state
                    break;
                case 4: //Case 4 is MMA
                    stateMachine.c_state = MMA_PG1;//Update current state
                    break;
                case 5: //Case 5 is MMA PULSE
                    stateMachine.c_state = MMA_PULSE_PG1;//Update current state
                    break;
                default:
                    break;
            }
            stateMachine.p_state = SELECTION_MENU;//update previous state
            //to SELECTION MENU
            break;
        case LOADSAVE_BUTTON: //case Load Save button pressed
            stateMachine.c_state = MEMORY_PG1; //Update current state
            stateMachine.p_state = SELECTION_MENU;//update previous state
            //to SELECTION MENU
            break;
        case SETTINGS_BUTTON: //caseSettings button pressed
            stateMachine.c_state = SETTINGS;  //Update current state
            stateMachine.p_state = SELECTION_MENU;//update previous state
            //to SELECTION MENU
            break;
        default:
            break;
        }
        FirstLoop = true; //FirsLoop flag set as true for the new state
        var_interrupt = NONE; //Clear event flag
        break;

    /*
         * ENCODER HOLD EVENT
         * - Change to Help menu page
         * - Update previous and current state
         */
    case ENC_HOLD: 
        nextion_page(HelpPage_HC); //Change nextion page to Help page of Home Change Current
        stateMachine.c_state = HELP; //update current state to Help 
        stateMachine.p_state = HOME_CHANGE_CURRENT; //update previous state to Home Change Current
        FirstLoop = true;  //FirsLoop flag set as true for the new state
        var_interrupt = NONE; //Clear event flag
        break;

    /*
         * ENCODER RIGHT EVENT
         * - Allows moving to the right between buttons, updating the highlights
         */
    case ENC_RIGHT:
        switch (buttons)
        {
            case MODES_BUTTON: //If on modes button and rotated right
                nextion_unselect(obj_buttonModes); //remove highlight on Modes button
                nextion_select(obj_buttonParams); //highlight his right button, Params
                buttons = PARAMS_BUTTON; //Update button to Params
                break;
            case PARAMS_BUTTON: //If on Params button and rotated right
                nextion_unselect(obj_buttonParams); //remove highlight on Params button
                nextion_select(obj_buttonLoadSave); //highlight his right button, LoadSave
                buttons = LOADSAVE_BUTTON; //Update button to LoadSave
                break;
            case LOADSAVE_BUTTON: //If on LoadSave button and rotated right
                nextion_unselect(obj_buttonLoadSave); //remove highlight on LoadSave button
                nextion_select(obj_buttonSettings); //highlight his right button, Settings
                buttons = SETTINGS_BUTTON; //Update button to LoadSave
                break;
            case SETTINGS_BUTTON:
                //No more buttons to the right
                buttons = SETTINGS_BUTTON; //Update button to Settings
                break;
            default:
                break;
        }
        var_interrupt = NONE; //Clear event flag
        break;

    /*
         * ENCODER LEFT EVENT
         * - Allows moving to the left between buttons, updating the highlights
         */
    case ENC_LEFT:
        switch (buttons)
        {
            case MODES_BUTTON: //If on modes button and rotated left
                //No more buttons to the left
                buttons = MODES_BUTTON; //Update button to Modes
                break;
            case PARAMS_BUTTON: //If on Params button and rotated left
                nextion_unselect(obj_buttonParams); //remove highlight on Params button
                nextion_select(obj_buttonModes); //highlight his left button, Modes
                buttons = MODES_BUTTON;//Update button to Modes
                break;
            case LOADSAVE_BUTTON: //If on Load Save button and rotated left
                nextion_unselect(obj_buttonLoadSave); //remove highlight on Load Save button
                nextion_select(obj_buttonParams); //highlight his left button, Params
                buttons = PARAMS_BUTTON;//Update button to Modes
                break;
            case SETTINGS_BUTTON://If on Settings button and rotated left
                nextion_unselect(obj_buttonSettings);//remove highlight on Settings button
                nextion_select(obj_buttonLoadSave);//highlight his left button, LoadSave
                buttons = LOADSAVE_BUTTON;//Update button to Modes
                break;
            default:
                break;
        }
        var_interrupt = NONE; //Clear event flag
        break;

    /*
         * NONE
         * - Update values on Nextion
         * - Set nextion page
         * - Highlight Current Object
         */
    case NONE:
        if (FirstLoop == true) //first time executing when on this state
        {    
            if(stateMachine.p_state != HOME_CHANGE_CURRENT) //if previous state was not Home Change Current
            {
                //When this happens, all values need to be updated on this menu.
                
                nextion_page(MenuPage_HC); //Change nextion page to Home Change Current
                
                
                param = VOLTAGE_READ; //memory parameter -> Voltage Read
                controller_cache_read(param, &VoltageValue); //read Voltage value from memory
                utils_int_to_str(NextionVoltageSend,VoltageValue, 3); //Convert to string
                nextion_change_val(obj_DisplayVoltage,NextionVoltageSend); //Send Voltage value to Nextion Voltage object

                param = WELD_PROCESS; //memory parameter -> Welding Process
                controller_cache_read(param, &WeldProc); //Read welding process from memory

                param = OP_MODE; //memory parameter -> Operation Mode
                controller_cache_read(param, &opmode);//Read operation mode from memory

                //Print on the menu the process and mode currently on use
                switch (WeldProc)
                {
                case 0: //Case 0 means it is TIG HF
                    MMAcurrent=false;
                    TIGcurrent=true;
                    switch (opmode) 
                    {
                        case 0: //Not available on TIG HF
                            break;
                        case 1: //Operation mode is 2T
                            nextion_change_txt(obj_weldobj,"TIG HF 2T"); //send to Nextion 
                            //intended object 
                            break;
                        case 2://Operation mode is 4T
                            nextion_change_txt(obj_weldobj,"TIG HF 4T");//send to Nextion 
                            //intended object 
                            break;
                        case 3://Operation mode is Spot
                            nextion_change_txt(obj_weldobj,"TIG HF SPOT");//send to Nextion 
                            //intended object 
                            break;
                        default:    
                            break;
                    }
                    break;
                case 1: //Case 1 means it is TIG HF PULSE
                    MMAcurrent=false;
                    TIGcurrent=true;                    
                    switch (opmode)
                    {
                        case 0: //Not available on TIG HF PULSE
                            break;
                        case 1: //Operation mode is 2T
                            nextion_change_txt(obj_weldobj,"TIG HF PULSE 2T");//send to Nextion 
                            //intended object 
                            break;
                        case 2://Operation mode is 4T
                            nextion_change_txt(obj_weldobj,"TIG HF PULSE 4T");//send to Nextion 
                            //intended object 
                            break;
                        case 3://Operation mode is Spot
                            nextion_change_txt(obj_weldobj,"TIG HF PULSE SPOT");//send to Nextion 
                            //intended object 
                            break;
                        default:    
                            break;
                    }
                    break;
                case 2: //Case 2 means it is LIFTIG
                    MMAcurrent=false;
                    TIGcurrent=true;
                    switch (opmode)
                    {
                        case 0:  //Not available on LIFTIG
                            break;
                        case 1://Operation mode is 2T
                            nextion_change_txt(obj_weldobj,"LIFTIG 2T");//send to Nextion 
                            //intended object 
                            break;
                        case 2://Operation mode is 4T
                            nextion_change_txt(obj_weldobj,"LIFTIG 4T");//send to Nextion 
                            //intended object 
                            break;
                        case 3://Operation mode is Spot
                            nextion_change_txt(obj_weldobj,"LIFTIG SPOT");//send to Nextion 
                            //intended object 
                            break;
                        default:    
                            break;
                    }
                    break;
                case 3://Case 3 means it is LIFTIG PULSE
                    MMAcurrent=false;
                    TIGcurrent=true;
                    switch (opmode)
                    {
                        case 0:
                            break;
                        case 1://Operation mode is 2T
                            nextion_change_txt(obj_weldobj,"LIFTIG PULSE 2T");//send to Nextion 
                            //intended object 
                            break;
                        case 2://Operation mode is 4T
                            nextion_change_txt(obj_weldobj,"LIFTIG PULSE 4T");//send to Nextion 
                            //intended object 
                            break;
                        case 3://Operation mode is Spot
                            nextion_change_txt(obj_weldobj,"LIFTIG PULSE SPOT");//send to Nextion 
                            //intended object 
                            break;
                        default:    
                            break;
                    }
                    break;
                case 4: //Case 4 means it is MMA
                    MMAcurrent=true;
                    TIGcurrent=false;
                    nextion_change_txt(obj_weldobj,"MMA");//send to Nextion 
                    //intended object 
                    break;
                case 5://Case 5 means it is MMA PULSE
                    MMAcurrent=true;
                    TIGcurrent=false;
                    nextion_change_txt(obj_weldobj,"MMA PULSE");//send to Nextion 
                    //intended object 
                    break;
                default:
                    break;
                }
                
                if(MMAcurrent==true)
                {
                    param = MMA_INOMINAL;//memory parameter -> MMA current
                    controller_cache_read(param, &inominal_mma);//Read Current Value from memory
                    utils_int_to_str(NextionCurrentSend,inominal_mma, 3); //Convert value to string
                    nextion_change_val(obj_DisplayCurrent,NextionCurrentSend);//Send value to Nextion Current Object   
                }else{
                    param = TIG_INOMINAL;//memory parameter -> TIG current
                    controller_cache_read(param, &inominal_tig);//Read Current Value from memory
                    utils_int_to_str(NextionCurrentSend,inominal_tig, 3); //Convert value to string
                    nextion_change_val(obj_DisplayCurrent,NextionCurrentSend);//Send value to Nextion Current Object
                }
                
                if(programID_memory < 10) //show only 1 digit
                {
                    utils_int_to_str(NextionProgSend,programID_memory,1); //Convert to string
                    nextion_change_txt(obj_program,NextionProgSend);
                }else{ //show 2
                    utils_int_to_str(NextionProgSend,programID_memory,2); //Convert to string
                    nextion_change_txt(obj_program,NextionProgSend);
                }
                
                nextion_change_val(obj_startweld,"00"); //WELD icon - OFF
                nextion_change_val(obj_vrd,"00"); //VRD icon - OFF
                nextion_change_val(obj_overheat,"00"); //Overheat icon - OFF
                
                param = ERRORS;//memory parameter -> Errors
                controller_cache_read(param, &overheat); //Read Errors from memory

                if(overheat == 1) //If error read is 1 -> overheat error
                {
                    nextion_change_val(obj_overheat,"01"); //Overheat icon - ON
                }else{
                    nextion_change_val(obj_overheat,"00"); //Overheat icon - OFF
                }
                overheat_flag=overheat; //Creat a copy of the value
                //to update on nextion only on change

                param = MMA_STATE;//memory parameter -> MMA state
                controller_cache_read(param, &vrd_actv); //Read MMA state from memory


                if(vrd_actv == 8) //If mma state read is 8 -> VRD activation
                {
                    nextion_change_val(obj_vrd,"01"); //VRD icon - ON
                }else{
                    nextion_change_val(obj_vrd,"00"); //VRD icon - OFF
                }
                vrd_actv_flag=vrd_actv;//Creat a copy of the value
                //to update on nextion only on change
                
                //Remove all highlights
                nextion_unselect(obj_buttonModes); //remove highlight from Modes button
                nextion_unselect(obj_buttonParams);//remove highlight from Params button
                nextion_unselect(obj_buttonLoadSave);//remove highlight from LoadSave button
                nextion_unselect(obj_buttonSettings);//remove highlight from Settings button
                nextion_unselect(obj_DisplayCurrent);//remove highlight from Display Current
                
                //Highlight Modes button
                nextion_select(obj_buttonModes); //Highlight Modes button
            }
            else
            {
                //Remove all highlights
                nextion_unselect(obj_buttonModes); //remove highlight from Modes button
                nextion_unselect(obj_buttonParams);//remove highlight from Params button
                nextion_unselect(obj_buttonLoadSave);//remove highlight from LoadSave button
                nextion_unselect(obj_buttonSettings);//remove highlight from Settings button
                nextion_unselect(obj_DisplayCurrent);//remove highlight from Display Current
                
                //Highlight Modes button
                nextion_select(obj_buttonModes); //Highlight Modes button
            }
            
            buttons = MODES_BUTTON; //Init buttons on Modes
     
            FirstLoop = false;  //Clear value update flag
        }
        
       if(val_update==true) //Val_update variable from timer for value updates 
            //on nextion
        {
            param = ERRORS; //memory parameter -> Errors
            controller_cache_read(param, &overheat); //Read Errors from memory

            if(overheat!=overheat_flag) //If overheat error changed
            {
                if(overheat == 1) //If it is 1 -> Overheat error on
                {
                   nextion_change_val(obj_overheat,"01"); //OVERHEAT icon - ON
                }else{
                    nextion_change_val(obj_overheat,"00"); //OVERHEAT icon - OFF
                } 
                overheat_flag=overheat; //Update flag value
            }

            param = MMA_STATE; //memory parameter -> MMA state
            controller_cache_read(param, &vrd_actv); //Read MMA STATE from memory

            if(vrd_actv!=vrd_actv_flag) //If VRD changed
            {
                if(vrd_actv == 8) //If it is 8 -> VRD on
                {
                    nextion_change_val(obj_vrd,"01"); //VRD icon - ON
                }else{
                    nextion_change_val(obj_vrd,"00"); //VRD icon - OFF
                }
                vrd_actv_flag=vrd_actv; //Update flag value
            }


            param = WELD_STATE; //memory parameter -> Welding state
            controller_cache_read(param, &weldstate); //Read Welding State from memory


            if(weldstate == 1)
            {
                stateMachine.c_state = HOME_CHANGE_CURRENT; //update current state to HOME CHANGE CURRENT
                stateMachine.p_state = SELECTION_MENU; //update previous state to SELECTION MENU
                var_interrupt = NONE; //Clear event flag
                FirstLoop = true; //FirsLoop flag set as true for the new state
            }
         val_update=false; //Clear value update flag
        }
        var_interrupt = NONE; //Clear event flag
        break;

    default:
        var_interrupt = NONE; //Clear event flag
        break;
    }
    return stateMachine;
}
