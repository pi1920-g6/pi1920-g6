#include <xc.h>
#include <sys/attribs.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "GeneralConfigs.h"


void Pinconfig(void)
{
    
    /* Set all other pins to Outputs */
    TRISB = (TRISB && 0x0000);
    TRISC = (TRISC && 0x0000);
    TRISD = (TRISD && 0x0000);
    TRISE = (TRISE && 0x0000);
    TRISF = (TRISF && 0x0000);
    TRISG = (TRISG && 0x0000);
    /*
     * PIN CONFIGURATION
     */
    TRISEbits.TRISE4 = 1; //Home
    TRISDbits.TRISD5 = 1; //Back
    TRISDbits.TRISD6 = 1; //Encoder button 
    
    // Setup the two encoder ports as digital inputs
    TRISDbits.TRISD7 = 1; // Set pin as input
    TRISEbits.TRISE3 = 1;
    
    TRISDbits.TRISD8 = 0; //INT CABLE
    
    TRISDbits.TRISD2 = 1;
    TRISDbits.TRISD3 = 1;
    

    /* Set Interrupt Controller for multi-vector mode */
    INTCONSET = _INTCON_MVEC_MASK;
    
    /* Enable Interrupt Exceptions */
    // set the CP0 status IE bit high to turn on interrupts globally
    __builtin_enable_interrupts();
}
