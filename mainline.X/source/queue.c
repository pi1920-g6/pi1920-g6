#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "queue.h"

/**
 * \brief   Checks if the queue is full.
 * 
 * \param queue Queue to be checked.
 * \return      bool True if full, False otherwise. 
 */
static bool queue_is_full(queue_t *queue)
{
	return (queue->size == QUEUE_MAX_SIZE);
}

/**
 * \brief   Checks if the queue is empty.
 * 
 * \param queue Queue to be checked.
 * \return      bool True if empty, False otherwise. 
 */
static bool queue_is_empty(queue_t *queue)
{
	return (queue->size == 0);
}

queue_error_t queue_init(queue_t *queue)
{
	queue->head = 0;
	queue->tail = QUEUE_MAX_SIZE - 1;
	queue->size = 0;
	uint16_t i = 0;
	for (i = 0; i < QUEUE_MAX_SIZE; i++)
	{
		queue->storage[i] = 0;
	}
	return QUEUE_SUCCESS;
}

queue_error_t queue_enqueue(queue_t *queue, uint8_t value)
{
	if (queue_is_full(queue))
	{
		return QUEUE_IS_FULL;
	}
	queue->tail = (queue->tail + 1) % QUEUE_MAX_SIZE;
	queue->storage[queue->tail] = value;
	queue->size = queue->size + 1;


	return QUEUE_SUCCESS;
}

queue_error_t queue_dequeue(queue_t *queue, uint8_t * value)
{
	if (queue_is_empty(queue))
	{
		return QUEUE_IS_EMPTY;
	}
	uint8_t item = queue->storage[queue->head];
	queue->head = (queue->head + 1) % QUEUE_MAX_SIZE;
	queue->size = queue->size - 1;
	memcpy(value, &item, sizeof (uint8_t));

	return QUEUE_SUCCESS;
}

queue_error_t queue_clear(queue_t *queue)
{
	queue->head = 0;
	queue->tail = QUEUE_MAX_SIZE - 1;
	queue->size = 0;
	uint16_t i = 0;
	for (i = 0; i < QUEUE_MAX_SIZE; i++)
	{
		queue->storage[i] = 0;
	}
	return QUEUE_SUCCESS;
}

uint8_t queue_size(queue_t *queue)
{
	return queue->size;
}
