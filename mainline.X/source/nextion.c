/**
 * \file    nextion.c
 * \brief   
 */
/**
 * \addtogroup  nx Nextion
 * \ref nextion.c
 */
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "utils.h"
#include "uart1.h"
#include "nextion.h"

void nextion_sendCmd(char *str)
{
    while(*str != '\0')
	{
        uart1_write(*(str++));
	}
    // 
    uart1_write(0xFF);
    uart1_write(0xFF);
    uart1_write(0xFF);
}

void nextion_select(char *objname) /*red background*/
{
    char cmd2send[30];
    strcpy(cmd2send,objname);
        
    if(strchr(objname, 'i') != NULL)
    {
        strcat(cmd2send,".val=1");
    }
    else
    {
        strcat(cmd2send,".bco=63488"); //b0.bco=63488´

    }
    nextion_sendCmd(cmd2send);
}
void nextion_unselect(char *objname) /*white background*/
{
    char cmd2send[30];
    strcpy(cmd2send,objname);
        
    if(strchr(objname, 'i') != NULL)
    {
        strcat(cmd2send,".val=0");
    }
    else
    {
        strcat(cmd2send,".bco=65535"); //b0.bco=65535´

    }
    nextion_sendCmd(cmd2send);
}

void nextion_page(char *pageNumber) /* ex: nextion_page("1") -> page 99 */
{
    char cmd2send[10] = "page ";
    strcat(cmd2send,pageNumber);
    
    nextion_sendCmd(cmd2send);
}

void nextion_change_val(char *objname,char *value) /* nextion_change_val("n0","5") -> n99.val=1000 */
{
    char cmd2send[30];
    strcpy(cmd2send,objname); 
    
    strcat(cmd2send,".val="); //n0.val=
    strcat(cmd2send,value); //n0.val=5
    
    nextion_sendCmd(cmd2send);
}

void nextion_increment_val(char *objname) /* nextion_increment_val("n0") -> n99.val++ */
{
    char cmd2send[30];
    strcpy(cmd2send,objname); 
    
    strcat(cmd2send,".val++"); //n0.val++
    
    nextion_sendCmd(cmd2send);
}

void nextion_decrement_val(char *objname) /* nextion_decrement_val("n0") -> n99.val-- */
{
    char cmd2send[30];
    strcpy(cmd2send,objname); 
    
    strcat(cmd2send,".val--"); //n0.val--
    
    nextion_sendCmd(cmd2send);
}

void nextion_change_txt(char *objname,char *txt) /* nextion_change_txt("t0",""hello"") -> t0.txt="hello" */
{
    char cmd2send[60];
    strcpy(cmd2send,objname); 
    strcat(cmd2send,".txt=\""); //t0.txt=
    strcat(cmd2send,txt); //t0.txt="hello"
    strcat(cmd2send,"\""); //t0.txt=
    nextion_sendCmd(cmd2send);
}

void nextion_checkbox_on(char *objname) /* nextion_checkbox_on("c0") -> c0.val=1 -> checkbox on */
{
    char cmd2send[20];
    strcpy(cmd2send,objname); 
    
    strcat(cmd2send,".val=1"); //c0.val=1
    
    nextion_sendCmd(cmd2send);
}

void nextion_checkbox_off(char *objname) /* nextion_checkbox_off("c0") -> c0.val=0 -> checkbox off */
{
    char cmd2send[20];
    strcpy(cmd2send,objname);
    
    strcat(cmd2send,".val=0"); //c0.val=0
    
    nextion_sendCmd(cmd2send);
}

void nextion_select_bar(char *objname) /*red bar*/
{
    char cmd2send[30];
    strcpy(cmd2send,objname);
    
    strcat(cmd2send,".pco=63488"); //j0.pco=63488

    nextion_sendCmd(cmd2send);
}

void nextion_unselect_bar(char *objname) /*gray bar*/
{
    char cmd2send[30];
    strcpy(cmd2send,objname); 
    
    strcat(cmd2send,".pco=33808"); //j0.pco=33808

    nextion_sendCmd(cmd2send);
}

void nextion_increment_bar(char *objname) /* nextion_increment_bar("j0") -> j99.val=j99.val+10 */
{
    char cmd2send[40];
    char result[31];
    strcpy(result,objname);
    strcpy(cmd2send,objname);

    strcat(result,".val+10"); //j0.val+10    
    strcat(cmd2send,".val="); //j0.val=
    strcat(cmd2send,result); //j0.val=j0.val+10
    
    nextion_sendCmd(cmd2send);
}

void nextion_decrement_bar(char *objname) /* nextion_decrement_bar("j0") -> j0.val=j0.val-10 */
{
    char cmd2send[40];
    char result[31];
    strcpy(result,objname);
    strcpy(cmd2send,objname);

    strcat(result,".val-10"); //j0.val-10    
    strcat(cmd2send,".val="); //j0.val=
    strcat(cmd2send,result); //j0.val=j0.val-10
    
    nextion_sendCmd(cmd2send);
}

void nextion_gray_background(char *objname) /*gray background*/
{
    char cmd2send[30];
    strcpy(cmd2send,objname);
    
    strcat(cmd2send,".bco=52825"); //b0.bco=52825

    nextion_sendCmd(cmd2send);
}

void nextion_visible(char *objname) /* ex: vis b0,1 */
{
    char cmd2send[30] = "vis ";
    strcat(cmd2send,objname); //vis b0
    strcat(cmd2send,",1"); // vis b0,1
    
    nextion_sendCmd(cmd2send);
}

void nextion_invisible(char *objname) /* ex: vis b0,0 */
{
    char cmd2send[30] = "vis ";
    strcat(cmd2send,objname); //vis b0
    strcat(cmd2send,",0"); // vis b0,0
    
    nextion_sendCmd(cmd2send);
}