/**
 * \file    settings.c
 */
/**
 * \addtogroup  smSett settings
 * \ingroup sm
 * @{
 *  \ref settings.c
 * @}
 */
#include <xc.h>
#include <sys/attribs.h>
#include <stdbool.h>

#include "SM_structures.h"
#include "settings.h"
#include "defines.h"
#include "nextion.h"
#include "utils.h"
#include "controller.h"
#include "FactoryReset.h"
#include "i2c4.h"

/** \brief  Maximum brightness value. */
#define MaxBrightness 100
/** \brief  Minimum brightness value. */
#define MinBrightness 0
/** \brief  Number of programs. */
#define ProgNumber 21


/** \brief   Variable to store the Brightness value. */
uint16_t BrightnessValue;
/** \brief   Variable to store the Language value. */
uint16_t LanguageValue;
/** \brief   Variable to store welding state. */
uint16_t weldstate;

/** \brief   Buttons on this menu. */
typedef enum st_buttons_t
{
    BRIGHTNESS_BUTTON,
    BRIGHTNESS_BAR,
    LANGUAGE_BUTTON,
    ENGLISH_BUTTON,
    DEUTSCH_BUTTON,
    PORTUGUES_BUTTON,
    FACTORYRESET_BUTTON
} st_buttons_t;

/** \brief   Variable used to identify the button Brightness object. */
static char obj_buttonBrightness[3]="t0";
/** \brief   Variable used to identify the button Language object. */
static char obj_buttonLanguage[3]="t1";
/** \brief   Variable used to identify the button English object. */
static char obj_buttonEnglish[3]="t2";
/** \brief   Variable used to identify the button Deutsch object. */
static char obj_buttonDeutsch[3]="t3";
/** \brief   Variable used to identify the button Portugues object. */
static char obj_buttonPortugues[3]="t4";
/** \brief   Variable used to identify the Brightness Progress bar object. */
static char obj_barBrightness[3]="j0";
/** \brief   Variable used to identify the button Factory Reset object. */
static char obj_buttonFactoryReset[3]="b0";
/** \brief   Variable used to identify the page of this menu. */
static char MenuPage_Settings[3]= "14";
/** \brief   Variable used to identify the Help page of this menu. */
static char HelpPage_Settings[3]="33";
/** \brief  Variable used as argument on nextion function. */
static char NextionBrightnessValue[4];

st_buttons_t Settings_buttons;


/** \brief   Buttons on the Factory Reset Confirmation Page. */
typedef enum frc_buttons_t
{
    FACTORYRESET_BUTTON_NO,
    FACTORYRESET_BUTTON_YES
} frc_buttons_t;

/** \brief   Variable used to identify the page of the Factory Reset's confirmation menu. */
static char ConfirmPage_FactoryReset[3]="15";
/** \brief   Variable used to identify the button NO in the Factory Reset. */
static char obj_buttonFactoryResetNo[3]="b0";
/** \brief   Variable used to identify the button YES in the Factory Reset. */
static char obj_buttonFactoryResetYes[3]="b1";

frc_buttons_t FactoryReset_buttons;


param_id_t param;



/**
 * \brief Responsible for the System's Settings.
 * 
 * This routine will control the screen's brightness, interface language and allows a factory reset of the system.
 * Encoder click will allow the user to move through the main options: Brightness, Language and Factory Reset.
 * Encoder hold will open a Help menu.
 * The Back button:
 *  -makes the interface go to the previous page if there is no option selected.
 *  -if the Brightness is selected the it resets the progress bar, unselects it and goes back to the options.
 *  -if the Language is selected it resets any language chosen and goes back to the options. 
 *  -if the Factory Reset is selected goes back to the Settings page without reseting the machine.
 * House button:
 *  -goes to the home page.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return State Machine current-state and previous-state.
 */
sm_t settings(sm_t stateMachine)
{
    switch(var_interrupt)
    {
        /*
         * HOME EVENT
         * - State machine current state is now Home_Change_Current
         */
        case HOME:
            stateMachine.c_state = HOME_CHANGE_CURRENT; //Current state is now Home_Change_Current
            stateMachine.p_state = SETTINGS; //Previous state is now Settings
            FirstLoop=true; //First Loop for next state
            var_interrupt=NONE; //Clear event flag
            break;
            
        /*
         * BACK EVENT
         *  -if the Brightness is selected it resets the progress bar, unselects it and goes back to the options.
         *  -if the Language is selected it resets any language chosen and goes back to the options. 
         */   
        case BACK:
            switch(Settings_buttons)
            {
                case BRIGHTNESS_BUTTON: //it it is on brightness button -> go to previous menu
                    stateMachine.c_state = SELECTION_MENU; //Current state is now Selection Menu
                    stateMachine.p_state = SETTINGS; // Previous State is now settings
                    FirstLoop=true; //First Loop for next state
                    var_interrupt=NONE; //Clear event flag
                    break;
                case LANGUAGE_BUTTON: //it it is on Language button -> go to previous menu
                    stateMachine.c_state = SELECTION_MENU;//Current state is now Selection Menu
                    stateMachine.p_state = SETTINGS;// Previous State is now settings
                    FirstLoop=true;//First Loop for next state
                    var_interrupt=NONE;//Clear event flag
                    break;
                case FACTORYRESET_BUTTON: //it it is on Factory reset button -> go to previous menu
                    stateMachine.c_state = SELECTION_MENU; //Current state is now Selection Menu
                    stateMachine.p_state = SETTINGS;// Previous State is now settings
                    FirstLoop=true;//First Loop for next state
                    var_interrupt=NONE;//Clear event flag
                    break;
                case BRIGHTNESS_BAR: //it it is on brightness bar value -> go to previous brightness button, discard changed
                    Settings_buttons=BRIGHTNESS_BUTTON; //update button to brightness button
                    
                    param=SETTINGS_BRIGHTNESS; //read Brightness from memory
                    controller_cache_read(param,&BrightnessValue);
                    utils_int_to_str(NextionBrightnessValue,BrightnessValue,3); //convert o string
                    
                    nextion_change_val(obj_barBrightness,NextionBrightnessValue);//reset the value on the brightness bar
                    nextion_unselect_bar(obj_barBrightness); //remove highlight from brightness bar
                    nextion_select(obj_buttonBrightness); //highlight brightness button
                    break;
                case ENGLISH_BUTTON:
                    Settings_buttons=LANGUAGE_BUTTON; //update button to Language button
                    nextion_select(obj_buttonLanguage); //highlight language button
                    nextion_unselect(obj_buttonEnglish); //remove highlight from english button
                    
                    param=SETTINGS_LANGUAGE; //read language from memory
                    controller_cache_read(param,&LanguageValue);
                    
                    switch(LanguageValue)
                     {
                        case 0:
                            nextion_select(obj_buttonEnglish); //Case 0 it is english
                            break;
                        case 1:
                            nextion_select(obj_buttonDeutsch);//Case 1 it is deutsch
                            break;
                        case 2:
                            nextion_select(obj_buttonPortugues);//Case 2 it is portuguese
                            break;
                        default:
                            break;
                    }
                    break;
                case DEUTSCH_BUTTON:
                    Settings_buttons=LANGUAGE_BUTTON; //update button to Language button
                    nextion_unselect(obj_buttonDeutsch);//remove highlight from deutsch button
                    nextion_select(obj_buttonLanguage); //highlight language button
                    param=SETTINGS_LANGUAGE; //read language from memory
                    controller_cache_read(param,&LanguageValue);
                    
                    switch(LanguageValue)
                     {
                        case 0:
                            nextion_select(obj_buttonEnglish);//Case 0 it is english
                            break;
                        case 1:
                            nextion_select(obj_buttonDeutsch); //Case 1 it is deutsch
                            break;
                        case 2:
                            nextion_select(obj_buttonPortugues); //Case 2 it is portuguese
                            break; 
                        default:
                            break;
                    }
                    break;
                case PORTUGUES_BUTTON:
                    Settings_buttons=LANGUAGE_BUTTON; //update button to Language button
                    nextion_unselect(obj_buttonPortugues); //remove highlight from portuguese button
                    nextion_select(obj_buttonLanguage); // highlight language button
                    
                    param=SETTINGS_LANGUAGE; //read language from memory
                    controller_cache_read(param,&LanguageValue);
                    
                    switch(LanguageValue)
                     {
                        case 0:
                            nextion_select(obj_buttonEnglish);//Case 0 it is english
                            break;
                        case 1:
                            nextion_select(obj_buttonDeutsch);//Case 1 it is deutsch
                            break;
                        case 2:
                            nextion_select(obj_buttonPortugues); //Case 2 it is portuguese
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
            var_interrupt=NONE; //Clear event flag
            break;
        
        /*
         * ENCODER BUTTON EVENT
         * - Go to value object when on button
         * - Validate changes when on value
         * - Go to factory reset confirmation menu
         */
        case ENC_BUTTON:
            switch(Settings_buttons)
            {
                case BRIGHTNESS_BUTTON:
                    Settings_buttons=BRIGHTNESS_BAR; //update button to brightness value
                    nextion_unselect(obj_buttonBrightness); //remove highlight from brightness button
                    nextion_select_bar(obj_barBrightness); //highlight brightness bar
                    break;
                case BRIGHTNESS_BAR:
                    Settings_buttons=BRIGHTNESS_BUTTON; //update button to brightness button
                    param=SETTINGS_BRIGHTNESS; 
                    controller_cache_write((int)param,BrightnessValue); //write new brightness value on memory
                    utils_int_to_str(NextionBrightnessValue,BrightnessValue,3); //convert to string
                    nextion_change_val(obj_barBrightness,NextionBrightnessValue); //send to nextion bar object
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters on output buffer
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase  
                    nextion_unselect_bar(obj_barBrightness); //remove hightlight from brighntess bar
                    nextion_select(obj_buttonBrightness); //highlight brightness button
                    break;
                case LANGUAGE_BUTTON: 
                    Settings_buttons=ENGLISH_BUTTON; //update button to English button
                    /*
                     * remove highlight from all languages
                     */
                    nextion_unselect(obj_buttonLanguage);
                    nextion_unselect(obj_buttonPortugues);
                    nextion_unselect(obj_buttonDeutsch);
                    
                    /*
                     * Highlight english 
                     */      
                    nextion_select(obj_buttonEnglish);
                    break;
                case ENGLISH_BUTTON:
                    Settings_buttons=LANGUAGE_BUTTON; //update button to language button
                    
                    param=SETTINGS_LANGUAGE;
                    LanguageValue=0; //set language value to 0 -> English
                    controller_cache_write((int)param,LanguageValue); //write value on memory
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase  
                    nextion_select(obj_buttonLanguage); //highlight language button
                    nextion_select(obj_buttonEnglish); //highlight  english language 
                    break;
                case DEUTSCH_BUTTON:
                    Settings_buttons=LANGUAGE_BUTTON; //update button to English button
                    
                    param=SETTINGS_LANGUAGE;
                    LanguageValue=1;  //set language value to 1 -> Deutsch
                    controller_cache_write((int)param,LanguageValue);//write value on memory
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase  
                    nextion_select(obj_buttonLanguage);//highlight language button
                    nextion_select(obj_buttonDeutsch);//highlight Deutsch language 
                    break;
                case PORTUGUES_BUTTON:
                    Settings_buttons=LANGUAGE_BUTTON; //update button to English button
                    param=SETTINGS_LANGUAGE;
                    LanguageValue=2;  //set language value to 2 -> Portuguese
                    controller_cache_write((int)param,LanguageValue);//write value on memory
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=0; //Set programID to previous saved just for showcase  
                    nextion_select(obj_buttonPortugues);//highlight language button
                    nextion_select(obj_buttonLanguage); //highlight Portuguese language 
                    break;
                case FACTORYRESET_BUTTON:
                    stateMachine.c_state = FACTORY_RESET; //update current state to Factory Reset confirmation
                    stateMachine.p_state = SETTINGS; //update previous state to settings
                    FirstLoop=true; //FirstLoop for next state
                    break;
                default:
                    break;
            }
            var_interrupt=NONE; //Clear event flag
            break;
            
        /*
         * ENCODER HOLD EVENT
         * - Change to Help menu page
         * - Update previous and current state
         */    
        case ENC_HOLD:
            nextion_page(HelpPage_Settings); //open nextion page 
            stateMachine.c_state = HELP;
            stateMachine.p_state = SETTINGS;
            FirstLoop=true;
            var_interrupt=NONE;//Clear event flag
            break;
            
        /*
         * ENCODER RIGHT EVENT
         * - Allows moving to the right between buttons, updating the highlights
         * - Allows value changed when on values
         */    
        case ENC_RIGHT:
            switch(Settings_buttons)
            {
                case BRIGHTNESS_BUTTON:
                    Settings_buttons=LANGUAGE_BUTTON;
                    nextion_unselect(obj_buttonBrightness);
                    nextion_select(obj_buttonLanguage);
                    break;
                case BRIGHTNESS_BAR:
                    if(BrightnessValue<MaxBrightness) //if on value and below max value
                    {
                    BrightnessValue=BrightnessValue+10; //update the bar by 10
                    nextion_increment_bar(obj_barBrightness); //nextion increment bar
                    }
                    break;    
                case LANGUAGE_BUTTON:
                    Settings_buttons=FACTORYRESET_BUTTON;
                    nextion_unselect(obj_buttonLanguage);
                    nextion_select(obj_buttonFactoryReset);
                    break;
                case ENGLISH_BUTTON:
                    Settings_buttons=DEUTSCH_BUTTON;
                    nextion_unselect(obj_buttonEnglish);
                    nextion_select(obj_buttonDeutsch);
                    break;
                case DEUTSCH_BUTTON:
                    Settings_buttons=PORTUGUES_BUTTON;
                    nextion_unselect(obj_buttonDeutsch);
                    nextion_select(obj_buttonPortugues);
                    break;
                default:
                    break;
            }
            var_interrupt=NONE; //Clear event flag
            break;  
        
        /*
         * ENCODER LEFT EVENT
         * - Allows moving to the lreft between buttons, updating the highlights
         */    
        case ENC_LEFT:
            switch(Settings_buttons)
            {
                case LANGUAGE_BUTTON:
                    Settings_buttons=BRIGHTNESS_BUTTON;
                    nextion_unselect(obj_buttonLanguage);
                    nextion_select(obj_buttonBrightness);
                    break;
                    
                case FACTORYRESET_BUTTON:
                    Settings_buttons=LANGUAGE_BUTTON;
                    nextion_unselect(obj_buttonFactoryReset);
                    nextion_select(obj_buttonLanguage);
                    break;   
                    
                case BRIGHTNESS_BAR: //if on value and above min value
                    if(BrightnessValue>MinBrightness)
                    {
                    nextion_decrement_bar(obj_barBrightness); //update the bar by 10
                    BrightnessValue=BrightnessValue-10; //nextion increment bar
                    }
                    break;  
                
                case DEUTSCH_BUTTON:
                    Settings_buttons=ENGLISH_BUTTON;
                    nextion_unselect(obj_buttonDeutsch);
                    nextion_select(obj_buttonEnglish);
                    break;
                    
                case PORTUGUES_BUTTON:
                    Settings_buttons=DEUTSCH_BUTTON;
                    nextion_unselect(obj_buttonPortugues);
                    nextion_select(obj_buttonDeutsch);
                    break;
                    
                default:
                    break;
            }
            var_interrupt=NONE; //Clear event flag
            break;  
        
        /*
         * NONE
         * - Update values on Nextion
         * - Set nextion page
         * - Highlight Current Object
         */ 
        case NONE:
            if(FirstLoop==true)
            {
                Settings_buttons=BRIGHTNESS_BUTTON; //set button to Brightness
                nextion_page(MenuPage_Settings); //set page on nextion
                nextion_select(obj_buttonBrightness); //select brightness
                
                param=SETTINGS_BRIGHTNESS; //read brightness from memory
                controller_cache_read(param,&BrightnessValue);
                utils_int_to_str(NextionBrightnessValue,BrightnessValue,3); //convert to string
                nextion_change_val(obj_barBrightness,NextionBrightnessValue);  //send to nextion
                
                param=SETTINGS_LANGUAGE; //read language from memory
                controller_cache_read(param,&LanguageValue);
                
                /*
                 * Remove all highlights on languages
                 */
                nextion_unselect(obj_buttonDeutsch);
                nextion_unselect(obj_buttonPortugues);
                nextion_unselect(obj_buttonEnglish);
                
                switch(LanguageValue)
                {
                    case 0:
                        nextion_select(obj_buttonEnglish); //if it is 0, English
                        break;
                    case 1:
                        nextion_select(obj_buttonDeutsch); //if it is 1, Deutsch
                        break;
                    case 2:
                        nextion_select(obj_buttonPortugues); //if it is 2, Portuguese
                        break;
                    default:
                        break;
                }
                FirstLoop=false; //FirstLoop flag disabled
            }
            
            if(val_update==true) // val_update from timer
            {

                param = WELD_STATE; //read welding state
                controller_cache_read(param, &weldstate);

                if(weldstate == 1) //it is active ( = 1 )
                {
                    stateMachine.c_state = HOME_CHANGE_CURRENT; //go to home menu
                    stateMachine.p_state = SETTINGS;
                    FirstLoop=true; //set first loop flag 
                    var_interrupt=NONE; //Clear event flag
                }
                val_update=false; //clear update flag
            }
            var_interrupt=NONE;//Clear event flag
            break;     
            
        default:
            var_interrupt=NONE; //Clear event flag
            break;
    }
    return stateMachine;
}

/**
 * \brief Responsible for the Factory Reset Confirmation.
 * 
 * This routine will allow a factory reset of the system.
 * Encoder click will allow the user to confirm the decision of making a Factory Reset.
 * The Back button:
 *  -goes back to the Settings page without reseting the machine.
 * Home button:
 *  -goes to the home page.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return State Machine current-state and previous-state.
 */

sm_t confirmFactoryReset(sm_t stateMachine)
{
    switch(var_interrupt)
    {
        /*
         * HOME EVENT
         * - State machine current state is now Home_Change_Current
         */
        case HOME:
            stateMachine.c_state = HOME_CHANGE_CURRENT; //update current state to HOME_CHANGE_CURRENT 
            stateMachine.p_state = FACTORY_RESET; //update previous state to FACTORY_RESET
            var_interrupt=NONE; //clear event flag
            FirstLoop=true; //FirstLoop for new state
            break;
            
        /*
         * BACK EVENT
         * - goes back to the Settings page without reseting the machine.
         */   
        case BACK:
            stateMachine.c_state = SETTINGS; //update current state to Settings 
            stateMachine.p_state = FACTORY_RESET; //update previous state to FACTORY_RESET
            var_interrupt=NONE; //clear event flag
            FirstLoop=true; //FirstLoop for new state
            break;
        
        /*
         * ENCODER BUTTON EVENT
         * - If No is pressed, return to previous menu
         * - If Yes is pressed, proced to factory reset
         */
        case ENC_BUTTON:
            switch(FactoryReset_buttons)
            {
                case FACTORYRESET_BUTTON_NO: //if NO is pressed
                    stateMachine.c_state = SETTINGS; //update current state to Settings 
                    stateMachine.p_state = FACTORY_RESET; //update previous state to FACTORY_RESET
                    break;
                case FACTORYRESET_BUTTON_YES:
                    controller_outbound_buffer_fill(FactoryResetRequest,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=50; //Set programID to random value
                    while(programID_memory != 0) //wait to receive from master program 0
                    {
                        if(i2c4_stop_condition()){
                            controller_inbound_buffer_save();
                        }
                    }//only leave this menu when programID is updated
                    stateMachine.c_state = HOME_CHANGE_CURRENT; //update current state to  HOME_CHANGE_CURRENT  
                    stateMachine.p_state = FACTORY_RESET; //update previous state to FACTORY_RESET
                    break;
                default:
                    break;
            }
            FirstLoop=true; //FirstLoop for new state
            var_interrupt=NONE; //clear event flag
            break;
            
        /*
         * ENCODER RIGHT EVENT
         * - Allows moving to the right between buttons, updating the highlights
         */    
        case ENC_RIGHT:
            switch(FactoryReset_buttons)
            {
                case FACTORYRESET_BUTTON_NO:
                    FactoryReset_buttons=FACTORYRESET_BUTTON_YES;
                    nextion_unselect(obj_buttonFactoryResetNo);
                    nextion_select(obj_buttonFactoryResetYes);
                    break;
                case FACTORYRESET_BUTTON_YES:
                    break;
                default:
                    break;
            }
            var_interrupt=NONE;
            break;  
        
        /*
         * ENCODER LEFT EVENT
         * - Allows moving to the lreft between buttons, updating the highlights
         */    
        case ENC_LEFT:
            switch(FactoryReset_buttons)
            {
                case FACTORYRESET_BUTTON_NO:
                    break;
                case FACTORYRESET_BUTTON_YES:
                    FactoryReset_buttons=FACTORYRESET_BUTTON_NO;
                    nextion_unselect(obj_buttonFactoryResetYes);
                    nextion_select(obj_buttonFactoryResetNo);
                default:
                    break;
            }
            var_interrupt=NONE;
            break;  
        
        /*
         * NONE
         * - Update values on Nextion
         * - Set nextion page
         * - Highlight Current Object
         */ 
        case NONE:
            if(FirstLoop==true)
            {
                nextion_page(ConfirmPage_FactoryReset); //set nextion page
                
                FactoryReset_buttons=FACTORYRESET_BUTTON_NO; //Set button as NO
                
                /*
                 * ´Remove highlight from objects
                 */
                nextion_unselect(obj_buttonFactoryResetYes);
                nextion_unselect(obj_buttonFactoryResetNo);
                
                /*
                 * Highlight NO object
                 */
                nextion_select(obj_buttonFactoryResetNo);
                
                FirstLoop=false; //disable first loop flag
            }
            if(val_update==true) // val_update from timer
            {

                param = WELD_STATE; //read welding state
                controller_cache_read(param, &weldstate);

                if(weldstate == 1) //it is active ( = 1 )
                {
                    stateMachine.c_state = HOME_CHANGE_CURRENT; //go to home menu
                    stateMachine.p_state = FACTORY_RESET;
                    FirstLoop=true; //set first loop flag 
                    var_interrupt=NONE; //Clear event flag
                }
                val_update=false; //clear update flag
            }

            var_interrupt=NONE; //clear event flag
            break;     
            
            
        default:
            var_interrupt=NONE; //clear event flag
            break;
    }
    return stateMachine;
}