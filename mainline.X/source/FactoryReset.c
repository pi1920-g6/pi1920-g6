/**
 * \file    FactoryReset.c
 */
/**
 * \addtogroup  smFr FactoryReset
 * \ingroup sm
 * @{
 *  \ref FactoryReset.c
 * @}
 */
#include <xc.h>
#include <sys/attribs.h>
#include <math.h>
#include <ieee754.h>
#include <stdio.h>
#include <stdbool.h>

#include "FactoryReset.h"
#include "SM_structures.h"
#include "defines.h"
#include "controller.h"



uint16_t FR_WeldingProcess=0;
uint16_t FR_Opmode=1;
uint16_t FR_Cooling=0;
uint16_t FR_TigState=0;
uint16_t FR_MMAState=0;
uint16_t FR_WeldingState=0;
uint16_t FR_Errors=0;
uint16_t FR_CurrentRead=0;
uint16_t FR_VoltageRead=0;
uint16_t FR_TIGPreGas=1;
uint16_t FR_TIGIstart=10;
uint16_t FR_TIGUpSlope=0;
uint16_t FR_TIGInominal=100;
uint16_t FR_TIGIBase=1;
uint16_t FR_TIGPulseWidth=10;
uint16_t FR_TIGPulseFreq=1;
uint16_t FR_TIGDownSlope=0;
uint16_t FR_TIGIEnd=10;
uint16_t FR_TIGPostGas=1;
uint16_t FR_TIGDynamics=0;
uint16_t FR_TIGSpotTime=1;
uint16_t FR_MMAHotStart=10;
uint16_t FR_MMAHotStartTime=1;
uint16_t FR_MMAInominal=100;
uint16_t FR_MMAArcForce=0;
uint16_t FR_MMAIbase=50;
uint16_t FR_MMAPulseWidth=10;
uint16_t FR_MMAPulseFreq=5;
uint16_t FR_Brightness=50;
uint16_t FR_Language=0;



param_id_t param;



void factory_reset_parameters(void) {
    
    param=WELD_PROCESS;
    controller_cache_write((int)param,FR_WeldingProcess);
    
    param=OP_MODE;
    controller_cache_write((int)param,FR_Opmode);
    
    param=COOLING;
    controller_cache_write((int)param,FR_Cooling);
    
    param=TIG_STATE;
    controller_cache_write((int)param,FR_TigState);
    
    param=MMA_STATE;
    controller_cache_write((int)param,FR_MMAState);
    
    param=WELD_STATE;
    controller_cache_write((int)param,FR_WeldingState);
    
    param=ERRORS;
    controller_cache_write((int)param,FR_Errors);
    
    param=CURRENT_READ;
    controller_cache_write((int)param,FR_CurrentRead);
    
    param=VOLTAGE_READ;
    controller_cache_write((int)param,FR_VoltageRead);
    
    param=TIG_PRE_GAS;
    controller_cache_write((int)param,FR_TIGPreGas);
    
    param=TIG_ISTART;
    controller_cache_write((int)param,FR_TIGIstart);
    
    param=TIG_UPSLOPE;
    controller_cache_write((int)param,FR_TIGUpSlope);
    
    param=TIG_INOMINAL;
    controller_cache_write((int)param,FR_TIGInominal);
    
    param=TIG_IBASE_;
    controller_cache_write((int)param,FR_TIGIBase);
    
    param=TIG_PULSE_WITH_;
    controller_cache_write((int)param,FR_TIGPulseWidth);
    
    param=TIG_PULSE_FREQUENCY;
    controller_cache_write((int)param,FR_TIGPulseFreq);
    
    param=TIG_DOWNSLOPE_;
    controller_cache_write((int)param,FR_TIGDownSlope);
    
    param=TIG_IEND;
    controller_cache_write((int)param,FR_TIGIEnd);
    
    param=TIG_POSTGAS;
    controller_cache_write((int)param,FR_TIGPostGas);
    
    param=TIG_DYNAMICS;
    controller_cache_write((int)param,FR_TIGDynamics);
    
    param=TIG_SPOTTIME;
    controller_cache_write((int)param,FR_TIGSpotTime);
    
    param=MMA_HOTSTART;
    controller_cache_write((int)param,FR_MMAHotStart);
    
    param=MMA_HOTSTART_TIME;
    controller_cache_write((int)param,FR_MMAHotStartTime);
    
    param=MMA_INOMINAL;
    controller_cache_write((int)param,FR_MMAInominal);
    
    param=MMA_ARCFORCE;
    controller_cache_write((int)param,FR_MMAArcForce);
    
    param=MMA_IBASE;
    controller_cache_write((int)param,FR_MMAIbase);  
    
    param=MMA_PULSE_WIDTH;
    controller_cache_write((int)param,FR_MMAPulseWidth);
    
    param=MMA_PULSE_FREQ;
    controller_cache_write((int)param,FR_MMAPulseFreq);
    
    param=SETTINGS_BRIGHTNESS;
    controller_cache_write((int)param,FR_Brightness);
    
    param=SETTINGS_LANGUAGE;
    controller_cache_write((int)param,FR_Language);
    
}