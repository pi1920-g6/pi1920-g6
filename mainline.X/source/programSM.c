/**
 * \file    programSM.c
 */
/**
 * \addtogroup  smProg programSM
 * \ingroup sm
 * @{
 *  \ref programSM.c
 * @}
 */
#include <stdbool.h>

#include "SM_structures.h"
#include "programSM.h"
#include "defines.h"
#include "nextion.h"
#include "utils.h"
#include "i2c4.h"
#include "controller.h"
#include "FactoryReset.h"

#ifndef __UNITTESTING__
    #include <xc.h>
    #include <sys/attribs.h>
#else
    #include "../../XC32_mock/mem_map.h"
#endif


/** \brief   Variable to store welding state. */
uint16_t weldstate;
/** \brief   Variable to store welding process. */
uint16_t wpr;
/** \brief   Variable to store the program in which parameters are going to be saved. */
uint8_t programtosave;

/** \brief   Variable to store the Hot Start value. */
uint16_t Program_MMAHotStart; 
/** \brief   Variable to store the Hot Start time value. */
uint16_t Program_MMAHotStartTime;
/** \brief   Variable to store the Nominal Current value. */
uint16_t Program_MMAINominal;
/** \brief   Variable to store the Arc Force value. */
uint16_t Program_MMAArcForce;
/** \brief   Variable to store the Base current value. */
uint16_t Program_MMAIBase;
/** \brief   Variable to store the Width of the peak current value. */
uint16_t Program_MMAWidthval;
/** \brief   Variable to store the Frequency of the pulse. */
uint16_t Program_MMAPFreq;

/** \brief   Variable to store the Pre gas value. */
uint16_t Program_TIGPreGas; 
/** \brief   Variable to store the Start current value. */
uint16_t Program_TIGIStart;
/** \brief   Variable to store the Up Slope time. */
uint16_t Program_TIGUpSlope;
/** \brief   Variable to store the Nominal Current value. */
uint16_t Program_TIGINominal;
/** \brief   Variable to store the Base Current value. */
uint16_t Program_TIGIBase;
/** \brief   Variable to store the Pulse Width value. */
uint16_t Program_TIGPulseWidth;
/** \brief   Variable to store the Pulse Frequency value. */
uint16_t Program_TIGPulseFreq;
/** \brief   Variable to store the Down Slope time. */
uint16_t Program_TIGDownSlope;
/** \brief   Variable to store the Final current value. */
uint16_t Program_TIGIEnd;
/** \brief   Variable to store the Post Gas value. */
uint16_t Program_TIGPostGas;
/** \brief   Variable to store the Dynamics value. */
uint16_t Program_TIGDynamics;
/** \brief   Variable to store the Spot Time value. */
uint16_t Program_TIGSpotTime;
/** \brief   Variable to store the Welding Process value. */
uint16_t Program_WeldProc;
/** \brief   Variable to store the Operation mode value. */
uint16_t Program_OpMode;

/*
 * Program selection page
 */
/** \brief   Variable used to identify the Program 0 object. */
char obj_bp0[3]= "b0";
/** \brief   Variable used to identify the Program 1 object. */
char obj_bp1[3]= "b1";
/** \brief   Variable used to identify the Program 2 object. */
char obj_bp2[3]= "b2";
/** \brief   Variable used to identify the Program 3 object. */
char obj_bp3[3]= "b3";
/** \brief   Variable used to identify the Program 4 object. */
char obj_bp4[3]= "b4";
/** \brief   Variable used to identify the Program 5 object. */
char obj_bp5[3]= "b5";
/** \brief   Variable used to identify the Program 6 object. */
char obj_bp6[3]= "b6";
/** \brief   Variable used to identify the Program 7 object. */
char obj_bp7[3]= "b7";
/** \brief   Variable used to identify the Program 8 object. */
char obj_bp8[3]= "b8";
/** \brief   Variable used to identify the Program 9 object. */
char obj_bp9[3]= "b9";
/** \brief   Variable used to identify the Program 10 object. */
char obj_bp10[4]= "b10";
/** \brief   Variable used to identify the Program 11 object. */
char obj_bp11[4]= "b11";
/** \brief   Variable used to identify the Program 12 object. */
char obj_bp12[4]= "b12";
/** \brief   Variable used to identify the Program 13 object. */
char obj_bp13[4]= "b13";
/** \brief   Variable used to identify the Program 15 object. */
char obj_bp14[4]= "b14";
/** \brief   Variable used to identify the Program 16 object. */
char obj_bp15[4]= "b15";
/** \brief   Variable used to identify the Program 16 object. */
char obj_bp16[4]= "b16";
/** \brief   Variable used to identify the Program 17 object. */
char obj_bp17[4]= "b17";
/** \brief   Variable used to identify the Program 18 object. */
char obj_bp18[4]= "b18";
/** \brief   Variable used to identify the Program 19 object. */
char obj_bp19[4]= "b19";
/** \brief   Variable used to identify the Program 20 object. */
char obj_bp20[4]= "b20";
/** \brief   Variable used to identify the first page marker.  */
char obj_buttonM1[3]="c0";
/** \brief   Variable used to identify the second page marker. */
char obj_buttonM2[3]="c1";

/*
 * Common to parameters 
 */
/** \brief   Variable used to identify the Program textbox object. */
char obj_ProgramName[3]= "t0";
/** \brief   Variable used to identify the Process textbox object. */
char obj_ProgramProcess[3]= "t1";
/** \brief   Variable used to identify the Mode textbox object. */
char obj_ProgramModes[3]= "t2";
/** \brief   Variable used to identify the Save button object. */
char obj_ProgramSavebutton[3]= "b2";
/** \brief   Variable used to identify the Load button object. */
char obj_ProgramLoadbutton[3]= "b0";
/** \brief   Variable used to identify the Reset button object. */
char obj_ProgramResetbutton[3]= "b1";
/** \brief   Variable used to send to nextion program. */
char Nextion_PrgNumber[3];

/*
 * TIG Program page
 */
/** \brief   Variable used to identify the Pre Gas textbox object. */
char obj_ProgramPreGas[3]= "x0";
/** \brief   Variable used to identify the Start Current textbox object. */
char obj_ProgramStartCurrent[3]= "n0";
/** \brief   Variable used to identify the Up Slope textbox object. */
char obj_ProgramUpSlope[3]= "x1";
/** \brief   Variable used to identify the Peak Current textbox object. */
char obj_ProgramPeakCurrent[3]= "n1";
/** \brief   Variable used to identify the Base Current textbox object. */
char obj_ProgramBaseCurrent[3]= "n2";
/** \brief   Variable used to identify the Width Peak textbox object. */
char obj_ProgramWidthPeak[3]= "n3";
/** \brief   Variable used to identify the Freq Pulse textbox object. */
char obj_ProgramFreqPulse[3]= "n4";
/** \brief   Variable used to identify the Down slope textbox object. */
char obj_ProgramDownSlope[3]= "x2";
/** \brief   Variable used to identify the Final current textbox object. */
char obj_ProgramFinalCurrent[3]= "n5";
/** \brief   Variable used to identify the Post Gas textbox object. */
char obj_ProgramPostGas[3]= "x3";
/** \brief   Variable used to identify the Dynamics textbox object. */
char obj_ProgramDynamics[3]= "n6";
/** \brief   Variable used to identify the Spot Time textbox object. */
char obj_ProgramSpotTime[3]= "x4";
/** \brief   Variable used to send to nextion TIG Pre Gas. */
char NextionTIG_PreGas[3];
/** \brief   Variable used to send to nextion TIG Start current. */
char NextionTIG_IStart[3];
/** \brief   Variable used to send to nextion TIG Up Slope. */
char NextionTIG_UpSlope[3];
/** \brief   Variable used to send to nextion TIG Nominal Current. */
char NextionTIG_INominal[3];
/** \brief   Variable used to send to nextion TIG base Current. */
char NextionTIG_IBase[3];
/** \brief   Variable used to send to nextion TIG Pulse width. */
char NextionTIG_PulseWidth[3];
/** \brief   Variable used to send to nextion TIG Pulse freq. */
char NextionTIG_PulseFreq[3];
/** \brief   Variable used to send to nextion TIG Down Slope. */
char NextionTIG_DownSlope[3];
/** \brief   Variable used to send to nextion TIG Final current. */
char NextionTIG_IEnd[3];
/** \brief   Variable used to send to nextion TIG Post Gas. */
char NextionTIG_PostGas[3];
/** \brief   Variable used to send to nextion TIG Dynamics value. */
char NextionTIG_Dynamics[1];
/** \brief   Variable used to send to nextion TIG Spot Time value. */
char NextionTIG_SpotTime[3];

/*
 * MMA Program page
 */
/** \brief   Variable used to identify the Hot Start textbox object. */
char obj_ProgramHotStart[3]= "n0";
/** \brief   Variable used to identify the Hot Start Time textbox object. */
char obj_ProgramHotStartTime[3]= "x0";
/** \brief   Variable used to identify the Peak Current textbox object. */
char obj_ProgramINominal[3]= "n1";
/** \brief   Variable used to identify the Arc Force textbox object. */
char obj_ProgramArcForce[3]= "n2";
/** \brief   Variable used to identify the Base Current textbox object. */
char obj_ProgramIbase[3]= "n2";
/** \brief   Variable used to identify the Pulse Width textbox object. */
char obj_ProgramPulseWidth[3]= "n3";
/** \brief   Variable used to identify the Freq Pulse textbox object. */
char obj_ProgramPulseFreq[3]= "x1";

/** \brief   Variable used to send to nextion MMA HotStart. */
char NextionMMA_HotStart[3];
/** \brief   Variable used to send to nextion MMA HotStart Time. */
char NextionMMA_HotStartTime[3];
/** \brief   Variable used to send to nextion MMA Nominal Current. */
char NextionMMA_INominal[3];
/** \brief   Variable used to send to nextion MMA Arc Force. */
char NextionMMA_AcrForce[3];
/** \brief   Variable used to send to nextion MMA Base Current. */
char NextionMMA_BaseCurrent[3];
/** \brief   Variable used to send to nextion MMA Width. */
char NextionMMA_Width[2];
/** \brief   Variable used to send to nextion MMA Pulse Frequency. */
char NextionMMA_PulseFreq[2];


/*
 * Pages
 */
/** \brief   Variable used to identify the Programs first page. */
char program_page1[3]= "17";
/** \brief   Variable used to identify the Programs second page. */
char program_page2[3]= "18";
/** \brief   Variable used to identify the program 0 MMA page. */
char program_MMA0[3]= "23";
/** \brief   Variable used to identify the program 0 MMA pulse page. */
char program_MMAPULSE0[3]= "25";
/** \brief   Variable used to identify the program 0 TIG page. */
char program_TIG0[3]= "19";
/** \brief   Variable used to identify the program 0 TIG pulse page. */
char program_TIGPULSE0[3]= "21";
/** \brief   Variable used to identify the programs MMA page. */
char program_MMA[3]= "24";
/** \brief   Variable used to identify the programs MMA pulse page. */
char program_MMAPULSE[3]= "26";
/** \brief   Variable used to identify the programs TIG page. */
char program_TIG[3]= "20";
/** \brief   Variable used to identify the programs TIG pulse page. */
char program_TIGPULSE[3]= "22";
/** \brief   Variable used to identify the Programs help page. */
char program_help[3]= "28";
/** \brief   Variable used to identify the Programs help page. */
char program_help0[3]= "27";

typedef enum Program_buttons_t
{
    P0,
    P1,
    P2,
    P3,
    P4,
    P5,   
    P6,
    P7,
    P8,
    P9,
    P10,
    P11,
    P12,
    P13,
    P14,
    P15,
    P16,
    P17,
    P18,
    P19,
    P20         
} Program_buttons_t;

typedef enum programpage_buttons_t
{
    SAVE,
    LOAD,
    RESET
} programpage_buttons_t;

param_id_t param;
Program_buttons_t programbuttons;
programpage_buttons_t  programpagebuttons;


sm_t programSM_page1(sm_t stateMachine)
{
    switch(var_interrupt)
    {
        /*
         * HOME EVENT
         * - State machine state is now Home_Change_Current
         */
        case HOME:
            stateMachine.c_state = HOME_CHANGE_CURRENT;
            stateMachine.p_state = MEMORY_PG1;
            FirstLoop=true;
            var_interrupt=NONE;
            break;
            
        /*
         * BACK EVENT
         *  - State machine state is now Home_Change_Current
         */   
        case BACK:
            stateMachine.c_state = SELECTION_MENU;
            stateMachine.p_state = MEMORY_PG1;
            FirstLoop=true;
            var_interrupt=NONE;
            break;
        
        /*
         * ENCODER BUTTON EVENT
         * - Load values from the program selected
         * - Open the corresponding page
         */
        case ENC_BUTTON:
            switch(programbuttons)
            {
                case P0:
                    break;
                case P1:
                    controller_outbound_buffer_fill(ReadProgramFromMaster,1);//Ask master for program parameters
                    controller_waitwrite();//Wait for NACK
                    while(programID_memory != 1)
                    {
                        if(i2c4_stop_condition()){
                            controller_inbound_buffer_save();
                        }
                    }//only leave this menu when programID is updated
                    break;
                case P2:
                    controller_outbound_buffer_fill(ReadProgramFromMaster,2);//Ask master for program parameters
                    controller_waitwrite();//Wait for NACK
                    while(programID_memory != 2)
                    {
                        if(i2c4_stop_condition()){
                            controller_inbound_buffer_save();
                        }
                    }//only leave this menu when programID is updated
                    break;
                case P3:
                    controller_outbound_buffer_fill(ReadProgramFromMaster,3);//Ask master for program parameters
                    controller_waitwrite();//Wait for NACK
                    while(programID_memory != 3)
                    {
                        if(i2c4_stop_condition()){
                            controller_inbound_buffer_save();
                        }
                    }//only leave this menu when programID is updated
                    break;
                case P4:
                    controller_outbound_buffer_fill(ReadProgramFromMaster,4);//Ask master for program parameters
                    controller_waitwrite();//Wait for NACK
                    while(programID_memory != 4)
                    {
                        if(i2c4_stop_condition()){
                            controller_inbound_buffer_save();
                        }
                    }//only leave this menu when programID is updated
                    break;
                case P5:
                    controller_outbound_buffer_fill(ReadProgramFromMaster,5);//Ask master for program parameters
                    controller_waitwrite();//Wait for NACK
                    while(programID_memory != 5)
                    {
                        if(i2c4_stop_condition()){
                            controller_inbound_buffer_save();
                        }
                    }//only leave this menu when programID is updated
                    break;
                case P6:
                    controller_outbound_buffer_fill(ReadProgramFromMaster,6);//Ask master for program parameters
                    controller_waitwrite();//Wait for NACK
                    while(programID_memory != 6)
                    {
                        if(i2c4_stop_condition()){
                            controller_inbound_buffer_save();
                        }
                    }//only leave this menu when programID is updated
                    break;
                case P7:
                    controller_outbound_buffer_fill(ReadProgramFromMaster,7);//Ask master for program parameters
                    controller_waitwrite();//Wait for NACK
                    while(programID_memory != 7)
                    {
                        if(i2c4_stop_condition()){
                            controller_inbound_buffer_save();
                        }
                    }//only leave this menu when programID is updated
                    break;
                case P8:
                    controller_outbound_buffer_fill(ReadProgramFromMaster,8);//Ask master for program parameters
                    controller_waitwrite();//Wait for NACK
                    while(programID_memory != 8)
                    {
                        if(i2c4_stop_condition()){
                            controller_inbound_buffer_save();
                        }
                    }//only leave this menu when programID is updated
                    break;
                case P9: 
                    controller_outbound_buffer_fill(ReadProgramFromMaster,9);//Ask master for program parameters
                    controller_waitwrite();//Wait for NACK
                    while(programID_memory != 9)
                    {
                        if(i2c4_stop_condition()){
                            controller_inbound_buffer_save();
                        }
                    }//only leave this menu when programID is updated
                    break;
                case P10:
                    controller_outbound_buffer_fill(ReadProgramFromMaster,10);//Ask master for program parameters
                    controller_waitwrite();//Wait for NACK
                    while(programID_memory != 10)
                    {
                        if(i2c4_stop_condition()){
                            controller_inbound_buffer_save();
                        }
                    }//only leave this menu when programID is updated
                    break;
                case P11:
                    controller_outbound_buffer_fill(ReadProgramFromMaster,11);//Ask master for program parameters
                    controller_waitwrite();//Wait for NACK
                    while(programID_memory != 11)
                    {
                        if(i2c4_stop_condition()){
                            controller_inbound_buffer_save();
                        }
                    }//only leave this menu when programID is updated
                    break;
                default:
                    break;
            }
            
            param = WELD_PROCESS; //memory parameter -> Welding Process
            controller_cache_read(param, &wpr); //Read welding process from memory
            
            if (programID_memory != 0)
            {
                switch (wpr)
                {
                    case 0: //Case 0 is TIG HF
                        stateMachine.c_state = MEMORY_TIG ; //Update current state
                        break;
                    case 1: //Case 1 is TIG HF PULSE
                        stateMachine.c_state = MEMORY_TIGPULSE;//Update current state
                        break;
                    case 2: //Case 2 is LIFTIG
                        stateMachine.c_state = MEMORY_TIG;//Update current state
                        break;
                    case 3: //Case 3 is LIFTIG PULSE
                        stateMachine.c_state = MEMORY_TIGPULSE;//Update current state
                        break;
                    case 4: //Case 4 is MMA
                        stateMachine.c_state = MEMORY_MMA;//Update current state
                        break;
                    case 5: //Case 5 is MMA PULSE
                        stateMachine.c_state = MEMORY_MMAPULSE;//Update current state
                        break;
                    default:
                        break;
                }
            }else{
                switch (wpr)
                {
                    case 0: //Case 0 is TIG HF
                        stateMachine.c_state = MEMORY_TIG0; //Update current state
                        break;
                    case 1: //Case 1 is TIG HF PULSE
                        stateMachine.c_state = MEMORY_TIGPULSE0;//Update current state
                        break;
                    case 2: //Case 2 is LIFTIG
                        stateMachine.c_state = MEMORY_TIG0;//Update current state
                        break;
                    case 3: //Case 3 is LIFTIG PULSE
                        stateMachine.c_state = MEMORY_TIGPULSE0;//Update current state
                        break;
                    case 4: //Case 4 is MMA
                        stateMachine.c_state = MEMORY_MMA0;//Update current state
                        break;
                    case 5: //Case 5 is MMA PULSE
                        stateMachine.c_state = MEMORY_MMAPULSE0;//Update current state
                        break;
                    default:
                        break;
                }
            }
            stateMachine.p_state = MEMORY_PG1;
            FirstLoop=true;
            var_interrupt=NONE;
            break;
            
        /*
         * ENCODER HOLD EVENT
         * - do nothing
         */    
        case ENC_HOLD:
            var_interrupt=NONE;
            break;
            
        /*
         * ENCODER RIGHT EVENT
         * - Moving between the different programs (right turn)
         */    
        case ENC_RIGHT:
            switch(programbuttons)
            {
                case P0:
                    nextion_unselect(obj_bp0); 
                    nextion_select(obj_bp1);
                    programbuttons=P1;
                    break;
                case P1:
                    nextion_unselect(obj_bp1);
                    nextion_select(obj_bp2);
                    programbuttons=P2;
                    break;
                case P2:
                    nextion_unselect(obj_bp2);
                    nextion_select(obj_bp3);
                    programbuttons=P3;
                    break;
                case P3:
                    nextion_unselect(obj_bp3);
                    nextion_select(obj_bp4);
                    programbuttons=P4;
                    break;
                case P4:
                    nextion_unselect(obj_bp4);
                    nextion_select(obj_bp5);
                    programbuttons=P5;
                    break;
                case P5:
                    nextion_unselect(obj_bp5);
                    nextion_select(obj_bp6);
                    programbuttons=P6;
                    break;
                case P6:
                    nextion_unselect(obj_bp6);
                    nextion_select(obj_bp7);
                    programbuttons=P7;
                    break;
                case P7:
                    nextion_unselect(obj_bp7);
                    nextion_select(obj_bp8);
                    programbuttons=P8;
                    break;
                case P8:
                    nextion_unselect(obj_bp8);
                    nextion_select(obj_bp9);
                    programbuttons=P9;
                    break;
                case P9: 
                    nextion_unselect(obj_bp9);
                    nextion_select(obj_bp10);
                    programbuttons=P10;
                    break;
                case P10:
                    nextion_unselect(obj_bp10);
                    nextion_select(obj_bp11);
                    programbuttons=P11;
                    break;
                case P11:
                    stateMachine.c_state = MEMORY_PG2;
                    stateMachine.p_state = MEMORY_PG1;
                    FirstLoop=true;
                    break;
                default:
                    break;
            }
            var_interrupt=NONE;
            break;  
        
        /*
         * ENCODER LEFT EVENT
         * - Moving between the different programs (left turn)
         */    
        case ENC_LEFT:
            switch(programbuttons)
            {
                case P0:
                    break;
                case P1:
                    nextion_unselect(obj_bp1);
                    nextion_select(obj_bp0);
                    programbuttons=P0;
                    break;
                case P2:
                    nextion_unselect(obj_bp2);
                    nextion_select(obj_bp1);
                    programbuttons=P1;
                    break;
                case P3:
                    nextion_unselect(obj_bp3);
                    nextion_select(obj_bp2);
                    programbuttons=P2;
                    break;
                case P4:
                    nextion_unselect(obj_bp4);
                    nextion_select(obj_bp3);
                    programbuttons=P3;
                    break;
                case P5:
                    nextion_unselect(obj_bp5);
                    nextion_select(obj_bp4);
                    programbuttons=P4;
                    break;
                case P6:
                    nextion_unselect(obj_bp6);
                    nextion_select(obj_bp5);
                    programbuttons=P5;
                    break;
                case P7:
                    nextion_unselect(obj_bp7);
                    nextion_select(obj_bp6);
                    programbuttons=P6;
                    break;
                case P8:
                    nextion_unselect(obj_bp8);
                    nextion_select(obj_bp7);
                    programbuttons=P7;
                    break;
                case P9: 
                    nextion_unselect(obj_bp9);
                    nextion_select(obj_bp8);
                    programbuttons=P8;
                    break;
                case P10:
                    nextion_unselect(obj_bp10);
                    nextion_select(obj_bp9);
                    programbuttons=P9;
                    break;
                case P11:
                    nextion_unselect(obj_bp11);
                    nextion_select(obj_bp10);
                    programbuttons=P10;
                    break;
                default:
                    break;
            }
            var_interrupt=NONE;
            break;  
        
        /*
         * NONE
         * - Set nextion page
         * - Highlight Current Object
         */ 
        case NONE:
            if(FirstLoop==true)
            {
                nextion_page(program_page1); //Program page 1
                
                /*
                 Removes highlight from objects
                 */
                nextion_unselect(obj_bp0);
                nextion_unselect(obj_bp1);
                nextion_unselect(obj_bp2);
                nextion_unselect(obj_bp3);
                nextion_unselect(obj_bp4);
                nextion_unselect(obj_bp5);
                nextion_unselect(obj_bp6);
                nextion_unselect(obj_bp7);
                nextion_unselect(obj_bp8);
                nextion_unselect(obj_bp9);
                nextion_unselect(obj_bp10);
                nextion_unselect(obj_bp11);
                nextion_checkbox_off(obj_buttonM2);
                
                /*
                 Highlight page marker object
                 */
                nextion_checkbox_on(obj_buttonM1);
                
                if(stateMachine.p_state == MEMORY_PG2) //if previous page is Page 2
                {
                    programbuttons=P11; //object is the bottom right one
                    nextion_select(obj_bp11); //highlight object
                }else{
                    programbuttons=P0; //object is the top left one
                    nextion_select(obj_bp0); //highlight object
                }
                
                FirstLoop=false; //Disable first loop, setup is done
            }
            
            if(val_update==true) //Update variable from timer
            {
                
                param = WELD_STATE;
                controller_cache_read(param, &weldstate); //reads welding state

                if(weldstate == 1) //if welding state is active
                {
                    stateMachine.c_state = HOME_CHANGE_CURRENT; //Go to home menu
                    stateMachine.p_state = MEMORY_PG1;
                    FirstLoop=true; //active first loop for new state
                    var_interrupt=NONE; //clear event flag
                }
                val_update=false; //reset flag
            }
            var_interrupt=NONE; //clear event flag
            break;     
            
        default:
            var_interrupt=NONE; //clear event flag
            break;
    }
    return stateMachine;
}


sm_t programSM_page2(sm_t stateMachine)
{
    switch(var_interrupt)
    {
        /*
         * HOME EVENT
         * - State machine state is now Home_Change_Current
         */
        case HOME:
            stateMachine.c_state = HOME_CHANGE_CURRENT;
            stateMachine.p_state = MEMORY_PG2;
            FirstLoop=true; //active first loop for new state
            var_interrupt=NONE; //clear event flag
            break;
            
        /*
         * BACK EVENT
         *  - State machine state is the previous program page
         */   
        case BACK:
            stateMachine.c_state = MEMORY_PG1;
            stateMachine.p_state = MEMORY_PG2;
            FirstLoop=true; //active first loop for new state
            var_interrupt=NONE;//clear event flag
            break;
        
        /*
         * ENCODER BUTTON EVENT
         * - Load program
         */
        case ENC_BUTTON:
            switch(programbuttons)
            {
                case P12:
                    controller_outbound_buffer_fill(ReadProgramFromMaster,12); //Ask master for program parameters
                    controller_waitwrite(); //wait for master NACK
                    while(programID_memory != 12) 
                    {
                        if(i2c4_stop_condition()){
                            controller_inbound_buffer_save();
                        }
                    }//only leave this menu when programID is updated
                    break;
                    
                case P13:
                    controller_outbound_buffer_fill(ReadProgramFromMaster,13);//Ask master for program parameters
                    controller_waitwrite();
                    while(programID_memory != 13)
                    {
                        if(i2c4_stop_condition()){
                            controller_inbound_buffer_save();
                        }
                    }//only leave this menu when programID is updated
                    break;
                    
                case P14:
                    controller_outbound_buffer_fill(ReadProgramFromMaster,14);//Ask master for program parameters
                    controller_waitwrite();
                    while(programID_memory != 14)
                    {
                        if(i2c4_stop_condition()){
                            controller_inbound_buffer_save();
                        }
                    }//only leave this menu when programID is updated
                    break;
                    
                case P15:
                    controller_outbound_buffer_fill(ReadProgramFromMaster,15);//Ask master for program parameters
                    controller_waitwrite();
                    while(programID_memory != 15)
                    {
                        if(i2c4_stop_condition()){
                            controller_inbound_buffer_save();
                        }
                    }//only leave this menu when programID is updated
                    break;
                    
                case P16:
                    controller_outbound_buffer_fill(ReadProgramFromMaster,16);//Ask master for program parameters
                    controller_waitwrite();
                    while(programID_memory != 16)
                    {
                        if(i2c4_stop_condition()){
                            controller_inbound_buffer_save();
                        }
                    }//only leave this menu when programID is updated
                    break;
                    
                case P17:
                    controller_outbound_buffer_fill(ReadProgramFromMaster,17);//Ask master for program parameters
                    controller_waitwrite();
                    while(programID_memory != 17)
                    {
                        if(i2c4_stop_condition()){
                            controller_inbound_buffer_save();
                        }
                    }//only leave this menu when programID is updated
                    break;
                    
                case P18:
                    controller_outbound_buffer_fill(ReadProgramFromMaster,18);//Ask master for program parameters
                    controller_waitwrite();
                    while(programID_memory != 18)
                    {
                        if(i2c4_stop_condition()){
                            controller_inbound_buffer_save();
                        }
                    }//only leave this menu when programID is updated
                    break;
                    
                case P19:
                    controller_outbound_buffer_fill(ReadProgramFromMaster,19);//Ask master for program parameters
                    controller_waitwrite();
                    while(programID_memory != 19)
                    {
                        if(i2c4_stop_condition()){
                            controller_inbound_buffer_save();
                        }
                    }//only leave this menu when programID is updated
                    break;
                    
                case P20:
                    controller_outbound_buffer_fill(ReadProgramFromMaster,20);//Ask master for program parameters
                    controller_waitwrite();
                    while(programID_memory != 20)
                    {
                        if(i2c4_stop_condition()){
                            controller_inbound_buffer_save();
                        }
                    }//only leave this menu when programID is updated
                    break;
                default:
                    break;
            }
            
            param = WELD_PROCESS; //memory parameter -> Welding Process
            controller_cache_read(param, &wpr); //Read welding process from memory
            
            switch (wpr)
            {
                    case 0: //Case 0 is TIG HF
                        stateMachine.c_state = MEMORY_TIG ; //Update current state
                        break;
                    case 1: //Case 1 is TIG HF PULSE
                        stateMachine.c_state = MEMORY_TIGPULSE;//Update current state
                        break;
                    case 2: //Case 2 is LIFTIG
                        stateMachine.c_state = MEMORY_TIG;//Update current state
                        break;
                    case 3: //Case 3 is LIFTIG PULSE
                        stateMachine.c_state = MEMORY_TIGPULSE;//Update current state
                        break;
                    case 4: //Case 4 is MMA
                        stateMachine.c_state = MEMORY_MMA;//Update current state
                        break;
                    case 5: //Case 5 is MMA PULSE
                        stateMachine.c_state = MEMORY_MMAPULSE;//Update current state
                        break;
                    default:
                        break;
            }
            stateMachine.p_state = MEMORY_PG2; //update previous state
            FirstLoop=true; //Active first loop for next state
            var_interrupt=NONE; //Clear event flag
            break;
            
        /*
         * ENCODER HOLD EVENT
         * - Do nothing
         */    
        case ENC_HOLD:
            var_interrupt=NONE;
            break;
            
        /*
         * ENCODER RIGHT EVENT
         * - Moving between the different programs (right turn)
         */    
        case ENC_RIGHT:
            switch(programbuttons)
            {
                case P12:
                    nextion_unselect(obj_bp12);
                    nextion_select(obj_bp13);
                    programbuttons=P13;
                    break;
                    
                case P13:
                    nextion_unselect(obj_bp13);
                    nextion_select(obj_bp14);
                    programbuttons=P14;
                    break;  
                    
                case P14:
                    nextion_unselect(obj_bp14);
                    nextion_select(obj_bp15);
                    programbuttons=P15;
                    break;
                    
                case P15:
                    nextion_unselect(obj_bp15);
                    nextion_select(obj_bp16);
                    programbuttons=P16;
                    break;
                    
                case P16:
                    nextion_unselect(obj_bp16);
                    nextion_select(obj_bp17);
                    programbuttons=P17;
                    break;
                    
                case P17:
                    nextion_unselect(obj_bp17);
                    nextion_select(obj_bp18);
                    programbuttons=P18;
                    break;
                    
                case P18:
                    nextion_unselect(obj_bp18);
                    nextion_select(obj_bp19);
                    programbuttons=P19;
                    break;
                    
                case P19:
                    nextion_unselect(obj_bp19);
                    nextion_select(obj_bp20);
                    programbuttons=P20;
                    break;
                    
                case P20:
                    break;
                    
                default:
                    break;
            }
            var_interrupt=NONE;
            break;  
        
        /*
         * ENCODER LEFT EVENT
         * - Moving between the different programs (left turn)
         */    
        case ENC_LEFT:
            switch(programbuttons)
            {
                case P12:
                    stateMachine.c_state = MEMORY_PG1;
                    stateMachine.p_state = MEMORY_PG2;
                    programbuttons=P11;
                    FirstLoop=true;
                    break;
                case P13:
                    nextion_unselect(obj_bp13);
                    nextion_select(obj_bp12);
                    programbuttons=P12;
                    break;  
                case P14:
                    nextion_unselect(obj_bp14);
                    nextion_select(obj_bp13);
                    programbuttons=P13;
                    break;
                case P15:
                    nextion_unselect(obj_bp15);
                    nextion_select(obj_bp14);
                    programbuttons=P14;
                    break;
                case P16:
                    nextion_unselect(obj_bp16);
                    nextion_select(obj_bp15);
                    programbuttons=P15;
                    break;
                case P17:
                    nextion_unselect(obj_bp17);
                    nextion_select(obj_bp16);
                    programbuttons=P16;
                    break;
                case P18:
                    nextion_unselect(obj_bp18);
                    nextion_select(obj_bp17);
                    programbuttons=P17;
                    break;
                case P19:
                    nextion_unselect(obj_bp19);
                    nextion_select(obj_bp18);
                    programbuttons=P18;
                    break;
                case P20:
                    nextion_unselect(obj_bp20);
                    nextion_select(obj_bp19);
                    programbuttons=P19;
                    break;
                default:
                    break;
            }
            var_interrupt=NONE;
            break;  
        
        /*
         * NONE
         * - Set nextion page
         * - Highlight Current Object
         */ 
        case NONE:
            if(FirstLoop==true)
            {
                //Set page on nextion
                nextion_page(program_page2);
                
                
                /*
                 Remove highlight from objects
                 */
                nextion_unselect(obj_bp12);
                nextion_unselect(obj_bp13);
                nextion_unselect(obj_bp14);
                nextion_unselect(obj_bp15);
                nextion_unselect(obj_bp16);
                nextion_unselect(obj_bp17);
                nextion_unselect(obj_bp18);
                nextion_unselect(obj_bp19);
                nextion_unselect(obj_bp20);
                nextion_checkbox_off(obj_buttonM1);
                
                /*
                 * Highlight page marker and object of program 12
                 */
                nextion_checkbox_on(obj_buttonM2);
                nextion_select(obj_bp12);
                
                //Set program to Program 12
                programbuttons=P12;
                
                FirstLoop=false; //disable first loop
            }
            
            if(val_update==true) //Update variable from timer
            {
                
                param = WELD_STATE;
                controller_cache_read(param, &weldstate); //Read welding state

                if(weldstate == 1) //if welding state is active
                {
                    stateMachine.c_state = HOME_CHANGE_CURRENT; //go to home page
                    stateMachine.p_state = MEMORY_PG2;
                    FirstLoop=true; //First Loop for new state
                    var_interrupt=NONE; //Clear event flag
                }
                val_update=false; //Disable update flag
            }
            var_interrupt=NONE;//Clear event flag
            break;     
            
        default:
            var_interrupt=NONE;//Clear event flag
            break;
    }
    return stateMachine;
}





sm_t programSM_memoryTIG(sm_t stateMachine)
{
    switch(var_interrupt)
    {
        /*
         * HOME EVENT
         * - Slave requests master program 0 parameters
         * - Slave waits for master to send the parameters
         * - State machine state is now Home_Change_Current
         */
        case HOME:
            controller_outbound_buffer_fill(ReadProgramFromMaster,0); //read parameters from program 0 on master
            controller_waitwrite(); //wait for master NACK
            while(programID_memory != 0)
            {
                if(i2c4_stop_condition()){
                      controller_inbound_buffer_save();
                }
            }//only leave this menu when programID is updated
            stateMachine.c_state = HOME_CHANGE_CURRENT; //Update current state to Home_Change_Current
            stateMachine.p_state = MEMORY_TIG; //Update previous state to MEMORY_TIG
            FirstLoop=true; //FirsLoop for first time on new state
            var_interrupt=NONE; //Clear event flag
            break;
            
        /*
         * BACK EVENT
         * - Slave requests master program 0 parameters
         * - Slave waits for master to send the parameters
         * - Go to program choose menu 
         */   
        case BACK:
            controller_outbound_buffer_fill(ReadProgramFromMaster,0); //read parameters from program 0 on master
            controller_waitwrite(); //wait for master NACK
            while(programID_memory != 0)
            {
                if(i2c4_stop_condition()){
                      controller_inbound_buffer_save();
                }
            }//only leave this menu when programID is updated
            stateMachine.c_state = stateMachine.p_state;  //Update current state
            stateMachine.p_state = MEMORY_TIG; //Update previous state
            FirstLoop=true; //FirsLoop for first time on new state
            var_interrupt=NONE; //Clear event flag
            break;
        
        /*
         * ENCODER BUTTON EVENT
         * -If pressed save, save program on master and go to home menu
         *- If pressed load, save the copy of parameters on program 0
         *- If pressed reset, reset this program to factory new values
         */
        case ENC_BUTTON:
            switch(programpagebuttons)
            {
                case SAVE:
                    controller_outbound_buffer_fill(ReadProgramFromMaster,0); //Read program parameters on master 
                    controller_waitwrite(); //wait for NACK
                    while(programID_memory != 0) //while program received is not 0
                    {
                        if(i2c4_stop_condition()){ //wait for the stop condition
                              controller_inbound_buffer_save(); //save buffer on memory
                        }
                    }//only leave this menu when programID is updated
                    controller_outbound_buffer_fill(WriteProgramToMaster,programID_memory); //Write program parameters on master 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=programtosave; //Set programID to previous saved just for showcase
                    stateMachine.c_state = HOME_CHANGE_CURRENT; //Update current state to home
                    stateMachine.p_state = MEMORY_TIG; //Update previous state
                    FirstLoop=true;  //FirsLoop for first time on new state
                    break;
                case LOAD:
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=programtosave; //Set programID to previous saved just for showcase
                    stateMachine.c_state = HOME_CHANGE_CURRENT; //Update current state to home
                    stateMachine.p_state = MEMORY_TIG; //Update previous state
                    FirstLoop=true; //FirsLoop for first time on new state
                    break;
                case RESET:
                    factory_reset_parameters(); //reset parameters
                    controller_outbound_buffer_fill(WriteProgramToMaster,programID_memory); //Write new parameters on the program
                    controller_waitwrite();  //wait for NACK
                    stateMachine.c_state = MEMORY_TIG; //Reset this state
                    stateMachine.p_state = MEMORY_TIG; //Reset this state
                    FirstLoop=true; //FirsLoop for first time on new state
                    break;
                default:
                    break;
            }
            var_interrupt=NONE; //Clear event flag
            break;
            
        /*
         * ENCODER HOLD EVENT
         */    
        case ENC_HOLD:
            stateMachine.c_state = HELP;
            stateMachine.p_state = MEMORY_TIG;
            FirstLoop=true;
            nextion_page(program_help);
            var_interrupt=NONE;
            break;
            
        /*
         * ENCODER RIGHT EVENT
         * - Moving between the different buttons
         */    
        case ENC_RIGHT:
            switch(programpagebuttons)
            {
                case SAVE: //Case save button
                    nextion_unselect(obj_ProgramSavebutton); //Remove highlight from save button
                    nextion_select(obj_ProgramLoadbutton); //highlight Load button
                    programpagebuttons=LOAD; //update button to Load
                    break;
                case LOAD: //Case Load button
                    nextion_unselect(obj_ProgramLoadbutton); //Remove highlight from Load button
                    nextion_select(obj_ProgramResetbutton); //highlight Reset button
                    programpagebuttons=RESET; //update button to Reset
                    break;
                case RESET: //Case Reset button -> nothing to the right
                    break;
                default:
                    break;
            }
            var_interrupt=NONE; //clear event flag
            break;  
        
        /*
         * ENCODER LEFT EVENT
         * - Moving between the different  buttons
         */    
        case ENC_LEFT:
            switch(programpagebuttons)
            {
                case SAVE: //Case Save -> do nothing, nothing to the left
                    break;
                case LOAD: //Case load
                    nextion_unselect(obj_ProgramLoadbutton); //remove highlight from Load
                    nextion_select(obj_ProgramSavebutton); //highlight Save
                    programpagebuttons=SAVE;  //update button to Save
                    break;
                case RESET:
                    nextion_unselect(obj_ProgramResetbutton); //remove highlight from Reset
                    nextion_select(obj_ProgramLoadbutton); //highlight Load
                    programpagebuttons=LOAD; //update button to Load
                    break;
                default:
                    break;
            }
            var_interrupt=NONE; //Clear event flag
            break;  
        
        /*
         * NONE
         * - Set nextion page
         * - Highlight Current Object
         */ 
        case NONE:
            if(FirstLoop==true)//FirsLoop for first time on new state
            {
                
                nextion_page(program_TIG); //Set nextion page
                
                programtosave = programID_memory;
                
                nextion_unselect(obj_ProgramSavebutton); //Remove highlight from Save button
                nextion_unselect(obj_ProgramLoadbutton); //Remove highlight from Load button
                nextion_unselect(obj_ProgramResetbutton); //Remove highlight from Reset button
                nextion_select(obj_ProgramSavebutton); //highlight Save button
                programpagebuttons=SAVE; //update buttons to Save
                
                param=TIG_PRE_GAS; //memory parameter -> TIG PRE GAS
                controller_cache_read(param,&Program_TIGPreGas); //read Pre Gas value from memory
                utils_int_to_str(NextionTIG_PreGas,Program_TIGPreGas,3); //Convert to string
                nextion_change_val(obj_ProgramPreGas,NextionTIG_PreGas); //Send value to Nextion
                
                param=TIG_ISTART; //memory parameter -> TIG ISTART
                controller_cache_read(param,&Program_TIGIStart); //read ISTART value from memory
                utils_int_to_str(NextionTIG_IStart,Program_TIGIStart,3); //Convert to string
                nextion_change_val(obj_ProgramStartCurrent,NextionTIG_IStart); //Send value to Nextion
                
                param=TIG_UPSLOPE; //memory parameter -> TIG UpSlope
                controller_cache_read(param,&Program_TIGUpSlope); //read Upslope value from memory
                utils_int_to_str(NextionTIG_UpSlope,Program_TIGUpSlope,3); //Convert to string
                nextion_change_val(obj_ProgramUpSlope,NextionTIG_UpSlope); //Send value to Nextion
               
                param=TIG_INOMINAL; //memory parameter -> TIG INominal
                controller_cache_read(param,&Program_TIGINominal); //read INominalvalue from memory
                utils_int_to_str(NextionTIG_INominal,Program_TIGINominal,3); //Convert to string
                nextion_change_val(obj_ProgramPeakCurrent,NextionTIG_INominal); //Send value to Nextion
                
                param=TIG_DOWNSLOPE_; //memory parameter -> TIG Downslope
                controller_cache_read(param,&Program_TIGDownSlope); //read Downslope value from memory
                utils_int_to_str(NextionTIG_DownSlope,Program_TIGDownSlope,3); //Convert to string
                nextion_change_val(obj_ProgramDownSlope,NextionTIG_DownSlope); //Send value to Nextion
                
                param=TIG_IEND; //memory parameter -> TIG IEND
                controller_cache_read(param,&Program_TIGIEnd); //readIEND value from memory
                utils_int_to_str(NextionTIG_IEnd,Program_TIGIEnd,3); //Convert to string
                nextion_change_val(obj_ProgramFinalCurrent,NextionTIG_IEnd); //Send value to Nextion
                
                param=TIG_POSTGAS; //memory parameter -> TIG Post Gas
                controller_cache_read(param,&Program_TIGPostGas); //read Post Gas value from memory
                utils_int_to_str(NextionTIG_PostGas,Program_TIGPostGas,3); //Convert to string
                nextion_change_val(obj_ProgramPostGas,NextionTIG_PostGas); //Send value to Nextion
                
                param=TIG_DYNAMICS; //memory parameter -> TIG Dynamics
                controller_cache_read(param,&Program_TIGDynamics); //read Dynamics value from memory
                utils_int_to_str(NextionTIG_Dynamics,Program_TIGDynamics,3); //Convert to string
                nextion_change_val(obj_ProgramDynamics,NextionTIG_Dynamics); //Send value to Nextion
                
                param=TIG_SPOTTIME; //memory parameter -> TIG Spot Time
                controller_cache_read(param,&Program_TIGSpotTime); //read Spot Time value from memory
                utils_int_to_str(NextionTIG_SpotTime,Program_TIGSpotTime,3); //Convert to string
                nextion_change_val(obj_ProgramSpotTime,NextionTIG_SpotTime); //Send value to Nextion
                
                
                param=WELD_PROCESS; //memory parameter -> Welding Process
                controller_cache_read(param,&Program_WeldProc); //read Welding process value from memory
                switch(Program_WeldProc)
                {
                    case 0:
                        nextion_change_txt(obj_ProgramProcess,"TIG HF"); //TIG HF process
                        break;
                    case 1:
                        break;
                    case 2:
                        nextion_change_txt(obj_ProgramProcess,"LIFTIG"); //LIFTIG process
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    default:
                        break;
                }
                
                
                param=OP_MODE; //memory parameter -> Operation mode Process
                controller_cache_read(param,&Program_OpMode); //read operation mode value from memory
                switch(Program_OpMode)
                {
                    case 0:
                        break;
                    case 1:
                        nextion_change_txt(obj_ProgramModes,"2T"); //2T
                        break;
                    case 2:
                        nextion_change_txt(obj_ProgramModes,"4T"); //4T
                        break;
                    case 3:
                        nextion_change_txt(obj_ProgramModes,"SPOT"); //SPOT
                        break;
                    default:
                        break;
                }
                
                
                utils_int_to_str(Nextion_PrgNumber,programID_memory,2); //Convert to string
                nextion_change_txt(obj_ProgramName,Nextion_PrgNumber); //Send to program name object
                
                FirstLoop=false; //Disable first loop
            }
            if(val_update==true) //Val_update variable from timer for value updates 
            {
                
                param = WELD_STATE;  //memory parameter -> Welding state
                controller_cache_read(param, &weldstate); //read Welding state value from memory

                if(weldstate == 1) //if welding is active
                {
                    controller_outbound_buffer_fill(ReadProgramFromMaster,0); //read parameters from program 0 on master
                    controller_waitwrite(); //wait for master NACK
                    programID_memory=50;
                    while(programID_memory != 0)
                    {
                        if(i2c4_stop_condition()){
                              controller_inbound_buffer_save();
                        }
                    }//only leave this menu when programID is updated
                    stateMachine.c_state = HOME_CHANGE_CURRENT; //go to home page
                    stateMachine.p_state = MEMORY_TIG;
                    FirstLoop=true; //FirsLoop for first time on new state
                    var_interrupt=NONE; //Clear event flag
                }
                val_update=false; //disable update flag
            }
            var_interrupt=NONE; //Clear event flag
            break;     
            
        default:
            var_interrupt=NONE; //Clear event flag
            break;
    }
    return stateMachine;
}



sm_t programSM_memoryTIG0(sm_t stateMachine)
{
    switch(var_interrupt)
    {
        /*
         * HOME EVENT
         * - State machine state is now Home_Change_Current
         */
        case HOME:
            stateMachine.c_state = HOME_CHANGE_CURRENT; //Update current state to Home_Change_Current
            stateMachine.p_state = MEMORY_TIG0; //Update previous state to MEMORY_TIG
            FirstLoop=true; //FirsLoop for first time on new state
            var_interrupt=NONE; //Clear event flag
            break;
            
        /*
         * BACK EVENT
         * - Previous menu is now the current state, as there is no need to load
         * program 0
         */   
        case BACK:
            stateMachine.c_state = stateMachine.p_state;  //Update current state
            stateMachine.p_state = MEMORY_TIG0; //Update previous state
            FirstLoop=true; //FirsLoop for first time on new state
            var_interrupt=NONE; //Clear event flag
            break;
        
        /*
         * ENCODER BUTTON EVENT
         * -If pressed save, save program on master and go to home menu
         *- If pressed load, save the copy of parameters on program 0
         *- If pressed reset, reset this program to factory new values
         */
        case ENC_BUTTON:
            switch(programpagebuttons)
            {
                case LOAD:
                    programID_memory=0; //Set programID to previous saved just for showcase
                    stateMachine.c_state = HOME_CHANGE_CURRENT; //Update current state to home
                    stateMachine.p_state = MEMORY_TIG0; //Update previous state
                    FirstLoop=true; //FirsLoop for first time on new state
                    break;
                case RESET:
                    factory_reset_parameters(); //reset parameters
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write new parameters on the program
                    controller_waitwrite();  //wait for NACK
                    stateMachine.c_state = MEMORY_TIG0; //Reset this state
                    stateMachine.p_state = MEMORY_TIG0; //Reset this state
                    FirstLoop=true; //FirsLoop for first time on new state
                    break;
                default:
                    break;
            }
            var_interrupt=NONE; //Clear event flag
            break;
            
        /*
         * ENCODER HOLD EVENT
         * - Open help menu (program 0 help menu)
         */    
        case ENC_HOLD:
            nextion_page(program_help0);
            stateMachine.c_state = HELP;
            stateMachine.p_state = MEMORY_TIG0;
            FirstLoop=true;
            var_interrupt=NONE;
            break;
            
        /*
         * ENCODER RIGHT EVENT
         * - Moving between the different buttons
         */    
        case ENC_RIGHT:
            switch(programpagebuttons)
            {
                case LOAD: //Case Load button
                    nextion_unselect(obj_ProgramLoadbutton); //Remove highlight from Load button
                    nextion_select(obj_ProgramResetbutton); //highlight Reset button
                    programpagebuttons=RESET; //update button to Reset
                    break;
                case RESET: //Case Reset button -> nothing to the right
                    break;
                default:
                    break;
            }
            var_interrupt=NONE; //clear event flag
            break;  
        
        /*
         * ENCODER LEFT EVENT
         * - Moving between the different  buttons
         */    
        case ENC_LEFT:
            switch(programpagebuttons)
            {
                case LOAD: //Case load
                    break;
                case RESET:
                    nextion_unselect(obj_ProgramResetbutton); //remove highlight from Reset
                    nextion_select(obj_ProgramLoadbutton); //highlight Load
                    programpagebuttons=LOAD; //update button to Load
                    break;
                default:
                    break;
            }
            var_interrupt=NONE; //Clear event flag
            break;  
        
        /*
         * NONE
         * - Set nextion page
         * - Highlight Current Object
         */ 
        case NONE:
            if(FirstLoop==true)//FirsLoop for first time on new state
            {
                nextion_page(program_TIG0); //Set nextion page

                nextion_unselect(obj_ProgramLoadbutton); //Remove highlight from Load button
                nextion_unselect(obj_ProgramResetbutton); //Remove highlight from Reset button
                nextion_select(obj_ProgramLoadbutton); //highlight Save button
                
                programpagebuttons=LOAD; //update buttons to Save
                
                param=TIG_PRE_GAS; //memory parameter -> TIG PRE GAS
                controller_cache_read(param,&Program_TIGPreGas); //read Pre Gas value from memory
                utils_int_to_str(NextionTIG_PreGas,Program_TIGPreGas,3); //Convert to string
                nextion_change_val(obj_ProgramPreGas,NextionTIG_PreGas); //Send value to Nextion
                
                param=TIG_ISTART; //memory parameter -> TIG ISTART
                controller_cache_read(param,&Program_TIGIStart); //read ISTART value from memory
                utils_int_to_str(NextionTIG_IStart,Program_TIGIStart,3); //Convert to string
                nextion_change_val(obj_ProgramStartCurrent,NextionTIG_IStart); //Send value to Nextion
                
                param=TIG_UPSLOPE; //memory parameter -> TIG UpSlope
                controller_cache_read(param,&Program_TIGUpSlope); //read Upslope value from memory
                utils_int_to_str(NextionTIG_UpSlope,Program_TIGUpSlope,3); //Convert to string
                nextion_change_val(obj_ProgramUpSlope,NextionTIG_UpSlope); //Send value to Nextion
               
                param=TIG_INOMINAL; //memory parameter -> TIG INominal
                controller_cache_read(param,&Program_TIGINominal); //read INominalvalue from memory
                utils_int_to_str(NextionTIG_INominal,Program_TIGINominal,3); //Convert to string
                nextion_change_val(obj_ProgramPeakCurrent,NextionTIG_INominal); //Send value to Nextion
                
                param=TIG_DOWNSLOPE_; //memory parameter -> TIG Downslope
                controller_cache_read(param,&Program_TIGDownSlope); //read Downslope value from memory
                utils_int_to_str(NextionTIG_DownSlope,Program_TIGDownSlope,3); //Convert to string
                nextion_change_val(obj_ProgramDownSlope,NextionTIG_DownSlope); //Send value to Nextion
                
                param=TIG_IEND; //memory parameter -> TIG IEND
                controller_cache_read(param,&Program_TIGIEnd); //readIEND value from memory
                utils_int_to_str(NextionTIG_IEnd,Program_TIGIEnd,3); //Convert to string
                nextion_change_val(obj_ProgramFinalCurrent,NextionTIG_IEnd); //Send value to Nextion
                
                param=TIG_POSTGAS; //memory parameter -> TIG Post Gas
                controller_cache_read(param,&Program_TIGPostGas); //read Post Gas value from memory
                utils_int_to_str(NextionTIG_PostGas,Program_TIGPostGas,3); //Convert to string
                nextion_change_val(obj_ProgramPostGas,NextionTIG_PostGas); //Send value to Nextion
                
                param=TIG_DYNAMICS; //memory parameter -> TIG Dynamics
                controller_cache_read(param,&Program_TIGDynamics); //read Dynamics value from memory
                utils_int_to_str(NextionTIG_Dynamics,Program_TIGDynamics,3); //Convert to string
                nextion_change_val(obj_ProgramDynamics,NextionTIG_Dynamics); //Send value to Nextion
                
                param=TIG_SPOTTIME; //memory parameter -> TIG Spot Time
                controller_cache_read(param,&Program_TIGSpotTime); //read Spot Time value from memory
                utils_int_to_str(NextionTIG_SpotTime,Program_TIGSpotTime,3); //Convert to string
                nextion_change_val(obj_ProgramSpotTime,NextionTIG_SpotTime); //Send value to Nextion
                
                
                param=WELD_PROCESS; //memory parameter -> Welding Process
                controller_cache_read(param,&Program_WeldProc); //read Welding process value from memory
                switch(Program_WeldProc)
                {
                    case 0:
                        nextion_change_txt(obj_ProgramProcess,"TIG HF"); //TIG HF process
                        break;
                    case 1:
                        break;
                    case 2:
                        nextion_change_txt(obj_ProgramProcess,"LIFTIG"); //LIFTIG process
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    default:
                        break;
                }
                
                
                param=OP_MODE; //memory parameter -> Operation mode Process
                controller_cache_read(param,&Program_OpMode); //read operation mode value from memory
                switch(Program_OpMode)
                {
                    case 0:
                        break;
                    case 1:
                        nextion_change_txt(obj_ProgramModes,"2T"); //2T
                        break;
                    case 2:
                        nextion_change_txt(obj_ProgramModes,"4T"); //4T
                        break;
                    case 3:
                        nextion_change_txt(obj_ProgramModes,"SPOT"); //SPOT
                        break;
                    default:
                        break;
                }
                
                FirstLoop=false; //Disable first loop
            }
            if(val_update==true) //Val_update variable from timer for value updates 
            {
                
                param = WELD_STATE;  //memory parameter -> Welding state
                controller_cache_read(param, &weldstate); //read Welding state value from memory

                if(weldstate == 1) //if welding is active
                {
                    stateMachine.c_state = HOME_CHANGE_CURRENT; //go to home page
                    stateMachine.p_state = MEMORY_TIG0;
                    FirstLoop=true; //FirsLoop for first time on new state
                    var_interrupt=NONE; //Clear event flag
                }
                val_update=false; //disable update flag
            }
            var_interrupt=NONE; //Clear event flag
            break;     
            
        default:
            var_interrupt=NONE; //Clear event flag
            break;
    }
    return stateMachine;
}


sm_t programSM_memoryTIG0Pulse(sm_t stateMachine)
{
    switch(var_interrupt)
    {
        /*
         * HOME EVENT
         * - Slave requests master program 0 parameters
         * - Slave waits for master to send the parameters
         * - State machine state is now Home_Change_Current
         */
        case HOME:
            stateMachine.c_state = HOME_CHANGE_CURRENT; //Update current state to Home_Change_Current
            stateMachine.p_state = MEMORY_TIGPULSE0; //Update previous state to MEMORY_TIG
            FirstLoop=true; //FirsLoop for first time on new state
            var_interrupt=NONE; //Clear event flag
            break;
            
        /*
         * BACK EVENT
         * - Slave requests master program 0 parameters
         * - Slave waits for master to send the parameters
         * - State machine state is now Home_Change_Current
         */
        case BACK:
            stateMachine.c_state = stateMachine.p_state;  //Update current state
            stateMachine.p_state = MEMORY_TIGPULSE0; //Update previous state
            FirstLoop=true; //FirsLoop for first time on new state
            var_interrupt=NONE; //Clear event flag
            break;
        
        /*
         * ENCODER BUTTON EVENT
         * -If pressed save, save program on master and go to home menu
         *- If pressed load, save the copy of parameters on program 0
         *- If pressed reset, reset this program to factory new values
         */
        case ENC_BUTTON:
            switch(programpagebuttons)
            {
                case LOAD:
                    programID_memory=0; //Set programID to previous saved just for showcase
                    stateMachine.c_state = HOME_CHANGE_CURRENT; //Update current state to home
                    stateMachine.p_state = MEMORY_TIGPULSE0; //Update previous state
                    FirstLoop=true; //FirsLoop for first time on new state
                    break;
                case RESET:
                    factory_reset_parameters(); //reset parameters
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write new parameters on the program
                    controller_waitwrite();  //wait for NACK
                    stateMachine.c_state = MEMORY_TIG0; //Reset this state
                    stateMachine.p_state = MEMORY_TIGPULSE0; //Reset this state
                    FirstLoop=true; //FirsLoop for first time on new state
                    break;
                default:
                    break;
            }
            var_interrupt=NONE; //Clear event flag
            break;
            
        /*
         * ENCODER HOLD EVENT
         * - Open help page of program 0
         */    
        case ENC_HOLD:
            nextion_page(program_help0);
            stateMachine.c_state = HELP;
            stateMachine.p_state = MEMORY_TIGPULSE0;
            FirstLoop=true;
            var_interrupt=NONE;
            break;
            
        /*
         * ENCODER RIGHT EVENT
         * - Moving between the different buttons
         */    
        case ENC_RIGHT:
            switch(programpagebuttons)
            {
                case LOAD: //Case Load button
                    nextion_unselect(obj_ProgramLoadbutton); //Remove highlight from Load button
                    nextion_select(obj_ProgramResetbutton); //highlight Reset button
                    programpagebuttons=RESET; //update button to Reset
                    break;
                case RESET: //Case Reset button -> nothing to the right
                    break;
                default:
                    break;
            }
            var_interrupt=NONE; //clear event flag
            break;  
        
        /*
         * ENCODER LEFT EVENT
         * - Moving between the different  buttons
         */    
        case ENC_LEFT:
            switch(programpagebuttons)
            {
                case LOAD: //Case load
                    break;
                case RESET:
                    nextion_unselect(obj_ProgramResetbutton); //remove highlight from Reset
                    nextion_select(obj_ProgramLoadbutton); //highlight Load
                    programpagebuttons=LOAD; //update button to Load
                    break;
                default:
                    break;
            }
            var_interrupt=NONE; //Clear event flag
            break;  
        
        /*
         * NONE
         * - Set nextion page
         * - Highlight Current Object
         */ 
        case NONE:
            if(FirstLoop==true)//FirsLoop for first time on new state
            {
                nextion_page(program_TIGPULSE0); //Set nextion page

                nextion_unselect(obj_ProgramLoadbutton); //Remove highlight from Load button
                nextion_unselect(obj_ProgramResetbutton); //Remove highlight from Reset button
                nextion_select(obj_ProgramLoadbutton); //highlight Save button
                programpagebuttons=LOAD; //update buttons to Save
                
                param=TIG_PRE_GAS; //memory parameter -> TIG PRE GAS
                controller_cache_read(param,&Program_TIGPreGas); //read Pre Gas value from memory
                utils_int_to_str(NextionTIG_PreGas,Program_TIGPreGas,3); //Convert to string
                nextion_change_val(obj_ProgramPreGas,NextionTIG_PreGas); //Send value to Nextion
                
                param=TIG_ISTART; //memory parameter -> TIG ISTART
                controller_cache_read(param,&Program_TIGIStart); //read ISTART value from memory
                utils_int_to_str(NextionTIG_IStart,Program_TIGIStart,3); //Convert to string
                nextion_change_val(obj_ProgramStartCurrent,NextionTIG_IStart); //Send value to Nextion
                
                param=TIG_UPSLOPE; //memory parameter -> TIG UpSlope
                controller_cache_read(param,&Program_TIGUpSlope); //read Upslope value from memory
                utils_int_to_str(NextionTIG_UpSlope,Program_TIGUpSlope,3); //Convert to string
                nextion_change_val(obj_ProgramUpSlope,NextionTIG_UpSlope); //Send value to Nextion
               
                param=TIG_INOMINAL; //memory parameter -> TIG INominal
                controller_cache_read(param,&Program_TIGINominal); //read INominalvalue from memory
                utils_int_to_str(NextionTIG_INominal,Program_TIGINominal,3); //Convert to string
                nextion_change_val(obj_ProgramPeakCurrent,NextionTIG_INominal); //Send value to Nextion
                
                param=TIG_IBASE_; //memory parameter -> TIG IBase
                controller_cache_read(param,&Program_TIGIBase); //read IBase from memory
                utils_int_to_str(NextionTIG_IBase,Program_TIGIBase,3); //Convert to string
                nextion_change_val(obj_ProgramBaseCurrent,NextionTIG_IBase); //Send value to Nextion
                
                param=TIG_PULSE_WITH_; //memory parameter -> TIG IBase
                controller_cache_read(param,&Program_TIGPulseWidth); //read IBase from memory
                utils_int_to_str(NextionTIG_PulseWidth,Program_TIGPulseWidth,3); //Convert to string
                nextion_change_val(obj_ProgramWidthPeak,NextionTIG_PulseWidth); //Send value to Nextion
                
                param=TIG_PULSE_FREQUENCY; //memory parameter -> TIG Pulse Frequency
                controller_cache_read(param,&Program_TIGPulseFreq); //read Pyulse Freq from memory
                utils_int_to_str(NextionTIG_PulseFreq,Program_TIGPulseFreq,3); //Convert to string
                nextion_change_val(obj_ProgramFreqPulse,NextionTIG_PulseFreq); //Send value to Nextion
                
                param=TIG_DOWNSLOPE_; //memory parameter -> TIG Downslope
                controller_cache_read(param,&Program_TIGDownSlope); //read Downslope value from memory
                utils_int_to_str(NextionTIG_DownSlope,Program_TIGDownSlope,3); //Convert to string
                nextion_change_val(obj_ProgramDownSlope,NextionTIG_DownSlope); //Send value to Nextion
                
                param=TIG_IEND; //memory parameter -> TIG IEND
                controller_cache_read(param,&Program_TIGIEnd); //readIEND value from memory
                utils_int_to_str(NextionTIG_IEnd,Program_TIGIEnd,3); //Convert to string
                nextion_change_val(obj_ProgramFinalCurrent,NextionTIG_IEnd); //Send value to Nextion
                
                param=TIG_POSTGAS; //memory parameter -> TIG Post Gas
                controller_cache_read(param,&Program_TIGPostGas); //read Post Gas value from memory
                utils_int_to_str(NextionTIG_PostGas,Program_TIGPostGas,3); //Convert to string
                nextion_change_val(obj_ProgramPostGas,NextionTIG_PostGas); //Send value to Nextion
                
                param=TIG_DYNAMICS; //memory parameter -> TIG Dynamics
                controller_cache_read(param,&Program_TIGDynamics); //read Dynamics value from memory
                utils_int_to_str(NextionTIG_Dynamics,Program_TIGDynamics,3); //Convert to string
                nextion_change_val(obj_ProgramDynamics,NextionTIG_Dynamics); //Send value to Nextion
                
                param=TIG_SPOTTIME; //memory parameter -> TIG Spot Time
                controller_cache_read(param,&Program_TIGSpotTime); //read Spot Time value from memory
                utils_int_to_str(NextionTIG_SpotTime,Program_TIGSpotTime,3); //Convert to string
                nextion_change_val(obj_ProgramSpotTime,NextionTIG_SpotTime); //Send value to Nextion
                
                
                param=WELD_PROCESS; //memory parameter -> Welding Process
                controller_cache_read(param,&Program_WeldProc); //read Welding process value from memory
                switch(Program_WeldProc)
                {
                    case 0:
                        break;
                    case 1:
                        nextion_change_txt(obj_ProgramProcess,"TIG HF PULSE"); //TIG HF process
                        break;
                    case 2:                      
                        break;
                    case 3:
                        nextion_change_txt(obj_ProgramProcess,"LIFTIG PULSE"); //LIFTIG process
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    default:
                        break;
                }
                
                
                param=OP_MODE; //memory parameter -> Operation mode Process
                controller_cache_read(param,&Program_OpMode); //read operation mode value from memory
                switch(Program_OpMode)
                {
                    case 0:
                        break;
                    case 1:
                        nextion_change_txt(obj_ProgramModes,"2T"); //2T
                        break;
                    case 2:
                        nextion_change_txt(obj_ProgramModes,"4T"); //4T
                        break;
                    case 3:
                        nextion_change_txt(obj_ProgramModes,"SPOT"); //SPOT
                        break;
                    default:
                        break;
                }
                
                FirstLoop=false; //Disable first loop
            }
            if(val_update==true) //Val_update variable from timer for value updates 
            {
                
                param = WELD_STATE;  //memory parameter -> Welding state
                controller_cache_read(param, &weldstate); //read Welding state value from memory

                if(weldstate == 1) //if welding is active
                {
                    stateMachine.c_state = HOME_CHANGE_CURRENT; //go to home page
                    stateMachine.p_state = MEMORY_TIGPULSE0;
                    FirstLoop=true; //FirsLoop for first time on new state
                    var_interrupt=NONE; //Clear event flag
                }
                val_update=false; //disable update flag
            }
            var_interrupt=NONE; //Clear event flag
            break;     
            
        default:
            var_interrupt=NONE; //Clear event flag
            break;
    }
    return stateMachine;
}

sm_t programSM_memoryTIGPulse(sm_t stateMachine)
{
    switch(var_interrupt)
    {
        /*
         * HOME EVENT
         * - State machine state is now Home_Change_Current
         */
        case HOME:
            controller_outbound_buffer_fill(ReadProgramFromMaster,0); //read parameters from program 0 on master
            controller_waitwrite(); //wait for master NACK
            while(programID_memory != 0)
            {
                if(i2c4_stop_condition()){
                      controller_inbound_buffer_save();
                }
            }//only leave this menu when programID is updated
            stateMachine.c_state = HOME_CHANGE_CURRENT; //Update current state to Home_Change_Current
            stateMachine.p_state = MEMORY_TIGPULSE; //Update previous state to MEMORY_TIG
            FirstLoop=true; //FirsLoop for first time on new state
            var_interrupt=NONE; //Clear event flag
            break;
            
        /*
         * BACK EVENT
         * - Read previous parameters
         * - Go to program choose menu
         */   
        case BACK:
            controller_outbound_buffer_fill(ReadProgramFromMaster,0); //read parameters from program 0 on master
            controller_waitwrite(); //wait for master NACK
            while(programID_memory != 0)
            {
                if(i2c4_stop_condition()){
                      controller_inbound_buffer_save();
                }
            }//only leave this menu when programID is updated
            stateMachine.c_state = stateMachine.p_state;  //Update current state
            stateMachine.p_state = MEMORY_TIGPULSE; //Update previous state
            FirstLoop=true; //FirsLoop for first time on new state
            var_interrupt=NONE; //Clear event flag
            break;
        case ENC_BUTTON:
            
            switch(programpagebuttons)
            {
                case SAVE:
                    controller_outbound_buffer_fill(ReadProgramFromMaster,0); //Read program parameters on master 
                    controller_waitwrite(); //wait for NACK
                    while(programID_memory != 0) //while program received is not 0
                    {
                        if(i2c4_stop_condition()){ //wait for the stop condition
                              controller_inbound_buffer_save(); //save buffer on memory
                        }
                    }//only leave this menu when programID is updated
                    controller_outbound_buffer_fill(WriteProgramToMaster,programID_memory); //Write program parameters on master 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=programtosave;
                    stateMachine.c_state = HOME_CHANGE_CURRENT; //Update current state to home
                    stateMachine.p_state = MEMORY_TIGPULSE; //Update previous state
                    FirstLoop=true;  //FirsLoop for first time on new state
                    break;
                case LOAD:
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=programtosave;
                    stateMachine.c_state = HOME_CHANGE_CURRENT; //Update current state to home
                    stateMachine.p_state = MEMORY_TIGPULSE; //Update previous state
                    FirstLoop=true; //FirsLoop for first time on new state
                    break;
                case RESET:
                    factory_reset_parameters(); //reset parameters
                    controller_outbound_buffer_fill(WriteProgramToMaster,programID_memory); //Write new parameters on the program
                    controller_waitwrite();  //wait for NACK
                    stateMachine.c_state = MEMORY_TIG; //Reset this state
                    stateMachine.p_state = MEMORY_TIGPULSE; //Reset this state
                    FirstLoop=true; //FirsLoop for first time on new state
                    break;
                default:
                    break;
            }
            var_interrupt=NONE; //Clear event flag
            break;
            
        /*
         * ENCODER HOLD EVENT
         */    
        case ENC_HOLD:
            stateMachine.c_state = HELP;
            stateMachine.p_state = MEMORY_TIGPULSE;
            FirstLoop=true;
            nextion_page(program_help);
            var_interrupt=NONE;
            break;
            
        /*
         * ENCODER RIGHT EVENT
         * - Moving between the different buttons
         */    
        case ENC_RIGHT:
            switch(programpagebuttons)
            {
                case SAVE: //Case save button
                    nextion_unselect(obj_ProgramSavebutton); //Remove highlight from save button
                    nextion_select(obj_ProgramLoadbutton); //highlight Load button
                    programpagebuttons=LOAD; //update button to Load
                    break;
                case LOAD: //Case Load button
                    nextion_unselect(obj_ProgramLoadbutton); //Remove highlight from Load button
                    nextion_select(obj_ProgramResetbutton); //highlight Reset button
                    programpagebuttons=RESET; //update button to Reset
                    break;
                case RESET: //Case Reset button -> nothing to the right
                    break;
                default:
                    break;
            }
            var_interrupt=NONE; //clear event flag
            break;  
        
        /*
         * ENCODER LEFT EVENT
         * - Moving between the different  buttons
         */    
        case ENC_LEFT:
            switch(programpagebuttons)
            {
                case SAVE: //Case Save -> do nothing, nothing to the left
                    break;
                case LOAD: //Case load
                    nextion_unselect(obj_ProgramLoadbutton); //remove highlight from Load
                    nextion_select(obj_ProgramSavebutton); //highlight Save
                    programpagebuttons=SAVE;  //update button to Save
                    break;
                case RESET:
                    nextion_unselect(obj_ProgramResetbutton); //remove highlight from Reset
                    nextion_select(obj_ProgramLoadbutton); //highlight Load
                    programpagebuttons=LOAD; //update button to Load
                    break;
                default:
                    break;
            }
            var_interrupt=NONE; //Clear event flag
            break;  
        
        /*
         * NONE
         * - Set nextion page
         * - Highlight Current Object
         */ 
        case NONE:
            if(FirstLoop==true)//FirsLoop for first time on new state
            {
                nextion_page(program_TIGPULSE); //Set nextion page
                
                programtosave = programID_memory;
                
                nextion_unselect(obj_ProgramSavebutton); //Remove highlight from Load button
                nextion_unselect(obj_ProgramLoadbutton); //Remove highlight from Load button
                nextion_unselect(obj_ProgramResetbutton); //Remove highlight from Reset button
                nextion_select(obj_ProgramSavebutton); //highlight Save button
                programpagebuttons=SAVE; //update buttons to Save
                
                param=TIG_PRE_GAS; //memory parameter -> TIG PRE GAS
                controller_cache_read(param,&Program_TIGPreGas); //read Pre Gas value from memory
                utils_int_to_str(NextionTIG_PreGas,Program_TIGPreGas,3); //Convert to string
                nextion_change_val(obj_ProgramPreGas,NextionTIG_PreGas); //Send value to Nextion
                
                param=TIG_ISTART; //memory parameter -> TIG ISTART
                controller_cache_read(param,&Program_TIGIStart); //read ISTART value from memory
                utils_int_to_str(NextionTIG_IStart,Program_TIGIStart,3); //Convert to string
                nextion_change_val(obj_ProgramStartCurrent,NextionTIG_IStart); //Send value to Nextion
                
                param=TIG_UPSLOPE; //memory parameter -> TIG UpSlope
                controller_cache_read(param,&Program_TIGUpSlope); //read Upslope value from memory
                utils_int_to_str(NextionTIG_UpSlope,Program_TIGUpSlope,3); //Convert to string
                nextion_change_val(obj_ProgramUpSlope,NextionTIG_UpSlope); //Send value to Nextion
               
                param=TIG_INOMINAL; //memory parameter -> TIG INominal
                controller_cache_read(param,&Program_TIGINominal); //read INominalvalue from memory
                utils_int_to_str(NextionTIG_INominal,Program_TIGINominal,3); //Convert to string
                nextion_change_val(obj_ProgramPeakCurrent,NextionTIG_INominal); //Send value to Nextion
                
                param=TIG_IBASE_; //memory parameter -> TIG IBase
                controller_cache_read(param,&Program_TIGIBase); //read IBase from memory
                utils_int_to_str(NextionTIG_IBase,Program_TIGIBase,3); //Convert to string
                nextion_change_val(obj_ProgramBaseCurrent,NextionTIG_IBase); //Send value to Nextion
                
                param=TIG_PULSE_WITH_; //memory parameter -> TIG IBase
                controller_cache_read(param,&Program_TIGPulseWidth); //read IBase from memory
                utils_int_to_str(NextionTIG_PulseWidth,Program_TIGPulseWidth,3); //Convert to string
                nextion_change_val(obj_ProgramWidthPeak,NextionTIG_PulseWidth); //Send value to Nextion
                
                param=TIG_PULSE_FREQUENCY; //memory parameter -> TIG Pulse Frequency
                controller_cache_read(param,&Program_TIGPulseFreq); //read Pyulse Freq from memory
                utils_int_to_str(NextionTIG_PulseFreq,Program_TIGPulseFreq,3); //Convert to string
                nextion_change_val(obj_ProgramFreqPulse,NextionTIG_PulseFreq); //Send value to Nextion
                
                param=TIG_DOWNSLOPE_; //memory parameter -> TIG Downslope
                controller_cache_read(param,&Program_TIGDownSlope); //read Downslope value from memory
                utils_int_to_str(NextionTIG_DownSlope,Program_TIGDownSlope,3); //Convert to string
                nextion_change_val(obj_ProgramDownSlope,NextionTIG_DownSlope); //Send value to Nextion
                
                param=TIG_IEND; //memory parameter -> TIG IEND
                controller_cache_read(param,&Program_TIGIEnd); //readIEND value from memory
                utils_int_to_str(NextionTIG_IEnd,Program_TIGIEnd,3); //Convert to string
                nextion_change_val(obj_ProgramFinalCurrent,NextionTIG_IEnd); //Send value to Nextion
                
                param=TIG_POSTGAS; //memory parameter -> TIG Post Gas
                controller_cache_read(param,&Program_TIGPostGas); //read Post Gas value from memory
                utils_int_to_str(NextionTIG_PostGas,Program_TIGPostGas,3); //Convert to string
                nextion_change_val(obj_ProgramPostGas,NextionTIG_PostGas); //Send value to Nextion
                
                param=TIG_DYNAMICS; //memory parameter -> TIG Dynamics
                controller_cache_read(param,&Program_TIGDynamics); //read Dynamics value from memory
                utils_int_to_str(NextionTIG_Dynamics,Program_TIGDynamics,3); //Convert to string
                nextion_change_val(obj_ProgramDynamics,NextionTIG_Dynamics); //Send value to Nextion
                
                param=TIG_SPOTTIME; //memory parameter -> TIG Spot Time
                controller_cache_read(param,&Program_TIGSpotTime); //read Spot Time value from memory
                utils_int_to_str(NextionTIG_SpotTime,Program_TIGSpotTime,3); //Convert to string
                nextion_change_val(obj_ProgramSpotTime,NextionTIG_SpotTime); //Send value to Nextion
                
                
                param=WELD_PROCESS; //memory parameter -> Welding Process
                controller_cache_read(param,&Program_WeldProc); //read Welding process value from memory
                switch(Program_WeldProc)
                {
                    case 0:
                        break;
                    case 1:
                        nextion_change_txt(obj_ProgramProcess,"TIG HF PULSE"); //TIG HF process
                        break;
                    case 2:                      
                        break;
                    case 3:
                        nextion_change_txt(obj_ProgramProcess,"LIFTIG PULSE"); //LIFTIG process
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    default:
                        break;
                }
                
                
                param=OP_MODE; //memory parameter -> Operation mode Process
                controller_cache_read(param,&Program_OpMode); //read operation mode value from memory
                switch(Program_OpMode)
                {
                    case 0:
                        break;
                    case 1:
                        nextion_change_txt(obj_ProgramModes,"2T"); //2T
                        break;
                    case 2:
                        nextion_change_txt(obj_ProgramModes,"4T"); //4T
                        break;
                    case 3:
                        nextion_change_txt(obj_ProgramModes,"SPOT"); //SPOT
                        break;
                    default:
                        break;
                }
                utils_int_to_str(Nextion_PrgNumber,programID_memory,2); //Convert to string
                nextion_change_txt(obj_ProgramName,Nextion_PrgNumber);
                
                FirstLoop=false; //Disable first loop
            }
            if(val_update==true) //Val_update variable from timer for value updates 
            {
                
                param = WELD_STATE;  //memory parameter -> Welding state
                controller_cache_read(param, &weldstate); //read Welding state value from memory

                if(weldstate == 1) //if welding is active
                {
                    controller_outbound_buffer_fill(ReadProgramFromMaster,0); //read parameters from program 0 on master
                    controller_waitwrite(); //wait for master NACK
                    programID_memory=50;
                    while(programID_memory != 0)
                    {
                        if(i2c4_stop_condition()){
                              controller_inbound_buffer_save();
                        }
                    }//only leave this menu when programID is updated
                    stateMachine.c_state = HOME_CHANGE_CURRENT; //go to home page
                    stateMachine.p_state = MEMORY_TIGPULSE0;
                    FirstLoop=true; //FirsLoop for first time on new state
                    var_interrupt=NONE; //Clear event flag
                }
                val_update=false; //disable update flag
            }
            var_interrupt=NONE; //Clear event flag
            break;     
            
        default:
            var_interrupt=NONE; //Clear event flag
            break;
    }
    return stateMachine;
}



sm_t programSM_memoryMMA(sm_t stateMachine)
{
    switch(var_interrupt)
    {
        /*
         * HOME EVENT
         * - Slave requests master program 0 parameters
         * - Slave waits for master to send the parameters
         * - State machine state is now Home_Change_Current
         */
        case HOME:
            controller_outbound_buffer_fill(ReadProgramFromMaster,0); //read parameters from program 0 on master
            controller_waitwrite(); //wait for master NACK
            while(programID_memory != 0)
            {
                if(i2c4_stop_condition()){
                      controller_inbound_buffer_save();
                }
            }//only leave this menu when programID is updated
            stateMachine.c_state = HOME_CHANGE_CURRENT; //Update current state to Home_Change_Current
            stateMachine.p_state = MEMORY_MMA; //Update previous state to MEMORY_TIG
            FirstLoop=true; //FirsLoop for first time on new state
            var_interrupt=NONE; //Clear event flag
            break;
            
        /*
         * BACK EVENT
         * - Slave requests master program 0 parameters
         * - Slave waits for master to send the parameters
         * - State machine state is now Home_Change_Current
         */  
        case BACK:
            controller_outbound_buffer_fill(ReadProgramFromMaster,0); //read parameters from program 0 on master
            controller_waitwrite(); //wait for master NACK
            while(programID_memory != 0)
            {
                if(i2c4_stop_condition()){
                      controller_inbound_buffer_save();
                }
            }//only leave this menu when programID is updated
            stateMachine.c_state = stateMachine.p_state;  //Update current state
            stateMachine.p_state = MEMORY_MMA; //Update previous state
            FirstLoop=true; //FirsLoop for first time on new state
            var_interrupt=NONE; //Clear event flag
            break;
        case ENC_BUTTON:
            
            switch(programpagebuttons)
            {
                case SAVE:
                    controller_outbound_buffer_fill(ReadProgramFromMaster,0); //Read program parameters on master 
                    controller_waitwrite(); //wait for NACK
                    while(programID_memory != 0) //while program received is not 0
                    {
                        if(i2c4_stop_condition()){ //wait for the stop condition
                              controller_inbound_buffer_save(); //save buffer on memory
                        }
                    }//only leave this menu when programID is updated
                    controller_outbound_buffer_fill(WriteProgramToMaster,programtosave); //Write program parameters on master 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=programtosave; //Set programID to previous saved just for showcase
                    stateMachine.c_state = HOME_CHANGE_CURRENT; //Update current state to home
                    stateMachine.p_state = MEMORY_MMA; //Update previous state
                    FirstLoop=true;  //FirsLoop for first time on new state
                    break;
                case LOAD:
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=programtosave; //Set programID to previous saved just for showcase
                    stateMachine.c_state = HOME_CHANGE_CURRENT; //Update current state to home
                    stateMachine.p_state = MEMORY_MMA; //Update previous state
                    FirstLoop=true; //FirsLoop for first time on new state
                    break;
                case RESET:
                    factory_reset_parameters(); //reset parameters
                    controller_outbound_buffer_fill(WriteProgramToMaster,programID_memory); //Write new parameters on the program
                    controller_waitwrite();  //wait for NACK
                    stateMachine.c_state = MEMORY_TIG; //Reset this state
                    stateMachine.p_state = MEMORY_MMA; //Reset this state
                    FirstLoop=true; //FirsLoop for first time on new state
                    break;
                default:
                    break;
            }
            var_interrupt=NONE; //Clear event flag
            break;
            
        /*
         * ENCODER HOLD EVENT
         */    
        case ENC_HOLD:
            stateMachine.c_state = HELP;
            stateMachine.p_state = MEMORY_MMA;
            FirstLoop=true;
            nextion_page(program_help);
            var_interrupt=NONE;
            break;
            
        /*
         * ENCODER RIGHT EVENT
         * - Moving between the different buttons
         */    
        case ENC_RIGHT:
            switch(programpagebuttons)
            {
                case SAVE: //Case save button
                    nextion_unselect(obj_ProgramSavebutton); //Remove highlight from save button
                    nextion_select(obj_ProgramLoadbutton); //highlight Load button
                    programpagebuttons=LOAD; //update button to Load
                    break;
                case LOAD: //Case Load button
                    nextion_unselect(obj_ProgramLoadbutton); //Remove highlight from Load button
                    nextion_select(obj_ProgramResetbutton); //highlight Reset button
                    programpagebuttons=RESET; //update button to Reset
                    break;
                case RESET: //Case Reset button -> nothing to the right
                    break;
                default:
                    break;
            }
            var_interrupt=NONE; //clear event flag
            break;  
        
        /*
         * ENCODER LEFT EVENT
         * - Moving between the different  buttons
         */    
        case ENC_LEFT:
            switch(programpagebuttons)
            {
                case SAVE: //Case Save -> do nothing, nothing to the left
                    break;
                case LOAD: //Case load
                    nextion_unselect(obj_ProgramLoadbutton); //remove highlight from Load
                    nextion_select(obj_ProgramSavebutton); //highlight Save
                    programpagebuttons=SAVE;  //update button to Save
                    break;
                case RESET:
                    nextion_unselect(obj_ProgramResetbutton); //remove highlight from Reset
                    nextion_select(obj_ProgramLoadbutton); //highlight Load
                    programpagebuttons=LOAD; //update button to Load
                    break;
                default:
                    break;
            }
            var_interrupt=NONE; //Clear event flag
            break;  
        
        /*
         * NONE
         * - Set nextion page
         * - Highlight Current Object
         */ 
        case NONE:
            if(FirstLoop==true)//FirsLoop for first time on new state
            {
                nextion_page(program_MMA); //Set nextion page
                nextion_unselect(obj_ProgramSavebutton); //Remove highlight from Load button
                nextion_unselect(obj_ProgramLoadbutton); //Remove highlight from Load button
                nextion_unselect(obj_ProgramResetbutton); //Remove highlight from Reset button
                nextion_select(obj_ProgramSavebutton); //highlight Save button
                programpagebuttons=SAVE; //update buttons to Save
               
                programtosave = programID_memory;
                
                /*
                Read from memory and load values to Nextion
                */
               param=MMA_HOTSTART; //memory parameter -> MMA HOTSTART
               controller_cache_read(param,&Program_MMAHotStart); //read HotStart value from memory
               utils_int_to_str(NextionMMA_HotStart,Program_MMAHotStart,3); //Convert to string
               nextion_change_val(obj_ProgramHotStart,NextionMMA_HotStart); //Send value to Nextion
               
               param=MMA_HOTSTART_TIME; //memory parameter -> MMA HOTSTARTTIME
               controller_cache_read(param,&Program_MMAHotStartTime); //read HotStartTime value from memory
               utils_int_to_str(NextionMMA_HotStartTime,Program_MMAHotStartTime,2); //Convert to string
               nextion_change_val(obj_ProgramHotStartTime,NextionMMA_HotStartTime); //Send value to Nextion
               
               param=MMA_INOMINAL; //memory parameter -> MMA INOMINAL
               controller_cache_read(param,&Program_MMAINominal); //read INominal value from memory
               utils_int_to_str(NextionMMA_INominal,Program_MMAINominal,3); //Convert to string
               nextion_change_val(obj_ProgramINominal,NextionMMA_INominal); //Send value to Nextion
               
               
               param=MMA_ARCFORCE; //memory parameter -> MMA ARCFORCE
               controller_cache_read(param,&Program_MMAArcForce); //read ArcForce value from memory
               utils_int_to_str(NextionMMA_AcrForce,Program_MMAArcForce,2); //Convert to string
               nextion_change_val(obj_ProgramArcForce,NextionMMA_AcrForce); //Send value to Nextion
                   
               utils_int_to_str(Nextion_PrgNumber,programID_memory,2); //Convert to string
               nextion_change_txt(obj_ProgramName,Nextion_PrgNumber);
               
                FirstLoop=false; //Disable first loop
            }
            
            if(val_update==true) //Val_update variable from timer for value updates 
            {
                
                param = WELD_STATE;  //memory parameter -> Welding state
                controller_cache_read(param, &weldstate); //read Welding state value from memory

                if(weldstate == 1) //if welding is active
                {
                    controller_outbound_buffer_fill(ReadProgramFromMaster,0); //read parameters from program 0 on master
                    controller_waitwrite(); //wait for master NACK
                    programID_memory=50;
                    while(programID_memory != 0)
                    {
                        if(i2c4_stop_condition()){
                              controller_inbound_buffer_save();
                        }
                    }//only leave this menu when programID is updated
                    stateMachine.c_state = HOME_CHANGE_CURRENT; //go to home page
                    stateMachine.p_state = MEMORY_MMA;
                    FirstLoop=true; //FirsLoop for first time on new state
                    var_interrupt=NONE; //Clear event flag
                }
                val_update=false; //disable update flag
            }
            var_interrupt=NONE; //Clear event flag
            break;     
            
        default:
            var_interrupt=NONE; //Clear event flag
            break;
    }
    return stateMachine;
}




sm_t programSM_memoryMMA0(sm_t stateMachine)
{
    switch(var_interrupt)
    {
        /*
         * HOME EVENT
         * - State machine state is now Home_Change_Current
         */
        case HOME:
            stateMachine.c_state = HOME_CHANGE_CURRENT; //Update current state to Home_Change_Current
            stateMachine.p_state = MEMORY_MMA0; //Update previous state to MEMORY_TIG
            FirstLoop=true; //FirsLoop for first time on new state
            var_interrupt=NONE; //Clear event flag
            break;
            
        /*
         * BACK EVENT
         * - Read previous parameters
         * - Go to program choose menu
         */   
        case BACK:
            stateMachine.c_state = stateMachine.p_state;  //Update current state
            stateMachine.p_state = MEMORY_MMA0; //Update previous state
            FirstLoop=true; //FirsLoop for first time on new state
            var_interrupt=NONE; //Clear event flag
            break;
        case ENC_BUTTON:
            
            switch(programpagebuttons)
            {
                case LOAD:
                    programID_memory=0; //Set programID to previous saved just for showcase
                    stateMachine.c_state = HOME_CHANGE_CURRENT; //Update current state to home
                    stateMachine.p_state = MEMORY_MMA0; //Update previous state
                    FirstLoop=true; //FirsLoop for first time on new state
                    break;
                case RESET:
                    factory_reset_parameters(); //reset parameters
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write new parameters on the program
                    controller_waitwrite();  //wait for NACK
                    stateMachine.c_state = MEMORY_TIG0; //Reset this state
                    stateMachine.p_state = MEMORY_MMA0; //Reset this state
                    FirstLoop=true; //FirsLoop for first time on new state
                    break;
                default:
                    break;
            }
            var_interrupt=NONE; //Clear event flag
            break;
            
        /*
         * ENCODER HOLD EVENT
         */    
        case ENC_HOLD:
            nextion_page(program_help0);
            stateMachine.c_state = HELP;
            stateMachine.p_state = MEMORY_MMA0;
            FirstLoop=true;
            var_interrupt=NONE;
            break;
            
         /*
         * ENCODER RIGHT EVENT
         * - Moving between the different buttons
         */    
        case ENC_RIGHT:
            switch(programpagebuttons)
            {
                case LOAD: //Case Load button
                    nextion_unselect(obj_ProgramLoadbutton); //Remove highlight from Load button
                    nextion_select(obj_ProgramResetbutton); //highlight Reset button
                    programpagebuttons=RESET; //update button to Reset
                    break;
                case RESET: //Case Reset button -> nothing to the right
                    break;
                default:
                    break;
            }
            var_interrupt=NONE; //clear event flag
            break;  
        
        /*
         * ENCODER LEFT EVENT
         * - Moving between the different  buttons
         */    
        case ENC_LEFT:
            switch(programpagebuttons)
            {
                case LOAD: //Case load
                    break;
                case RESET:
                    nextion_unselect(obj_ProgramResetbutton); //remove highlight from Reset
                    nextion_select(obj_ProgramLoadbutton); //highlight Load
                    programpagebuttons=LOAD; //update button to Load
                    break;
                default:
                    break;
            }
            var_interrupt=NONE; //Clear event flag
            break;  
        
        /*
         * NONE
         * - Set nextion page
         * - Highlight Current Object
         */ 
        case NONE:
            if(FirstLoop==true)//FirsLoop for first time on new state
            {
                nextion_page(program_MMA0); //Set nextion page
                nextion_unselect(obj_ProgramLoadbutton); //Remove highlight from Load button
                nextion_unselect(obj_ProgramResetbutton); //Remove highlight from Reset button
                nextion_select(obj_ProgramLoadbutton); //highlight Save button
                programpagebuttons=LOAD; //update buttons to Save
                
                /*
                Read from memory and load values to Nextion
                */
               param=MMA_HOTSTART; //memory parameter -> MMA HOTSTART
               controller_cache_read(param,&Program_MMAHotStart); //read HotStart value from memory
               utils_int_to_str(NextionMMA_HotStart,Program_MMAHotStart,3); //Convert to string
               nextion_change_val(obj_ProgramHotStart,NextionMMA_HotStart); //Send value to Nextion
               
               param=MMA_HOTSTART_TIME; //memory parameter -> MMA HOTSTARTTIME
               controller_cache_read(param,&Program_MMAHotStartTime); //read HotStartTime value from memory
               utils_int_to_str(NextionMMA_HotStartTime,Program_MMAHotStartTime,2); //Convert to string
               nextion_change_val(obj_ProgramHotStartTime,NextionMMA_HotStartTime); //Send value to Nextion
               
               param=MMA_INOMINAL; //memory parameter -> MMA INOMINAL
               controller_cache_read(param,&Program_MMAINominal); //read INominal value from memory
               utils_int_to_str(NextionMMA_INominal,Program_MMAINominal,3); //Convert to string
               nextion_change_val(obj_ProgramINominal,NextionMMA_INominal); //Send value to Nextion
               
               
               param=MMA_ARCFORCE; //memory parameter -> MMA ARCFORCE
               controller_cache_read(param,&Program_MMAArcForce); //read ArcForce value from memory
               utils_int_to_str(NextionMMA_AcrForce,Program_MMAArcForce,2); //Convert to string
               nextion_change_val(obj_ProgramArcForce,NextionMMA_AcrForce); //Send value to Nextion
                   
                
                FirstLoop=false; //Disable first loop
            }
            if(val_update==true) //Val_update variable from timer for value updates 
            {
                
                param = WELD_STATE;  //memory parameter -> Welding state
                controller_cache_read(param, &weldstate); //read Welding state value from memory

                if(weldstate == 1) //if welding is active
                {
                    stateMachine.c_state = HOME_CHANGE_CURRENT; //go to home page
                    stateMachine.p_state = MEMORY_MMA0;
                    FirstLoop=true; //FirsLoop for first time on new state
                    var_interrupt=NONE; //Clear event flag
                }
                val_update=false; //disable update flag
            }
            var_interrupt=NONE; //Clear event flag
            break;     
            
        default:
            var_interrupt=NONE; //Clear event flag
            break;
    }
    return stateMachine;
}


sm_t programSM_memoryMMAPulse(sm_t stateMachine)
{
    switch(var_interrupt)
    {
        /*
         * HOME EVENT
         * - Slave requests master program 0 parameters
         * - Slave waits for master to send the parameters
         * - State machine state is now Home_Change_Current
         */
        case HOME:
            controller_outbound_buffer_fill(ReadProgramFromMaster,0); //read parameters from program 0 on master
            controller_waitwrite(); //wait for master NACK
            while(programID_memory != 0) //while program received is not 0
            {
                if(i2c4_stop_condition()){ //wait for the stop condition
                      controller_inbound_buffer_save(); //save buffer on memory
                }
            }//only leave this menu when programID is updated
            stateMachine.c_state = HOME_CHANGE_CURRENT; //Update current state to Home_Change_Current
            stateMachine.p_state = MEMORY_MMAPULSE; //Update previous state to MEMORY_TIG
            FirstLoop=true; //FirsLoop for first time on new state
            var_interrupt=NONE; //Clear event flag
            break;
            
        /*
         * BACK EVENT
         * - Slave requests master program 0 parameters
         * - Slave waits for master to send the parameters
         * - State machine state is now Home_Change_Current
         */
        case BACK:
            controller_outbound_buffer_fill(ReadProgramFromMaster,0); //read parameters from program 0 on master
            controller_waitwrite(); //wait for master NACK
            while(programID_memory != 0)  //while program received is not 0
            {
                if(i2c4_stop_condition()){  //wait for the stop condition
                      controller_inbound_buffer_save(); //save buffer on memory
                }
            }//only leave this menu when programID is updated
            stateMachine.c_state = stateMachine.p_state;  //Update current state
            stateMachine.p_state = MEMORY_MMAPULSE; //Update previous state
            FirstLoop=true; //FirsLoop for first time on new state
            var_interrupt=NONE; //Clear event flag
            break;
            
        case ENC_BUTTON:
            switch(programpagebuttons)
            {
                case SAVE:
                    controller_outbound_buffer_fill(ReadProgramFromMaster,0); //Read program parameters on master 
                    controller_waitwrite(); //wait for NACK
                    while(programID_memory != 0) //while program received is not 0
                    {
                        if(i2c4_stop_condition()){ //wait for the stop condition
                              controller_inbound_buffer_save(); //save buffer on memory
                        }
                    }//only leave this menu when programID is updated
                    controller_outbound_buffer_fill(WriteProgramToMaster,programtosave); //Write program parameters on master 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=programtosave; //Set programID to previous saved just for showcase
                    stateMachine.c_state = HOME_CHANGE_CURRENT; //Update current state to home
                    stateMachine.p_state = MEMORY_MMAPULSE; //Update previous state
                    FirstLoop=true;  //FirsLoop for first time on new state
                    break;
                case LOAD:
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write parameters 
                    controller_waitwrite(); //wait for NACK
                    programID_memory=programtosave; //Set programID to previous saved just for showcase
                    stateMachine.c_state = HOME_CHANGE_CURRENT; //Update current state to home
                    stateMachine.p_state = MEMORY_MMAPULSE; //Update previous state
                    FirstLoop=true; //FirsLoop for first time on new state
                    break;
                case RESET:
                    factory_reset_parameters(); //reset parameters
                    controller_outbound_buffer_fill(WriteProgramToMaster,programID_memory); //Write new parameters on the program
                    controller_waitwrite();  //wait for NACK
                    stateMachine.c_state = MEMORY_TIG; //Reset this state
                    stateMachine.p_state = MEMORY_MMAPULSE; //Reset this state
                    FirstLoop=true; //FirsLoop for first time on new state
                    break;
                default:
                    break;
            }
            var_interrupt=NONE; //Clear event flag
            break;
            
        /*
         * ENCODER HOLD EVENT
         */    
        case ENC_HOLD:
            stateMachine.c_state = HELP;
            stateMachine.p_state = MEMORY_MMAPULSE;
            FirstLoop=true;
            nextion_page(program_help);
            var_interrupt=NONE;
            break;
            
        /*
         * ENCODER RIGHT EVENT
         * - Moving between the different buttons
         */    
        case ENC_RIGHT:
            switch(programpagebuttons)
            {
                case SAVE: //Case save button
                    nextion_unselect(obj_ProgramSavebutton); //Remove highlight from save button
                    nextion_select(obj_ProgramLoadbutton); //highlight Load button
                    programpagebuttons=LOAD; //update button to Load
                    break;
                case LOAD: //Case Load button
                    nextion_unselect(obj_ProgramLoadbutton); //Remove highlight from Load button
                    nextion_select(obj_ProgramResetbutton); //highlight Reset button
                    programpagebuttons=RESET; //update button to Reset
                    break;
                case RESET: //Case Reset button -> nothing to the right
                    break;
                default:
                    break;
            }
            var_interrupt=NONE; //clear event flag
            break;  
        
        /*
         * ENCODER LEFT EVENT
         * - Moving between the different  buttons
         */    
        case ENC_LEFT:
            switch(programpagebuttons)
            {
                case SAVE: //Case Save -> do nothing, nothing to the left
                    break;
                case LOAD: //Case load
                    nextion_unselect(obj_ProgramLoadbutton); //remove highlight from Load
                    nextion_select(obj_ProgramSavebutton); //highlight Save
                    programpagebuttons=SAVE;  //update button to Save
                    break;
                case RESET:
                    nextion_unselect(obj_ProgramResetbutton); //remove highlight from Reset
                    nextion_select(obj_ProgramLoadbutton); //highlight Load
                    programpagebuttons=LOAD; //update button to Load
                    break;
                default:
                    break;
            }
            var_interrupt=NONE; //Clear event flag
            break;  
        
        /*
         * NONE
         * - Set nextion page
         * - Highlight Current Object
         */ 
        case NONE:
            if(FirstLoop==true)//FirsLoop for first time on new state
            {
                programtosave = programID_memory;
                
                nextion_page(program_MMAPULSE); //Set nextion page
                nextion_unselect(obj_ProgramSavebutton); //Remove highlight from Load button
                nextion_unselect(obj_ProgramLoadbutton); //Remove highlight from Load button
                nextion_unselect(obj_ProgramResetbutton); //Remove highlight from Reset button
                nextion_select(obj_ProgramSavebutton); //highlight Save button
                programpagebuttons=SAVE; //update buttons to Save
                
                /*
                Read from memory and load values to Nextion
                */
               param=MMA_HOTSTART; //memory parameter -> MMA HOTSTART
               controller_cache_read(param,&Program_MMAHotStart); //read HotStart value from memory
               utils_int_to_str(NextionMMA_HotStart,Program_MMAHotStart,3); //Convert to string
               nextion_change_val(obj_ProgramHotStart,NextionMMA_HotStart); //Send value to Nextion
               
               param=MMA_HOTSTART_TIME; //memory parameter -> MMA HOTSTARTTIME
               controller_cache_read(param,&Program_MMAHotStartTime); //read HotStartTime value from memory
               utils_int_to_str(NextionMMA_HotStartTime,Program_MMAHotStartTime,2); //Convert to string
               nextion_change_val(obj_ProgramHotStartTime,NextionMMA_HotStartTime); //Send value to Nextion
               
               param=MMA_INOMINAL; //memory parameter -> MMA INOMINAL
               controller_cache_read(param,&Program_MMAINominal); //read INominal value from memory
               utils_int_to_str(NextionMMA_INominal,Program_MMAINominal,3); //Convert to string
               nextion_change_val(obj_ProgramINominal,NextionMMA_INominal); //Send value to Nextion
                            
               param=MMA_IBASE;//memory parameter -> MMA Base current
               controller_cache_read(param,&Program_MMAIBase); //read BaseCurrent value from memory
               utils_int_to_str(NextionMMA_BaseCurrent,Program_MMAIBase,2); //Convert to string
               nextion_change_val(obj_ProgramIbase,NextionMMA_BaseCurrent); //Send value to Nextion
               
               param=MMA_PULSE_WIDTH;  //memory parameter -> MMA Pulse Width
               controller_cache_read(param,&Program_MMAWidthval); //read PulseWidth value from memory
               utils_int_to_str(NextionMMA_Width,Program_MMAWidthval,2); //Convert to string
               nextion_change_val(obj_ProgramPulseWidth,NextionMMA_Width); //Send value to Nextion
               
               param=MMA_PULSE_FREQ; //memory parameter -> MMA Pulse Freq
               controller_cache_read(param,&Program_MMAPFreq); //read PulseFreq value from memory
               utils_int_to_str(NextionMMA_PulseFreq,Program_MMAPFreq,3); //Convert to string
               nextion_change_val(obj_ProgramPulseFreq,NextionMMA_PulseFreq); //Send value to Nextion
               
               utils_int_to_str(Nextion_PrgNumber,programID_memory,2); //Convert to string
               nextion_change_txt(obj_ProgramName,Nextion_PrgNumber);
               
                FirstLoop=false; //Disable first loop
            }
            if(val_update==true) //Val_update variable from timer for value updates 
            {
                
                param = WELD_STATE;  //memory parameter -> Welding state
                controller_cache_read(param, &weldstate); //read Welding state value from memory

                if(weldstate == 1) //if welding is active
                {
                    controller_outbound_buffer_fill(ReadProgramFromMaster,0); //read parameters from program 0 on master
                    controller_waitwrite(); //wait for master NACK
                    programID_memory=50;
                    while(programID_memory != 0)
                    {
                        if(i2c4_stop_condition()){
                              controller_inbound_buffer_save();
                        }
                    }//only leave this menu when programID is updated
                    stateMachine.c_state = HOME_CHANGE_CURRENT; //go to home page
                    stateMachine.p_state = MEMORY_MMAPULSE;
                    FirstLoop=true; //FirsLoop for first time on new state
                    var_interrupt=NONE; //Clear event flag
                }
                val_update=false; //disable update flag
            }
            var_interrupt=NONE; //Clear event flag
            break;     
            
        default:
            var_interrupt=NONE; //Clear event flag
            break;
    }
    return stateMachine;
}




sm_t programSM_memoryMMAPulse0(sm_t stateMachine)
{
    switch(var_interrupt)
    {
        /*
         * HOME EVENT
         * - State machine state is now Home_Change_Current
         */
        case HOME:
            stateMachine.c_state = HOME_CHANGE_CURRENT; //Update current state to Home_Change_Current
            stateMachine.p_state = MEMORY_MMAPULSE0; //Update previous state to MEMORY_TIG
            FirstLoop=true; //FirsLoop for first time on new state
            var_interrupt=NONE; //Clear event flag
            break;
            
        /*
         * BACK EVENT
         * - Read previous parameters
         * - Go to program choose menu
         */   
        case BACK:
            stateMachine.c_state = stateMachine.p_state;  //Update current state
            stateMachine.p_state = MEMORY_MMAPULSE0; //Update previous state
            FirstLoop=true; //FirsLoop for first time on new state
            var_interrupt=NONE; //Clear event flag
            break;
            
        case ENC_BUTTON:
            
            switch(programpagebuttons)
            {
                case LOAD:
                    programID_memory=0; //Set programID to previous saved just for showcase
                    stateMachine.c_state = HOME_CHANGE_CURRENT; //Update current state to home
                    stateMachine.p_state = MEMORY_MMAPULSE0; //Update previous state
                    FirstLoop=true; //FirsLoop for first time on new state
                    break;
                case RESET:
                    factory_reset_parameters(); //reset parameters
                    controller_outbound_buffer_fill(WriteProgramToMaster,0); //Write new parameters on the program
                    controller_waitwrite();  //wait for NACK
                    stateMachine.c_state = MEMORY_TIG0; //Reset this state
                    stateMachine.p_state = MEMORY_MMAPULSE0; //Reset this state
                    FirstLoop=true; //FirsLoop for first time on new state
                    break;
                default:
                    break;
            }
            var_interrupt=NONE; //Clear event flag
            break;
            
        /*
         * ENCODER HOLD EVENT
         */    
        case ENC_HOLD:
            nextion_page(program_help0);
            stateMachine.c_state = HELP;
            stateMachine.p_state = MEMORY_MMAPULSE0;
            FirstLoop=true;
            var_interrupt=NONE;
            break;
            
         /*
         * ENCODER RIGHT EVENT
         * - Moving between the different buttons
         */    
        case ENC_RIGHT:
            switch(programpagebuttons)
            {
                case LOAD: //Case Load button
                    nextion_unselect(obj_ProgramLoadbutton); //Remove highlight from Load button
                    nextion_select(obj_ProgramResetbutton); //highlight Reset button
                    programpagebuttons=RESET; //update button to Reset
                    break;
                case RESET: //Case Reset button -> nothing to the right
                    break;
                default:
                    break;
            }
            var_interrupt=NONE; //clear event flag
            break;  
        
        /*
         * ENCODER LEFT EVENT
         * - Moving between the different  buttons
         */    
        case ENC_LEFT:
            switch(programpagebuttons)
            {
                case LOAD: //Case load
                    break;
                case RESET:
                    nextion_unselect(obj_ProgramResetbutton); //remove highlight from Reset
                    nextion_select(obj_ProgramLoadbutton); //highlight Load
                    programpagebuttons=LOAD; //update button to Load
                    break;
                default:
                    break;
            }
            var_interrupt=NONE; //Clear event flag
            break;  
        
        /*
         * NONE
         * - Set nextion page
         * - Highlight Current Object
         */ 
        case NONE:
            if(FirstLoop==true)//FirsLoop for first time on new state
            {
                nextion_page(program_MMAPULSE0); //Set nextion page
                nextion_unselect(obj_ProgramLoadbutton); //Remove highlight from Load button
                nextion_unselect(obj_ProgramResetbutton); //Remove highlight from Reset button
                nextion_select(obj_ProgramLoadbutton); //highlight Save button
                programpagebuttons=LOAD; //update buttons to Save
                
                /*
                Read from memory and load values to Nextion
                */
               param=MMA_HOTSTART; //memory parameter -> MMA HOTSTART
               controller_cache_read(param,&Program_MMAHotStart); //read HotStart value from memory
               utils_int_to_str(NextionMMA_HotStart,Program_MMAHotStart,3); //Convert to string
               nextion_change_val(obj_ProgramHotStart,NextionMMA_HotStart); //Send value to Nextion
               
               param=MMA_HOTSTART_TIME; //memory parameter -> MMA HOTSTARTTIME
               controller_cache_read(param,&Program_MMAHotStartTime); //read HotStartTime value from memory
               utils_int_to_str(NextionMMA_HotStartTime,Program_MMAHotStartTime,2); //Convert to string
               nextion_change_val(obj_ProgramHotStartTime,NextionMMA_HotStartTime); //Send value to Nextion
               
               param=MMA_INOMINAL; //memory parameter -> MMA INOMINAL
               controller_cache_read(param,&Program_MMAINominal); //read INominal value from memory
               utils_int_to_str(NextionMMA_INominal,Program_MMAINominal,3); //Convert to string
               nextion_change_val(obj_ProgramINominal,NextionMMA_INominal); //Send value to Nextion
                            
               param=MMA_IBASE;//memory parameter -> MMA Base current
               controller_cache_read(param,&Program_MMAIBase); //read BaseCurrent value from memory
               utils_int_to_str(NextionMMA_BaseCurrent,Program_MMAIBase,2); //Convert to string
               nextion_change_val(obj_ProgramIbase,NextionMMA_BaseCurrent); //Send value to Nextion
               
               param=MMA_PULSE_WIDTH;  //memory parameter -> MMA Pulse Width
               controller_cache_read(param,&Program_MMAWidthval); //read PulseWidth value from memory
               utils_int_to_str(NextionMMA_Width,Program_MMAWidthval,2); //Convert to string
               nextion_change_val(obj_ProgramPulseWidth,NextionMMA_Width); //Send value to Nextion
               
               param=MMA_PULSE_FREQ; //memory parameter -> MMA Pulse Freq
               controller_cache_read(param,&Program_MMAPFreq); //read PulseFreq value from memory
               utils_int_to_str(NextionMMA_PulseFreq,Program_MMAPFreq,3); //Convert to string
               nextion_change_val(obj_ProgramPulseFreq,NextionMMA_PulseFreq); //Send value to Nextion
                
                FirstLoop=false; //Disable first loop
            }
            if(val_update==true) //Val_update variable from timer for value updates 
            {
                
                param = WELD_STATE;  //memory parameter -> Welding state
                controller_cache_read(param, &weldstate); //read Welding state value from memory

                if(weldstate == 1) //if welding is active
                {
                    stateMachine.c_state = HOME_CHANGE_CURRENT; //go to home page
                    stateMachine.p_state = MEMORY_MMAPULSE0;
                    FirstLoop=true; //FirsLoop for first time on new state
                    var_interrupt=NONE; //Clear event flag
                }
                val_update=false; //disable update flag
            }
            var_interrupt=NONE; //Clear event flag
            break;     
            
        default:
            var_interrupt=NONE; //Clear event flag
            break;
    }
    return stateMachine;
}