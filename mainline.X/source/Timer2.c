/**
 * \file    Timer2.c
 * \brief   Timer2 device-driver.
 */
/**
 * \defgroup dd Device drivers
 */
/**
 * \addtogroup  tim2 Timer2
 * \ingroup dd
 * @{
 *  \ref Timer2.c
 * @}
 */
#include <xc.h>
#include <sys/attribs.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include "Timer2.h"
#include "GeneralConfigs.h"
#include "SM_structures.h"
#include "nextion.h"

#define TIMER_CON_ENABLE        T2CONbits.ON
#define TIMER_CON_PRESCALER     T2CONbits.TCKPS
#define TIMER_CON_32BIT         T2CONbits.T32
#define TIMER_INT_ENABLE        IEC0bits.T2IE 
#define TIMER_INT_FLAG          IFS0bits.T2IF 
#define TIMER_INT_PRIORITY      IPC2bits.T2IP 

#define ButtonTransition 0
#define HoldCycle 30 //7
#define ClickCycle 1 //2

volatile uint16_t encoderclick; 
volatile uint16_t homeclick;
volatile uint16_t backclick;

void Timer2Init(void){
    T2CONbits.ON = 0; // Stop timer
    T2CONbits.SIDL = 0; // Continue operation when in IDLE mode
    T2CONbits.TGATE = 0; // Gate time accumulation is disabled
    T2CONbits.TCKPS = 7; // 256  prescaler
    T2CONbits.T32 = 0; // 16-bit timer operation
    // interrupts
    IEC0bits.T2IE = 1; // Disable timer 2 interrupts
    IFS0bits.T2IF = 0; // Reset timer 2 interrupt flag
    IPC2bits.T2IP = 5; // Interrupt priority
    IPC2bits.IC2IS = 1; // Interrupt subpriorities
    PR2=0x186A; // Period set to 40ms
    TMR2=0;
    T2CONbits.ON=1; // Start the timer
    
    
    //Init variables
    encoderclick=0;
    homeclick=0;
    backclick=0;  
}

/*
 *  INTERRUPT TIMER 2 - BUTTONS
 * Click -> button released within max 1 timer interrupt
 * Hold -> buttons released with at least 30 timer interrupt (1,2s)
 */


void __ISR (_TIMER_2_VECTOR, IPL5SOFT) T2Interrupt(void)
{
   if(PORTDbits.RD6 == ButtonTransition){ 
        encoderclick++; 
    }
    if(PORTDbits.RD6 != ButtonTransition) 
    {
        if(encoderclick >=ClickCycle && encoderclick <HoldCycle) 
        {
        var_interrupt=ENC_BUTTON;
        encoderclick=0; 
        }else if(encoderclick >=HoldCycle){
        var_interrupt=ENC_HOLD;
        encoderclick=0;
        }
    }
    
    if(PORTDbits.RD5 == ButtonTransition){
        backclick++; 
    }
    if(PORTDbits.RD5 != ButtonTransition)
    {
        if(backclick >= ClickCycle ) 
        {
        var_interrupt=BACK;
        backclick=0;  
        }
    }

    if(PORTEbits.RE4 == ButtonTransition){ 
        homeclick++; 
    }
    if(PORTEbits.RE4 != ButtonTransition)
    {
        if(homeclick >= ClickCycle ) 
        {
        var_interrupt=HOME;
        homeclick=0;
        }
    }
    TIMER_INT_FLAG  = 0; 
}