/**
 * \file    controller.c
 * \brief   Macros and functions related to the memory management.
 */
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <xc.h>

#include "queue.h"
#include "controller.h"
#include "utils.h"
#include "defines.h"
#include "i2c4.h"
/*
 * Macro definitions
 */
/** \brief  Number of bytes of the checksum */
#define BUFFER_FLETCHER_N_BYTES 2
/** \brief  Number of bytes of the data of the entry */
#define BUFFER_ENTRY_N_DATA_BYTES 2
/** \brief  Maximum number of excepted entries */
#define BUFFER_N_ENTRIES 84
volatile bool i2c4_read_write_flag = false;
volatile uint8_t programID_memory;

/*
 * Type definitions
 */
typedef struct header_t
{
	uint8_t type : 3;
	uint8_t programID : 5;
	uint8_t nParams : 8;
	uint8_t checksum[BUFFER_FLETCHER_N_BYTES];
} header_t;

typedef struct entry_t
{
	uint8_t id;
	uint8_t data[BUFFER_ENTRY_N_DATA_BYTES];
} entry_t;

/*
 * Local variables
 */
static queue_t inbound_buf;
static queue_t outbound_buf;
static uint16_t cache[CACHE_SIZE];

void controller_init()
{
	queue_clear(&inbound_buf);
	queue_clear(&outbound_buf);

	uint16_t i = 0;
	for (i = 0; i < CACHE_SIZE; i++)
	{
		cache[i] = 0;
	}
}

void controller_cache_read(param_id_t index, uint16_t *value)
{
	memcpy(value, &cache[ (uint8_t) index], sizeof (uint16_t));
}

bool controller_outbound_buffer_fill(uint8_t optype, uint8_t opprogram)
{
	uint8_t body_buffer[CACHE_SIZE * sizeof (entry_t)];
	uint8_t header_buffer[sizeof (header_t)];

	uint8_t param_id;
	uint16_t index;
	bool lsb_msb = true;

	// copy body data to a temporary buffer
	for (index = 0, param_id = 0; param_id < CACHE_SIZE; param_id++)
	{
		// first byte is the parameter id
		body_buffer[index++] = param_id;
		// second and third bytes are the least significant byte and most
		// significant byte respectively.
		do
		{
			// first lsb
			if (lsb_msb)
			{
				body_buffer[index++] = (uint8_t) (cache[param_id] & 0x00FF);
				lsb_msb = false; // switch
			}
				// then msb
			else
			{
				body_buffer[index++] = (uint8_t) ((cache[param_id] & 0xFF00) >> 8);
				lsb_msb = true; // switch
			}

		}
		while (lsb_msb == false);
	}
	// create header
	header_t all_params;
	all_params.type = optype;
	all_params.programID = opprogram;
	all_params.nParams = 32;
	// calculate checksum (data only!)
	uint16_t checksum = utils_fletcher16(body_buffer, CACHE_SIZE * sizeof (entry_t));
	all_params.checksum[1] = (uint8_t) ((checksum & 0xFF00) >> 8);
	all_params.checksum[0] = (uint8_t) (checksum & 0x00FF);
	// copy to a temporary buffer
	memcpy(header_buffer, &all_params, sizeof (header_t));
	queue_clear(&outbound_buf);
	// send header to queue
	for (index = 0; index < sizeof (header_t); index++)
	{
		controller_outbound_buffer_enqueue(header_buffer[index]);
	}

	// send parameters to queue
	for (index = 0; index < CACHE_SIZE * sizeof (entry_t); index++)
	{
		controller_outbound_buffer_enqueue(body_buffer[index]);
	}

	return true;
}

void controller_cache_write(uint8_t index, uint16_t value)
{
	cache[index] = value;
}

controller_error_t controller_inbound_buffer_enqueue(uint8_t data)
{

	if (queue_enqueue(&inbound_buf, data) == QUEUE_SUCCESS)
	{
		return CONTROLLER_SUCESS;
	}
	else
	{
		return CONTROLLER_FAIL;
	}
}

controller_error_t controller_outbound_buffer_enqueue(uint8_t data)
{

	if (queue_enqueue(&outbound_buf, data) == QUEUE_SUCCESS)
	{
		return CONTROLLER_SUCESS;
	}
	else
	{
		return CONTROLLER_FAIL;
	}
}

controller_error_t controller_outbound_buffer_dequeue(uint8_t *data)
{
	if (queue_dequeue(&outbound_buf, data) == QUEUE_SUCCESS)
	{
		return CONTROLLER_SUCESS;
	}
	else
	{
		return CONTROLLER_FAIL;
	}
}

controller_error_t controller_inbound_buffer_save(void)
{

	// buffer holding encoded received data
	uint8_t buffer[sizeof (header_t) + (sizeof (entry_t) * BUFFER_N_ENTRIES)];

	uint16_t i;

	// copy contents from queue to the buffer
	for (i = 0; i < sizeof (header_t) + (sizeof (entry_t) * BUFFER_N_ENTRIES); i++)
	{
		uint8_t buf = 0;
		if (queue_dequeue(&inbound_buf, &buf) == QUEUE_SUCCESS)
		{
			buffer[i] = buf;
		}
		else
		{
			buffer[i] = 0;
		}
	}

	header_t frame_header; // received frame header

	// decode received frame header
	memcpy(&frame_header, buffer, sizeof (header_t));
	// buffer holding decoded received data
	entry_t frame_body[frame_header.nParams * sizeof (entry_t)]; // received frame body entries
	// decode received frame body
	memcpy(&frame_body, (buffer + sizeof (header_t)), (frame_header.nParams * sizeof (entry_t)));
	// calculated checksum
	uint16_t clc_checksum = utils_fletcher16((uint8_t *) frame_body, (frame_header.nParams * sizeof (entry_t)));
	// received checksum
	uint16_t rcv_checksum = ((frame_header.checksum[1] << 8) | frame_header.checksum[0]);

	if (clc_checksum == rcv_checksum)
	{
        if(frame_header.type == MasterRequestFromSlave)
        {
            controller_outbound_buffer_fill(WriteProgramToMaster,0);
            controller_waitwrite(); 
            queue_clear(&inbound_buf);
            return CONTROLLER_SUCESS;
        }else{
            entry_t *e = frame_body;
            programID_memory=frame_header.programID;
            while (e != frame_body + frame_header.nParams)
            {
                uint16_t data = (uint16_t) ((e->data[1] << 8) | e->data[0]);
                memcpy(&cache[e->id], &data, sizeof (uint16_t));
                e++;
            }
            queue_clear(&inbound_buf);
            return CONTROLLER_SUCESS;
        }
	}
	else
	{
		queue_clear(&inbound_buf);
        controller_outbound_buffer_fill(ReadProgramFromMaster,frame_header.programID);
        controller_waitwrite(); 
		return CONTROLLER_CHECKSUM_FAIL;
	}
}

bool controller_waitwrite(void)
{
    PORTDbits.RD8=1; 
    while((!i2c4_stop_condition()) && (!i2c4_read_write_flag));
    PORTDbits.RD8=0; 
    return true;
}