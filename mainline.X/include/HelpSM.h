#ifndef HELPSM_H
#define	HELPSM_H

/**
 * \brief Responsible for the Help Menu pages.
 * 
 * This routine will control the help page events.
 * When on the help menu, if the user clicks on the encoder, on home or back button
 * will quit the help page and return to the previous state. 
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t help_menu(sm_t stateMachine);

#endif	/* HELPSM_H */

