#ifndef PARAMTIG_H
#define	PARAMTIG_H

/**
 * \brief Responsible for the TIG (NOT PULSED) parameteres and value changes.
 * 
 * This routine will control the flow between the TIG parameters aswell
 * as allowing the user to change them according to max and min value.
 * 
 * Parameters available on this page:
 * TIG Pre Gas  - Min value 1, Max value 100  (Seconds/10)
 * TIG IStart   - Min value 1, Max value 220  (A)
 * TIG UpSlope  - Min value 0, Max value 100  (Seconds/10)
 * TIG INominal - Min value 10, Max value 220 (%)
 * 
 * Page 1/2 for TIG.
 * 
 *  Events:
 * - Encoder rotation to the right or left will change the button or the value
 * of the parameter.
 * - Encoder click will allow the user to open parameter value so it can be 
 * edited if on the button, if on the value, will validate the change and save it.
 * - Encoder hold will open a Help menu for this page.
 * - Back will take the user back to Selection Menu buttons on Home menu if on 
 * buttons. If on values, will discard changes.
 * - Home will take the user back to Current Value change on Home menu.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t TIG_parameters_page1(sm_t stateMachine);

/**
 * \brief Responsible for the TIG (NOT PULSED) parameteres and value changes.
 * 
 * This routine will control the flow between the TIG parameters aswell
 * as allowing the user to change them according to max and min value.
 * 
 * Parameters available on this page:
 * TIG DownSlope - Min value 0, Max value 100  (Seconds/10)
 * TIG IEnd      - Min value 10, Max value 220 (A)
 * TIG PostGas   - Min value 1, Max value 200  (Seconds/10)
 * TIG Dynamicis - On/Off
 * 
 * Page 2/2 for TIG.
 * 
 *  Events:
 * - Encoder rotation to the right or left will change the button or the value
 * of the parameter.
 * - Encoder click will allow the user to open parameter value so it can be 
 * edited if on the button, if on the value, will validate the change and save it.
 * - Encoder hold will open a Help menu for this page.
 * - Back will take the user back to Selection Menu buttons on Home menu if on 
 * buttons. If on values, will discard changes.
 * - Home will take the user back to Current Value change on Home menu.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t TIG_parameters_page2(sm_t stateMachine);

/**
 * \brief Responsible for the TIG (NOT PULSED) parameteres in Spot mode and value changes.
 * 
 * This routine will control the flow between the TIG parameters in Spot mode 
 * aswell as allowing the user to change them according to max and min value.
 * 
 * Parameters available on this page:
 * TIG Pre Gas  - Min value 1, Max value 100  (Seconds/10)
 * TIG IStart   - Min value 1, Max value 220  (A)
 * TIG UpSlope  - Min value 0, Max value 100  (Seconds/10)
 * TIG INominal - Min value 10, Max value 220 (%)
 * 
 * Page 1/3 for TIG Spot.
 * 
 *  Events:
 * - Encoder rotation to the right or left will change the button or the value
 * of the parameter.
 * - Encoder click will allow the user to open parameter value so it can be 
 * edited if on the button, if on the value, will validate the change and save it.
 * - Encoder hold will open a Help menu for this page.
 * - Back will take the user back to Selection Menu buttons on Home menu if on 
 * buttons. If on values, will discard changes.
 * - Home will take the user back to Current Value change on Home menu.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t TIG_Spot_parameters_page1(sm_t stateMachine);

/**
 * \brief Responsible for the TIG (NOT PULSED) parameteres in Spot mode and value changes.
 * 
 * This routine will control the flow between the TIG parameters in Spot mode 
 * aswell as allowing the user to change them according to max and min value.
 * 
 * Parameters available on this page:
 * TIG DownSlope - Min value 0, Max value 100  (Seconds/10)
 * TIG IEnd      - Min value 10, Max value 220 (A)
 * TIG PostGas   - Min value 1, Max value 200  (Seconds/10)
 * TIG Dynamicis - On/Off
 * 
 * Page 2/3 for TIG Spot.
 * 
 *  Events:
 * - Encoder rotation to the right or left will change the button or the value
 * of the parameter.
 * - Encoder click will allow the user to open parameter value so it can be 
 * edited if on the button, if on the value, will validate the change and save it.
 * - Encoder hold will open a Help menu for this page.
 * - Back will take the user back to Selection Menu buttons on Home menu if on 
 * buttons. If on values, will discard changes.
 * - Home will take the user back to Current Value change on Home menu.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t TIG_Spot_parameters_page2(sm_t stateMachine);

/**
 * \brief Responsible for the TIG (NOT PULSED) parameteres in Spot mode and value changes.
 * 
 * This routine will control the flow between the TIG parameters in Spot mode 
 * aswell as allowing the user to change them according to max and min value.
 * 
 * Parameters available on this page:
 * TIG SpotTime - Min value 1, Max value 100  (Seconds/10)
 * 
 * Page 3/3 for TIG Spot.
 * 
 *  Events:
 * - Encoder rotation to the right or left will change the button or the value
 * of the parameter.
 * - Encoder click will allow the user to open parameter value so it can be 
 * edited if on the button, if on the value, will validate the change and save it.
 * - Encoder hold will open a Help menu for this page.
 * - Back will take the user back to Selection Menu buttons on Home menu if on 
 * buttons. If on values, will discard changes.
 * - Home will take the user back to Current Value change on Home menu.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t TIG_Spot_parameters_page3(sm_t stateMachine);

/**
 * \brief Responsible for the TIG PULSED parameteres and value changes.
 * 
 * This routine will control the flow between the TIG parameters aswell
 * as allowing the user to change them according to max and min value.
 * 
 * Parameters available on this page:
 * TIG Pre Gas  - Min value 1, Max value 100  (Seconds/10)
 * TIG IStart   - Min value 1, Max value 220  (A)
 * TIG UpSlope  - Min value 0, Max value 100  (Seconds/10)
 * TIG INominal - Min value 10, Max value 220 (%)
 * 
 * Page 1/3 for TIG PULSED.
 * 
 *  Events:
 * - Encoder rotation to the right or left will change the button or the value
 * of the parameter.
 * - Encoder click will allow the user to open parameter value so it can be 
 * edited if on the button, if on the value, will validate the change and save it.
 * - Encoder hold will open a Help menu for this page.
 * - Back will take the user back to Selection Menu buttons on Home menu if on 
 * buttons. If on values, will discard changes.
 * - Home will take the user back to Current Value change on Home menu.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t Pulse_TIG_parameters_page1(sm_t stateMachine);

/**
 * \brief Responsible for the TIG PULSED parameteres and value changes.
 * 
 * This routine will control the flow between the TIG parameters aswell
 * as allowing the user to change them according to max and min value.
 * 
 * Parameters available on this page:
 * TIG IBase      - Min value 1, Max value 95  (%)
 * TIG PulseWidth - Min value 10, Max value 90 (A)
 * TIG PulseFreq. - Min value 1, Max value 200 (Hz)
 * TIG DownSlope  - Min value 0, Max value 100 (Seconds/10)
 * 
 * Page 2/3 for TIG PULSED.
 * 
 *  Events:
 * - Encoder rotation to the right or left will change the button or the value
 * of the parameter.
 * - Encoder click will allow the user to open parameter value so it can be 
 * edited if on the button, if on the value, will validate the change and save it.
 * - Encoder hold will open a Help menu for this page.
 * - Back will take the user back to Selection Menu buttons on Home menu if on 
 * buttons. If on values, will discard changes.
 * - Home will take the user back to Current Value change on Home menu.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t Pulse_TIG_parameters_page2(sm_t stateMachine);

/**
 * \brief Responsible for the TIG PULSED parameteres and value changes.
 * 
 * This routine will control the flow between the TIG parameters aswell
 * as allowing the user to change them according to max and min value.
 * 
 * Parameters available on this page:
 * TIG IEnd     - Min value 10, Max value 220  (A)
 * TIG PostGas  - Min value 1, Max value 200 (Seconds/10)
 * TIG Dynamics - On/Off
 * 
 * Page 3/3 for TIG PULSED (Whitout Spot).
 * 
 *  Events:
 * - Encoder rotation to the right or left will change the button or the value
 * of the parameter.
 * - Encoder click will allow the user to open parameter value so it can be 
 * edited if on the button, if on the value, will validate the change and save it.
 * - Encoder hold will open a Help menu for this page.
 * - Back will take the user back to Selection Menu buttons on Home menu if on 
 * buttons. If on values, will discard changes.
 * - Home will take the user back to Current Value change on Home menu.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t Pulse_TIG_parameters_page3(sm_t stateMachine);

/**
 * \brief Responsible for the TIG PULSED parameteres and value changes.
 * 
 * This routine will control the flow between the TIG parameters aswell
 * as allowing the user to change them according to max and min value.
 * 
 * Parameters available on this page:
 * TIG IEnd     - Min value 10, Max value 220  (A)
 * TIG PostGas  - Min value 1, Max value 200 (Seconds/10)
 * TIG Dynamics - On/Off
 * TIG SpotTime - Min value 1, Max value 100 (Seconds/10)
 * 
 * Page 3/3 for TIG PULSED (Whit Spot).
 * 
 *  Events:
 * - Encoder rotation to the right or left will change the button or the value
 * of the parameter.
 * - Encoder click will allow the user to open parameter value so it can be 
 * edited if on the button, if on the value, will validate the change and save it.
 * - Encoder hold will open a Help menu for this page.
 * - Back will take the user back to Selection Menu buttons on Home menu if on 
 * buttons. If on values, will discard changes.
 * - Home will take the user back to Current Value change on Home menu.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t TIG_Pulse_Spot_parameters_page(sm_t stateMachine);


#endif	/* PARAMTIG_H */

