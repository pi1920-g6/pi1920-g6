#ifndef SETTINGS_H
#define	SETTINGS_H

sm_t settings(sm_t stateMachine);

sm_t confirmFactoryReset(sm_t stateMachine);

#endif	/* SETTINGS_H */

