#ifndef PROGRAMSM_H
#define	PROGRAMSM_H

/**
 * \brief This routine is responsible for allowing the user to cycle throught different programs.
 * 
 * Page 1/2, allows user to cycle between program 0 and 11.
 * Before switching to the program page from the selected program, slave sends
 * a request for master to read the parameters of the program.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t programSM_page1(sm_t stateMachine);


/**
 * \brief This routine is responsible for allowing the user to cycle throught different programs.
 * 
 * Page 2/2, allows user to cycle between program 11 and 20.
 * Before switching to the program page from the selected program, slave sends
 * a request for master to read the parameters of the program.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t programSM_page2(sm_t stateMachine);


/**
 * \brief This routine is responsible for showing the TIG parameters for the program 0.
 * 
 * Program 0 doesn't have the ability to save the program, only load or reset.
 * Program 0 parameters are loaded. 
 * User can load this program parameters or reset them.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t programSM_memoryTIG0(sm_t stateMachine);


/**
 * \brief This routine is responsible for showing the TIG parameters for the other programs.
 * 
 * Unlike program 0, programs from 1-20 can save the parameters being used, load
 * saved parameters or reset a program parameters to factory default.
 * 
 * When save option is selected:
 * - Slave requests program 0 parameters from master  (Type 2)
 * - Slave requests write on selected program with the requested parameters to master (Type 1)
 * 
 * When load option is selected:
 * - Slave requests write of the loaded parameters on the program 0
 * 
 * When reset option is selected:
 * - All parameters from this program are on factory new values
 * - Slave requests write on the selected program of the factory new values
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t programSM_memoryTIG(sm_t stateMachine);


/**
 * \brief This routine is responsible for showing the TIG PULSE parameters for the other programs.
 * 
 * Unlike program 0, programs from 1-20 can save the parameters being used, load
 * saved parameters or reset a program parameters to factory default.
 * 
 * When save option is selected:
 * - Slave requests program 0 parameters from master  (Type 2)
 * - Slave requests write on selected program with the requested parameters to master (Type 1)
 * 
 * When load option is selected:
 * - Slave requests write of the loaded parameters on the program 0
 * 
 * When reset option is selected:
 * - All parameters from this program are on factory new values
 * - Slave requests write on the selected program of the factory new values
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t programSM_memoryTIGPulse(sm_t stateMachine);


/**
 * \brief This routine is responsible for showing the TIG Pulse parameters for the program 0.
 * 
 * Program 0 doesn't have the ability to save the program, only load or reset.
 * Program 0 parameters are loaded. 
 * User can load this program parameters or reset them.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t programSM_memoryTIG0Pulse(sm_t stateMachine);


/**
 * \brief This routine is responsible for showing the MMA parameters for the other programs.
 * 
 * Unlike program 0, programs from 1-20 can save the parameters being used, load
 * saved parameters or reset a program parameters to factory default.
 * 
 * When save option is selected:
 * - Slave requests program 0 parameters from master  (Type 2)
 * - Slave requests write on selected program with the requested parameters to master (Type 1)
 * 
 * When load option is selected:
 * - Slave requests write of the loaded parameters on the program 0
 * 
 * When reset option is selected:
 * - All parameters from this program are on factory new values
 * - Slave requests write on the selected program of the factory new values
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t programSM_memoryMMA(sm_t stateMachine);


/**
 * \brief This routine is responsible for showing the MMA Pulse parameters for the other programs.
 * 
 * Unlike program 0, programs from 1-20 can save the parameters being used, load
 * saved parameters or reset a program parameters to factory default.
 * 
 * When save option is selected:
 * - Slave requests program 0 parameters from master  (Type 2)
 * - Slave requests write on selected program with the requested parameters to master (Type 1)
 * 
 * When load option is selected:
 * - Slave requests write of the loaded parameters on the program 0
 * 
 * When reset option is selected:
 * - All parameters from this program are on factory new values
 * - Slave requests write on the selected program of the factory new values
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t programSM_memoryMMAPulse(sm_t stateMachine);


/**
 * \brief This routine is responsible for showing the MMA parameters for the program 0.
 * 
 * Program 0 doesn't have the ability to save the program, only load or reset.
 * Program 0 parameters are loaded. 
 * User can load this program parameters or reset them.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t programSM_memoryMMA0(sm_t stateMachine);


/**
 * \brief This routine is responsible for showing the MMA Pulse parameters for the program 0.
 * 
 * Program 0 doesn't have the ability to save the program, only load or reset.
 * Program 0 parameters are loaded. 
 * User can load this program parameters or reset them.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t programSM_memoryMMAPulse0(sm_t stateMachine);

#endif	/* PROGRAMSM_H */

