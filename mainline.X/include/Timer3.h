#ifndef TIMER3_H
#define	TIMER3_H

/**
 * \brief Initialises the Timer3.
 * 
 * This routine initialises the Timer3 with 10Hz frequency (100ms).
 * This timer is used to toggle on the update variable.

 */
void Timer3Init(void);

#endif	/* TIMER3_H */

