# include "SM_structures.h"

#ifndef MODESELECTIONSM_H
#define	MODESELECTIONSM_H

/**
 * \brief Enum structure which serves to breakdown the various welding options.
 * The values are ordered in the same way that the machine handles the respective
 * routine, and so converting them into integer values from 0 to 5 allows us to
 * directly send the corresponding value to the machine.
 */
typedef enum weld_opt
{
    ms_TIGHF,
    ms_TIGHF_P,
    ms_LIFTIG,
    ms_LIFTIG_P,
    ms_MMA,
    ms_MMA_P
} weld_opt;

/**
 * \brief Enum structure which serves to breakdown the various opmode options.
 * The values are ordered in the same way that the machine handles the respective
 * routine, and so converting them into integer values from 0 to 3 allows us to
 * directly send the corresponding value to the machine.
 */
typedef enum opmode_opt
{
    ms_MMAp,
    ms_T2,
    ms_T4,
    ms_SPOT        
} opmode_opt;

/**
 * \brief Responsible for moving between the three sub-menus.
 * 
 * This routine will control the flow between the various submenus, acting as an
 * index between the three.
 * 
 * Options Available:
 * PARAMETERS - Selecting this will transfer you to the Parameters submenu.
 * PULSE - Selecting this will transfer you to the Pulse submenu.
 * OPMODE - Selecting this will transfer you to the OpMode submenu.
 * 
 *  Events:
 * - Encoder rotation to the right or left will change which button is selected.
 * - Encoder click will switch the user to the selected submenu.
 * - Encoder hold will open a Help menu for the selected submenu.
 * - Back will take the user back to Selection Menu buttons on Home menu.
 * - Home will take the user back to Current Value change on Home menu.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t mode_selection(sm_t stateMachine);

/**
 * \brief Responsible for moving between and updating the three Parameter options
 * 
 * This routine will control the flow between the three parameters buttons and
 * will update the machine's values if a change is performed.
 * 
 * Options Available:
 * LIFTIG - Selecting this will alter the Process to LIFTIG and return to the 
 * page's main menu.
 * TIGHF - Selecting this will alter the Process to TIGHF and return to the 
 * page's main menu.
 * MMA - Selecting this will alter the Process to MMA and return to the page's
 * main menu, this time, with the OPMODE option disabled.
 * 
 *  Events:
 * - Encoder rotation to the right or left will change which button is selected.
 * - Encoder click will update the Parameters value with the selected one and 
 * return to the page's main menu.
 * - Encoder hold will open a Help menu for this submenu.
 * - Back will take the user back to page's main menu and reset the selected
 * button their previous states.
 * - Home will take the user back to Current Value change on Home menu.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t ms_Processes(sm_t stateMachine);

/**
 * \brief Responsible for moving between and updating the two Pulse options
 * 
 * This routine will control the flow between the three Pulse buttons and will 
 * update the machine's values if a change is performed.
 * 
 * Options Available:
 * ON - Selecting this will alter enable the PULSE value and return to the
 * page's main menu.
 * OFF - Selecting this will alter disable the PULSE value and return to the
 * page's main menu.
 *  
 *  Events:
 * - Encoder rotation to the right or left will change which button is selected.
 * - Encoder click will update the PULSE value with the selected one and return
 * to the page's main menu.
 * - Encoder hold will open a Help menu for this submenu.
 * - Back will take the user back to page's main menu and reset the selected
 * button their previous states.
 * - Home will take the user back to Current Value change on Home menu.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t ms_Pulse(sm_t stateMachine);

/**
 * \brief Responsible for moving between and updating the three OpMode options
 * 
 * This routine will control the flow between the three OpMode buttons and will 
 * update the machine's values if a change is performed.
 * 
 * Options Available:
 * 2T - Selecting this will alter the OPMODE value to 2T and return to the 
 * page's main menu.
 * 4T - Selecting this will alter the OPMODE value to 4T and return to the 
 * page's main menu.
 * SPOT - Selecting this will alter the OPMODE value to SPOT and return to the
 * page's main menu, this time, with the OPMODE option disabled.
 * 
 *  Events:
 * - Encoder rotation to the right or left will change which button is selected.
 * - Encoder click will update the OpMode value with the selected one and return
 * to the page's main menu.
 * - Encoder hold will open a Help menu for this submenu.
 * - Back will take the user back to page's main menu and reset the selected
 * button their previous states.
 * - Home will take the user back to Current Value change on Home menu.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t ms_Mode(sm_t stateMachine);

#endif	/* MODESELECTIONSM_H */

