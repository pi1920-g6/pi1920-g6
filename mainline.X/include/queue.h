#ifndef __QUEUE_H__
#define __QUEUE_H__

#include <stdint.h>

#define QUEUE_MAX_SIZE 256

typedef enum queue_error_t
{
	QUEUE_SUCCESS,
	QUEUE_IS_FULL,
	QUEUE_IS_EMPTY,
	QUEUE_FAILURE
} queue_error_t;

/**
 * \brief   Queue type definition.
 */
typedef struct queue_t
{
	uint8_t head;
	uint8_t tail;
	uint8_t size;
	uint8_t storage[QUEUE_MAX_SIZE];
} queue_t;


/**
 * 
 * \param queue
 */
queue_error_t queue_init(queue_t *queue);

/**
 * 
 * \param queue
 * \param item
 */
queue_error_t queue_enqueue(queue_t *queue, uint8_t byte);

/**
 * 
 * \param queue
 * \param byte
 * \return 
 */
queue_error_t queue_dequeue(queue_t *queue, uint8_t * byte);

queue_error_t queue_clear(queue_t *queue);
/**
 * 
 * \param queue
 * \return 
 */
uint8_t queue_size(queue_t *queue);

#endif