#ifndef __CONTROLLER_H__
#define __CONTROLLER_H__

#include <stdint.h>
#include "defines.h"
/** \brief  Number of elements of the cache*/
#define CACHE_SIZE 32
/** \brief  Size of the header, in bytes*/
#define HEADER_SIZE 4
/** \brief Type 4 -> Slave requests FactoryReset  */
#define FactoryResetRequest 4
/** \brief Type 3 -> Master requests all parameters from slave  */
#define MasterRequestFromSlave 3
/** \brief Type 2 -> Slave requests master to read program  */
#define ReadProgramFromMaster 2
/** \brief  Type 1 -> Slave requests master to save program  */
#define WriteProgramToMaster 1
/** \brief  Type 0 -> Program 0 parameters sent by master */
#define MasterWriteParameters 0
/** \brief Flag that signals if a new I2C1 Slave Event was read or write. */
extern volatile bool i2c4_read_write_flag;
/** \brief Flag that signals the Program received on the memory. */
extern volatile uint8_t programID_memory;

typedef enum controller_error_t
{
	CONTROLLER_SUCESS,
	CONTROLLER_FAIL,
	CONTROLLER_CHECKSUM_FAIL
} controller_error_t;

/**
 * \brief initialises the buffer.
 * 
 */
void controller_init(void);

/**
 * \brief Adds a byte to the buffer.
 * \param data Value of the byte to be added to the buffer.
 * 
 */
controller_error_t controller_inbound_buffer_enqueue(volatile uint8_t data);
controller_error_t controller_outbound_buffer_enqueue(uint8_t data);
controller_error_t controller_outbound_buffer_dequeue(uint8_t *data);

/**
 * \brief Moves the data from the buffer to the a cache.
 *
 * Moves the data from the buffer to the a cache in-memory so it may
 * be accessed directly. The buffer is cleared after the operation in
 * done. It is assumes that the byte addressing done in little-endian.
 * For example:
 *      ID  B1  B2
 *  hex 0A  03  E7
 *  -> cache[0A] = 0x03E7;
 */
controller_error_t controller_inbound_buffer_save(void);


/**
 * \brief	Reads a value from the pool of instructions.
 * 
 * \param index	[in]		Index of the value to be read.
 * \param value	[in/out]	Buffer to store the read value.
 */
void controller_cache_read(param_id_t index, uint16_t *value);

/**
 * \brief	Fills the output buffer 
 * 
 * \param optype	[in]		Type of the operation
 * \param opprogram	[in]		Program to where the operation should happen
 */
bool controller_outbound_buffer_fill(uint8_t optype, uint8_t opprogram);
/**
 * \brief	Writes a value to of instructions.
 * 
 * \param index	[in]	Index of the value to be written.
 * \param value	[in]	Value to store.
 */
void controller_cache_write(uint8_t index, uint16_t value);


/**
 * \brief	Waits for the slave write operation to be finished.
 * 
 */
bool controller_waitwrite(void);
#endif