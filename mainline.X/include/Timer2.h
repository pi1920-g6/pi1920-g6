#ifndef TIMER2_H
#define	TIMER2_H

/**
 * \brief Initialises the Timer2.
 * 
 * This routine initialises the Timer2 with 25Hz frequency (40ms).
 * This timer is used for the buttons (home, back and encoder).

 */
void Timer2Init(void);

#endif	/* TIMER2_H */

