#ifndef UART1_H
#define	UART1_H
#include <stdbool.h>

void uart1_init(void);
void uart1_write(uint8_t c);
bool uart1_read(uint8_t *byte);
void uart1_read_string(uint8_t *string);

#endif	// UART1_H