#ifndef CHANGENOTIFICATION_H
#define	CHANGENOTIFICATION_H

volatile uint8_t encrotation;

/**
 * \brief Initialises the Change Notification.
 * 
 * This routine initialises the change notification with CN16 as the
 * selected channel. 
 * This interrupt is responsible for the encoder rotation.
 * 
 */
void CNInterruptinit(void);

#endif	/* CHANGENOTIFICATION_H */

