#ifndef SM_STRUCTURES_H
#define	SM_STRUCTURES_H

#include <stdbool.h>
// Here we declare the various states of the machine
typedef enum states {
    HOME_CHANGE_CURRENT,
    SELECTION_MENU,
    MODE_SELECTION,
    MS_PROCESSES,
    MS_PULSE,
    MS_MODE,
    SETTINGS,
    MEMORY_PG1,
    MEMORY_PG2,  
    MEMORY_TIG,
    MEMORY_TIG0,
    MEMORY_TIGPULSE,
    MEMORY_TIGPULSE0,
    MEMORY_MMA,
    MEMORY_MMA0,
    MEMORY_MMAPULSE,
    MEMORY_MMAPULSE0,
    TIG_PG1,
    TIG_PG2,
    TIG_SPOT_PG1,
    TIG_SPOT_PG2,
    TIG_SPOT_PG3,
    TIG_PULSE_PG1,
    TIG_PULSE_PG2,
    TIG_PULSE_PG3,     
    TIG_PULSE_SPOT_PG3,   
    MMA_PG1,
    MMA_PULSE_PG1,
    MMA_PULSE_PG2,        
    HELP,
    FACTORY_RESET
    // Add more as needed. Note: Must still add parameters states
} state;

typedef struct sm_t {
    state c_state;
    state p_state;
}sm_t;


//Here we declare the various events that might cause state changes
typedef enum events {
    HOME,
    BACK,
    ENC_BUTTON,
    ENC_HOLD,
    ENC_RIGHT,
    ENC_LEFT,
    TEST,
    NONE,
} event;

extern volatile event var_interrupt;
extern volatile bool FirstLoop;
extern volatile bool val_update;

#endif	/* SM_STRUCTURES_H */

