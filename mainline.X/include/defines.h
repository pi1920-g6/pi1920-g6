#ifndef __DEFINES_H__
#define __DEFINES_H__

/** \brief system clock frequency (80 MHz) */
#define SYSCLK 80E6L
/** \brief Peripheral clock frequency (in Hz) */
#define PBCLK (SYS_CLK / 2)

/**
 * \brief List of every parameters.
 */
typedef enum param_id_t
{
	WELD_PROCESS,
	OP_MODE,
	COOLING,
	TIG_STATE,
	MMA_STATE,
	WELD_STATE,
	ERRORS,
	CURRENT_READ,
	VOLTAGE_READ,
	TIG_PRE_GAS,
	TIG_ISTART,
	TIG_UPSLOPE,
	TIG_INOMINAL,
	TIG_IBASE_,
	TIG_PULSE_WITH_,
	TIG_PULSE_FREQUENCY,
	TIG_DOWNSLOPE_,
	TIG_IEND,
	TIG_POSTGAS,
	TIG_DYNAMICS,
	TIG_SPOTTIME,
	MMA_HOTSTART,
	MMA_HOTSTART_TIME,
	MMA_INOMINAL,
	MMA_ARCFORCE,
	MMA_IBASE,
	MMA_PULSE_WIDTH,
	MMA_PULSE_FREQ,
    SETTINGS_BRIGHTNESS,
    SETTINGS_LANGUAGE        
} param_id_t;


#endif