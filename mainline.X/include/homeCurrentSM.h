#ifndef HOMECURRENTSM_H
#define	HOMECURRENTSM_H

#include <stdbool.h>
#include <stdint.h>

/** \brief   Buttons on this menu: Modes, Params, Load/Save and Settings. */
typedef enum hc_buttons_t
{
    MODES_BUTTON,
    PARAMS_BUTTON,
    LOADSAVE_BUTTON,
    SETTINGS_BUTTON
} hc_buttons_t;


#ifdef __UNITTESTING__
#include "defines.h"
extern uint16_t CurrentValue;
extern uint16_t WeldProc;
extern uint16_t VoltageValue;
extern uint16_t opmode;
extern uint16_t dynmode;
extern hc_buttons_t buttons;
#endif
/**
 * \brief Responsible for the Current value changes.
 * 
 * This routine will control the different events while on Current change in the 
 * home menu.
 * Events:
 * - Encoder rotation to the right or left will change current value and save it,
 * save it on memory and send it to master.
 * - Encoder click will allow the user to move to the buttons.
 * - Encoder hold will open a Help menu for this page.
 * - Back and Home will do nothing on this menu.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
 sm_t home_change_current( sm_t stateMachine);
 
 
 
 /**
 * \brief Responsible for the Home menu buttons (Selection Menu).
 * 
 * This routine will control the Selection Menu buttons and 
 * redirect to the correct state when pressed.
 * Events:
 * - Encoder rotation to the right or left will change the current button,
 * placing highlight to the one being used and removed from the previous.
 * - Encoder click will allow the user to open the selected buttons' page.
 * - Encoder hold will open a Help menu for this page.
 * - Back and Home will take the user back to Current Value change on this menu.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return State Machine current-state and previous-state.
 */
 sm_t selection_menu( sm_t stateMachine);
 

#endif	/* HOMECURRENTSM_H */

