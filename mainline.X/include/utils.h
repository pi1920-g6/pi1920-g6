#ifndef __UTILS_H__
#define	__UTILS_H__

#include <stdint.h>

#ifndef __UNITTESTING__
/**
 * \brief   Delays the system for the desired time, in milliseconds.
 * 
 * \param delay_val Time to delay, in milliseconds.
 */
void delay_ms(uint16_t delay_val);

/**
 * \brief   Delays the system for the desired time, in microseconds.
 * 
 * \param delay_val Time to delay, in microseconds.
 */
void delay_us(uint16_t delay_val);
#endif
/**
 * \brief   Calculates the Fletcher-16's checksum of a buffer.
 * 
 * \param data[in] Buffer with the data to be checked.
 * \param bytes[in] Number of elements of the buffer.
 * \return checksum
 */
uint16_t utils_fletcher16(const uint8_t *data, uint16_t bytes);

/**
 * \brief   Converts an unsigned 16-bit integer to a string with a given number
 *          of digits.
 * 
 * @param str [in/out]  String with the number
 * @param val [in]      Number to be converted
 * @param digits [in]   Number of leading digits
 */
void utils_int_to_str(char *str, uint16_t val, uint8_t digits);
#endif