#ifndef NEXTION_H
#define	NEXTION_H

/**
 * \brief   Function used to send the developed nextion functions through UART.
 * 
 * \param str Comand to send through UART (String data type).
 */
void nextion_sendCmd(char *str);

/**
 * \brief   Changes the background color of an object to red, 
 * simulating an object being selected or the cursor's position. 
 * (Ex.:b0.bco=63488)
 * 
 * \param objname Name of the object.
 */
void nextion_select(char *objname);

/**
 * \brief   Changes the background color of an object to white, 
 * simulating an object being deselected or, 
 * combined with the function nextion_select(char *objname), the cursor's movement.
 * (Ex.:b0.bco=65535)
 * 
 * \param objname Name of the object.
 */
void nextion_unselect(char *objname);

/**
 * \brief   Changes the Nextion HMI's current page.
 * 
 * \param pageNumber Number of the new current page.
 */
void nextion_page(char *pageNumber);

/**
 * \brief   Changes the value of a "Number" or an "Xfloat".
 * (Ex.:page 4)
 * 
 * \param objname Name of the object.
 * \param value New numeric value in the object.
 */
void nextion_change_val(char *objname,char *value);

/**
 * \brief   Increments the value of a "Number" or an "Xfloat" by 1.
 * (Ex.:n0.val++)
 * 
 * \param objname Name of the object.
 */
void nextion_increment_val(char *objname);

/**
 * \brief   Decrements the value of a "Number" or "Xfloat" by 1.
 * (Ex.:n0.val--)
 * 
 * \param objname Name of the object.
 */
void nextion_decrement_val(char *objname);

/**
 * \brief   Changes the text of a "Button" or "Text".
 * (Ex.:t0.txt="hello")
 * 
 * \param objname Name of the object.
 * \param txt New text in the object.
 */
void nextion_change_txt(char *objname,char *txt);

/**
 * \brief   Checks a "Checkbox", simulating the scroll's current position.
 * (Ex.:c0.val=1)
 * 
 * \param objname Name of the object.
 */
void nextion_checkbox_on(char *objname);

/**
 * \brief   Unchecks a "Checkbox". Combined with the function nextion_checkbox_on(char *objname),
 * simulates the scrolls's movement.
 * (Ex.:c0.val=0)
 * 
 * \param objname Name of the object.
 */
void nextion_checkbox_off(char *objname);

/**
 * \brief   Changes the color of a "Progress bar" to red, 
 * simulating its selection.
 * (Ex.:j0.pco=63488)
 * 
 * \param objname Name of the object.
 */
void nextion_select_bar(char *objname);

/**
 * \brief   Changes the color of a "Progress bar" to gray, 
 * simulating it being deselected.
 * (Ex.:j0.pco=33808)
 * 
 * \param objname Name of the object.
 */
void nextion_unselect_bar(char *objname);

/**
 * \brief   Increments the value of a "Progress bar" by 10.
 * (Ex.:j0.val=j0.val+10")
 * 
 * \param objname Name of the object.
 */
void nextion_increment_bar(char *objname);

/**
 * \brief   Decrements the value of a "Progress bar" by 10.
 * (Ex.:j0.val=j0.val-10")
 * 
 * \param objname Name of the object.
 */
void nextion_decrement_bar(char *objname);

/**
 * \brief   Changes the background color of an object to gray.
 * (Ex.:b0.bco=52825)
 * 
 * \param objname Name of the object.
 */
void nextion_gray_background(char *objname);

/**
 * \brief   Makes the object visble.
 * (Ex.:vis b0,1)
 * 
 * \param objname Name of the object.
 */
void nextion_visible(char *objname);

/**
 * \brief   Makes the object invisble.
 * (Ex.:vis b0,0)
 * 
 * \param objname Name of the object.
 */
void nextion_invisible(char *objname);

#endif	/* NEXTION_H */




