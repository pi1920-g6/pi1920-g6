#ifndef PARAMMMA_H
#define	PARAMMMA_H


#include <stdbool.h>
#include <stdint.h>

#ifdef __UNITTESTING__
#include "defines.h"
MMAParam_buttons_t MMA_buttons;
MMAPulse1Param_buttons_t MMA_Pulse_Buttons1;
MMAPulse2Param_buttons_t MMA_Pulse_Buttons2;
extern uint16_t MMAHotStart; 
extern uint16_t MMAHotStartTime;
extern uint16_t MMAINominal;
extern uint16_t MMAArcForce;
extern uint16_t MMAIBase;
extern uint16_t MMAWidth;
extern uint16_t MMAPFreq;
#endif

/**
 * \brief Responsible for the MMA (NOT PULSED) parameteres and value changes.
 * 
 * This routine will control the flow between the MMA parameters aswell
 * as allowing the user to change them according to max and min value.
 * 
 * Parameters available on this page:
 * MMA HOTSTART - Min value 10, Max value 100 (%)
 * MMA HOTSTART TIME - Min value 1, Max value 20 (Seconds/10)
 * MMA INOMINAL - Min value 10, Max value 220 (A)
 * MMA ARCFORCE - Min value 0, Max value 50 (%)
 * 
 * Page 1/1 for MMA.
 * 
 *  Events:
 * - Encoder rotation to the right or left will change the button or the value
 * of the parameter.
 * - Encoder click will allow the user to open parameter value so it can be 
 * edited if on the button, if on the value, will validate the change and save it.
 * - Encoder hold will open a Help menu for this page.
 * - Back will take the user back to Selection Menu buttons on Home menu if on 
 * buttons. If on values, will discard changes.
 * - Home will take the user back to Current Value change on Home menu.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t MMA_parameters_page(sm_t stateMachine);

/**
 * \brief Responsible for the MMA PULSED parameteres and value changes.
 * 
 * This routine will control the flow between the MMA Pulsed parameters aswell
 * as allowing the user to change them according to max and min value.
 * 
 * Parameters available on this page:
 * MMA HOTSTART - Min value 10, Max value 100 (%)
 * MMA HOTSTART TIME - Min value 1, Max value 20 (Seconds/10)
 * MMA INOMINAL - Min value 10, Max value 220 (A)
 * MMA IBASE - Min value 50, Max value 95 (%)
 * 
 * Page 1/2 for MMA Pulsed.
 * 
 *  Events:
 * - Encoder rotation to the right or left will change the button or the value
 * of the parameter.
 * - Encoder click will allow the user to open parameter value so it can be 
 * edited if on the button, if on the value, will validate the change and save it.
 * - Encoder hold will open a Help menu for this page.
 * - Back will take the user back to Selection Menu buttons on Home menu if on 
 * buttons. If on values, will discard changes.
 * - Home will take the user back to Current Value change on Home menu.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t MMA_PULSE_parameters_page1(sm_t stateMachine);


/**
 * \brief Responsible for the MMA PULSED parameteres and value changes (2nd page).
 * 
 * This routine will control the flow between the MMA Pulsed parameters aswell
 * as allowing the user to change them according to max and min value.
 * 
 * Parameters available on this page:
 * MMA PULSE WIDTH - Min value 10, Max value 90 (%)
 * MMA PULSE FREQUENCY - Min value 5, Max value 100 (Hz/10)
 * 
 * Page 2/2 for MMA Pulsed.
 * 
 *  Events:
 * - Encoder rotation to the right or left will change the button or the value
 * of the parameter.
 * - Encoder click will allow the user to open parameter value so it can be 
 * edited if on the button, if on the value, will validate the change and save it.
 * - Encoder hold will open a Help menu for this page.
 * - Back will take the user back to Selection Menu buttons on Home menu if on 
 * buttons. If on values, will discard changes.
 * - Home will take the user back to Current Value change on Home menu.
 * 
 * \param stateMachine	State Machine current-state and previous-state.
 * 
 * \return stateMachine State Machine current-state and previous-state.
 */
sm_t MMA_PULSE_parameters_page2(sm_t stateMachine);


#endif	/* PARAMMMA_H */

