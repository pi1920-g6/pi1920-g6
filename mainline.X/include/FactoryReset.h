#ifndef FACTORYRESET_H
#define	FACTORYRESET_H

/**
 * \brief Responsible setting the parameters of a program to factory default.
 * 
 * This routine will set the parameters of the current program to factory default,
 * will be used in the reset option on the programs.
 * 
 */
void factory_reset_parameters(void);

#endif	/* FACTORYRESET_H */

