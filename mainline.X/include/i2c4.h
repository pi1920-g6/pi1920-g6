#ifndef __I2C_H__
#define __I2C_H__

#include <stdint.h>
#include <stdbool.h>

#define I2C1_SLAVE_ADDRESS 0x37
#define I2C1_SLAVE_NO_MASK 0x00

/* This routine is public if unit testing is enabled */
#ifdef __UNITTESTING__
bool i2c4_is_valid_7_bit_addr(uint8_t address);
#endif

/** \brief I2C error/status codes */
typedef enum i2c_error_t
{
	I2C_SUCCESS,
	I2C_INVALID_ADDRESS
} i2c_error_t;

/**
 * \brief Initialises the I2C1 peripheral.
 * 
 * This routine initialises the I2C1 peripheral with an address \c address 
 * masked by a \c mask. If the given address is a reserved I2C address the
 * initialisation fails.
 * 
 * \param address	I2C slave address.
 * \param mask		I2C slave address mask.
 * 
 * \return \c I2C_SUCCESS if is initialised, \c I2C_INVALID_ADDRESS otherwise.
 */
i2c_error_t i2c4_init(uint8_t address, uint8_t mask);

/**
 * \brief   Detects if a Stop condition has occurred.
 * 
 * After the detection of a Stop event, the slave module sets the Stop bit and 
 * clears the Start bit. Those bits will remain unchanged until a Start or
 * Repeated Start event occurred, so an additional flag is used and cleared when
 * this function is called.  The flag is set at every slave interrupt.
 * 
 * \return \c true if a new stop condition has been detected, \c false otherwise.
 */
bool i2c4_stop_condition(void);
bool i2c4_end_read_write(void);
#endif /* __I2C_H__ */
